import 'package:flutter/material.dart';

class FavoriteProvider with ChangeNotifier {
  bool _favorited = false;


  bool get isfavorited => _favorited;


  void initial(bool t) => _favorited = t;


  void unfavorite() {
    _favorited = false;
    notifyListeners();
  }

  void favorite() {
    _favorited = true;
    notifyListeners();
  }

}
