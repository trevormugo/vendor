import 'package:flutter/material.dart';
import 'package:vendor/models/defaults.dart';

class ShopImageProvider with ChangeNotifier {
  String _currentPlanImage = PlanImages.basic;


  String get currentPlan => _currentPlanImage;


  void initialPlan(String t) => _currentPlanImage = t;


  void planUpdated(String newplan) {
    _currentPlanImage = newplan;
    notifyListeners();
  }

}
