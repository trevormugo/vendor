import 'package:flutter/material.dart';

class SocialsLogin extends StatefulWidget {
  SocialsLogin({
    required this.icon,
    required this.detail,
  });
  final IconData icon;
  final String detail;

  @override
  createState() => _SocialsLoginState();
}

class _SocialsLoginState extends State<SocialsLogin> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 10.0,
        right: 10.0,
        bottom: 5.0,
        top: 5.0,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white60,
          border: Border.all(
            color: Colors.black12,
            width: 1.0,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: ListTile(
          leading: Icon(widget.icon),
          onTap: null,
          title: Text(
            '${widget.detail}',
            style: TextStyle(
                color: Colors.black,
                letterSpacing: 1.2,
                fontSize: 16.0,
                height: 1.3),
          ),
        ),
      ),
    );
  }
}
