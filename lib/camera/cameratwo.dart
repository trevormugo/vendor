import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_icons/line_icons.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'dart:io';
import 'dart:math';

class CameraPageTwo extends StatefulWidget {
  CameraPageTwo({
    required this.title,
  });
  final String title;
  @override
  State createState() => _CameraPageTwoInstance();
}

class _CameraPageTwoInstance extends State<CameraPageTwo> {
  PageController pageViewController = PageController(initialPage: 0);
  final _formKey = GlobalKey<FormState>();
  File? _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Mama Bidiis Beauty shop',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
            subtitle: Text(
              'details',
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: screenheight * 0.4,
                      width: double.infinity,
                      child: PageView(
                        controller: pageViewController,
                        children: [
                          Center(
                            child: GestureDetector(
                              onTap: getImage,
                              child: Icon(
                                LineIcons.camera,
                              ),
                            ),
                          ),
                          Center(
                            child: GestureDetector(
                              onTap: getImage,
                              child: Icon(
                                LineIcons.camera,
                              ),
                            ),
                          ),
                          Center(
                            child: GestureDetector(
                              onTap: getImage,
                              child: Icon(
                                LineIcons.camera,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: screenheight * 0.1,
                      width: double.infinity,
                      child: Center(
                        child: SmoothPageIndicator(
                          controller: pageViewController,
                          count: 3,
                          effect: WormEffect(
                            dotWidth: 10.0,
                            dotHeight: 10.0,
                            radius: 10.0,
                            activeDotColor: Color.fromARGB(255, 27, 81, 211),
                          ),
                        ),
                      ),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            height: screenheight * 0.06,
                            width: screenwidth * 0.9,
                            child: TextField(
                              obscureText: false,
                              decoration: InputDecoration(
                                labelStyle: TextStyle(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color.fromARGB(255, 27, 81, 211),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                ),
                                labelText: "Item Name",
                              ),
                            ),
                          ),
                          Container(
                            height: screenheight * 0.06,
                            width: screenwidth * 0.9,
                            child: TextField(
                              obscureText: false,
                              decoration: InputDecoration(
                                labelStyle: TextStyle(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color.fromARGB(255, 27, 81, 211),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                ),
                                labelText: "No of Items",
                              ),
                            ),
                          ),
                          TextField(
                            obscureText: false,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Item Price in KSH each",
                            ),
                          ),
                          Container(
                            height: screenheight * 0.1,
                            width: double.infinity,
                            child: TextField(
                              obscureText: false,
                              maxLines: 8,
                              decoration: InputDecoration(
                                labelStyle: TextStyle(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color.fromARGB(255, 27, 81, 211),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(12),
                                  ),
                                ),
                                labelText: "Item Description",
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
