import 'package:flutter/material.dart';
import 'package:camera/camera.dart';

import 'dart:async';
import 'dart:io';

class CameraPage extends StatefulWidget {
  CameraPage({
    required this.title,
  });
  final String title;
  @override
  State createState() => _CameraPageInstance();
}

class _CameraPageInstance extends State<CameraPage> {
  CameraController? controller;
  late List cameras;
  late int selectedCameraIdx;
  String? imagePath;

  Future _initCameraController(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller!.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.high);
    controller!.addListener(() {
      if (mounted) {
        setState(() {});
      }
      if (controller!.value.hasError) {
        print('Camera error ${controller!.value.errorDescription}');
      }
    });

    try {
      await controller!.initialize();
    } on CameraException catch (e) {
      print('ERRORRR');
    }

    if (mounted) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIdx = 0;
        });
        _initCameraController(cameras[selectedCameraIdx]).then((void v) {});
      } else {
        print("No camera available");
      }
    }).catchError((err) {
      print('Error: $err.code\nError Message: $err.message');
    });
  }

  @override
  void dispose() {
    controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    double screenheight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              '${widget.title}',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          actions: [
            GestureDetector(
              onTap: null,
              child: Icon(
                Icons.info_outline,
              ),
            ),
          ],
        ),
      ),
      body: Stack(alignment: Alignment.bottomCenter, children: <Widget>[
        (controller == null || !controller!.value.isInitialized)
            ? CircularProgressIndicator()
            : Positioned.fill(
                child: CameraPreview(controller!),
              ),


      ]),
    );
  }
}
