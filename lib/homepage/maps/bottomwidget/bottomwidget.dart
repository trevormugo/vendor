import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:line_icons/line_icons.dart';
import 'package:vendor/homepage/shop/shopreviews.dart';
import 'package:vendor/providers/detailupdate_provider.dart';
import 'package:vendor/restapi.dart';
import '../../shop/categories/onecategory.dart';

import 'package:line_awesome_flutter/line_awesome_flutter.dart';

import '../../../models/defaults.dart';
import '../../../models/oneShopQuery/oneshopquery.dart';
import '../../../ip.dart';
import '../../profiles/profilepage.dart';
import '../../shop/shopdetails.dart';
import '../../shop/newshopreviews.dart';
import '../../../horizontalitems.dart';
import '../../../models/shops.dart';

class ShopProfileWidget extends StatefulWidget {
  ShopProfileWidget({
    required this.isdraggable,
    required this.scrollController,
    required this.background,
    required this.oneshop,
    required this.plan,
    required this.token,
    required this.userId,
  });
  final ScrollController? scrollController;
  final bool isdraggable;
  final Color? background;
  final OneShopQuery oneshop;
  final String plan;
  final String token;
  final String userId;
  @override
  State createState() => _ShopProfileWidgetInstance();
}

class _ShopProfileWidgetInstance extends State<ShopProfileWidget> {
  PageController pageViewController = PageController(initialPage: 0);
  List<Widget> imagePanel = [];

  @override
  void initState() {
    super.initState();
    //_initialWork();
  }

/*
  void _initialWork() {
    imagePanel.add(
      Center(
        child: (widget.plan == "Basic")
            ? Image.asset(PlanImages.basic)
            : (widget.plan == "Advanced")
                ? Image.asset(PlanImages.advanced)
                : Image.asset(PlanImages.enterprise),
      ),
    );
    widget.oneshop.shopImages!.forEach((element) {
      imagePanel.add(
        ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: FittedBox(
            child: Container(
              height: screenheight * 0.4,
              width: screenwidth * 0.9,
              color: Colors.transparent,
              child: Image.network(
                Adress.myip + "/shops/fetchuploadthumbnail?id=${element.id}",
                errorBuilder: (BuildContext context, Object exception,
                    StackTrace? stackTrace) {
                  return Image.asset(
                    PlanImages.basic,
                    fit: BoxFit.cover,
                  );
                },
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      );
    });
  }*/

  @override
  void dispose() {
    super.dispose();
  }

  void showmodal() {
    print('showing modal');
  }

  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    double screenheight = MediaQuery.of(context).size.height;

    void naigatetoreviews(OneShopQuery oneShopQuery) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopReviews(
            oneShopQuery: oneShopQuery,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void navigatetodetails(OneShopQuery oneShopQuery) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopDetails(
            oneShopQuery: oneShopQuery,
            token: widget.token,
            userId: widget.userId,
            callBack: () {},
          ),
        ),
      );
    }

    void navigatetocategory() {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneCategory(
                  categoryname: 'category',
                  categoryicon: Icon(
                    LineAwesomeIcons.utensils,
                    color: Colors.lightGreenAccent,
                  ),
                  token: widget.token,
                  userId: widget.userId,
                )),
      );
    }

    return Container(
      color: widget.background,
      child: SingleChildScrollView(
        controller: widget.scrollController,
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: screenheight * 0.1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => navigatetodetails(widget.oneshop),
                      child: Icon(
                        LineAwesomeIcons.info_circle,
                      ),
                    ),
                    Container(
                      width: screenwidth * 0.6,
                      child: Center(
                        child: Text(
                          '${widget.oneshop.shop.shopName}',
                          softWrap: true,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () => naigatetoreviews(widget.oneshop),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(
                            Icons.star_border,
                          ),
                          Text('${widget.oneshop.shop.rating}'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  height: screenheight * 0.4,
                  width: double.infinity,
                  child: Center(
                    child: (widget.plan == PlanNames.basic)
                        ? Image.asset(PlanImages.basic)
                        : (widget.plan == PlanNames.advanced)
                            ? Image.asset(PlanImages.advanced)
                            : (widget.plan == PlanNames.enterprise)
                                ? Image.asset(
                                    PlanImages.enterprise,
                                  )
                                : Image.asset(
                                    ErrorImage.noimage,
                                  ),
                  )),
              ListView.builder(
                itemCount: widget.oneshop.categories!.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: GestureDetector(
                          onTap: navigatetocategory,
                          child: Text(
                            '${widget.oneshop.categories![index].categoryName}',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      HorizontalItems(
                        items: widget.oneshop.categories![index].items,
                        token: widget.token,
                        userId: widget.userId,
                      ),
                    ],
                  );
                },
              )
            ]),
      ),
    );
  }
}
