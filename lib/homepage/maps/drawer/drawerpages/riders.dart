import 'dart:convert';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/remoteprofilepage.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/acceptAffiliation.dart';
import 'package:vendor/models/affiliaterider.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/riderSearch/ridersearch.dart';
import 'package:vendor/models/ridersTabQuery/riderstabquery.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';
import 'package:vendor/restapi.dart';

import 'dart:math';

class Riders extends StatefulWidget {
  Riders({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _RidersInstance();
}

class _RidersInstance extends State<Riders> with TickerProviderStateMixin {
  late TabController _tabcontroller;
  late ScrollController _scrollController;
  late Future<RidersTabQuery> _fetchrideraffiliations;
  late RidersTabQuery _rideraffiliations;
  late RiderSearch _riderSearch;
  TextEditingController _ridersearchcontroller = TextEditingController();
  bool _operationinprogress = false;
  List<int> selectedIndexes = [];
  List<int> responseCodes = [];

  @override
  void initState() {
    super.initState();
    _tabcontroller = TabController(length: 3, vsync: this);
    _scrollController = ScrollController();
    _fetchrideraffiliations = fetchrideraffiliations();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
    dispose();
  }

  Future<RidersTabQuery> fetchrideraffiliations() async {
    var riders = await RestApi().quaryRidersTab(
        "/ridermanagement/ridersTabQuery?vendorId=${widget.userId}",
        widget.token);
    if (riders.statusCode == 200) {
      print("${riders.body}");
      _rideraffiliations = RidersTabQuery.fromJson(json.decode(riders.body));
      return Future.value(_rideraffiliations);
    } else if (riders.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${riders.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${riders.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${riders.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${riders.statusCode}');
    }
  }

  void affiliateRider(
      Account rider, String vendorId, List<ShopWithPlans> shops) async {
    addAffiliation(0, selectedIndexes.length - 1, shops, vendorId, rider.id)
        .listen((event) {
      setState(() {
        _operationinprogress = true;
      });
      Fluttertoast.showToast(
        msg:
            'Code ${event.statusCode} : affiliating ${rider.fullName} , ${event.body}',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0,
      );
      responseCodes.add(event.statusCode);
      responseCodes.where((element) => element == 201).length;
      if (event.index == selectedIndexes.length - 1) {
        if (responseCodes.where((element) => element == 201).length ==
            selectedIndexes.length) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: "SUCCESS",
            desc:
                '${rider.fullName} is now affiliated with all ${responseCodes.where((element) => element == 201).length} shops selected',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            title: "NOTE",
            desc:
                '${rider.fullName} was affiliated to only ${responseCodes.where((element) => element == 201).length} shops , ${responseCodes.where((element) => element != 201).length} shops failed',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
        }
        setState(() {
          responseCodes = [];
          _operationinprogress = false;
          _fetchrideraffiliations = fetchrideraffiliations();
        });
      }
    });
  }

  Stream<PostAffiliatedShopResponse> addAffiliation(int start, int finish,
      List<ShopWithPlans> shops, String vendorId, String riderId) async* {
    for (int i = start; i <= finish; i++) {
      AffiliateRider affiliateRider = AffiliateRider(
        riderId: riderId,
        vendorId: vendorId,
        shopId: shops[i].shop.id,
        inititatorUser: widget.userId,
      );
      var response = await RestApi().affiliateRider(
          "/ridermanagement/affiliateRider", widget.token, affiliateRider);
      yield PostAffiliatedShopResponse(
        index: i,
        statusCode: response.statusCode,
        body: response.body,
      );
    }
  }

  Future<void> navigatetoriderssprofile(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (id == prefs.get('userid').toString()) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error Detected",
        desc: 'Youre account shouldnt be in this list!',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RemoteProfilePage(
            token: widget.token,
            userId: id,
          ),
        ),
      );
    }
  }

  void navigatetoshopprofile(String shopId) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ShopProfile(
          shopId: shopId,
          token: widget.token,
          userId: widget.userId,
        ),
      ),
    );
  }

  void removeShopFromAffiliationRequest(String shopId, String riderId) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi().removeShopFromRiderAffiliation(
        "/ridermanagement/removeShopFromAffiliation?shopId=$shopId&riderId=$riderId",
        widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
        _fetchrideraffiliations = fetchrideraffiliations();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Rider unaffiliated from shop',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  void acceptAffiliationsRequest(String superVisorId) async {
    setState(() {
      _operationinprogress = true;
    });
    AcceptAffiliation acceptAffiliation = AcceptAffiliation(
      userId: superVisorId,
      vendorId: widget.userId,
    );
    var response = await RestApi().acceptRiderAffiliatinRequest(
        "/ridermanagement/acceptAffiliatinRequest",
        widget.token,
        acceptAffiliation);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
        _fetchrideraffiliations = fetchrideraffiliations();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Rider is now affiliated',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  void unaffiliateRider(String riderId) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi().unaffiliateRider(
        "/ridermanagement/unaffiliateRider?vendorId=${widget.userId}&riderId=$riderId",
        widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
        _fetchrideraffiliations = fetchrideraffiliations();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Rider is now unaffiliated',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  @override
  void dispose() {
    _tabcontroller.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void showshopaffiliates(List<ShopWithPlans> shops, Account rider) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return StatefulBuilder(
                builder: (BuildContext context, StateSetter setModalState) {
              return Container(
                child: ListView.builder(
                    itemCount: shops.length + 1,
                    itemBuilder: (BuildContext context, int index) {
                      return (index == 0)
                          ? TextButton(
                              onPressed: () => (_operationinprogress == true)
                                  ? null
                                  : acceptAffiliationsRequest(rider.id),
                              child: Text('Affiliate Rider'),
                            )
                          : ListTile(
                              leading: GestureDetector(
                                onTap: () => navigatetoshopprofile(
                                    shops[index - 1].shop.id),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20.0),
                                  child: FittedBox(
                                    child: Container(
                                      height: screenheight * 0.4,
                                      width: screenwidth * 0.9,
                                      color: Colors.transparent,
                                      child: (shops[index - 1].planName ==
                                              PlanNames.basic)
                                          ? Image.asset(
                                              PlanImages.basic,
                                            )
                                          : (shops[index - 1].planName ==
                                                  PlanNames.advanced)
                                              ? Image.asset(
                                                  PlanImages.advanced,
                                                )
                                              : (shops[index - 1].planName ==
                                                      PlanNames.enterprise)
                                                  ? Image.asset(
                                                      PlanImages.enterprise,
                                                    )
                                                  : Image.asset(
                                                      ErrorImage.noimage,
                                                    ),
                                    ),
                                  ),
                                ),
                              ),
                              title: Text(' ${shops[index - 1].shop.shopName}'),
                              trailing: GestureDetector(
                                onTap: () {
                                  removeShopFromAffiliationRequest(
                                      shops[index - 1].shop.id, rider.id);
                                  setModalState(() {
                                    shops.removeAt(index - 1);
                                  });
                                },
                                child: Icon(
                                  LineIcons.timesCircle,
                                  color: Colors.red,
                                ),
                              ),
                            );
                    }),
              );
            });
          });
    }

    void showshopaffiliatesforrealaffiliates(
        List<ShopWithPlans> shops, Account rider) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return StatefulBuilder(
                builder: (BuildContext context, StateSetter setModalState) {
              return Container(
                child: ListView.builder(
                    itemCount: shops.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: GestureDetector(
                          onTap: () =>
                              navigatetoshopprofile(shops[index].shop.id),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: FittedBox(
                              child: Container(
                                height: screenheight * 0.4,
                                width: screenwidth * 0.9,
                                color: Colors.transparent,
                                child:
                                    (shops[index].planName == PlanNames.basic)
                                        ? Image.asset(
                                            PlanImages.basic,
                                          )
                                        : (shops[index].planName ==
                                                PlanNames.advanced)
                                            ? Image.asset(
                                                PlanImages.advanced,
                                              )
                                            : (shops[index].planName ==
                                                    PlanNames.enterprise)
                                                ? Image.asset(
                                                    PlanImages.enterprise,
                                                  )
                                                : Image.asset(
                                                    ErrorImage.noimage,
                                                  ),
                              ),
                            ),
                          ),
                        ),
                        title: Text(' ${shops[index].shop.shopName}'),
                        trailing: GestureDetector(
                          onTap: () {
                            removeShopFromAffiliationRequest(
                                shops[index].shop.id, rider.id);
                            setModalState(() {
                              shops.removeAt(index);
                            });
                          },
                          child: Icon(
                            LineIcons.timesCircle,
                            color: Colors.red,
                          ),
                        ),
                      );
                    }),
              );
            });
          });
    }

    void searchrider(String email, List<ShopWithPlans> shops) async {
      setState(() {
        _operationinprogress = true;
      });
      var riderssearch = await RestApi().searchRider(
          "/ridermanagement/searchrider?riderEmail=$email", widget.token);
      if (riderssearch.statusCode == 200) {
        print("${riderssearch.body}");
        setState(() {
          _operationinprogress = false;
        });
        _riderSearch = RiderSearch.fromJson(json.decode(riderssearch.body));
        showModalBottomSheet(
            context: context,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
            ),
            builder: (context) {
              return StatefulBuilder(
                  builder: (BuildContext context, StateSetter setModalState) {
                return Container(
                  child: ListView.builder(
                      itemCount: shops.length + 1,
                      itemBuilder: (BuildContext context, int index) {
                        return (index == 0)
                            ? TextButton(
                                onPressed: () => (_operationinprogress == true)
                                    ? null
                                    : affiliateRider(_riderSearch.rider!,
                                        widget.userId, shops),
                                child: Text('Affiliate Rider'),
                              )
                            : ListTile(
                                selected: (selectedIndexes.contains(index - 1))
                                    ? true
                                    : false,
                                leading: GestureDetector(
                                  onTap: () => navigatetoshopprofile(
                                      shops[index - 1].shop.id),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: FittedBox(
                                      child: Container(
                                        height: screenheight * 0.4,
                                        width: screenwidth * 0.9,
                                        color: Colors.transparent,
                                        child: (shops[index - 1].planName ==
                                                PlanNames.basic)
                                            ? Image.asset(
                                                PlanImages.basic,
                                              )
                                            : (shops[index - 1].planName ==
                                                    PlanNames.advanced)
                                                ? Image.asset(
                                                    PlanImages.advanced,
                                                  )
                                                : (shops[index - 1].planName ==
                                                        PlanNames.enterprise)
                                                    ? Image.asset(
                                                        PlanImages.enterprise,
                                                      )
                                                    : Image.asset(
                                                        ErrorImage.noimage,
                                                      ),
                                      ),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  if (selectedIndexes.contains(index - 1)) {
                                    setModalState(() {
                                      selectedIndexes.removeWhere(
                                          (element) => element == index - 1);
                                    });
                                  } else {
                                    setModalState(() {
                                      selectedIndexes.add(index - 1);
                                    });
                                  }
                                },
                                title:
                                    Text(' ${shops[index - 1].shop.shopName}'),
                                trailing: (selectedIndexes.contains(index - 1))
                                    ? Icon(
                                        LineIcons.check,
                                        color: Colors.green,
                                      )
                                    : null,
                              );
                      }),
                );
              });
            });
      } else if (riderssearch.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${riderssearch.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${riderssearch.statusCode}",
          desc: '${riderssearch.body}',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      }
    }

    return Stack(children: [
      DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: ListTile(
              title: Text(
                'Riders',
                softWrap: true,
                textAlign: TextAlign.start,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            bottom: const TabBar(
              tabs: [
                Tab(
                  text: "Affiliated",
                ),
                Tab(
                  text: "Requests",
                ),
                Tab(
                  text: "Affiliate",
                ),
              ],
            ),
          ),
          extendBodyBehindAppBar: true,
          body: FutureBuilder(
              future: _fetchrideraffiliations,
              builder: (context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return TabBarView(
                    children: [
                      (snapshot.data.affiliatedRiders.length == 0)
                          ? Center(
                              child: Text(
                                "You are not affiliated with any riders yet",
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            )
                          : Container(
                              height: screenheight * 0.9,
                              width: screenwidth * 0.8,
                              child: ListView.builder(
                                physics: BouncingScrollPhysics(),
                                shrinkWrap: true,
                                itemCount:
                                    snapshot.data.affiliatedRiders.length,
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        ListTile(
                                  leading: GestureDetector(
                                    onTap: () => navigatetoriderssprofile(
                                        snapshot.data.affiliatedRiders[index]
                                            .rider.id),
                                    child: CircleAvatar(),
                                  ),
                                  onTap: () =>
                                      showshopaffiliatesforrealaffiliates(
                                          snapshot.data.affiliatedRiders[index]
                                              .shopWithPlan,
                                          snapshot.data.affiliatedRiders[index]
                                              .rider),
                                  title: Text(
                                      '${snapshot.data.affiliatedRiders[index].rider.fullName}'),
                                  subtitle: Text(
                                      '${TimeConversion.readTimestamp(snapshot.data.affiliatedRiders[index].affiliation.timestamp)}'),
                                  trailing: GestureDetector(
                                    onTap: () => unaffiliateRider(snapshot
                                        .data.affiliatedRiders[index].rider.id),
                                    child: Icon(
                                      LineIcons.timesCircle,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                      (snapshot.data.affiliationRequestSent.length == 0)
                          ? Center(
                              child: Text(
                                  "You have not recieved any affiliation requests yet"),
                            )
                          : Container(
                              height: screenheight * 0.9,
                              width: screenwidth * 0.8,
                              child: ListView.builder(
                                physics: BouncingScrollPhysics(),
                                shrinkWrap: true,
                                itemCount:
                                    snapshot.data.affiliationRequestSent.length,
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        ListTile(
                                  leading: GestureDetector(
                                    onTap: () => navigatetoriderssprofile(
                                        snapshot
                                            .data
                                            .affiliationRequestSent[index]
                                            .rider
                                            .id),
                                    child: CircleAvatar(),
                                  ),
                                  onTap: () => showshopaffiliates(
                                      snapshot
                                          .data
                                          .affiliationRequestSent[index]
                                          .shopWithPlan,
                                      snapshot.data
                                          .affiliationRequestSent[index].rider),
                                  title: Text(
                                      '${snapshot.data.affiliationRequestSent[index].rider.fullName}'),
                                  subtitle: Text(
                                      '${TimeConversion.readTimestamp(snapshot.data.affiliationRequestSent[index].affiliation.timestamp)}'),
                                  trailing: Icon(
                                    LineIcons.angleUp,
                                  ),
                                ),
                              ),
                            ),
                      Container(
                        height: screenheight * 0.9,
                        width: screenwidth * 0.8,
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Container(
                                width: screenwidth * 0.8,
                                height: 50,
                                margin: EdgeInsets.all(20),
                                child: TextFormField(
                                  enabled: !_operationinprogress,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    return null;
                                  },
                                  controller: _ridersearchcontroller,
                                  obscureText: false,
                                  decoration: InputDecoration(
                                      labelStyle: TextStyle(
                                        color: Color.fromARGB(255, 27, 81, 211),
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 27, 81, 211),
                                        ),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12),
                                        ),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12),
                                        ),
                                      ),
                                      labelText: "Enter rider's email"),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  if (snapshot.data.shopWithPlan == null) {
                                    AwesomeDialog(
                                      context: context,
                                      dialogType: DialogType.ERROR,
                                      animType: AnimType.BOTTOMSLIDE,
                                      title: "You don't own any shops yet",
                                      desc:
                                          'Head over to inventory and create some shops first',
                                      btnCancelOnPress: () {},
                                      btnOkOnPress: () {},
                                    )..show();
                                  } else {
                                    if (_operationinprogress == true) {
                                      Fluttertoast.showToast(
                                          msg: "Operation in progress...",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.TOP,
                                          timeInSecForIosWeb: 1,
                                          fontSize: 16.0);
                                    } else {
                                      searchrider(_ridersearchcontroller.text,
                                          snapshot.data.shopWithPlan);
                                    }
                                  }
                                },
                                child: Icon(
                                  LineIcons.search,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Icon(
                          LineIcons.server,
                          color: Colors.grey,
                          size: 70.0,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text("${snapshot.error}"),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: SpinKitFadingCube(
                      size: 60,
                      color: Color.fromARGB(255, 27, 81, 211),
                    ),
                  );
                }
              }),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
