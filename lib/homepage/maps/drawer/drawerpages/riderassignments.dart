import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/riderassignmentscustomers/riderassignmentscustomers.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/riderAssignments/riderassignmentsresponse.dart';
import 'package:vendor/restapi.dart';

class RiderAssignments extends StatefulWidget {
  RiderAssignments({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _RiderAssignmentsInstance();
}

class _RiderAssignmentsInstance extends State<RiderAssignments> {
  late Future<RiderAssignmentsResponse> _fetchRiderAssignments;
  late RiderAssignmentsResponse _riderAssignments;

  @override
  void initState() {
    super.initState();
    _fetchRiderAssignments = fetchRiderAssignments();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<RiderAssignmentsResponse> fetchRiderAssignments() async {
    var assignments = await RestApi().queryRiderAssignments(
        "/items/getAllAssignmentsForRider/${widget.userId}", widget.token);
    if (assignments.statusCode == 200) {
      print("${assignments.body}");
      _riderAssignments =
          RiderAssignmentsResponse.fromJson(json.decode(assignments.body));
      return Future.value(_riderAssignments);
    } else if (assignments.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${assignments.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${assignments.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${assignments.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${assignments.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigatetoshopprofile(String shopId) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopProfile(
            shopId: shopId,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void navigatetoriderassignmentcustomerspage(
        List<Account> customers, Shop shop) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => RiderAssignmentsCustomers(
                  customers: customers,
                  shop: shop,
                  token: widget.token,
                  userId: widget.userId,
                )),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Assignments',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: FutureBuilder(
          future: _fetchRiderAssignments,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (snapshot.data.shopOrdersWithRiderAssignments.length == 0)
                  ? Center(
                      child: Text("No assignments yet"),
                    )
                  : ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: _riderAssignments
                          .shopOrdersWithRiderAssignments!.length,
                      itemBuilder: (BuildContext context, int index) =>
                          ListTile(
                        leading: GestureDetector(
                          onTap: () => navigatetoshopprofile(snapshot.data
                              .shopOrdersWithRiderAssignments[index].shop.id),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: FittedBox(
                              child: Container(
                                height: screenheight * 0.4,
                                width: screenwidth * 0.9,
                                color: Colors.transparent,
                                child: (snapshot
                                            .data
                                            .shopOrdersWithRiderAssignments[index]
                                            .planName ==
                                        PlanNames.basic)
                                    ? Image.asset(
                                        PlanImages.basic,
                                      )
                                    : (snapshot
                                                .data
                                                .shopOrdersWithRiderAssignments[index]
                                                .planName ==
                                            PlanNames.advanced)
                                        ? Image.asset(
                                            PlanImages.advanced,
                                          )
                                        : (snapshot
                                                    .data
                                                    .shopOrdersWithRiderAssignments[
                                                        index]
                                                    .planName ==
                                                PlanNames.enterprise)
                                            ? Image.asset(
                                                PlanImages.enterprise,
                                              )
                                            : Image.asset(
                                                ErrorImage.noimage,
                                              ),
                              ),
                            ),
                          ),
                        ),
                        title: GestureDetector(
                          onTap: () => navigatetoriderassignmentcustomerspage(
                              snapshot
                                  .data
                                  .shopOrdersWithRiderAssignments[index]
                                  .customers,
                              snapshot.data
                                  .shopOrdersWithRiderAssignments[index].shop),
                          child: Text(
                              '${snapshot.data.shopOrdersWithRiderAssignments[index].shop.shopName}'),
                        ),
                        subtitle: Text(
                            '${snapshot.data.shopOrdersWithRiderAssignments[index].customers.length} orders'),
                        trailing: Icon(
                          LineIcons.angleRight,
                        ),
                      ),
                    );
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
    );
  }
}
