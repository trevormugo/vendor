import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'package:line_icons/line_icons.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/itemsincustomerorder/itemsincustomerorder.dart';
import 'package:vendor/homepage/profiles/profileheader/profileimage.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/insight.dart';
import 'dart:math';

import 'package:vendor/restapi.dart';

class QrScannerPage extends StatefulWidget {
  QrScannerPage({
    required this.token,
    required this.userId,
    required this.userName,
    required this.orderType,
    required this.connString,
  });
  final String token;
  final String userId;
  final String orderType;
  final String userName;
  final String connString;
  @override
  State createState() => _QrScannerPageInstance();
}

class _QrScannerPageInstance extends State<QrScannerPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late Barcode result;
  late QRViewController controller;
  bool _operationinprogress = false;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      if (_operationinprogress == false) {
        result = scanData;
        setState(() {
          _operationinprogress = true;
        });
        var purchaserequest = await RestApi().sendPurchaseItemRequest(
            "${widget.connString}?uid=${result.code}&userId=${widget.userId}",
            widget.token);
        if (purchaserequest.statusCode == 200) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Success',
            desc: 'Order was purchased',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else if (purchaserequest.statusCode == 403) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: "App Defaults Error ${purchaserequest.statusCode}",
            desc: 'Verification error , Log In required',
            btnCancelOnPress: () => logout(),
            btnOkOnPress: () => logout(),
          )..show();
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: "Error ${purchaserequest.statusCode}",
            desc: '${purchaserequest.body}',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.height, MediaQuery.of(context).size.width);
    double screenwidth = min(
        MediaQuery.of(context).size.height, MediaQuery.of(context).size.width);

    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: (screenheight * 0.8) * 0.2,
            width: screenwidth * 0.9,
            color: Colors.transparent,
            child: Center(
              child: Chip(
                label: Text('${widget.orderType}'),
              ),
            ),
          ),
          Container(
            height: (screenheight * 0.8) * 0.5,
            child: QRView(
              key: qrKey,
              overlay: QrScannerOverlayShape(
                borderColor: Colors.black,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 250,
                overlayColor: Colors.white,
              ),
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Container(
            height: (screenheight * 0.8) * 0.2,
            width: screenwidth * 0.9,
            color: Colors.transparent,
            child: Center(
              child: Chip(
                avatar: CircleAvatar(
                  backgroundColor: Colors.grey.shade800,
                  child: Icon(LineIcons.user),
                ),
                label: Text('${widget.userName}'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
