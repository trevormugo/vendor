import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:math';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/remoteprofilepage.dart';

class QrPage extends StatefulWidget {
  QrPage({
    required this.userName,
    required this.data,
    required this.orderType,
    required this.price,
    required this.token,
    required this.handOverUserId,
  });
  final String userName;
  final String data;
  final String orderType;
  final String price;
  final String token;
  final String handOverUserId;
  @override
  State createState() => _QrPageInstance();
}

class _QrPageInstance extends State<QrPage> {
  @override
  void initState() {
    super.initState();
  }

  Future<void> navigatetocustomersprofile(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (id == prefs.get('userid').toString()) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error Detected",
        desc: 'Youre account shouldnt be in this list!',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RemoteProfilePage(
            token: widget.token,
            userId: id,
          ),
        ),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return SafeArea(
      top: true,
      bottom: false,
      left: false,
      right: false,
      child: LayoutBuilder(builder: (context, constraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
                minWidth: constraints.maxWidth,
                minHeight: constraints.maxHeight),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: (screenheight * 0.8) * 0.2,
                  width: screenwidth * 0.9,
                  child: Center(
                    child: Chip(
                      label: Text('${widget.orderType}'),
                    ),
                  ),
                ),
                Container(
                  height: (screenheight * 0.8) * 0.5,
                  width: screenwidth * 0.6,
                  child: Center(
                    child: QrImage(
                      data: widget.data,
                      version: QrVersions.auto,
                      size: 300,
                      gapless: false,
                      errorStateBuilder: (cxt, err) {
                        return Container(
                          child: Center(
                            child: Icon(
                              Icons.error_outline_sharp,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Container(
                  height: (screenheight * 0.8) * 0.2,
                  width: screenwidth * 0.9,
                  child: Center(
                    child: GestureDetector(
                      onTap: () =>
                          navigatetocustomersprofile(widget.handOverUserId),
                      child: Chip(
                        avatar: CircleAvatar(
                          backgroundColor: Colors.grey.shade800,
                          child: Icon(LineIcons.user),
                        ),
                        label: Text('${widget.userName}'),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
