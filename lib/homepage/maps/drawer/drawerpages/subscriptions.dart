import 'package:flutter/material.dart';

import '../../../../accountpool/pagelayout/pagelayout.dart';

class Subscriptions extends StatefulWidget {
  @override
  State createState() => _SubscriptionsInstance();
}

class _SubscriptionsInstance extends State<Subscriptions> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Subscriptions',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: PageLayout(
        username: 'null',
        email: 'null',
        id: 'null',
        phonenumber: 'null',
      ),
    );
  }
}
