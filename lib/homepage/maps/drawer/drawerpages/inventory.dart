import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addShop/addshop.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/inventoryTabsResponse/inventroiesouterlist.dart';
import 'package:vendor/restapi.dart';

import './categoryinventory/categorinventory.dart';

class Inventory extends StatefulWidget {
  Inventory({required this.token, required this.userId, required this.roles});
  final String token;
  final String userId;
  final List<String> roles;
  @override
  State createState() => _InventoryInstance();
}

class _InventoryInstance extends State<Inventory> {
  PageController pageViewController = PageController(initialPage: 0);
  late Future<InventoriesOuterList> _fetchVendorShops;
  InventoriesOuterList? _inventoryTab;

  @override
  void initState() {
    super.initState();
    _fetchVendorShops = fetchVendorShops();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<InventoriesOuterList> fetchVendorShops() async {
    var inventory = await RestApi().queryForInventoryTab(
        "/shops/queryForInventoryTab/${widget.userId}", widget.token);
    if (inventory.statusCode == 200) {
      print("${inventory.body}");
      _inventoryTab =
          InventoriesOuterList.fromJson(json.decode(inventory.body));
      return Future.value(_inventoryTab);
    } else if (inventory.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${inventory.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${inventory.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${inventory.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${inventory.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigatetoshopprofile(String shopId) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopProfile(
            shopId: shopId,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void _navigatetoshopcategories(String id, String name) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CategoryInventory(
            token: widget.token,
            userId: widget.userId,
            shopId: id,
            roles: widget.roles,
            shopName: name,
          ),
        ),
      );
    }

    void _navigatetocreateshop() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddShop(
            ownerId: widget.userId,
            token: widget.token,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Inventory',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      floatingActionButton:/* (widget.roles.contains("Vendor"))
          ?*/ FloatingActionButton(
              onPressed: _navigatetocreateshop,
              child: Icon(
                PlanIcons.basic,
                color: Color.fromARGB(255, 207, 118, 0),
              ),
              backgroundColor: Color.fromARGB(255, 27, 81, 211),
            ),
         // : null,
      body: FutureBuilder(
          future: _fetchVendorShops,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (snapshot.data.items.length == 0)
                  ? Center(
                      child: Text("No shops yet"),
                    )
                  : ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.items.length,
                      itemBuilder: (BuildContext context, int index) =>
                          ListTile(
                        leading: GestureDetector(
                          onTap: () => navigatetoshopprofile(
                              snapshot.data.items[index].shop.id),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: FittedBox(
                              child: Container(
                                height: screenheight * 0.4,
                                width: screenwidth * 0.9,
                                color: Colors.transparent,
                                child: (snapshot.data.items[index].planName ==
                                        PlanNames.basic)
                                    ? Image.asset(
                                        PlanImages.basic,
                                      )
                                    : (snapshot.data.items[index].planName ==
                                            PlanNames.advanced)
                                        ? Image.asset(
                                            PlanImages.advanced,
                                          )
                                        : (snapshot.data.items[index]
                                                    .planName ==
                                                PlanNames.enterprise)
                                            ? Image.asset(
                                                PlanImages.enterprise,
                                              )
                                            : Image.asset(
                                                ErrorImage.noimage,
                                              ),
                              ),
                            ),
                          ),
                        ),
                        title: GestureDetector(
                          onTap: () => _navigatetoshopcategories(
                              snapshot.data.items[index].shop.id,
                              snapshot.data.items[index].shop.shopName),
                          child: Text(
                              '${snapshot.data.items[index].shop.shopName}'),
                        ),
                        subtitle: Text(
                            '${snapshot.data.items[index].shopCategoriesNo} categories'),
                        trailing: Text(
                            "${TimeConversion.readTimestamp(snapshot.data.items[index].shop.timestamp)}"),
                      ),
                    );
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
    );
  }
}
