import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vendor/homepage/shop/oneitem/oneitem.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/models/defaults.dart';

class ItemTile extends StatefulWidget {
  ItemTile({
    required this.title,
    required this.subtitle,
    required this.itemId,
    required this.timestamp,
    required this.userId,
    required this.token,
  });

  final String title;
  final String subtitle;
  final String itemId;
  final int timestamp;
  final String userId;
  final String token;
  @override
  State createState() => _ItemTileInstance();
}

class _ItemTileInstance extends State<ItemTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    void navigateToItem(String itemId) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneItem(
                  icon: Icon(
                    Icons.restaurant,
                    color: Colors.lightGreenAccent,
                  ),
                  itemId: itemId,
                  userId: widget.userId,
                  token: widget.token,
                )),
      );
    }

    return ListTile(
      leading: GestureDetector(
        onTap: () => navigateToItem(widget.itemId),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: FittedBox(
            child: Container(
              height: screenheight * 0.4,
              width: screenwidth * 0.9,
              color: Colors.transparent,
              child: Image.network(
                Adress.myip + "/items/fetchuploadthumbnail?id${widget.itemId}",
                errorBuilder: (BuildContext context, Object exception,
                    StackTrace? stackTrace) {
                  return Image.asset(
                    ErrorImage.noimage,
                  );
                },
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
      title: Text(
        '${widget.title}',
        style: TextStyle(
          fontSize: 14,
        ),
      ),
      trailing: Text("${TimeConversion.readTimestamp(widget.timestamp)}"),
    );
  }
}
