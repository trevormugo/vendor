import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/models/defaults.dart';

class ShopTile extends StatefulWidget {
  ShopTile({
    required this.title,
    required this.subtitle,
    required this.shopId,
    required this.timestamp,
    required this.userId,
    required this.token,
  });

  final String title;
  final String subtitle;
  final String shopId;
  final int timestamp;
  final String userId;
  final String token;

  @override
  State createState() => _ShopTileInstance();
}

class _ShopTileInstance extends State<ShopTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigatetoshopprofile(String shopId) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopProfile(
            shopId: shopId,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    return ListTile(
      leading: GestureDetector(
        onTap: () => navigatetoshopprofile(widget.shopId),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: FittedBox(
            child: Container(
              height: screenheight * 0.2,
              width: screenheight * 0.2,
              color: Colors.transparent,
              child: Image.network(
                Adress.myip +
                    "/shops/fetchuploadthumbnailbyshopid?id=${widget.shopId}",
                errorBuilder: (BuildContext context, Object exception,
                    StackTrace? stackTrace) {
                  return Image.asset(
                    PlanImages.basic,
                    fit: BoxFit.cover,
                  );
                },
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
      title: Text(
        '${widget.title}',
        style: TextStyle(
          fontSize: 14,
        ),
      ),
      subtitle: Text('${widget.subtitle}'),
      trailing: Text("${TimeConversion.readTimestamp(widget.timestamp)}"),
    );
  }
}
