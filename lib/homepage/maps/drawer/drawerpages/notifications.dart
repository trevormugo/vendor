import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/listtiles/itemtile.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/listtiles/shoptile.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/notifications/notificationsresponse.dart';
import 'package:vendor/restapi.dart';
import 'package:cached_network_image/cached_network_image.dart';


class Notifications extends StatefulWidget {
  Notifications({
    required this.token,
    required this.userId,
  });
  final String token;
  final String userId;
  @override
  State createState() => _NotificationsInstance();
}

class _NotificationsInstance extends State<Notifications> {
  String accountType = 'Customer';
  String currency = 'Ksh';

  String notificationType = 'Inventory';

  late Future<NotificationsResponse> _fetchNotifications;
  late NotificationsResponse _notifications;

  @override
  void initState() {
    super.initState();
    _fetchNotifications = fetchNotifications();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<NotificationsResponse> fetchNotifications() async {
    var notifications = await RestApi().queryVendorOrders(
        "/notificationscontroller/getNotifications?userId=${widget.userId}", widget.token);
    if (notifications.statusCode == 200) {
      print("${notifications.body}");
      _notifications =
          NotificationsResponse.fromJson(json.decode(notifications.body));
      return Future.value(_notifications);
    } else if (notifications.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${notifications.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${notifications.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${notifications.statusCode}",
        desc: '${notifications.body}',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${notifications.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Notifications',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: FutureBuilder(
          future: _fetchNotifications,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (snapshot.data.notifications.length == 0)
                  ? Center(
                      child: Text("No notifications yet"),
                    )
                  : ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.notifications.length,
                      itemBuilder: (BuildContext context, int index) =>
                          (snapshot.data.notifications[index].notification.type == 'SHOP' ||
                                  snapshot.data.notifications[index].notification.type ==
                                      'LIKE' ||
                                  snapshot.data.notifications[index].notification.type ==
                                      'REVIEW')
                              ? ShopTile(
                                  shopId: snapshot
                                      .data.notifications[index].notification.leading,
                                  timestamp: snapshot
                                      .data.notifications[index].notification.timestamp,
                                  title:
                                      snapshot.data.notifications[index].notification.title,
                                  subtitle: snapshot
                                      .data.notifications[index].notification.subTitle,
                                  userId: widget.userId,
                                  token: widget.token,
                                )
                              : (snapshot.data.notifications[index].notification.type ==
                                      'ITEM')
                                  ? ItemTile(
                                      itemId: snapshot
                                          .data.notifications[index].notification.leading,
                                      subtitle: snapshot
                                          .data.notifications[index].notification.subTitle,
                                      timestamp: snapshot
                                          .data.notifications[index].notification.timestamp,
                                      title: snapshot
                                          .data.notifications[index].notification.title,
                                      userId: widget.userId,
                                      token: widget.token,
                                    )
                                  : Container());
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
    );
  }
}
