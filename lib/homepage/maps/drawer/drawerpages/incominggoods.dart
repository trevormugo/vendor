import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/oneincominggoodsorder/oneincominggoodsorder.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/incomingGoods/incominggoodsforcustomer.dart';
import 'package:vendor/models/incomingGoods/incominggoodsresponse.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/restapi.dart';

class IncomingGoods extends StatefulWidget {
  IncomingGoods({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _IncomingGoodsInstance();
}

class _IncomingGoodsInstance extends State<IncomingGoods> {
  late Future<IncomingGoodsResponse> _fetchIncomingGoods;
  late IncomingGoodsResponse _incominggoods;

  @override
  void initState() {
    super.initState();
    _fetchIncomingGoods = fetchIncomingGoods();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<IncomingGoodsResponse> fetchIncomingGoods() async {
    var incomingGoods = await RestApi().queryIncomingGoods(
        "/items/getShopsWithIncomingGoodsForCustomer/${widget.userId}",
        widget.token);
    if (incomingGoods.statusCode == 200) {
      print("${incomingGoods.body}");
      _incominggoods =
          IncomingGoodsResponse.fromJson(json.decode(incomingGoods.body));
      return Future.value(_incominggoods);
    } else if (incomingGoods.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${incomingGoods.statusCode}",
        desc: 'Verification error , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${incomingGoods.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${incomingGoods.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${incomingGoods.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigatetoshopprofile(String shopId) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopProfile(
            shopId: shopId,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void navigatetooneincominggoodsorderpage(
        IncomingGoodsForCustomer oneincominggoodsorder, Shop shop) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneIncomingGoodsOrder(
                  incominggoods: oneincominggoodsorder,
                  token: widget.token,
                  userId: widget.userId,
                  shop: shop,
                )),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Incoming Goods',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: FutureBuilder(
          future: _fetchIncomingGoods,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (snapshot.data.incomingGoods.length == 0)
                  ? Center(
                      child: Text("No goods ordered yet"),
                    )
                  : ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.incomingGoods.length,
                      itemBuilder: (BuildContext context, int index) =>
                          ListTile(
                        leading: GestureDetector(
                          onTap: () => navigatetoshopprofile(
                              snapshot.data.incomingGoods[index].shop.id),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: FittedBox(
                              child: Container(
                                height: screenheight * 0.4,
                                width: screenwidth * 0.9,
                                color: Colors.transparent,
                                child: (snapshot.data.incomingGoods[index].planName ==
                                        PlanNames.basic)
                                    ? Image.asset(
                                        PlanImages.basic,
                                      )
                                    : (snapshot.data.incomingGoods[index].planName ==
                                            PlanNames.advanced)
                                        ? Image.asset(
                                            PlanImages.advanced,
                                          )
                                        : (snapshot.data.incomingGoods[index]
                                                    .planName ==
                                                PlanNames.enterprise)
                                            ? Image.asset(
                                                PlanImages.enterprise,
                                              )
                                            : Image.asset(
                                                ErrorImage.noimage,
                                              ),
                              ),
                            ),
                          ),
                        ),
                        title: GestureDetector(
                          onTap: () => navigatetooneincominggoodsorderpage(
                              snapshot.data.incomingGoods[index],
                              snapshot.data.incomingGoods[index].shop),
                          child: Text(
                              '${snapshot.data.incomingGoods[index].shop.shopName}'),
                        ),
                        subtitle: Text(
                            '${snapshot.data.incomingGoods[index].incomingGoodsOrdersForCustomer.length} orders'),
                        trailing: Icon(
                          LineAwesomeIcons.angle_right,
                        ),
                      ),
                    );
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
    );
  }
}
