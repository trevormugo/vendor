import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:vendor/homepage/shop/oneitem/oneitem.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';

class ItemsInOneCustomerOrder extends StatefulWidget {
  ItemsInOneCustomerOrder({
    required this.orders,
    required this.token,
    required this.userId,
  });
  final List<OneOrder> orders;
  final String token;
  final String userId;
  @override
  State createState() => _ItemsInOneCustomerOrderInstance();
}

class _ItemsInOneCustomerOrderInstance extends State<ItemsInOneCustomerOrder> {
  int finalPrice = 0;
  @override
  void initState() {
    super.initState();
    getFinalPrice();
  }

  void getFinalPrice() {
    //int finalPrice = 0;
    for (var i = 0; i < widget.orders.length; i++) {
      setState(() {
        finalPrice = finalPrice +
            (widget.orders[i].order!.noOfOrderedItems! *
                widget.orders[i].item!.ammount);
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  void navigateToItem(String itemId) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => OneItem(
                icon: Icon(
                  Icons.restaurant,
                  color: Colors.lightGreenAccent,
                ),
                itemId: itemId,
                userId: widget.userId,
                token: widget.token,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Container(
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.orders.length + 1,
        itemBuilder: (BuildContext context, int index) => (index == 0)
            ? ListTile(
                leading: Icon(
                  LineIcons.moneyBill,
                  color: Colors.green,
                ),
                title: Center(
                  child: Text("${widget.orders.length} orders"),
                ),
                trailing: Text(
                  "$finalPrice sh",
                  style: TextStyle(
                    color: Colors.green,
                  ),
                ),
              )
            : ListTile(
                leading: GestureDetector(
                  onTap: () =>
                      navigateToItem(widget.orders[index - 1].item!.id),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: FittedBox(
                      child: Container(
                        height: screenheight * 0.4,
                        width: screenwidth * 0.9,
                        color: Colors.transparent,
                        child: Image.network(
                          Adress.myip +
                              "/items/fetchuploadthumbnailbyitemid?id=${widget.orders[index - 1].item!.id}",
                          errorBuilder: (BuildContext context, Object exception,
                              StackTrace? stackTrace) {
                            return Image.asset(
                              ErrorImage.noimage,
                              fit: BoxFit.cover,
                            );
                          },
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                title: GestureDetector(
                  child: Text('${widget.orders[index - 1].item!.itemName}'),
                ),
                subtitle: Text(
                    "${TimeConversion.readTimestamp(widget.orders[index - 1].order!.timestamp!)}"),
                trailing: Text(
                  '${(widget.orders[index - 1].order!.noOfOrderedItems!) * (widget.orders[index - 1].item!.ammount)} sh',
                  style: TextStyle(
                    color: Colors.green,
                  ),
                ),
              ),
      ),
    );
  }
}
