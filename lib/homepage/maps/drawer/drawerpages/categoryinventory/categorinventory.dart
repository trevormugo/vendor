import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:math';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addItem/addItem.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/inventoriesResponse/inventoriesresponse.dart';
import 'package:vendor/restapi.dart';

import '../../../../../models/insight.dart';
import '../../../../profiles/profileheader/profileimage.dart';
import './Inventorycards/inventorycards.dart';

class CategoryInventory extends StatefulWidget {
  CategoryInventory({
    required this.token,
    required this.userId,
    required this.shopId,
    required this.shopName,
    required this.roles,
  });
  final String token;
  final String userId;
  final String shopId;
  final String shopName;
  final List<String> roles;

  @override
  State createState() => _CategoryInventoryInstance();
}

class _CategoryInventoryInstance extends State<CategoryInventory> {
  PageController pageViewController = PageController(initialPage: 0);
  final _random = new Random();

  InventoriesResponse? inventories;
  late Future<InventoriesResponse> _fetchInventoryCategories;

  List<Insight> insights = [];

  @override
  void initState() {
    super.initState();
    _fetchInventoryCategories = fetchInventoryCategories();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  int next(int min, int max) => min + _random.nextInt(max - min);

  Future<InventoriesResponse> fetchInventoryCategories() async {
    var inventory = await RestApi().queryInventories(
        "/shops/queryInventories/${widget.shopId}", widget.token);
    if (inventory.statusCode == 200) {
      print("${inventory.body}");
      inventories = InventoriesResponse.fromJson(json.decode(inventory.body));
      if (inventories!.items.length != 0) {
        CategoryInsights.insights.forEach((element) {
          inventories!.items.forEach((category) {
            if (element.containsKey(category.categoryName)) {
              //use insight data
              var percentage = ((category.items!.length) * 100) / 30;
              element[category.categoryName]!.insightpercentage =
                  percentage / 100;
              insights.add(element[category.categoryName]!);
            } else {
              int firstValue = next(0, 255);
              int secondValue = next(0, 255);
              int thirdValue = next(0, 255);
              var percentage = ((category.items!.length) * 100) / 30;
              insights.add(
                Insight(
                  paintcolor:
                      Color.fromARGB(255, firstValue, secondValue, thirdValue),
                  insightpercentage: percentage / 100,
                  category: '${category.categoryName}',
                  icon: Icon(
                    LineAwesomeIcons.shopping_bag,
                    color: Color.fromARGB(
                        255, firstValue, secondValue, thirdValue),
                  ),
                ),
              );
            }
          });
        });
      }
      return Future.value(inventories);
    } else if (inventory.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${inventory.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${inventory.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${inventory.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${inventory.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigatetoadditem() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddItem(
            ownerId: widget.userId,
            token: widget.token,
            shopId: widget.shopId,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Categories',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
            subtitle: Text(
              '${widget.shopName}',
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      floatingActionButton: FloatingActionButton(
        onPressed: navigatetoadditem,
        child: Icon(
          LineIcons.plus,
          color: Color.fromARGB(255, 207, 118, 0),
        ),
        backgroundColor: Color.fromARGB(255, 27, 81, 211),
      ),
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: FutureBuilder(
            future: _fetchInventoryCategories,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return (snapshot.data.items.length == 0)
                    ? Center(
                        child: Text("No categories yet"),
                      )
                    : LayoutBuilder(builder: (context, constraints) {
                        return SingleChildScrollView(
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: constraints.maxWidth,
                                minHeight: constraints.maxHeight),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  height: screenheight * 0.8,
                                  width: screenwidth * 0.9,
                                  child: PageView(
                                      allowImplicitScrolling: true,
                                      controller: pageViewController,
                                      children: List.generate(
                                        snapshot.data.items.length,
                                        (index) => InventoryCards(
                                          token: widget.token,
                                          userId: widget.userId,
                                          shopName: widget.shopName,
                                          itemsno: snapshot
                                              .data.items[index].items.length,
                                          itemscap: 30,
                                          categories:
                                              snapshot.data.items[index],
                                          title: snapshot
                                              .data.items[index].categoryName,
                                          percentage:
                                              insights[index].insightpercentage,
                                          bodywidget: Container(
                                            height: screenheight * 0.9,
                                            width: screenwidth * 0.8,
                                            child: CenteredWidgetInsightsBorder(
                                              insights: [insights[index]],
                                              middle: insights[index].icon,
                                              shouldanimate: false,
                                              backgroundstrokewidth: 6,
                                              foregroundstrokewidth: 6,
                                            ),
                                          ),
                                        ),
                                      )),
                                ),
                                Container(
                                  height: screenheight * 0.1,
                                  width: double.infinity,
                                  child: Center(
                                    child: SmoothPageIndicator(
                                      controller: pageViewController,
                                      count: snapshot.data.items.length,
                                      effect: WormEffect(
                                        dotWidth: 10.0,
                                        dotHeight: 10.0,
                                        radius: 10.0,
                                        activeDotColor:
                                            Color.fromARGB(255, 27, 81, 211),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      });
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("${snapshot.error}"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
    );
  }
}
