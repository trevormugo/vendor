import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:vendor/homepage/shop/oneitem/oneitem.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/categories.dart';

class InventoryItems extends StatefulWidget {
  InventoryItems({
    required this.categories,
    required this.token,
    required this.userId,
  });
  final Categories categories;
  final String token;
  final String userId;
  @override
  State createState() => _InventoryItemsInstance();
}

class _InventoryItemsInstance extends State<InventoryItems> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void navigateToItem(String itemId) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => OneItem(
                icon: Icon(
                  Icons.restaurant,
                  color: Colors.lightGreenAccent,
                ),
                itemId: itemId,
                userId: widget.userId,
                token: widget.token,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Text(
            '${widget.categories.categoryName}',
            softWrap: true,
            textAlign: TextAlign.start,
            overflow: TextOverflow.fade,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: ListView.builder(
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.categories.items!.length,
        itemBuilder: (BuildContext context, int index) => (widget
                    .categories.items!.length ==
                0)
            ? Center(
                child: Text("No items in cart yet"),
              )
            : ListTile(
                onTap: null,
                leading: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: FittedBox(
                    child: Container(
                      height: screenheight * 0.4,
                      width: screenwidth * 0.9,
                      color: Colors.transparent,
                      child: GestureDetector(
                        onTap: () =>
                            navigateToItem(widget.categories.items![index].id),
                        child: Image(
                          fit: BoxFit.cover,
                          image: NetworkImage(Adress.myip +
                              "/items/fetchuploadthumbnailbyitemid?id=${widget.categories.items![index].id}"),
                          errorBuilder: (BuildContext context, Object exception,
                              StackTrace? stackTrace) {
                            return Image.asset(
                              ErrorImage.noimage,
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
                title: Text('${widget.categories.items![index].itemName}'),
                subtitle:
                    Text('${widget.categories.items![index].noOfItems} items'),
                trailing: Text(
                    "${TimeConversion.readTimestamp(widget.categories.items![index].timestamp)}"),
              ),
      ),
    );
  }
}
