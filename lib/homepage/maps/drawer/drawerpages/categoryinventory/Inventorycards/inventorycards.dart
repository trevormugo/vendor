import 'package:flutter/material.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/itemsininventory.dart';
import 'package:vendor/models/inventoriesResponse/inventoriesresponse.dart';
import 'package:vendor/models/oneShopQuery/categories.dart';
import 'dart:math';

import '../../../../../../camera/cameratwo.dart';

class InventoryCards extends StatefulWidget {
  InventoryCards(
      {required this.bodywidget,
      required this.title,
      required this.percentage,
      required this.categories,
      required this.token,
      required this.userId,
      required this.shopName,
      required this.itemsno,
      required this.itemscap});
  final Widget bodywidget;
  final String title;
  final double percentage;
  final Categories categories;
  final String token;
  final String userId;
  final String shopName;
  final int itemsno;
  final int itemscap;

  @override
  State createState() => _InventoryCardsInstance();
}

class _InventoryCardsInstance extends State<InventoryCards> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigateToItemsPage() {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => InventoryItems(
                  token: widget.token,
                  categories: widget.categories,
                  userId: widget.userId,
                )),
      );
    }

    return GestureDetector(
      onTap: navigateToItemsPage,
      child: Card(
        elevation: 2,
        child: GridTile(
          header: GridTileBar(
            leading: Icon(
              Icons.store_mall_directory_outlined,
              color: Colors.black,
            ),
            title: Text(
              '${widget.title}',
              softWrap: true,
              textAlign: TextAlign.center,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontWeight: FontWeight.w800,
              ),
            ),
            trailing: Text(
              '${(widget.percentage * 100).round()} %',
              softWrap: true,
              textAlign: TextAlign.center,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          child: widget.bodywidget,
          footer: GridTileBar(
            title: Text(
              '${widget.shopName}',
              softWrap: true,
              textAlign: TextAlign.center,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontWeight: FontWeight.w800,
              ),
            ),
            subtitle: Text(
              '${widget.itemsno} / ${widget.itemscap}',
              softWrap: true,
              textAlign: TextAlign.center,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
