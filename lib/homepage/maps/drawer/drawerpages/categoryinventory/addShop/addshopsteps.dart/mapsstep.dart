import 'dart:async';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vendor/models/defaults.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';

class MapStep extends StatefulWidget {
  MapStep({
    required this.stepDisabled,
    required this.operationinprogress,
  });
  final bool stepDisabled;
  final bool operationinprogress;
  @override
  State createState() => MapStepInstance();
}

class MapStepInstance extends State<MapStep> {
  Completer<GoogleMapController> _controller = Completer();
  late Future<Position> determinePosition;

  static List<Placemark>? placemarks;

  static Marker? oneMarker;

  late BitmapDescriptor basicPinLocationIcon;
  late BitmapDescriptor advancedPinLocationIcon;
  late BitmapDescriptor enterprisePinLocationIcon;

  static late Map<String, dynamic> mapdetails;

  @override
  void initState() {
    super.initState();
    determinePosition = _determinePosition();
    Fluttertoast.showToast(
        msg: "Tap on the map to specify shop location",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0);
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    await setCustomMapPin();
    if (!serviceEnabled) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc: 'Location services are disabled to continue',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Please Allow Location Permission',
          desc: 'Location permissions are required to continue',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc:
            'Location permissions are permanently denied, we cannot request permissions',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return Future.value(Geolocator.getCurrentPosition());
  }

  Future<void> setCustomMapPin() async {
    basicPinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.basic);
    advancedPinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.advanced);
    enterprisePinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.enterprise);
  }

  void setMarkerPosition(LatLng position) async {
    placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    setState(() {
      oneMarker = Marker(
        markerId: MarkerId("oneMarker"),
        position: LatLng(position.latitude, position.longitude),
        icon: enterprisePinLocationIcon,
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = MediaQuery.of(context).size.height;

    double screenwidth = MediaQuery.of(context).size.width;

    return Container(
      width: screenwidth * 0.9,
      height: screenheight * 0.7,
      child: FutureBuilder(
          future: determinePosition,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (widget.stepDisabled == true)
                  ? Center(
                      child: Text("Shop marker already added"),
                    )
                  : (widget.operationinprogress == true)
                      ? Center(
                          child: SpinKitFadingCube(
                            size: 60,
                            color: Color.fromARGB(255, 207, 118, 0),
                          ),
                        )
                      : GoogleMap(
                          onTap: (position) => setMarkerPosition(position),
                          mapType: MapType.normal,
                          initialCameraPosition: CameraPosition(
                            target: LatLng(
                              snapshot.data.latitude,
                              snapshot.data.longitude,
                            ),
                            zoom: 14.4746,
                          ),
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                          markers: (oneMarker == null) ? {} : {oneMarker!},
                          gestureRecognizers:
                              <Factory<OneSequenceGestureRecognizer>>[
                            Factory<OneSequenceGestureRecognizer>(
                              () => EagerGestureRecognizer(),
                            ),
                          ].toSet(),
                        );
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
    );
  }
}
