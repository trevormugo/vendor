import 'dart:async';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vendor/models/defaults.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ShopInfo extends StatefulWidget {
  ShopInfo({
    required this.stepDisabled,
    required this.operationinprogress,
  });
  final bool stepDisabled;
  final bool operationinprogress;
  @override
  State createState() => ShopInfoInstance();
}

class ShopInfoInstance extends State<ShopInfo> {
  static final formKey = GlobalKey<FormState>();
  static TextEditingController shopnamecontroller = TextEditingController();
 static TextEditingController openhourscontroller = TextEditingController();
static  TextEditingController closinghourscontroller = TextEditingController();

  bool _operationinprogress = false;

  @override
  void initState() {
    super.initState();
    Fluttertoast.showToast(
        msg: "Provide required extra shop info ",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0);
  }

  Future<void> _showFirstTimePicker() async {
    final TimeOfDay? result =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (result != null) {
      setState(() {
        openhourscontroller.text = result.format(context);
      });
    }
  }

  Future<void> _showSecondTimePicker() async {
    final TimeOfDay? result =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (result != null) {
      setState(() {
        closinghourscontroller.text = result.format(context);
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
   // shopnamecontroller.dispose();
   // openhourscontroller.dispose();
   // closinghourscontroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = MediaQuery.of(context).size.height;

    double screenwidth = MediaQuery.of(context).size.width;

    return Form(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(20),
            child: TextFormField(
              enabled: !widget.stepDisabled,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
              keyboardType: TextInputType.emailAddress,
              controller: shopnamecontroller,
              obscureText: false,
              decoration: InputDecoration(
                labelStyle: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                ),
                labelText: "Shop name",
              ),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(20),
            child: TextFormField(
              enabled: !widget.stepDisabled,
              onTap: _showFirstTimePicker,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
              controller: openhourscontroller,
              obscureText: false,
              keyboardType: TextInputType.datetime,
              decoration: InputDecoration(
                labelStyle: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Color.fromARGB(255, 27, 81, 211)),
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                ),
                labelText: "Open hours",
              ),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(20),
            child: TextFormField(
              enabled: !widget.stepDisabled,
              onTap: _showSecondTimePicker,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
              controller: closinghourscontroller,
              obscureText: false,
              keyboardType: TextInputType.datetime,
              decoration: InputDecoration(
                labelStyle: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Color.fromARGB(255, 27, 81, 211)),
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(12),
                  ),
                ),
                labelText: "Closing hours",
              ),
            ),
          ),
          (widget.operationinprogress == true)
              ? Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 207, 118, 0),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
