import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addShop/addshopsteps.dart/mapsstep.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addShop/addshopsteps.dart/shopinfo.dart';
import 'package:vendor/homepage/maps/maps.dart';
import 'package:vendor/models/shops.dart';
import 'package:vendor/restapi.dart';

class AddShop extends StatefulWidget {
  AddShop({
    required this.ownerId,
    required this.token,
  });
  final String ownerId;
  final String token;
  @override
  State createState() => _AddShopInstance();
}

class _AddShopInstance extends State<AddShop> {
  int _currentStep = 0;
  bool stepDisabled = false;
  bool _operationinprogress = false;
  int currentindex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<void> addShop() async {
      if (ShopInfoInstance.formKey.currentState!.validate() &&
          MapStepInstance.oneMarker != null &&
          MapStepInstance.placemarks != null) {
        setState(() {
          _operationinprogress = true;
        });
        CreateShopRequest shopRequest = CreateShopRequest(
          ownerId: widget.ownerId,
          latitude: MapStepInstance.oneMarker!.position.latitude,
          longitude: MapStepInstance.oneMarker!.position.longitude,
          shopName: ShopInfoInstance.shopnamecontroller.text,
          country: MapStepInstance.placemarks![0].country!,
          openHours:
              "${ShopInfoInstance.openhourscontroller.text} - ${ShopInfoInstance.closinghourscontroller.text}",
        );
        var createShop = await RestApi()
            .createShop("/shops/createShop", widget.token, shopRequest);
        if (createShop.statusCode == 201) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: "Success ${createShop.statusCode}",
            desc: 'Shop was added successfully',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
          setState(() {
            _operationinprogress = false;
            stepDisabled = true;
          });
          Navigator.pop(context);
        } else if (createShop.statusCode == 401) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: "Error ${createShop.statusCode}",
            desc: 'You are not authorized to own a shop',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
          setState(() {
            _operationinprogress = false;
          });
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error ${createShop.statusCode}',
            desc: 'Unable to create Shop ${createShop.body}',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
          setState(() {
            _operationinprogress = false;
          });
        }
      }
    }

    void tapped(int step) {
      if (step == 1 && stepDisabled == false) {
        addShop();
      }
      setState(() => _currentStep = step);
    }

    Future<void> continued() async {
      if (_currentStep == 1 && stepDisabled == false) {
        await addShop();
      } else if (_currentStep < 1) {
        setState(() => _currentStep += 1);
      }
    }

    void cancel() {
      if (_currentStep > 0) {
        setState(() => _currentStep -= 1);
      }
    }

    return Scaffold(
      extendBodyBehindAppBar: false,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: Stepper(
          type: StepperType.horizontal,
          currentStep: _currentStep,
          onStepTapped: (step) => tapped(step),
          onStepCancel: cancel,
          onStepContinue: continued,
          controlsBuilder: null,
          steps: <Step>[
            Step(
              title: Text('Location'),
              content: Center(
                child: MapStep(
                  stepDisabled: stepDisabled,
                  operationinprogress: _operationinprogress,
                ),
              ),
            ),
            Step(
              title: Text('Info'),
              content: Center(
                child: ShopInfo(
                  stepDisabled: stepDisabled,
                  operationinprogress: _operationinprogress,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
