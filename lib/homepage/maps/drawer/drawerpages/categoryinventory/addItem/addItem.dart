import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addItem/addIemSteps/itemcategoriesstep.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addItem/addIemSteps/itemimages.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addItem/addIemSteps/iteminfo.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addItem/addIemSteps/itemreturnpolicy.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/categoryinventory/addShop/addshopsteps.dart/mapsstep.dart';
import 'package:vendor/homepage/maps/maps.dart';
import 'package:vendor/models/itemsrequest.dart';
import 'package:vendor/models/returnpolicyrequest.dart';
import 'package:vendor/models/shops.dart';
import 'package:vendor/restapi.dart';

class AddItem extends StatefulWidget {
  AddItem({
    required this.ownerId,
    required this.token,
    required this.shopId,
  });
  final String ownerId;
  final String token;
  final String shopId;
  @override
  State createState() => _AddItemInstance();
}

class _AddItemInstance extends State<AddItem> {
  int _currentStep = 0;
  bool stepDisabled = false;
  bool _operationinprogress = false;
  int currentindex = 0;
  String? itemId;
  int policycurrentindex = 0;

  @override
  void initState() {
    super.initState();
  }

  Future<void> addItem() async {
    if (ItemInfoInstance.formKey.currentState!.validate() &&
        ItemCategoriesStepInstance.availableCategoriesRes != null &&
        ItemCategoriesStepInstance.availableCategoriesRes!.items!.length != 0) {
      setState(() {
        _operationinprogress = true;
      });
      CreateItemRequest itemRequest = CreateItemRequest(
        itemName: ItemInfoInstance.itemnamecontroller.text,
        itemCategoryId: ItemCategoriesStepInstance.availableCategoriesRes!
            .items![ItemCategoriesStepInstance.selectedindex!].id!,
        noOfItems: int.tryParse(ItemInfoInstance.noofitemscontroller.text)!,
        shopId: widget.shopId,
        canBeDelivered: ItemInfoInstance.isSwitched,
        vendorId: widget.ownerId,
        ammount: int.tryParse(ItemInfoInstance.ammountcontroller.text)!,
      );
      var createItem =
          await RestApi().addItem("/items/postItem", widget.token, itemRequest);
      if (createItem.statusCode == 201) {
        print("FIOBWBEFWUB ${createItem.body}");
        itemId = json.decode(createItem.body)["id"];
        print("FIOBWBEFWUB $itemId");
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Success ${createItem.statusCode}",
          desc: 'Item was added successfully',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        uploadImage(currentindex, json.decode(createItem.body)["id"]);
      } else if (createItem.statusCode == 401) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${createItem.statusCode}",
          desc: 'You are not authorized to own an item',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        setState(() {
          _operationinprogress = false;
        });
      } else {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Error ${createItem.statusCode}',
          desc: 'Unable to create Shop ${createItem.body}',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
        setState(() {
          _operationinprogress = false;
        });
      }
    }
  }

  void uploadImage(int currentindex, String itemId) async {
    ItemThumbnailUploadRequest itemThumbnailRequest =
        ItemThumbnailUploadRequest(
      itemId: itemId,
      thumbnail: ItemImagesUploadInstance.images[currentindex],
    );
    var uploadShopImage = await RestApi().itemThumbnailFileUpload(
        "/items/thumbnailfileupload", itemThumbnailRequest, widget.token);
    if (uploadShopImage == 201) {
      currentindex += 1;
      if (currentindex <= ItemImagesUploadInstance.images.length - 1) {
        uploadImage(currentindex, itemId);
      } else {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Success $uploadShopImage",
          desc: 'Shop was added successfully',
          btnCancelOnPress: () {
            ItemImagesUploadInstance.images = [];
            Navigator.pop(context);
          },
          btnOkOnPress: () {
            ItemImagesUploadInstance.images = [];
            Navigator.pop(context);
          },
        )..show();
        setState(() {
          _operationinprogress = false;
          stepDisabled = true;
          _currentStep += 1;
        });
      }
    } else if (uploadShopImage == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error $uploadShopImage",
        desc: 'You are not authorized to upload shop image',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      setState(() {
        _operationinprogress = false;
      });
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Error $uploadShopImage',
        desc: 'Unable to upload Shop image',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      setState(() {
        _operationinprogress = false;
      });
    }
  }

  void addReturnPolicy(String policy, int policycurrentindex) async {
    if (ItemReturnPolicyInstance.addedpolicies.length >
            ItemReturnPolicyInstance.returnplociesresponse.items.length &&
        ItemReturnPolicyInstance.returnplociesresponse.items.length != 0) {
      setState(() {
        _operationinprogress = true;
      });
      policycurrentindex =
          ItemReturnPolicyInstance.returnplociesresponse.items.length;
      ReturnPolicyRequest returnPolicyRequest = ReturnPolicyRequest(
        policyText: policy,
        itemId: itemId!,
      );
      var addReturnPolicyBody = await RestApi().addReturnPolicy(
          "/items/addReturnPolicies", widget.token, returnPolicyRequest);
      if (addReturnPolicyBody.statusCode == 201) {
        policycurrentindex += 1;
        if (policycurrentindex <=
            ItemReturnPolicyInstance.addedpolicies.length - 1) {
          addReturnPolicy(
              ItemReturnPolicyInstance.addedpolicies[policycurrentindex],
              policycurrentindex);
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: "Success ${addReturnPolicyBody.statusCode}",
            desc: 'Policies were added successfully',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
          Navigator.pop(context);
        }
      } else if (addReturnPolicyBody.statusCode == 401) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${addReturnPolicyBody.statusCode}",
          desc: 'You are not authorized to own a shop',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        setState(() {
          _operationinprogress = false;
        });
      } else {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Error EE ${addReturnPolicyBody.statusCode}',
          desc: '${addReturnPolicyBody.body}',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
        setState(() {
          _operationinprogress = false;
        });
      }
    }
  }

  void tapped(int step) {
    if (step == 3 && stepDisabled == false) {
      if (ItemImagesUploadInstance.images.length == 0) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'No Images Were Provided ',
          desc: 'Images are compulsory',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else {
        addItem();
      }
    }
    setState(() => _currentStep = step);
  }

  Future<void> continued() async {
    if (_currentStep == 2 && stepDisabled == false) {
      if (ItemImagesUploadInstance.images.length == 0) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'No Images Were Provided ',
          desc: 'Images are compulsory',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else {
        await addItem();
      }
    } else if (_currentStep < 3) {
      setState(() => _currentStep += 1);
    } else {
      if (ItemReturnPolicyInstance.addedpolicies.length >
              ItemReturnPolicyInstance.returnplociesresponse.items.length &&
          ItemReturnPolicyInstance.returnplociesresponse.items.length != 0 &&
          itemId != null) {
        addReturnPolicy(
            ItemReturnPolicyInstance.addedpolicies[policycurrentindex],
            policycurrentindex);
      } else {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Unexpected error',
          desc: 'Add Policies later',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        Navigator.pop(context);
      }
    }
  }

  void cancel() {
    if (_currentStep > 0) {
      setState(() => _currentStep -= 1);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Text("Add Item"),
        ),
      ),
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: Stepper(
          type: StepperType.vertical,
          currentStep: _currentStep,
          onStepTapped: (step) => tapped(step),
          onStepCancel: cancel,
          onStepContinue: continued,
          controlsBuilder: null,
          steps: <Step>[
            Step(
              title: Text('Category'),
              content: Center(
                child: ItemCategoriesStep(
                  stepDisabled: stepDisabled,
                  operationinprogress: _operationinprogress,
                  token: widget.token,
                ),
              ),
            ),
            Step(
              title: Text('Info'),
              content: Center(
                child: ItemInfo(
                  stepDisabled: stepDisabled,
                  operationinprogress: _operationinprogress,
                ),
              ),
            ),
            Step(
              title: Text('Images'),
              content: Center(
                child: ItemImagesUpload(
                  stepDisabled: stepDisabled,
                  operationinprogress: _operationinprogress,
                ),
              ),
            ),
            Step(
              title: Text('Return Policy'),
              content: Center(
                child: ItemReturnPolicy(
                  operationinprogress: _operationinprogress,
                  token: widget.token,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
