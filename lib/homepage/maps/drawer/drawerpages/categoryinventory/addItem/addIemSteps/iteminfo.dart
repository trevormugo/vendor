import 'dart:async';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ItemInfo extends StatefulWidget {
  ItemInfo({
    required this.stepDisabled,
    required this.operationinprogress,
  });
  final bool stepDisabled;
  final bool operationinprogress;
  @override
  State createState() => ItemInfoInstance();
}

class ItemInfoInstance extends State<ItemInfo> {
  static final formKey = GlobalKey<FormState>();
  static TextEditingController itemnamecontroller = TextEditingController();
  static TextEditingController ammountcontroller = TextEditingController();
  static TextEditingController noofitemscontroller = TextEditingController();

  static bool isSwitched = false;
  String textValue = "No delivery";

  @override
  void initState() {
    super.initState();
    Fluttertoast.showToast(
        msg: "Provide required extra item info",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0);
  }

  void toggleSwitch(bool value) {
    if (isSwitched == false) {
      setState(() {
        isSwitched = true;
        textValue = 'Can be delivered';
      });
    } else {
      setState(() {
        isSwitched = false;
        textValue = 'No delivery';
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    // shopnamecontroller.dispose();
    // openhourscontroller.dispose();
    // closinghourscontroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = MediaQuery.of(context).size.height;

    double screenwidth = MediaQuery.of(context).size.width;

    return (widget.stepDisabled == true)
        ? Center(
            child: Text("Item already added"),
          )
        : (widget.operationinprogress == true)
            ? Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 207, 118, 0),
                ),
              )
            : Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("$textValue"),
                    Switch(
                      onChanged: toggleSwitch,
                      value: isSwitched,
                      activeColor: Color.fromARGB(255, 27, 81, 211),
                      activeTrackColor: Color.fromARGB(255, 27, 81, 211),
                      inactiveThumbColor: Color.fromARGB(255, 27, 81, 211),
                      inactiveTrackColor: Color.fromARGB(255, 207, 118, 0),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        enabled: !widget.stepDisabled,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        keyboardType: TextInputType.emailAddress,
                        controller: itemnamecontroller,
                        obscureText: false,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromARGB(255, 27, 81, 211),
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          labelText: "Item name",
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        enabled: !widget.stepDisabled,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: ammountcontroller,
                        obscureText: false,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 27, 81, 211)),
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          labelText: "Ammount for one item in KES",
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        enabled: !widget.stepDisabled,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: noofitemscontroller,
                        obscureText: false,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 27, 81, 211)),
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          labelText: "No of Items",
                        ),
                      ),
                    ),
                    (widget.operationinprogress == true)
                        ? Center(
                            child: SpinKitFadingCube(
                              size: 60,
                              color: Color.fromARGB(255, 207, 118, 0),
                            ),
                          )
                        : Container(),
                  ],
                ),
              );
  }
}
