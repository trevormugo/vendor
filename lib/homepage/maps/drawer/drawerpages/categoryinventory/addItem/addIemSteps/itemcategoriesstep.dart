import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/availableCategoriesResponse/availablecategoriesres.dart';
import 'package:vendor/models/defaults.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';
import 'package:vendor/restapi.dart';

class ItemCategoriesStep extends StatefulWidget {
  ItemCategoriesStep({
    required this.stepDisabled,
    required this.operationinprogress,
    required this.token,
  });
  final bool stepDisabled;
  final bool operationinprogress;
  final String token;
  @override
  State createState() => ItemCategoriesStepInstance();
}

class ItemCategoriesStepInstance extends State<ItemCategoriesStep> {
  late Future<AvailableCategoriesRes> fetchcategories;
  static AvailableCategoriesRes? availableCategoriesRes;
  final _random = new Random();
  static int? selectedindex;

  @override
  void initState() {
    super.initState();
    fetchcategories = _fetchcategories();
    Fluttertoast.showToast(
        msg: "Select the category the iem can be classified in",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0);
  }

  int next(int min, int max) => min + _random.nextInt(max - min);

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<AvailableCategoriesRes> _fetchcategories() async {
    var categories = await RestApi()
        .queryForInventoryTab("/shops/queryCategories", widget.token);
    if (categories.statusCode == 200) {
      print("${categories.body}");
      availableCategoriesRes =
          AvailableCategoriesRes.fromJson(json.decode(categories.body));
      return Future.value(availableCategoriesRes);
    } else if (categories.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${categories.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${categories.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${categories.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${categories.statusCode}');
    }
  }

  void selectCategory(int index) {
    setState(() {
      selectedindex = index;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = MediaQuery.of(context).size.height;

    double screenwidth = MediaQuery.of(context).size.width;

    return (widget.stepDisabled == true)
        ? Center(
            child: Text("Item Category already added"),
          )
        : (widget.operationinprogress == true)
            ? Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 207, 118, 0),
                ),
              )
            : Container(
                width: screenwidth * 0.9,
                height: screenheight * 0.7,
                child: FutureBuilder(
                    future: fetchcategories,
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return (snapshot.data.items.length == 0)
                            ? Center(
                                child:
                                    Text("No Categories have been allowed yet"),
                              )
                            : GridView.builder(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2),
                                itemCount: snapshot.data.items.length,
                                itemBuilder: (BuildContext context, int index) {
                                  int firstValue = next(0, 255);
                                  int secondValue = next(0, 255);
                                  int thirdValue = next(0, 255);
                                  return (!(CategoryInsights.insights
                                          .asMap()
                                          .containsKey(index)))
                                      ? GestureDetector(
                                          onTap: () => selectCategory(index),
                                          child: Container(
                                            height: screenheight * 0.4,
                                            width: screenwidth * 0.9,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              border: Border.all(
                                                color: (selectedindex == index)
                                                    ? Color.fromARGB(
                                                        255,
                                                        firstValue,
                                                        secondValue,
                                                        thirdValue)
                                                    : Colors.grey,
                                                width: 2.0,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                            ),
                                            margin: EdgeInsets.all(5),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisSize: MainAxisSize.max,
                                              children: <Widget>[
                                                Icon(
                                                  LineAwesomeIcons.shopping_bag,
                                                  color: Color.fromARGB(
                                                      255,
                                                      firstValue,
                                                      secondValue,
                                                      thirdValue),
                                                ),
                                                Padding(
                                                  padding: EdgeInsets.all(10.0),
                                                  child: Text(
                                                    "${snapshot.data.items[index].categoryName}",
                                                    style: TextStyle(
                                                      color: (selectedindex ==
                                                              index)
                                                          ? Color.fromARGB(
                                                              255,
                                                              firstValue,
                                                              secondValue,
                                                              thirdValue)
                                                          : Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      : (CategoryInsights.insights[index]
                                              .containsKey(snapshot.data
                                                  .items[index].categoryName))
                                          ? GestureDetector(
                                              onTap: () =>
                                                  selectCategory(index),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border.all(
                                                    color: (selectedindex ==
                                                            index)
                                                        ? CategoryInsights
                                                            .insights[index][
                                                                snapshot
                                                                    .data
                                                                    .items[
                                                                        index]
                                                                    .categoryName]!
                                                            .paintcolor!
                                                        : Colors.grey,
                                                    width: 2.0,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ),
                                                margin: EdgeInsets.all(5),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  mainAxisSize:
                                                      MainAxisSize.max,
                                                  children: <Widget>[
                                                    CategoryInsights
                                                        .insights[index][
                                                            snapshot
                                                                .data
                                                                .items[index]
                                                                .categoryName]!
                                                        .icon,
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.all(10.0),
                                                      child: Text(
                                                        "${CategoryInsights.insights[index][snapshot.data.items[index].categoryName]!.category}",
                                                        style: TextStyle(
                                                          color: (selectedindex ==
                                                                  index)
                                                              ? CategoryInsights
                                                                  .insights[
                                                                      index][
                                                                      snapshot
                                                                          .data
                                                                          .items[
                                                                              index]
                                                                          .categoryName]!
                                                                  .paintcolor!
                                                              : Colors.black,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          : GestureDetector(
                                              onTap: () =>
                                                  selectCategory(index),
                                              child: Container(
                                                height: screenheight * 0.4,
                                                width: screenwidth * 0.9,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border.all(
                                                    color:
                                                        (selectedindex == index)
                                                            ? Color.fromARGB(
                                                                255,
                                                                firstValue,
                                                                secondValue,
                                                                thirdValue)
                                                            : Colors.grey,
                                                    width: 2.0,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ),
                                                margin: EdgeInsets.all(5),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  mainAxisSize:
                                                      MainAxisSize.max,
                                                  children: <Widget>[
                                                    Icon(
                                                      LineAwesomeIcons
                                                          .shopping_bag,
                                                      color: Color.fromARGB(
                                                          255,
                                                          firstValue,
                                                          secondValue,
                                                          thirdValue),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          EdgeInsets.all(10.0),
                                                      child: Text(
                                                        "${snapshot.data.items[index].categoryName}",
                                                        style: TextStyle(
                                                          color: (selectedindex ==
                                                                  index)
                                                              ? Color.fromARGB(
                                                                  255,
                                                                  firstValue,
                                                                  secondValue,
                                                                  thirdValue)
                                                              : Colors.black,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                },
                              );
                      } else if (snapshot.hasError) {
                        return Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Icon(
                                LineIcons.server,
                                color: Colors.grey,
                                size: 70.0,
                              ),
                              Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Text("${snapshot.error}"),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Center(
                          child: SpinKitFadingCube(
                            size: 60,
                            color: Color.fromARGB(255, 27, 81, 211),
                          ),
                        );
                      }
                    }),
              );
  }
}
