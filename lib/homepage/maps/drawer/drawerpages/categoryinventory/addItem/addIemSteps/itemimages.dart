import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_icons/line_icons.dart';
import 'package:geolocator/geolocator.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/models/defaults.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ItemImagesUpload extends StatefulWidget {
  ItemImagesUpload({
    required this.stepDisabled,
    required this.operationinprogress,
  });
  final bool stepDisabled;
  final bool operationinprogress;
  @override
  State createState() => ItemImagesUploadInstance();
}

class ItemImagesUploadInstance extends State<ItemImagesUpload> {
  PageController pageViewController = PageController(initialPage: 0);
  static late List<File> images = [];
  final picker = ImagePicker();
  String dropdownValue = "Camera";
  ImageSource imagesrc = ImageSource.camera;

  @override
  void initState() {
    super.initState();
    Fluttertoast.showToast(
      msg: "Add compulsory item photos",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 1,
      fontSize: 16.0,
    );
  }

  Future getImage(int index) async {
    final pickedFile = await picker.pickImage(source: imagesrc);
    setState(() {
      if (pickedFile != null) {
        if (images.length <= 3) {
          images.add(File(pickedFile.path));
        } else {
          Fluttertoast.showToast(
            msg: "Only three images are allowed",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 1,
            fontSize: 16.0,
          );
        }
      } else {
        print('No image selected.');
      }
    });
  }

  Future editImage(int index) async {
    final pickedFile = await picker.pickImage(source: imagesrc);
    setState(() {
      if (pickedFile != null) {
        images[index] = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void dispose() {
    images = [];
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = MediaQuery.of(context).size.height;

    double screenwidth = MediaQuery.of(context).size.width;

    return (widget.stepDisabled == true)
        ? Center(
            child: Text("Item images already added"),
          )
        : (widget.operationinprogress == true)
            ? Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 207, 118, 0),
                ),
              )
            : Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: screenheight * 0.4,
                    width: double.infinity,
                    child: PageView(
                      controller: pageViewController,
                      children: [
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              (images.asMap()[0] == null)
                                  ? getImage(0)
                                  : editImage(0);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),
                              child: FittedBox(
                                child: Container(
                                  height: screenheight * 0.5,
                                  width: screenwidth * 0.8,
                                  color: Color.fromARGB(255, 27, 81, 211),
                                  child: (images.length >= 1)
                                      ? Image.file(
                                          images[0],
                                          fit: BoxFit.cover,
                                        )
                                      : Icon(
                                          LineIcons.camera,
                                          color:
                                              Color.fromARGB(255, 207, 118, 0),
                                        ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              (images.asMap()[1] == null)
                                  ? getImage(1)
                                  : editImage(1);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),
                              child: FittedBox(
                                child: Container(
                                  height: screenheight * 0.5,
                                  width: screenwidth * 0.8,
                                  color: Color.fromARGB(255, 27, 81, 211),
                                  child: (images.length >= 2)
                                      ? Image.file(
                                          images[1],
                                          fit: BoxFit.cover,
                                        )
                                      : Icon(
                                          LineIcons.camera,
                                          color:
                                              Color.fromARGB(255, 207, 118, 0),
                                        ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              (images.asMap()[2] == null)
                                  ? getImage(2)
                                  : editImage(2);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),
                              child: FittedBox(
                                child: Container(
                                  height: screenheight * 0.5,
                                  width: screenwidth * 0.8,
                                  color: Color.fromARGB(255, 27, 81, 211),
                                  child: (images.length >= 3)
                                      ? Image.file(
                                          images[2],
                                          fit: BoxFit.cover,
                                        )
                                      : Icon(
                                          LineIcons.camera,
                                          color:
                                              Color.fromARGB(255, 207, 118, 0),
                                        ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: screenheight * 0.1,
                    width: double.infinity,
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      icon: Icon(Icons.arrow_downward),
                      iconSize: 24,
                      elevation: 5,
                      style: TextStyle(
                        color: Color.fromARGB(255, 207, 118, 0),
                      ),
                      underline: Container(
                        height: 2,
                        color: Color.fromARGB(255, 27, 81, 211),
                      ),
                      onChanged: (String? newValue) {
                        if (newValue == "Gallery") {
                          setState(() {
                            dropdownValue = newValue!;
                            imagesrc = ImageSource.gallery;
                          });
                        } else {
                          setState(() {
                            dropdownValue = newValue!;
                            imagesrc = ImageSource.camera;
                          });
                        }
                      },
                      items: <String>['Camera', 'Gallery']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  Container(
                    height: screenheight * 0.1,
                    width: double.infinity,
                    child: Center(
                      child: SmoothPageIndicator(
                        controller: pageViewController,
                        count: 3,
                        effect: WormEffect(
                          dotWidth: 10.0,
                          dotHeight: 10.0,
                          radius: 10.0,
                          activeDotColor: Color.fromARGB(255, 27, 81, 211),
                        ),
                      ),
                    ),
                  ),
                ],
              );
  }
}
