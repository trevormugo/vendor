import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/returnPolicyResponse/returnpolicyresponse.dart';

import '../../../../../../../restapi.dart';

class ItemReturnPolicy extends StatefulWidget {
  ItemReturnPolicy({
    required this.operationinprogress,
    required this.token,
  });
  final bool operationinprogress;
  final String token;
  @override
  State createState() => ItemReturnPolicyInstance();
}

class ItemReturnPolicyInstance extends State<ItemReturnPolicy> {
  final formKey = GlobalKey<FormState>();

  TextEditingController returnpolicytextcontroller = TextEditingController();

  late Future<List<String>> _fetchReturnPolicies;

  static late ReturnPolicyResponse returnplociesresponse;

  static List<String> addedpolicies = [];

  @override
  void initState() {
    super.initState();
    _fetchReturnPolicies = fetchReturnPolicies();
    Fluttertoast.showToast(
        msg: "Add or keep item return policies",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0);
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<List<String>> fetchReturnPolicies() async {
    var rpolicy = await RestApi()
        .queryReturnPolicies("/items/getReturnPolicies", widget.token);
    if (rpolicy.statusCode == 200) {
      print("FGAUFGUISGFZD ${rpolicy.body}");
      returnplociesresponse =
          ReturnPolicyResponse.fromJson(json.decode(rpolicy.body));
      returnplociesresponse.items.forEach((element) {
        addedpolicies.add(element.policyText);
      });
      return Future.value(addedpolicies);
    } else if (rpolicy.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${rpolicy.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${rpolicy.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${rpolicy.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${rpolicy.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = MediaQuery.of(context).size.height;

    double screenwidth = MediaQuery.of(context).size.width;

    Future addpolicy() {
      return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text('Add Policy'),
          content: Form(
            key: formKey,
            child: Container(
              width: screenwidth * 0.8,
              height: screenheight * 0.1,
              margin: EdgeInsets.all(20),
              child: TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                keyboardType: TextInputType.emailAddress,
                controller: returnpolicytextcontroller,
                obscureText: false,
                decoration: InputDecoration(
                  labelStyle: TextStyle(
                    color: Color.fromARGB(255, 27, 81, 211),
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color.fromARGB(255, 27, 81, 211),
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                  labelText: "Return Policy",
                ),
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Add',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                addedpolicies.add(returnpolicytextcontroller.text);
                setState(() {});
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }

    return FutureBuilder(
        future: _fetchReturnPolicies,
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return (widget.operationinprogress == true)
                ? Center(
                    child: SpinKitFadingCube(
                      size: 60,
                      color: Color.fromARGB(255, 207, 118, 0),
                    ),
                  )
                : Container(
                    height: screenheight * 0.5,
                    width: screenwidth,
                    child: ListView.builder(
                        itemCount: snapshot.data.length + 1,
                        itemBuilder: (BuildContext context, int index) {
                          return (index == (snapshot.data.length + 1) - 1)
                              ? GestureDetector(
                                  // leading: Icon(widget.icon),
                                  onTap: addpolicy,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                      left: 10.0,
                                      right: 10.0,
                                      bottom: 5.0,
                                      top: 5.0,
                                    ),
                                    child: Container(
                                      height: screenheight * 0.1,
                                      decoration: BoxDecoration(
                                        color: Colors.white60,
                                        border: Border.all(
                                          color:
                                              Color.fromARGB(255, 27, 81, 211),
                                          width: 1.0,
                                        ),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(20),
                                        ),
                                      ),
                                      child: Center(
                                        child: RichText(
                                          text: TextSpan(
                                            children: [
                                              WidgetSpan(
                                                child: Icon(
                                                  Icons.add,
                                                  size: 20,
                                                  color: Color.fromARGB(
                                                      255, 27, 81, 211),
                                                ),
                                              ),
                                              TextSpan(
                                                text: 'Add Policy',
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  color: Color.fromARGB(
                                                      255, 27, 81, 211),
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : Padding(
                                  padding: EdgeInsets.only(
                                    left: 10.0,
                                    right: 10.0,
                                    bottom: 5.0,
                                    top: 5.0,
                                  ),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white60,
                                      border: Border.all(
                                        color: (index == snapshot.data.length)
                                            ? Color.fromARGB(255, 27, 81, 211)
                                            : Colors.black12,
                                        width: 1.0,
                                      ),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                    ),
                                    child: ListTile(
                                      // leading: Icon(widget.icon),
                                      onTap: () {},
                                      title: Text(
                                        '${snapshot.data[index]}',
                                        style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                        }),
                  );
          } else if (snapshot.hasError) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Icon(
                    LineIcons.server,
                    color: Colors.grey,
                    size: 70.0,
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("${snapshot.error}"),
                  ),
                ],
              ),
            );
          } else {
            return Center(
              child: SpinKitFadingCube(
                size: 60,
                color: Color.fromARGB(255, 27, 81, 211),
              ),
            );
          }
        });
  }
}
