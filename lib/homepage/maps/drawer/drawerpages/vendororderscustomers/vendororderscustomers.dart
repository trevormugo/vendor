import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/vendororderscustomers/onecustomerorder/onecustomerorder.dart';
import 'package:vendor/homepage/profiles/profilepage.dart';
import 'package:vendor/homepage/profiles/remoteprofilepage.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';

class VendorOrdersCustomers extends StatefulWidget {
  VendorOrdersCustomers({
    required this.shop,
    required this.customers,
    required this.token,
    required this.userId,
  });
  final Shop shop;
  final List<Account> customers;
  final String token;
  final String userId;
  @override
  State createState() => _VendorOrdersCustomersInstance();
}

class _VendorOrdersCustomersInstance extends State<VendorOrdersCustomers> {
  @override
  void initState() {
    super.initState();
  }

  Future<void> navigatetocustomersprofile(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (id == prefs.get('userid').toString()) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error Detected",
        desc: 'Youre account shouldnt be in this list!',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RemoteProfilePage(
            token: widget.token,
            userId: id,
          ),
        ),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void navigatetoonecustomerorderpage(Account customer) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneCustomerOrder(
                  token: widget.token,
                  customer: customer,
                  shop: widget.shop,
                  myUserId: widget.userId,
                )),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              "${widget.shop.shopName}'s Customers",
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: ListView.builder(
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.customers.length,
        itemBuilder: (BuildContext context, int index) => ListTile(
          leading: GestureDetector(
            onTap: () {
              navigatetocustomersprofile(widget.customers[index].id);
            },
            child: CircleAvatar(),
          ),
          title: GestureDetector(
            onTap: () {
              navigatetoonecustomerorderpage(widget.customers[index]);
            },
            child: Text('${widget.customers[index].fullName}'),
          ),
          subtitle: Text('${widget.customers[index].email}'),
          trailing: GestureDetector(
            onTap: () {
              navigatetoonecustomerorderpage(widget.customers[index]);
            },
            child: Icon(
              Icons.qr_code_scanner_outlined,
            ),
          ),
        ),
      ),
    );
  }
}
