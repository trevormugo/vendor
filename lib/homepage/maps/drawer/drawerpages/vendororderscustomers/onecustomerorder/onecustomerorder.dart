import 'dart:convert';
import 'dart:math';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/itemsincustomerorder/itemsincustomerorder.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/qrpage/qrscanner.dart';
import 'package:vendor/homepage/maps/maps.dart';
import 'package:vendor/homepage/profiles/remoteprofilepage.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/assingRider.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/oneVendorOrder/onevendororderresponse.dart';
import 'package:vendor/restapi.dart';

class OneCustomerOrder extends StatefulWidget {
  OneCustomerOrder({
    required this.token,
    required this.customer,
    required this.shop,
    required this.myUserId,
  });
  final String token;
  final String myUserId;
  final Account customer;
  final Shop shop;
  @override
  State createState() => _OneCustomerOrderInstance();
}

class _OneCustomerOrderInstance extends State<OneCustomerOrder> {
  late Future<OneVendorOrderResponse> _fetchOneVendorOrder;
  late OneVendorOrderResponse _oneVendorOrder;
  PageController pageViewController = PageController(initialPage: 0);
  int currentPage = 0;
  bool _operationinprogress = false;

  @override
  void initState() {
    super.initState();
    _fetchOneVendorOrder = fetchOneVendorOrder();
    SignalRSocket.connection.on("SendPurchaseItemRequest", (message) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Success",
        desc: 'Payment Complete',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    });
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
    dispose();
  }

  Future<OneVendorOrderResponse> fetchOneVendorOrder() async {
    var oneVendorOrder = await RestApi().getCustomerCartItemsByCustomerId(
        "/items/getCustomerOrdersByCustomerIdForVendor?customerId=${widget.customer.id}&shopId=${widget.shop.id}",
        widget.token);
    if (oneVendorOrder.statusCode == 200) {
      print("${oneVendorOrder.body}");
      _oneVendorOrder =
          OneVendorOrderResponse.fromJson(json.decode(oneVendorOrder.body));
      return Future.value(_oneVendorOrder);
    } else if (oneVendorOrder.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${oneVendorOrder.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${oneVendorOrder.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${oneVendorOrder.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${oneVendorOrder.statusCode}');
    }
  }

  Future<void> navigatetoriderssprofile(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (id == prefs.get('userid').toString()) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error Detected",
        desc: 'Youre account shouldnt be in this list!',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RemoteProfilePage(
            token: widget.token,
            userId: id,
          ),
        ),
      );
    }
  }

  void cancelOrder(String customerId, String shopId) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi().cancelOrder(
        "/items/cancelOrder?customerId=$customerId&shopId=$shopId",
        widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = true;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Success ${response.statusCode}",
        desc: 'Order Cancelled',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) =>
              CustomerMap(id: widget.myUserId, token: widget.token),
        ),
      );
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification error , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: ' ${response.body}',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    }
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void assignRider(Account rider, String qrMappingId) async {
      setState(() {
        _operationinprogress = true;
      });
      AssignRiderRequest assignRiderRequest = AssignRiderRequest(
        riderId: rider.id,
        qrMappingId: qrMappingId,
      );
      var assignRider = await RestApi()
          .assignRider("/items/assignRider", widget.token, assignRiderRequest);
      if (assignRider.statusCode == 204) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Success",
          desc: 'Rider ${rider.fullName} was assigned to this order',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else if (assignRider.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${assignRider.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${assignRider.statusCode}",
          desc: '${assignRider.body}',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      }
    }

    void showassignridermodal(Account rider, String qrMappingId) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: screenheight * 0.3,
                    width: screenwidth,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          height: screenheight * 0.3,
                          width: screenwidth * 0.3,
                          child: GestureDetector(
                            onTap: () => navigatetoriderssprofile(rider.id),
                            child: CircleAvatar(),
                          ),
                        ),
                        Container(
                          height: screenheight * 0.3,
                          width: screenwidth * 0.6,
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ListTile(
                                  title: Text(
                                    "${rider.email}",
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                                ListTile(
                                  title: Text(
                                    "${rider.fullName}",
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                                ListTile(
                                  title: Text(
                                    "${rider.phoneNumber}",
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: screenheight * 0.1,
                    width: screenwidth,
                    child: TextButton(
                      onPressed: () => assignRider(rider, qrMappingId),
                      child: Text(
                        "Assign Rider",
                        style: TextStyle(
                          color: Color.fromARGB(255, 27, 81, 211),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          });
    }

    void showordersmodal(List<OneOrder> orders, int index) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return ItemsInOneCustomerOrder(
              orders: _oneVendorOrder.oneVendorOrder[index].orders!,
              token: widget.token,
              userId: widget.customer.id,
            );
          });
    }

    void showConfirmOrderCancellationModal(String customerId, String shopId) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text('Cancel this order?'),
          actions: <Widget>[
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                cancelOrder(customerId, shopId);
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }

    return Stack(children: [
      FutureBuilder(
          future: _fetchOneVendorOrder,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (snapshot.data.oneVendorOrder.length == 0)
                  ? Center(
                      child: Text("Unexpected Error"),
                    )
                  : (snapshot.data.oneVendorOrder.length > 1)
                      ? Scaffold(
                          appBar: AppBar(
                            title: ListTile(
                              title: Text(
                                "${widget.shop.shopName}'s orders",
                                softWrap: true,
                                textAlign: TextAlign.start,
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ),
                            actions: [
                              GestureDetector(
                                onTap: () => showConfirmOrderCancellationModal(
                                    widget.customer.id, widget.shop.id),
                                child: Icon(
                                  LineIcons.timesCircle,
                                  color: Colors.red,
                                ),
                              )
                            ],
                          ),
                          floatingActionButton: FloatingActionButton(
                            onPressed: () => (currentPage == 0)
                                ? (snapshot.data.oneVendorOrder[0]
                                            .toBeDelivered ==
                                        false)
                                    ? showordersmodal(
                                        snapshot.data.oneVendorOrder[0].orders,
                                        0)
                                    : showordersmodal(
                                        snapshot.data.oneVendorOrder[1].orders,
                                        1)
                                : (snapshot.data.oneVendorOrder[0]
                                            .toBeDelivered ==
                                        true)
                                    ? showordersmodal(
                                        snapshot.data.oneVendorOrder[0].orders,
                                        0)
                                    : showordersmodal(
                                        snapshot.data.oneVendorOrder[1].orders,
                                        1),
                            child: Icon(
                              LineIcons.list,
                              color: Color.fromARGB(255, 207, 118, 0),
                            ),
                            backgroundColor: Color.fromARGB(255, 27, 81, 211),
                          ),
                          body: SafeArea(
                            top: true,
                            bottom: false,
                            left: false,
                            right: false,
                            child: LayoutBuilder(
                              builder: (context, constraints) {
                                return SingleChildScrollView(
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                        minWidth: constraints.maxWidth,
                                        minHeight: constraints.maxHeight),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          height: screenheight * 0.8,
                                          width: double.infinity,
                                          child: PageView(
                                            controller: pageViewController,
                                            scrollDirection: Axis.horizontal,
                                            onPageChanged: (index) {
                                              setState(() {
                                                currentPage = index;
                                              });
                                            },
                                            children: [
                                              /*(snapshot.data.oneVendorOrder[0]
                                              .toBeDelivered ==
                                          false)
                                      ?*/
                                              QrScannerPage(
                                                token: widget.token,
                                                userId: widget.customer.id,
                                                orderType: "Pick up",
                                                userName:
                                                    "By ${widget.customer.fullName}",
                                                connString:
                                                    "/items/sendPurchaseItemRequest",
                                              ),
                                              (snapshot.data.oneVendorOrder[1]
                                                          .toBeDelivered ==
                                                      true)
                                                  ? Container(
                                                      height: screenheight,
                                                      width: screenwidth,
                                                      child: (snapshot
                                                                  .data
                                                                  .oneVendorOrder[
                                                                      1]
                                                                  .oneRiderObj
                                                                  .length ==
                                                              0)
                                                          ? Center(
                                                              child: Text(
                                                                  "No riders affiliated yet"),
                                                            )
                                                          : ListView.builder(
                                                              physics:
                                                                  BouncingScrollPhysics(),
                                                              shrinkWrap: true,
                                                              itemCount: snapshot
                                                                      .data
                                                                      .oneVendorOrder[
                                                                          1]
                                                                      .oneRiderObj
                                                                      .length +
                                                                  1,
                                                              itemBuilder: (BuildContext
                                                                          context,
                                                                      int index) =>
                                                                  (index == 0)
                                                                      ? ListTile(
                                                                          title:
                                                                              Center(
                                                                            child:
                                                                                Chip(
                                                                              label: Text('Delivery'),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : ListTile(
                                                                          leading:
                                                                              GestureDetector(
                                                                            onTap: () =>
                                                                                navigatetoriderssprofile(snapshot.data.oneVendorOrder[1].oneRiderObj[index - 1].id),
                                                                            child:
                                                                                CircleAvatar(),
                                                                          ),
                                                                          title:
                                                                              GestureDetector(
                                                                            onTap: () =>
                                                                                showassignridermodal(snapshot.data.oneVendorOrder[1].oneRiderObj[index - 1], snapshot.data.oneVendorOrder[1].qrMappingId),
                                                                            child:
                                                                                Text('${snapshot.data.oneVendorOrder[1].oneRiderObj[index - 1].rider.fullName}'),
                                                                          ),
                                                                          subtitle:
                                                                              Text('${snapshot.data.oneVendorOrder[1].oneRiderObj[index - 1].rider.userName}'),
                                                                          trailing: (snapshot.data.oneVendorOrder[1].oneRiderObj[index - 1].isAssigned)
                                                                              ? Icon(
                                                                                  LineIcons.check,
                                                                                  color: Colors.green,
                                                                                )
                                                                              : null,
                                                                        ),
                                                            ),
                                                    )
                                                  : Container(
                                                      height: screenheight,
                                                      width: screenwidth,
                                                      child: (snapshot
                                                                  .data
                                                                  .oneVendorOrder[
                                                                      0]
                                                                  .oneRiderObj
                                                                  .length ==
                                                              0)
                                                          ? Center(
                                                              child: Text(
                                                                  "No riders affiliated yet"),
                                                            )
                                                          : ListView.builder(
                                                              physics:
                                                                  BouncingScrollPhysics(),
                                                              shrinkWrap: true,
                                                              itemCount: snapshot
                                                                      .data
                                                                      .oneVendorOrder[
                                                                          0]
                                                                      .oneRiderObj
                                                                      .length +
                                                                  1,
                                                              itemBuilder: (BuildContext
                                                                          context,
                                                                      int index) =>
                                                                  (index == 0)
                                                                      ? ListTile(
                                                                          title:
                                                                              Center(
                                                                            child:
                                                                                Chip(
                                                                              label: Text('Delivery'),
                                                                            ),
                                                                          ),
                                                                        )
                                                                      : ListTile(
                                                                          leading:
                                                                              GestureDetector(
                                                                            onTap: () =>
                                                                                navigatetoriderssprofile(snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].rider.id),
                                                                            child:
                                                                                CircleAvatar(),
                                                                          ),
                                                                          title:
                                                                              GestureDetector(
                                                                            onTap: () =>
                                                                                showassignridermodal(snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].rider, snapshot.data.oneVendorOrder[0].qrMappingId),
                                                                            child:
                                                                                Text('${snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].rider.fullName}'),
                                                                          ),
                                                                          subtitle:
                                                                              Text('${snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].rider.userName}'),
                                                                          trailing: (snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].isAssigned)
                                                                              ? Icon(
                                                                                  LineIcons.check,
                                                                                  color: Colors.green,
                                                                                )
                                                                              : null,
                                                                        ),
                                                            ),
                                                    )
                                            ],
                                          ),
                                        ),
                                        Container(
                                          height: screenheight * 0.1,
                                          width: double.infinity,
                                          child: Center(
                                            child: SmoothPageIndicator(
                                              axisDirection: Axis.horizontal,
                                              controller: pageViewController,
                                              count: snapshot
                                                  .data.oneVendorOrder.length,
                                              effect: WormEffect(
                                                dotWidth: 10.0,
                                                dotHeight: 10.0,
                                                radius: 10.0,
                                                activeDotColor: Color.fromARGB(
                                                    255, 27, 81, 211),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        )
                      : Scaffold(
                          appBar: PreferredSize(
                            preferredSize: Size.fromHeight(60),
                            child: AppBar(
                              title: ListTile(
                                title: Text(
                                  "${widget.shop.shopName}'s orders",
                                  softWrap: true,
                                  textAlign: TextAlign.start,
                                  overflow: TextOverflow.fade,
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              ),
                              actions: [
                                GestureDetector(
                                  onTap: () =>
                                      showConfirmOrderCancellationModal(
                                          widget.customer.id, widget.shop.id),
                                  child: Icon(
                                    LineIcons.timesCircle,
                                    color: Colors.red,
                                  ),
                                )
                              ],
                            ),
                          ),
                          extendBodyBehindAppBar: true,
                          extendBody: true,
                          floatingActionButton: FloatingActionButton(
                            onPressed: () => showordersmodal(
                                snapshot.data.oneVendorOrder[0].orders, 0),
                            child: Icon(
                              LineIcons.list,
                              color: Color.fromARGB(255, 207, 118, 0),
                            ),
                            backgroundColor: Color.fromARGB(255, 27, 81, 211),
                          ),
                          body: SafeArea(
                            top: true,
                            bottom: false,
                            left: false,
                            right: false,
                            child: LayoutBuilder(
                              builder: (context, constraints) {
                                return SingleChildScrollView(
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                        minWidth: constraints.maxWidth,
                                        minHeight: constraints.maxHeight),
                                    child: Container(
                                      height: screenheight * 0.8,
                                      width: double.infinity,
                                      child: (snapshot.data.oneVendorOrder[0]
                                                  .toBeDelivered ==
                                              false)
                                          ? QrScannerPage(
                                              token: widget.token,
                                              userId: widget.customer.id,
                                              orderType: "Pick up",
                                              userName:
                                                  "By ${widget.customer.fullName}",
                                              connString:
                                                  "/items/sendPurchaseItemRequest",
                                            )
                                          : Container(
                                              height: screenheight,
                                              width: screenwidth,
                                              child: (snapshot
                                                          .data
                                                          .oneVendorOrder[0]
                                                          .oneRiderObj
                                                          .length ==
                                                      0)
                                                  ? Center(
                                                      child: Text(
                                                          "No riders affiliated yet"),
                                                    )
                                                  : ListView.builder(
                                                      physics:
                                                          BouncingScrollPhysics(),
                                                      shrinkWrap: true,
                                                      itemCount: snapshot
                                                              .data
                                                              .oneVendorOrder[0]
                                                              .oneRiderObj
                                                              .length +
                                                          1,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                                  int index) =>
                                                              (index == 0)
                                                                  ? ListTile(
                                                                      title:
                                                                          Center(
                                                                        child:
                                                                            Chip(
                                                                          label:
                                                                              Text('Delivery'),
                                                                        ),
                                                                      ),
                                                                    )
                                                                  : ListTile(
                                                                      leading:
                                                                          GestureDetector(
                                                                        onTap: () => navigatetoriderssprofile(snapshot
                                                                            .data
                                                                            .oneVendorOrder[
                                                                                0]
                                                                            .oneRiderObj[index -
                                                                                1]
                                                                            .rider
                                                                            .id),
                                                                        child:
                                                                            CircleAvatar(),
                                                                      ),
                                                                      title:
                                                                          GestureDetector(
                                                                        onTap: () => showassignridermodal(
                                                                            snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].rider,
                                                                            snapshot.data.oneVendorOrder[0].qrMappingId),
                                                                        child: Text(
                                                                            '${snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].rider.fullName}'),
                                                                      ),
                                                                      subtitle:
                                                                          Text(
                                                                              '${snapshot.data.oneVendorOrder[0].oneRiderObj[index - 1].rider.userName}'),
                                                                      trailing: (snapshot
                                                                              .data
                                                                              .oneVendorOrder[0]
                                                                              .oneRiderObj[index - 1]
                                                                              .isAssigned)
                                                                          ? Icon(
                                                                              LineIcons.check,
                                                                              color: Colors.green,
                                                                            )
                                                                          : null,
                                                                    ),
                                                    ),
                                            ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        );
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
