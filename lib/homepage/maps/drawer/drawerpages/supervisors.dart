import 'dart:convert';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/remoteprofilepage.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/acceptAffiliation.dart';
import 'package:vendor/models/affiliaterider.dart';
import 'package:vendor/models/affiliatesupervisor.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/riderSearch/ridersearch.dart';
import 'package:vendor/models/ridersTabQuery/riderstabquery.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';
import 'package:vendor/models/supervisorSearch/supervisorsearch.dart';
import 'package:vendor/models/supervisorsTabQuery/supervisorstabquaery.dart';
import 'package:vendor/restapi.dart';

import 'dart:math';

class Supervisors extends StatefulWidget {
  Supervisors({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _SupervisorsInstance();
}

class _SupervisorsInstance extends State<Supervisors>
    with TickerProviderStateMixin {
  late TabController _tabcontroller;
  late ScrollController _scrollController;
  late Future<SuperVisorsTabQuery> _fetchsupervisoraffiliations;
  late SuperVisorsTabQuery _supervisoraffiliations;
  late SupervisorSearch _supervisorSearch;
  TextEditingController _supervisorsearchcontroller = TextEditingController();
  bool _operationinprogress = false;
  List<int> selectedIndexes = [];
  List<int> responseCodes = [];

  @override
  void initState() {
    super.initState();
    _tabcontroller = TabController(length: 3, vsync: this);
    _scrollController = ScrollController();
    _fetchsupervisoraffiliations = fetchsupervisoraffiliations();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
    dispose();
  }

  Future<SuperVisorsTabQuery> fetchsupervisoraffiliations() async {
    var supervisors = await RestApi().quarySuppervisorsTab(
        "/supervisormanagement/supervisorsTabQuery?vendorId=${widget.userId}",
        widget.token);
    if (supervisors.statusCode == 200) {
      print("${supervisors.body}");
      _supervisoraffiliations =
          SuperVisorsTabQuery.fromJson(json.decode(supervisors.body));
      return Future.value(_supervisoraffiliations);
    } else if (supervisors.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${supervisors.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${supervisors.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${supervisors.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error ${supervisors.statusCode}');
    }
  }

  void affiliateSupervisor(
      Account supervisor, String vendorId, List<ShopWithPlans> shops) async {
    addAffiliation(
            0, selectedIndexes.length - 1, shops, vendorId, supervisor.id)
        .listen((event) {
      setState(() {
        _operationinprogress = true;
      });
      responseCodes.add(event.statusCode);
      responseCodes.where((element) => element == 201).length;
      if (event.index == selectedIndexes.length - 1) {
        if (responseCodes.where((element) => element == 201).length ==
            selectedIndexes.length) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: "SUCCESS",
            desc:
                '${supervisor.fullName} is now affiliated with all ${responseCodes.where((element) => element == 201).length} shops selected',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            title: "NOTE",
            desc:
                '${supervisor.fullName} was affiliated to only ${responseCodes.where((element) => element == 201).length} shops , ${responseCodes.where((element) => element != 201).length} shops failed',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
        }
        setState(() {
          responseCodes = [];
          _operationinprogress = false;
          _fetchsupervisoraffiliations = fetchsupervisoraffiliations();
        });
      }
    });
  }

  Stream<PostAffiliatedShopResponse> addAffiliation(int start, int finish,
      List<ShopWithPlans> shops, String vendorId, String supervisorId) async* {
    for (int i = start; i <= finish; i++) {
      AffiliateSupervisor affiliateSupervisor = AffiliateSupervisor(
        supervisorId: supervisorId,
        vendorId: vendorId,
        shopId: shops[i].shop.id,
        inititatorUser: widget.userId,
      );
      var response = await RestApi().affiliateSupervisor(
          "/supervisormanagement/affiliateSupervisor",
          widget.token,
          affiliateSupervisor);
      Fluttertoast.showToast(
          msg: "${response.body}",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 1,
          fontSize: 16.0);
      yield PostAffiliatedShopResponse(
        index: i,
        statusCode: response.statusCode,
        body: response.body,
      );
    }
  }

  Future<void> navigatetoriderssprofile(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (id == prefs.get('userid').toString()) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error Detected",
        desc: 'Youre account shouldnt be in this list!',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RemoteProfilePage(
            token: widget.token,
            userId: id,
          ),
        ),
      );
    }
  }

  void navigatetoshopprofile(String shopId) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ShopProfile(
          shopId: shopId,
          token: widget.token,
          userId: widget.userId,
        ),
      ),
    );
  }

  void removeShopFromAffiliationRequest(
      String shopId, String superVisorId) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi().removeShopFromAffiliation(
        "/supervisormanagement/removeShopFromAffiliation?shopId=$shopId&supervisorId=$superVisorId",
        widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
        _fetchsupervisoraffiliations = fetchsupervisoraffiliations();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Rider unaffiliated from shop',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  void acceptAffiliationsRequest(String superVisorId) async {
    setState(() {
      _operationinprogress = true;
    });
    AcceptAffiliation acceptAffiliation = AcceptAffiliation(
      userId: superVisorId,
      vendorId: widget.userId,
    );
    var response = await RestApi().acceptAffiliatinRequest(
        "/supervisormanagement/acceptAffiliatinRequest",
        widget.token,
        acceptAffiliation);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
        _fetchsupervisoraffiliations = fetchsupervisoraffiliations();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Supervisor is now affiliated',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  void unaffiliateSuperVisor(String superVisorId) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi().unaffiliateSupervisor(
        "/supervisormanagement/unaffiliateSupervisor?vendorId=${widget.userId}&supervisorId=$superVisorId",
        widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
        _fetchsupervisoraffiliations = fetchsupervisoraffiliations();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Supervisor is now unaffiliated',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  @override
  void dispose() {
    _tabcontroller.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void showshopaffiliates(List<ShopWithPlans> shops, Account supervisor) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return StatefulBuilder(
                builder: (BuildContext context, StateSetter setModalState) {
              return Container(
                child: ListView.builder(
                    itemCount: shops.length + 1,
                    itemBuilder: (BuildContext context, int index) {
                      return (index == 0)
                          ? TextButton(
                              onPressed: () => (_operationinprogress == true)
                                  ? null
                                  : acceptAffiliationsRequest(supervisor.id),
                              child: Text('Affiliate Supervisor'),
                            )
                          : ListTile(
                              leading: GestureDetector(
                                onTap: () => navigatetoshopprofile(
                                    shops[index - 1].shop.id),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20.0),
                                  child: FittedBox(
                                    child: Container(
                                      height: screenheight * 0.4,
                                      width: screenwidth * 0.9,
                                      color: Colors.transparent,
                                      child: (shops[index - 1].planName ==
                                              PlanNames.basic)
                                          ? Image.asset(
                                              PlanImages.basic,
                                            )
                                          : (shops[index - 1].planName ==
                                                  PlanNames.advanced)
                                              ? Image.asset(
                                                  PlanImages.advanced,
                                                )
                                              : (shops[index - 1].planName ==
                                                      PlanNames.enterprise)
                                                  ? Image.asset(
                                                      PlanImages.enterprise,
                                                    )
                                                  : Image.asset(
                                                      ErrorImage.noimage,
                                                    ),
                                    ),
                                  ),
                                ),
                              ),
                              title: Text(' ${shops[index - 1].shop.shopName}'),
                              trailing: GestureDetector(
                                onTap: () {
                                  removeShopFromAffiliationRequest(
                                      shops[index - 1].shop.id, supervisor.id);
                                  setModalState(() {
                                    shops.removeAt(index - 1);
                                  });
                                },
                                child: Icon(
                                  LineIcons.timesCircle,
                                  color: Colors.red,
                                ),
                              ),
                            );
                    }),
              );
            });
          });
    }

    void showshopaffiliatesforrealaffiliates(
        List<ShopWithPlans> shops, Account supervisor) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return StatefulBuilder(
                builder: (BuildContext context, StateSetter setModalState) {
              return Container(
                child: ListView.builder(
                    itemCount: shops.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: GestureDetector(
                          onTap: () =>
                              navigatetoshopprofile(shops[index].shop.id),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: FittedBox(
                              child: Container(
                                height: screenheight * 0.4,
                                width: screenwidth * 0.9,
                                color: Colors.transparent,
                                child: (shops[index].planName ==
                                        PlanNames.basic)
                                    ? Image.asset(
                                        PlanImages.basic,
                                      )
                                    : (shops[index].planName ==
                                            PlanNames.advanced)
                                        ? Image.asset(
                                            PlanImages.advanced,
                                          )
                                        : (shops[index].planName ==
                                                PlanNames.enterprise)
                                            ? Image.asset(
                                                PlanImages.enterprise,
                                              )
                                            : Image.asset(
                                                ErrorImage.noimage,
                                              ),
                              ),
                            ),
                          ),
                        ),
                        title: Text(' ${shops[index].shop.shopName}'),
                        trailing: GestureDetector(
                          onTap: () {
                            removeShopFromAffiliationRequest(
                                shops[index].shop.id, supervisor.id);
                            setModalState(() {
                              shops.removeAt(index);
                            });
                          },
                          child: Icon(
                            LineIcons.timesCircle,
                            color: Colors.red,
                          ),
                        ),
                      );
                    }),
              );
            });
          });
    }

    void searchsupervisor(String email, List<ShopWithPlans> shops) async {
      setState(() {
        _operationinprogress = true;
      });
      var supervisorsearch = await RestApi().searchSupervisor(
          "/supervisormanagement/searchsupervisor?userEmail=$email",
          widget.token);

      if (supervisorsearch.statusCode == 200) {
        setState(() {
          _operationinprogress = false;
        });
        _supervisorSearch =
            SupervisorSearch.fromJson(json.decode(supervisorsearch.body));
        showModalBottomSheet(
            context: context,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
              ),
            ),
            builder: (context) {
              return StatefulBuilder(
                  builder: (BuildContext context, StateSetter setModalState) {
                return Container(
                  child: ListView.builder(
                      itemCount: shops.length + 1,
                      itemBuilder: (BuildContext context, int index) {
                        return (index == 0)
                            ? TextButton(
                                onPressed: () => (_operationinprogress == true)
                                    ? null
                                    : affiliateSupervisor(
                                        _supervisorSearch.supervisor!,
                                        widget.userId,
                                        shops),
                                child: Text('Affiliate Supervisor'),
                              )
                            : ListTile(
                                selected: (selectedIndexes.contains(index - 1))
                                    ? true
                                    : false,
                                leading: GestureDetector(
                                  onTap: () => navigatetoshopprofile(
                                      shops[index - 1].shop.id),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: FittedBox(
                                      child: Container(
                                        height: screenheight * 0.4,
                                        width: screenwidth * 0.9,
                                        color: Colors.transparent,
                                        child: (shops[index - 1].planName ==
                                                PlanNames.basic)
                                            ? Image.asset(
                                                PlanImages.basic,
                                              )
                                            : (shops[index - 1].planName ==
                                                    PlanNames.advanced)
                                                ? Image.asset(
                                                    PlanImages.advanced,
                                                  )
                                                : (shops[index - 1].planName ==
                                                        PlanNames.enterprise)
                                                    ? Image.asset(
                                                        PlanImages.enterprise,
                                                      )
                                                    : Image.asset(
                                                        ErrorImage.noimage,
                                                      ),
                                      ),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  if (selectedIndexes.contains(index - 1)) {
                                    setModalState(() {
                                      selectedIndexes.removeWhere(
                                          (element) => element == index - 1);
                                    });
                                  } else {
                                    setModalState(() {
                                      selectedIndexes.add(index - 1);
                                    });
                                  }
                                },
                                title: Text(' ${shops[index - 1].shop.shopName}'),
                                trailing: (selectedIndexes.contains(index - 1))
                                    ? Icon(
                                        LineIcons.check,
                                        color: Colors.green,
                                      )
                                    : null,
                              );
                      }),
                );
              });
            });
      } else if (supervisorsearch.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${supervisorsearch.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${supervisorsearch.statusCode}",
          desc: ' ${supervisorsearch.body}',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      }
    }

    return Stack(children: [
      DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: ListTile(
              title: Text(
                'Supervisors',
                softWrap: true,
                textAlign: TextAlign.start,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            bottom: const TabBar(
              tabs: [
                Tab(
                  text: "Affiliated",
                ),
                Tab(
                  text: "Requests",
                ),
                Tab(
                  text: "Affiliate",
                ),
              ],
            ),
          ),
          extendBodyBehindAppBar: true,
          body: FutureBuilder(
              future: _fetchsupervisoraffiliations,
              builder: (context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return TabBarView(
                    children: [
                      (snapshot.data.affiliatedSuperVisors.length == 0)
                          ? Center(
                              child: Text(
                                "You are not affiliated with any supervisors yet",
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                              ),
                            )
                          : Container(
                              height: screenheight * 0.9,
                              width: screenwidth * 0.8,
                              child: ListView.builder(
                                physics: BouncingScrollPhysics(),
                                shrinkWrap: true,
                                itemCount:
                                    snapshot.data.affiliatedSuperVisors.length,
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        ListTile(
                                  leading: GestureDetector(
                                    onTap: () => navigatetoriderssprofile(
                                        snapshot
                                            .data
                                            .affiliatedSuperVisors[index]
                                            .supervisor
                                            .id),
                                    child: CircleAvatar(),
                                  ),
                                  onTap: () =>
                                      showshopaffiliatesforrealaffiliates(
                                          snapshot
                                              .data
                                              .affiliatedSuperVisors[index]
                                              .shopWithPlan,
                                          snapshot
                                              .data
                                              .affiliatedSuperVisors[index]
                                              .supervisor),
                                  title: Text(
                                      '${snapshot.data.affiliatedSuperVisors[index].supervisor.fullName}'),
                                  subtitle: Text(
                                      '${TimeConversion.readTimestamp(snapshot.data.affiliatedSuperVisors[index].affiliation.timestamp)}'),
                                  trailing: GestureDetector(
                                    onTap: () => unaffiliateSuperVisor(snapshot
                                        .data
                                        .affiliatedSuperVisors[index]
                                        .supervisor
                                        .id),
                                    child: Icon(
                                      LineIcons.timesCircle,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                      (snapshot.data.affiliationRequestSent.length == 0)
                          ? Center(
                              child: Text(
                                  "You have not recieved any affiliation requests yet"),
                            )
                          : Container(
                              height: screenheight * 0.9,
                              width: screenwidth * 0.8,
                              child: ListView.builder(
                                physics: BouncingScrollPhysics(),
                                shrinkWrap: true,
                                itemCount:
                                    snapshot.data.affiliationRequestSent.length,
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        ListTile(
                                  leading: GestureDetector(
                                    onTap: () => navigatetoriderssprofile(
                                        snapshot
                                            .data
                                            .affiliationRequestSent[index]
                                            .supervisor
                                            .id),
                                    child: CircleAvatar(),
                                  ),
                                  onTap: () => showshopaffiliates(
                                      snapshot.data
                                          .affiliationRequestSent[index].shopWithPlan,
                                      snapshot
                                          .data
                                          .affiliationRequestSent[index]
                                          .supervisor),
                                  title: Text(
                                      '${snapshot.data.affiliationRequestSent[index].supervisor.fullName}'),
                                  subtitle: Text(
                                      '${TimeConversion.readTimestamp(snapshot.data.affiliationRequestSent[index].affiliation.timestamp)}'),
                                  trailing: Icon(
                                    LineIcons.angleUp,
                                  ),
                                ),
                              ),
                            ),
                      Container(
                        height: screenheight * 0.9,
                        width: screenwidth * 0.8,
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Container(
                                width: screenwidth * 0.8,
                                height: 50,
                                margin: EdgeInsets.all(20),
                                child: TextFormField(
                                  enabled: !_operationinprogress,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    return null;
                                  },
                                  controller: _supervisorsearchcontroller,
                                  obscureText: false,
                                  decoration: InputDecoration(
                                      labelStyle: TextStyle(
                                        color: Color.fromARGB(255, 27, 81, 211),
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color:
                                              Color.fromARGB(255, 27, 81, 211),
                                        ),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12),
                                        ),
                                      ),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(12),
                                        ),
                                      ),
                                      labelText: "Enter user's email"),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  if (snapshot.data.shopWithPlan == null) {
                                    AwesomeDialog(
                                      context: context,
                                      dialogType: DialogType.ERROR,
                                      animType: AnimType.BOTTOMSLIDE,
                                      title: "You don own any shops yet",
                                      desc:
                                          'Head over to inventory and create some shops first',
                                      btnCancelOnPress: () {},
                                      btnOkOnPress: () {},
                                    )..show();
                                  } else {
                                    if (_operationinprogress == true) {
                                      Fluttertoast.showToast(
                                          msg: "Operation in progress...",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.TOP,
                                          timeInSecForIosWeb: 1,
                                          fontSize: 16.0);
                                    } else {
                                      searchsupervisor(
                                          _supervisorsearchcontroller.text,
                                          snapshot.data.shopWithPlan);
                                    }
                                  }
                                },
                                child: Icon(
                                  LineIcons.search,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Icon(
                          LineIcons.server,
                          color: Colors.grey,
                          size: 70.0,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text("${snapshot.error}"),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: SpinKitFadingCube(
                      size: 60,
                      color: Color.fromARGB(255, 27, 81, 211),
                    ),
                  );
                }
              }),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
