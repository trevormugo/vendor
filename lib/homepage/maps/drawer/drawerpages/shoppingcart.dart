import 'dart:convert';
import 'dart:math';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/settings/adddeliverylocation/adddeliverylocation.dart';
import 'package:vendor/homepage/profiles/settings/deliverylocationlist/deliverylocationlist.dart';
import 'package:vendor/homepage/shop/oneitem/oneitem.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/models/customerCartResponse/customercartobject.dart';
import 'package:vendor/models/customerCartResponse/customercartresponse.dart';
import 'package:vendor/models/customerCartResponse/deliverylocations.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/logInRequest/loginrequest.dart';
import 'package:vendor/models/orderitemrequest.dart';
import 'package:vendor/restapi.dart';

import '../../../../main.dart';

class ShoppingCart extends StatefulWidget {
  ShoppingCart({required this.token, required this.userId});
  final String token;
  final String userId;
  @override
  State createState() => _ShoppingCartInstance();
}

class _ShoppingCartInstance extends State<ShoppingCart> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _passwordcontroller = TextEditingController();

  late Future<CustomerCartResponse> _fetchCustomerCartItems;
  late CustomerCartResponse _shoppingCart;
  List<int> selectedIndexes = [];
  int selectedIndex = 0;
  int deliveryFee = 0;
  List<int> responseCodes = [];
  bool _operationinprogress = false;

  @override
  void initState() {
    super.initState();
    _fetchCustomerCartItems = fetchCustomerCartItems();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<CustomerCartResponse> fetchCustomerCartItems() async {
    var customerCart = await RestApi().getCustomerCartItemsByCustomerId(
        "/items/getCustomerCartItemsByCustomerId/${widget.userId}",
        widget.token);
    if (customerCart.statusCode == 200) {
      print("${customerCart.body}");
      _shoppingCart =
          CustomerCartResponse.fromJson(json.decode(customerCart.body));
      print("${_shoppingCart.deliveryLocations.toString()}");
      return Future.value(_shoppingCart);
    } else if (customerCart.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${customerCart.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${customerCart.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${customerCart.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${customerCart.statusCode}');
    }
  }

  void navigateToItem(String itemId) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => OneItem(
                icon: Icon(
                  Icons.restaurant,
                  color: Colors.lightGreenAccent,
                ),
                itemId: itemId,
                userId: widget.userId,
                token: widget.token,
              )),
    );
  }

  void orderItems(List<CustomerCartObject> customerCartObject,
      List<DeliveryLocations> deliveryLocations) async {
    addOrderItemObj(0, customerCartObject.length - 1, customerCartObject,
            deliveryLocations)
        .listen((event) {
      setState(() {
        _operationinprogress = true;
      });
      Fluttertoast.showToast(
        msg:
            'Code ${event.statusCode} : ordering ${customerCartObject[event.index].item.itemName} , ${event.body}',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0,
      );
      responseCodes.add(event.statusCode);
      if (event.index == customerCartObject.length - 1) {
        if (responseCodes.where((element) => element == 201).length ==
            customerCartObject.length) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: "SUCCESS",
            desc:
                '${responseCodes.where((element) => element == 201).length} / ${responseCodes.where((element) => element != 201).length} orders went through',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            title: "NOTE",
            desc:
                'Only ${responseCodes.where((element) => element == 201).length} / ${responseCodes.where((element) => element != 201).length} orders were placed',
            btnCancelOnPress: null,
            btnOkOnPress: null,
          )..show();
        }
        setState(() {
          _operationinprogress = false;
          _fetchCustomerCartItems = fetchCustomerCartItems();
          responseCodes = [];
        });
      }
    });
  }

  Stream<PostAddOrderResponse> addOrderItemObj(
    int start,
    int finish,
    List<CustomerCartObject> customerCartObject,
    List<DeliveryLocations> deliveryLocations,
  ) async* {
    for (int i = start; i <= finish; i++) {
      OrderItemRequest orderItemRequest = OrderItemRequest(
        customerCartId: customerCartObject[i].customerCartObject.id,
        deliveryLocationId: deliveryLocations[selectedIndex].id,
        toBeDelivered: (selectedIndexes.contains(i)) ? true : false,
      );
      var response = await RestApi()
          .orderItems("/items/orderItem", widget.token, orderItemRequest);
      yield PostAddOrderResponse(
        index: i,
        statusCode: response.statusCode,
        body: response.body,
      );
    }
  }

  Future removeCustomerCartItem(String customerCartId) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi().removeItemFromCart(
        "/items/removeItemFromCart/$customerCartId", widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
        _fetchCustomerCartItems = fetchCustomerCartItems();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Item removed from cart',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      return Future.value(response);
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification error , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${response.statusCode}');
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      return Future.error('Error  ${response.statusCode}');
    }
  }

  Future approvecheck(List<CustomerCartObject> customerCartObject,
      List<DeliveryLocations> deliveryLocations) async {
    if (_formKey.currentState!.validate()) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        _operationinprogress = true;
      });
      var loginresponse = await RestApi().login(
          "/accountspool/authorizePayment",
          LogInRequest(
            email: prefs.getString("useremail"),
            password: _passwordcontroller.text,
          ));
      if (loginresponse.statusCode == 200) {
        setState(() {
          _operationinprogress = false;
        });
        orderItems(customerCartObject, deliveryLocations);
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Error ${loginresponse.statusCode}',
          desc: '${loginresponse.body}',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      }
    }
  }

  @override
  void dispose() {
    _passwordcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    Future<bool> confirmpayment(List<CustomerCartObject> customerCartObject,
        List<DeliveryLocations> deliveryLocations) async {
      int finalPrice = 0;
      int noOfItems = 0;

      for (var i = 0; i < customerCartObject.length; i++) {
        noOfItems =
            noOfItems + customerCartObject[i].customerCartObject.noOfItems;
        finalPrice = finalPrice +
            (customerCartObject[i].customerCartObject.noOfItems *
                customerCartObject[i].item.ammount);
      }
      return await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text('Confirm Payment'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                (deliveryFee == 0)
                    ? Text('KSH $finalPrice for $noOfItems items')
                    : Text(
                        'TOTAL PRICE: ${finalPrice + deliveryFee} sh  -  $finalPrice sh for $noOfItems items + $deliveryFee sh delivery fee'),
                Text('Enter password to approve this order'),
                Form(
                  key: _formKey,
                  child: Container(
                    width: double.infinity,
                    height: 50,
                    margin: EdgeInsets.all(20),
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.emailAddress,
                      controller: _passwordcontroller,
                      obscureText: true,
                      decoration: InputDecoration(
                        labelStyle: TextStyle(
                          color: Color.fromARGB(255, 27, 81, 211),
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color.fromARGB(255, 27, 81, 211),
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        labelText: "Enter you\'re password",
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            TextButton(
              child: Text(
                'Approve',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                approvecheck(customerCartObject, deliveryLocations);
                Navigator.of(context).pop(true);
              },
            ),
          ],
        ),
      );
    }

    Future<bool> confirmItemRemovalFromCart(
        CustomerCartObject customerCartObject) async {
      return await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text('Remove item from cart?'),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            TextButton(
              child: Text(
                'Remove',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                removeCustomerCartItem(
                    customerCartObject.customerCartObject.id);
                Navigator.of(context).pop(true);
              },
            ),
          ],
        ),
      );
    }

    void addDeliveryLocation() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddDeiveryLocations(
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void reinitializefuture() {
      setState(() {
        _fetchCustomerCartItems = fetchCustomerCartItems();
      });
    }

    return Stack(children: [
      Scaffold(
        body: FutureBuilder(
            future: _fetchCustomerCartItems,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return (snapshot.data.items.length == 0)
                    ? Scaffold(
                        appBar: PreferredSize(
                          preferredSize: Size.fromHeight(60),
                          child: AppBar(
                            leading: IconButton(
                              icon: Icon(LineIcons.arrowLeft),
                              iconSize: 20.0,
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            title: ListTile(
                              title: Text(
                                'Shopping Cart',
                                softWrap: true,
                                textAlign: TextAlign.start,
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ),
                          ),
                        ),
                        body: Center(
                          child: Text("No items in cart yet"),
                        ),
                      )
                    : Scaffold(
                        appBar: PreferredSize(
                          preferredSize: Size.fromHeight(60),
                          child: AppBar(
                            leading: IconButton(
                              icon: Icon(LineIcons.arrowLeft),
                              iconSize: 20.0,
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            title: ListTile(
                              title: Text(
                                'Shopping Cart',
                                softWrap: true,
                                textAlign: TextAlign.start,
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ),
                          ),
                        ),
                        extendBodyBehindAppBar: false,
                        extendBody: true,
                        floatingActionButton: FloatingActionButton(
                          onPressed: () => confirmpayment(snapshot.data.items,
                              snapshot.data.deliveryLocations),
                          child: Icon(
                            LineAwesomeIcons.credit_card,
                            color: Color.fromARGB(255, 207, 118, 0),
                          ),
                          backgroundColor: Color.fromARGB(255, 27, 81, 211),
                        ),
                        endDrawer: Drawer(
                          child: SafeArea(
                            top: true,
                            child: Container(
                              color: Colors.white,
                              height: screenheight,
                              width: double.infinity,
                              child: (snapshot.data.deliveryLocations.length ==
                                      0)
                                  ? Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Text(
                                            "Delivery locations not specified"),
                                        TextButton(
                                          onPressed: () =>
                                              addDeliveryLocation(),
                                          child: Text("Add Delivery Location"),
                                        ),
                                      ],
                                    )
                                  : Container(
                                      height: screenheight,
                                      child: DeliveryLocationList(
                                        deliveryLocations:
                                            snapshot.data.deliveryLocations,
                                        token: widget.token,
                                        userId: widget.userId,
                                        callback: reinitializefuture,
                                      ),
                                    ),
                            ),
                          ),
                        ),
                        body: Stack(
                          children: [
                            ListView.builder(
                              physics: BouncingScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: snapshot.data.items.length + 1,
                              itemBuilder: (BuildContext context, int index) =>
                                  (index ==
                                          (snapshot.data.items.length + 1) - 1)
                                      ? Container(
                                          height: screenheight * 0.1,
                                          child: null,
                                        )
                                      : Dismissible(
                                          key: UniqueKey(),
                                          confirmDismiss: (DismissDirection
                                              direction) async {
                                            if (direction ==
                                                DismissDirection.startToEnd) {
                                              return await confirmpayment(
                                                  [snapshot.data.items[index]],
                                                  snapshot
                                                      .data.deliveryLocations);
                                            } else {
                                              return await confirmItemRemovalFromCart(
                                                  snapshot.data.items[index]);
                                            }
                                          },
                                          onDismissed: (direction) {
                                            setState(() {
                                              snapshot.data.items
                                                  .removeAt(index);
                                            });
                                          },
                                          background: Container(
                                            padding: EdgeInsets.only(left: 12),
                                            alignment: Alignment.centerLeft,
                                            color: Colors.green,
                                            child: Row(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      right: 8.0),
                                                  child: Text(
                                                    "order",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                Icon(
                                                  Icons.check,
                                                  color: Colors.white,
                                                ),
                                              ],
                                            ),
                                          ),
                                          secondaryBackground: Container(
                                            padding: EdgeInsets.only(right: 12),
                                            alignment: Alignment.centerRight,
                                            color: Colors.red,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 8.0),
                                                  child: Text(
                                                    "remove",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                Icon(
                                                  Icons.cancel,
                                                  color: Colors.white,
                                                ),
                                              ],
                                            ),
                                          ),
                                          child: ListTile(
                                            onTap: null,
                                            leading: GestureDetector(
                                              onTap: () => navigateToItem(
                                                  snapshot.data.items[index]
                                                      .item.id),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(20.0),
                                                child: FittedBox(
                                                  child: Container(
                                                    height: screenheight * 0.4,
                                                    width: screenwidth * 0.9,
                                                    color: Colors.transparent,
                                                    child: Image.network(
                                                      Adress.myip +
                                                          "/items/fetchuploadthumbnail?id${snapshot.data.items[index].itemImages.id}",
                                                      errorBuilder:
                                                          (BuildContext context,
                                                              Object exception,
                                                              StackTrace?
                                                                  stackTrace) {
                                                        return Image.asset(
                                                          ErrorImage.noimage,
                                                        );
                                                      },
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            title: Text(
                                                '${snapshot.data.items[index].item.itemName}'),
                                            subtitle: Text(
                                                '${snapshot.data.items[index].customerCartObject.noOfItems} items - ${snapshot.data.items[index].item.ammount}'),
                                            trailing: (snapshot
                                                    .data
                                                    .items[index]
                                                    .item
                                                    .canBeDelivered)
                                                ? GestureDetector(
                                                    onTap: () {
                                                      if (!selectedIndexes
                                                              .contains(
                                                                  index) &&
                                                          snapshot
                                                                  .data
                                                                  .items[index]
                                                                  .item
                                                                  .canBeDelivered ==
                                                              true) {
                                                        if (snapshot
                                                                    .data
                                                                    .deliveryLocations
                                                                    .length ==
                                                                0 ||
                                                            snapshot.data
                                                                    .deliveryLocations ==
                                                                null ||
                                                            snapshot
                                                                    .data
                                                                    .items[
                                                                        index]
                                                                    .deliveryDetails ==
                                                                null) {
                                                          Fluttertoast
                                                              .showToast(
                                                            msg:
                                                                "No delivery locations were specified",
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .TOP,
                                                            timeInSecForIosWeb:
                                                                1,
                                                            fontSize: 16.0,
                                                          );
                                                        } else {
                                                          setState(() {
                                                            selectedIndexes
                                                                .add(index);
                                                            deliveryFee = (deliveryFee +
                                                                snapshot
                                                                    .data
                                                                    .items[
                                                                        index]
                                                                    .deliveryDetails
                                                                    .deliveryAmmount) as int;
                                                          });

                                                          Fluttertoast
                                                              .showToast(
                                                            msg:
                                                                "${snapshot.data.items[index].item.itemName} tagged for delivery",
                                                            toastLength: Toast
                                                                .LENGTH_SHORT,
                                                            gravity:
                                                                ToastGravity
                                                                    .TOP,
                                                            timeInSecForIosWeb:
                                                                1,
                                                            fontSize: 16.0,
                                                          );
                                                        }
                                                      } else {
                                                        Fluttertoast.showToast(
                                                          msg:
                                                              "${snapshot.data.items[index].item.itemName} cant be tagged for delivery",
                                                          toastLength: Toast
                                                              .LENGTH_SHORT,
                                                          gravity:
                                                              ToastGravity.TOP,
                                                          timeInSecForIosWeb: 1,
                                                          fontSize: 16.0,
                                                        );
                                                      }
                                                    },
                                                    child: Icon(
                                                      LineAwesomeIcons.tag,
                                                      color: (selectedIndexes
                                                              .contains(index))
                                                          ? Colors.green
                                                          : Colors.grey,
                                                    ),
                                                  )
                                                : null,
                                          ),
                                        ),
                            ),
                            (selectedIndexes.length == 0)
                                ? Container()
                                : DraggableScrollableSheet(
                                    maxChildSize: 0.9,
                                    minChildSize: 0.1,
                                    initialChildSize: 0.1,
                                    expand: true,
                                    builder: (BuildContext context,
                                        ScrollController scrollController) {
                                      return SingleChildScrollView(
                                        controller: scrollController,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                            border: Border.all(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                            ),
                                            color: Colors.white,
                                          ),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                  height: screenheight * 0.1,
                                                  child:
                                                      Icon(LineIcons.angleUp)),
                                              Container(
                                                height: screenheight * 0.7,
                                                child: ListView.builder(
                                                    physics:
                                                        ClampingScrollPhysics(),
                                                    shrinkWrap: true,
                                                    controller:
                                                        scrollController,
                                                    itemCount:
                                                        selectedIndexes.length +
                                                            2,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return (index == 0)
                                                          ? Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceAround,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .max,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  "+ $deliveryFee",
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .green,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "${selectedIndexes.length} tagged for delivery",
                                                                ),
                                                                Icon(LineIcons
                                                                    .mapMarker),
                                                              ],
                                                            )
                                                          : (index ==
                                                                  (selectedIndexes
                                                                              .length +
                                                                          2) -
                                                                      1)
                                                              ? Container(
                                                                  height:
                                                                      screenheight *
                                                                          0.1,
                                                                  child: null,
                                                                )
                                                              : ListTile(
                                                                  title: Text(
                                                                      '${snapshot.data.items[selectedIndexes[index - 1]].item.itemName}'),
                                                                  subtitle:
                                                                      Text(
                                                                    'fee - ${snapshot.data.items[selectedIndexes[index - 1]].deliveryDetails.deliveryAmmount} sh',
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                          .green,
                                                                    ),
                                                                  ),
                                                                  trailing:
                                                                      GestureDetector(
                                                                    onTap: () {
                                                                      setState(
                                                                          () {
                                                                        deliveryFee =
                                                                            (deliveryFee - snapshot.data.items[selectedIndexes[index - 1]].deliveryDetails.deliveryAmmount)
                                                                                as int;
                                                                        selectedIndexes.removeAt(
                                                                            index -
                                                                                1);
                                                                      });
                                                                      Fluttertoast
                                                                          .showToast(
                                                                        msg:
                                                                            "${snapshot.data.items[selectedIndexes[index - 1]].item.itemName} untagged for delivery",
                                                                        toastLength:
                                                                            Toast.LENGTH_SHORT,
                                                                        gravity:
                                                                            ToastGravity.TOP,
                                                                        timeInSecForIosWeb:
                                                                            1,
                                                                        fontSize:
                                                                            16.0,
                                                                      );
                                                                    },
                                                                    child: Icon(
                                                                      LineAwesomeIcons
                                                                          .times_circle,
                                                                      color: Colors
                                                                          .red,
                                                                    ),
                                                                  ),
                                                                );
                                                    }),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                          ],
                        ),
                      );
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("${snapshot.error}"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
