import 'dart:math';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/itemsincustomerorder/itemsincustomerorder.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/qrpage/qrpage.dart';
import 'package:vendor/homepage/maps/maps.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/incomingGoods/incominggoodsforcustomer.dart';
import 'package:vendor/models/incomingGoods/incominggoodsordersforcustomer.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/restapi.dart';

class OneIncomingGoodsOrder extends StatefulWidget {
  OneIncomingGoodsOrder({
    required this.token,
    required this.userId,
    required this.incominggoods,
    required this.shop,
  });
  final String token;
  final String userId;
  final IncomingGoodsForCustomer incominggoods;
  final Shop shop;
  @override
  State createState() => _OneIncomingGoodsOrderInstance();
}

class _OneIncomingGoodsOrderInstance extends State<OneIncomingGoodsOrder> {
  bool _operationinprogress = false;
  PageController pageViewController = PageController(initialPage: 0);
  int currentPage = 0;

  @override
  void initState() {
    super.initState();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void cancelOrder(String customerId, String shopId) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi().cancelOrder(
        "/items/cancelOrder?customerId=$customerId&shopId=$shopId",
        widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = true;
      });
      Fluttertoast.showToast(
        msg: 'Code ${response.statusCode} : order was cancelled',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        fontSize: 16.0,
      );
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) =>
              CustomerMap(id: widget.userId, token: widget.token),
        ),
      );
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification error , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: ' ${response.body}',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    Widget qrcodepage(
        IncomingGoodsOrdersForCustomer pageDetails, String ordertype) {
      int price = 0;
      for (var item in pageDetails.orders!) {
        price += item.item!.ammount * item.order!.noOfOrderedItems!;
      }
      return QrPage(
        userName: '${pageDetails.handOverUser!.fullName}',
        data: pageDetails.qrCode!.qrCodeUid!,
        orderType: ordertype,
        price: '$price ksh',
        handOverUserId: '${pageDetails.handOverUser!.id}',
        token: widget.token,
      );
    }

    void showordersmodal(List<OneOrder> orders) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return ItemsInOneCustomerOrder(
              orders: orders,
              token: widget.token,
              userId: widget.userId,
            );
          });
    }

    void showConfirmOrderCancellationModal(String customerId, String shopId) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text('Cancel this order?'),
          actions: <Widget>[
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                cancelOrder(customerId, shopId);
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }

    return Stack(children: [
      Scaffold(
        appBar: AppBar(
          title: ListTile(
            title: Text(
              '${widget.incominggoods.shop!.shopName}',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          actions: [
            GestureDetector(
              onTap: () => showConfirmOrderCancellationModal(
                  widget.userId, widget.shop.id),
              child: Icon(
                LineIcons.timesCircle,
                color: Colors.red,
              ),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            (currentPage == 0)
                ? (widget.incominggoods.incomingGoodsOrdersForCustomer![0]
                            .qrCode!.toBeDelivered ==
                        true)
                    ? showordersmodal(widget.incominggoods
                        .incomingGoodsOrdersForCustomer![0].orders!)
                    : showordersmodal(widget.incominggoods
                        .incomingGoodsOrdersForCustomer![1].orders!)
                : (widget.incominggoods.incomingGoodsOrdersForCustomer![0]
                            .qrCode!.toBeDelivered ==
                        false)
                    ? showordersmodal(widget.incominggoods
                        .incomingGoodsOrdersForCustomer![0].orders!)
                    : showordersmodal(widget.incominggoods
                        .incomingGoodsOrdersForCustomer![1].orders!);
          },
          child: Icon(
            LineIcons.list,
            color: Color.fromARGB(255, 207, 118, 0),
          ),
          backgroundColor: Color.fromARGB(255, 27, 81, 211),
        ),
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: constraints.maxWidth,
                      minHeight: constraints.maxHeight),
                  child: (widget.incominggoods.incomingGoodsOrdersForCustomer!
                              .length >
                          1)
                      ? Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                              Container(
                                height: screenheight * 0.8,
                                width: double.infinity,
                                child: PageView(
                                  onPageChanged: (index) {
                                    setState(() {
                                      currentPage = index;
                                    });
                                  },
                                  controller: pageViewController,
                                  scrollDirection: Axis.horizontal,
                                  children: [
                                    (widget
                                                .incominggoods
                                                .incomingGoodsOrdersForCustomer![
                                                    0]
                                                .qrCode!
                                                .toBeDelivered ==
                                            false)
                                        ? qrcodepage(
                                            widget.incominggoods
                                                    .incomingGoodsOrdersForCustomer![
                                                0],
                                            "Pick up")
                                        : qrcodepage(
                                            widget.incominggoods
                                                .incomingGoodsOrdersForCustomer![1],
                                            "Delivery"),
                                    (widget
                                                .incominggoods
                                                .incomingGoodsOrdersForCustomer![
                                                    1]
                                                .qrCode!
                                                .toBeDelivered ==
                                            true)
                                        ? qrcodepage(
                                            widget.incominggoods
                                                    .incomingGoodsOrdersForCustomer![
                                                1],
                                            "Delivery")
                                        : qrcodepage(
                                            widget.incominggoods
                                                .incomingGoodsOrdersForCustomer![0],
                                            "Pick up")
                                  ],
                                ),
                              ),
                              Container(
                                height: screenheight * 0.1,
                                width: double.infinity,
                                child: Center(
                                  child: SmoothPageIndicator(
                                    axisDirection: Axis.horizontal,
                                    controller: pageViewController,
                                    count: widget.incominggoods
                                        .incomingGoodsOrdersForCustomer!.length,
                                    effect: WormEffect(
                                      dotWidth: 10.0,
                                      dotHeight: 10.0,
                                      radius: 10.0,
                                      activeDotColor:
                                          Color.fromARGB(255, 27, 81, 211),
                                    ),
                                  ),
                                ),
                              ),
                            ])
                      : Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            (widget
                                        .incominggoods
                                        .incomingGoodsOrdersForCustomer![0]
                                        .qrCode!
                                        .toBeDelivered ==
                                    false)
                                ? Container(
                                    height: screenheight * 0.8,
                                    width: double.infinity,
                                    child: qrcodepage(
                                      widget.incominggoods
                                          .incomingGoodsOrdersForCustomer![0],
                                      "Pick up",
                                    ),
                                  )
                                : Container(
                                    height: screenheight * 0.8,
                                    width: double.infinity,
                                    child: qrcodepage(
                                        widget.incominggoods
                                            .incomingGoodsOrdersForCustomer![0],
                                        "Delivery"),
                                  ),
                            Container(
                              height: screenheight * 0.1,
                              width: double.infinity,
                              child: null,
                            ),
                          ],
                        ),
                ),
              );
            },
          ),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
