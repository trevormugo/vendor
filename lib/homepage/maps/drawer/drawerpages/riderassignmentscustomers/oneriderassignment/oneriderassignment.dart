import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/itemsincustomerorder/itemsincustomerorder.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/qrpage/qrpage.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/qrpage/qrscanner.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/oneRiderAssignment/oneriderassignmentresponse.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/restapi.dart';

class OneRiderAssignment extends StatefulWidget {
  OneRiderAssignment({
    required this.token,
    required this.userId,
    required this.customer,
    required this.shop,
  });
  final String token;
  final String userId;
  final Account customer;
  final Shop shop;

  @override
  State createState() => _OneRiderAssignmentInstance();
}

class _OneRiderAssignmentInstance extends State<OneRiderAssignment> {
  late Future<OneRiderAssignmentResponse> _fetchOneRiderAssignment;
  late OneRiderAssignmentResponse _oneRiderAssignment;
  PageController pageViewController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    _fetchOneRiderAssignment = fetchOneRiderAssignment();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<OneRiderAssignmentResponse> fetchOneRiderAssignment() async {
    var oneAssignment = await RestApi().queryOneRiderAssignment(
        "/items/GetCustomerOrdersByCustomerIdForRider?customerid=${widget.customer.id}&riderid=${widget.userId}",
        widget.token);
    if (oneAssignment.statusCode == 200) {
      print("${oneAssignment.body}");
      _oneRiderAssignment =
          OneRiderAssignmentResponse.fromJson(json.decode(oneAssignment.body));
      return Future.value(_oneRiderAssignment);
    } else if (oneAssignment.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${oneAssignment.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${oneAssignment.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${oneAssignment.statusCode}",
        desc: '${oneAssignment.body}',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${oneAssignment.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void showordersmodal(List<OneOrder> orders) {
      showModalBottomSheet(
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return ItemsInOneCustomerOrder(
              orders: orders,
              token: widget.token,
              userId: widget.userId,
            );
          });
    }

    return FutureBuilder(
        future: fetchOneRiderAssignment(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return (snapshot.data.oneRiderAssignment.orders.length == 0)
                ? Center(
                    child: Text("Unexpected Error"),
                  )
                : Scaffold(
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(60),
                      child: AppBar(
                        title: ListTile(
                          title: Text(
                            "${widget.shop.shopName}'s Assignments",
                            softWrap: true,
                            textAlign: TextAlign.start,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                      ),
                    ),
                    extendBodyBehindAppBar: true,
                    floatingActionButton: FloatingActionButton(
                      onPressed: () => showordersmodal(
                          snapshot.data.oneRiderAssignment.orders),
                      child: Icon(
                        LineIcons.list,
                        color: Color.fromARGB(255, 207, 118, 0),
                      ),
                      backgroundColor: Color.fromARGB(255, 27, 81, 211),
                    ),
                    body: SafeArea(
                      top: true,
                      bottom: false,
                      left: false,
                      right: false,
                      child: LayoutBuilder(
                        builder: (context, constraints) {
                          return SingleChildScrollView(
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                  minWidth: constraints.maxWidth,
                                  minHeight: constraints.maxHeight),
                              child: Column(
                                children: [
                                  Container(
                                    height: screenheight * 0.8,
                                    width: double.infinity,
                                    child: PageView(
                                      controller: pageViewController,
                                      scrollDirection: Axis.horizontal,
                                      children: [
                                        QrPage(
                                          userName:
                                              'By ${widget.customer.fullName}',
                                          data: _oneRiderAssignment
                                              .oneRiderAssignment
                                              .assignment
                                              .assignmentCodeUid!,
                                          orderType: "Delivery Payment",
                                          price: '0 ksh',
                                          handOverUserId:
                                              '${widget.customer.id}',
                                          token: widget.token,
                                        ),
                                        QrScannerPage(
                                          token: widget.token,
                                          userId: widget.userId,
                                          orderType: "Deliver",
                                          userName: "To ${widget.customer.fullName}",
                                          connString:
                                              "/items/sendPurchaseItemRequest",
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: screenheight * 0.1,
                                    width: double.infinity,
                                    child: Center(
                                      child: SmoothPageIndicator(
                                        axisDirection: Axis.horizontal,
                                        controller: pageViewController,
                                        count: 2,
                                        effect: WormEffect(
                                          dotWidth: 10.0,
                                          dotHeight: 10.0,
                                          radius: 10.0,
                                          activeDotColor:
                                              Color.fromARGB(255, 27, 81, 211),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  );
          } else if (snapshot.hasError) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Icon(
                    LineIcons.server,
                    color: Colors.grey,
                    size: 70.0,
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("${snapshot.error}"),
                  ),
                ],
              ),
            );
          } else {
            return Center(
              child: SpinKitFadingCube(
                size: 60,
                color: Color.fromARGB(255, 27, 81, 211),
              ),
            );
          }
        });
  }
}
