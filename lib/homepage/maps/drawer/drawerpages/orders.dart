import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/vendororderscustomers/vendororderscustomers.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/vendorOrders/vendorordersresponse.dart';
import 'package:vendor/restapi.dart';

import '../../../../ip.dart';
import '../../../../main.dart';

class Orders extends StatefulWidget {
  Orders({
    required this.token,
    required this.userId,
  });
  final String token;
  final String userId;
  @override
  State createState() => _OrdersInstance();
}

class _OrdersInstance extends State<Orders> {
  late Future<VendorOrdersResponse> _fetchVendorOrders;
  late VendorOrdersResponse _vendororders;

  @override
  void initState() {
    super.initState();
    _fetchVendorOrders = fetchVendorOrders();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<VendorOrdersResponse> fetchVendorOrders() async {
    var vendorOrders = await RestApi().queryVendorOrders(
        "/items/getShopsWithOrdersForVendor/${widget.userId}", widget.token);
    if (vendorOrders.statusCode == 200) {
      print("${vendorOrders.body}");
      _vendororders =
          VendorOrdersResponse.fromJson(json.decode(vendorOrders.body));
      return Future.value(_vendororders);
    } else if (vendorOrders.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${vendorOrders.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${vendorOrders.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${vendorOrders.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${vendorOrders.statusCode}');
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigatetoshopprofile(String shopId) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopProfile(
            shopId: shopId,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void navigatetovendororderscustomerspage(
        List<Account> customers, Shop shop) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => VendorOrdersCustomers(
                  customers: customers,
                  shop: shop,
                  token: widget.token,
                  userId: widget.userId,
                )),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Orders',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: FutureBuilder(
          future: _fetchVendorOrders,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (snapshot.data.shopsWithVendorOrders.length == 0)
                  ? Center(
                      child: Text("No orders yet"),
                    )
                  : ListView.builder(
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.shopsWithVendorOrders.length,
                      itemBuilder: (BuildContext context, int index) =>
                          ListTile(
                        //leading: Icon(Icons.qr_code_sharp),
                        leading: GestureDetector(
                          onTap: () => navigatetoshopprofile(snapshot
                              .data.shopsWithVendorOrders[index].shop.id),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: FittedBox(
                              child: Container(
                                height: screenheight * 0.4,
                                width: screenwidth * 0.9,
                                color: Colors.transparent,
                                child: (snapshot.data.shopsWithVendorOrders[index].planName ==
                                        PlanNames.basic)
                                    ? Image.asset(
                                        PlanImages.basic,
                                      )
                                    : (snapshot.data.shopsWithVendorOrders[index].planName ==
                                            PlanNames.advanced)
                                        ? Image.asset(
                                            PlanImages.advanced,
                                          )
                                        : (snapshot.data.shopsWithVendorOrders[index]
                                                    .planName ==
                                                PlanNames.enterprise)
                                            ? Image.asset(
                                                PlanImages.enterprise,
                                              )
                                            : Image.asset(
                                                ErrorImage.noimage,
                                              ),
                              ),
                            ),
                          ),
                        ),
                        title: GestureDetector(
                          onTap: () => navigatetovendororderscustomerspage(
                              snapshot
                                  .data.shopsWithVendorOrders[index].customers,
                              snapshot.data.shopsWithVendorOrders[index].shop),
                          child: Text(
                              '${snapshot.data.shopsWithVendorOrders[index].shop.shopName}'),
                        ),
                        subtitle: Text(
                            '${snapshot.data.shopsWithVendorOrders[index].customers.length} customers'),
                        trailing: Icon(
                          LineAwesomeIcons.angle_right,
                        ),
                      ),
                    );
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
    );
  }
}
