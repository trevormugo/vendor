import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/orders.dart';
import '../drawer/drawerpages/riderassignments.dart';

import 'dart:math';

import '../../../main.dart';
import '../../../restapi.dart';
import './customtile.dart';
import './drawerpages/notifications.dart';
import './drawerpages/shoppingcart.dart';
import './drawerpages/inventory.dart';
import './drawerpages/riders.dart';
import './drawerpages/supervisors.dart';
import './drawerpages/subscriptions.dart';
import './drawerpages/incominggoods.dart';
import '../../../models/userRoles/userRoles.dart';

class DrawerWidget extends StatefulWidget {
  DrawerWidget({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _DrawerWidgetInstance();
}

class _DrawerWidgetInstance extends State<DrawerWidget> {
  bool enabled = true;
  late UserRoles _userRoles;
  @override
  void initState() {
    super.initState();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<UserRoles> _fetchUserRole() async {
    var appDefaults = await RestApi().getUserRoles(
        "/accountspool/getUserRoles/${widget.userId}", widget.token);
    if (appDefaults.statusCode == 200) {
      _userRoles = UserRoles.fromJson(json.decode(appDefaults.body));
      return Future.value(_userRoles);
    } else if (appDefaults.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${appDefaults.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${appDefaults.statusCode}');
    } else {
      return Future.error('Error  ${appDefaults.statusCode}');
    }
  }

  bool shouldShow(String forRoleOne, String forRoleTwo) {
    if (_userRoles.roles.contains(forRoleOne) ||
        _userRoles.roles.contains(forRoleTwo)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Drawer(
      child: SafeArea(
        top: true,
        child: Container(
          color: Color.fromARGB(255, 27, 81, 211),
          height: screenheight,
          width: double.infinity,
          child: FutureBuilder(
              future: _fetchUserRole(),
              builder: (context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      Container(
                        color: Color.fromARGB(255, 27, 81, 211),
                        height: screenheight * 0.4,
                        width: double.infinity,
                        child: Center(
                          child: Image.asset(
                            "assets/images/InverseLogoCropped.png",
                            //scale: 4,
                          ),
                        ),
                      ),
                      CustomListItem(
                        showbadge: true,
                        icon: Icon(
                          LineIcons.bell,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Notifications',
                        count: snapshot.data.unreadNotifications,
                        enabled: shouldShow("Customer", "Vendor"),
                        page: Notifications(
                          userId: widget.userId,
                          token: widget.token,
                        ),
                      ),
                      CustomListItem(
                        showbadge: true,
                        icon: Icon(
                          LineIcons.shoppingCart,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Shopping cart',
                        count: snapshot.data.shoppingCart,
                        enabled: shouldShow("Customer", "Vendor"),
                        page: ShoppingCart(
                          userId: widget.userId,
                          token: widget.token,
                        ),
                      ),
                      CustomListItem(
                        showbadge: true,
                        icon: Icon(
                          LineIcons.qrcode,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Incoming Goods',
                        count: snapshot.data.incomingGoods,
                        enabled: shouldShow("Customer", "Customer"),
                        page: IncomingGoods(
                          userId: widget.userId,
                          token: widget.token,
                        ),
                      ),
                      CustomListItem(
                        showbadge: true,
                        icon: Icon(
                          LineIcons.briefcase,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Assignments',
                        count: snapshot.data.unreadAssignments,
                        enabled: shouldShow("Customer", "Customer"),
                        page: RiderAssignments(
                          userId: widget.userId,
                          token: widget.token,
                        ),
                      ),
                      CustomListItem(
                        showbadge: true,
                        icon: Icon(
                          Icons.qr_code_scanner,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Orders',
                        count: snapshot.data.unreadOrders,
                        enabled: shouldShow("Vendor", "Customer"),
                        page: Orders(
                          token: widget.token,
                          userId: widget.userId,
                        ),
                      ),
                      CustomListItem(
                        showbadge: true,
                        icon: Icon(
                          LineAwesomeIcons.warehouse,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Inventory',
                        count: 0,
                        enabled: shouldShow("Vendor", "Customer"),
                        page: Inventory(
                          token: widget.token,
                          userId: widget.userId,
                          roles:snapshot.data.roles,
                        ),
                      ),
                      CustomListItem(
                        showbadge: false,
                        icon: Icon(
                          LineIcons.users,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Riders',
                        count: snapshot.data.unreadRiders,
                        enabled: shouldShow("Vendor", "Customer"),
                        page: Riders(
                          userId: widget.userId,
                          token: widget.token,
                        ),
                      ),
                      CustomListItem(
                        showbadge: false,
                        icon: Icon(
                          Icons.supervisor_account_outlined,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Supervisors',
                        count: snapshot.data.unreadSupervisors,
                        enabled: shouldShow("Vendor", "Customer"),
                        page: Supervisors(
                          userId: widget.userId,
                          token: widget.token,
                        ),
                      ),
                      CustomListItem(
                        showbadge: false,
                        icon: Icon(
                          LineIcons.moneyBill,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Subscriptions',
                        count: 0,
                        enabled: true,
                        page: Subscriptions(),
                      ),
                      CustomListItem(
                        showbadge: false,
                        icon: Icon(
                          LineIcons.removeUser,
                          color: Colors.white,
                          size: 30.0,
                        ),
                        texttype: 'Log Out',
                        count: 0,
                        enabled: true,
                        page: LoginPage(),
                      ),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Icon(
                          LineIcons.server,
                          color: Colors.grey,
                          size: 70.0,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text("${snapshot.error}"),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: SpinKitFadingCube(
                      size: 60,
                      color: Color.fromARGB(255, 207, 118, 0),
                    ),
                  );
                }
              }),
        ),
      ),
    );
  }
}
