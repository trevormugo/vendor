import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:badges/badges.dart';

class CustomListItem extends StatefulWidget {
  const CustomListItem({
    required this.icon,
    required this.texttype,
    this.count,
    required this.enabled,
    required this.page,
    required this.showbadge,
  });
  final Icon icon;
  final String texttype;
  final int? count;
  final bool enabled;
  final Widget page;
  final bool showbadge;
  State<StatefulWidget> createState() {
    return Custom();
  }
}

class Custom extends State<CustomListItem> {
  void _nav() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    void navigate() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => widget.page),
      );
    }

    void logout() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove('userid');
      prefs.remove('useremail');
      prefs.remove('token');
      prefs.remove('userphonenumber');
      prefs.remove('username');
      prefs.remove('deviceToken');
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => widget.page),
      );
    }

    return (widget.enabled == true)
        ? ListTile(
            onTap: (widget.texttype == 'Log Out') ? logout : navigate,
            enabled: widget.enabled,
            leading: Badge(
              badgeColor: Color.fromARGB(255, 207, 118, 0),
              badgeContent: null,
              showBadge: !(widget.count == 0),
              child: widget.icon,
            ),
            title: Text(
              '${widget.texttype}',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            trailing: (widget.count == 0)
                ? null
                : Text(
                    '${widget.count}',
                    style: TextStyle(
                      color: Color.fromARGB(255, 207, 118, 0),
                    ),
                  ),
          )
        : Container();
  }
}
