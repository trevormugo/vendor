import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math' show cos, sqrt, asin;

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:fluttertoast/fluttertoast.dart';
//import 'package:flutter_map/flutter_map.dart';
//import 'package:latlong2/latlong.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:badges/badges.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/models/account.dart';
import 'package:vendor/models/googleverificationrequest.dart';
import 'package:vendor/providers/detailupdate_provider.dart';
import 'package:provider/provider.dart';

import '../../models/homeShopsQuery/homeshopquery.dart';
import '../../models/oneShopQuery/oneshopquery.dart';

import '../../../main.dart';
import '../../models/appDefaults/appdefaultsresponse.dart';
import '../../models/appDefaults/visibilityradius.dart';

import '../../models/shops.dart';
import '../../restapi.dart';
import './drawer/drawer.dart';
import './bottomwidget/bottomwidget.dart';
import '../profiles/profilepage.dart';
import '../../models/defaults.dart';

class CustomerMap extends StatefulWidget {
  CustomerMap({required this.id, required this.token});
  final id;
  final token;
  @override
  State createState() => _CustomerMapState();
}

class _CustomerMapState extends State<CustomerMap> {
  PageController pageViewController = PageController(initialPage: 0);
  Completer<GoogleMapController> _controller = Completer();

  bool showsearchbar = false;
  TextEditingController searchbar = TextEditingController();

  late FirebaseMessaging messaging;
  late HomeShopsQuery shopsQuery;
  late AppDefaultsResponse appDefaultsResponse;
  late VisibilityRadius visibilityRadius;
  Set<Marker>? shopmarkers = {};
  OneShopQuery? _oneShop;
  String? plan;

  late Future initialWork;
  late Future<Set<Marker>> fetchAppDefaults;

  late Position currentPosition;

  late BitmapDescriptor basicPinLocationIcon;
  late BitmapDescriptor advancedPinLocationIcon;
  late BitmapDescriptor enterprisePinLocationIcon;

  late StreamSubscription<ConnectionResult> _connectionSubscription;
  List<IAPItem>? _products = [];
  List<PurchasedItem> _pastPurchases = [];

  @override
  void initState() {
    super.initState();

    setCustomMapPin();
    initialWork = _initalwork();
    fetchAppDefaults = _fetchAppDefaults("ALL");
    initConnection();
  }

  void initConnection() async {
    await FlutterInappPurchase.instance.initConnection;
    _connectionSubscription =
        FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });
    _getItems();
    _getPastPurchases();
  }

  Future<void> _getItems() async {
    _products = await FlutterInappPurchase.instance.getSubscriptions([]);
  }

  void _getPastPurchases() async {
    if (Platform.isIOS) {
      return;
    }
    List<PurchasedItem>? purchasedItems =
        await FlutterInappPurchase.instance.getAvailablePurchases();
    if (purchasedItems == null) {
      return;
    }
    for (var purchasedItem in purchasedItems) {
      bool isValid = false;
      if (Platform.isAndroid) {
        Map map = json.decode(purchasedItem.transactionReceipt!);
        if (!map['acknowledged']) {
          isValid = await _verifyPurchase(purchasedItem);
          if (isValid) {
            FlutterInappPurchase.instance.finishTransaction(purchasedItem);
            /*PREMIUM USER*/
            _becomeAVendor(_products!
                .firstWhere(
                    (element) => element.productId == purchasedItem.productId)
                .title!);
          }
        } else {
          /*PREMIUM USER*/
          _becomeAVendor(_products!
              .firstWhere(
                  (element) => element.productId == purchasedItem.productId)
              .title!);
        }
      }
    }
    _pastPurchases.addAll(purchasedItems);
  }

  Future<bool> _verifyPurchase(PurchasedItem purchasedItem) async {
    GoogleVerificationRequest googleVerificationRequest =
        GoogleVerificationRequest(
      productId: purchasedItem.productId!,
      purchaseToken: purchasedItem.purchaseToken!,
    );
    var response = await RestApi().verifyPurchaseToken(
        "/accountspool/verifyPurchaseToken", googleVerificationRequest);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  void _becomeAVendor(String planName) async {
    RegisterVendorRequest registerVendorRequest = RegisterVendorRequest(
      id: widget.id!,
      planName: planName,
    );
    var response = await RestApi()
        .registervendor("/accountspool/registervendor", registerVendorRequest);
    if (response.statusCode == 200) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'You are now a vendor',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  void setCustomMapPin() async {
    basicPinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.basic);
    advancedPinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.advanced);
    enterprisePinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.enterprise);
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<void> _initalwork() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    messaging = FirebaseMessaging.instance;
    String? _devicetoken = await messaging.getToken();
    prefs.setString('deviceToken', _devicetoken!);
    messaging.onTokenRefresh.listen((newToken) {
      if (newToken != prefs.getString('deviceToken')) {
        prefs.setString('deviceToken', newToken);
      }
    });
    SignalRSocket.initconnection(widget.id, prefs.getString('deviceToken')!);
    await _checkToken(prefs.getString('deviceToken'), widget.id);
    return Future.value('Success');
  }

  Future<void> _checkToken(String? token, String userId) async {
    var checkTokenResponse = await RestApi().checkDeviceToken(
        "/shops/checkDeviceToken?userId=$userId&deviceToken=$token",
        widget.token);
    if (checkTokenResponse.statusCode == 200) {
      print("${checkTokenResponse.body}");
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Token Verified',
        desc: 'Verified Device Token',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.value();
    } else if (checkTokenResponse.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${checkTokenResponse.statusCode}",
        desc: '${widget.token} Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${checkTokenResponse.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${checkTokenResponse.statusCode}",
        desc: 'Token was not verified',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${checkTokenResponse.statusCode}');
    }
  }

  Future<Set<Marker>> _fetchAppDefaults(String queryString) async {
    var appDefaults = await RestApi().fetchappdefaults(
        "/shops/queryAppDefaults?userId=${widget.id}", widget.token);
    if (appDefaults.statusCode == 200) {
      print("${appDefaults.body}");
      appDefaultsResponse =
          AppDefaultsResponse.fromJson(json.decode(appDefaults.body));
      print("${appDefaultsResponse.toString()}");
      Set<Marker> markers =
          await _fetchmarkers(appDefaultsResponse.visibility, queryString);
      return Future.value(markers);
    } else if (appDefaults.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${appDefaults.statusCode}",
        desc: '${appDefaults.body}',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${appDefaults.statusCode}');
    } else {
      visibilityRadius = VisibilityRadius(
        basic: 2,
        advanced: 8,
      );
      Set<Marker> markers = await _fetchmarkers(visibilityRadius, queryString);
      return Future.value(markers);
    }
  }

  double calculateDistanceInKm(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc: 'Location services are disabled to continue',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Please Allow Location Permission',
          desc: 'Location permissions are required to continue',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc:
            'Location permissions are permanently denied, we cannot request permissions',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  Future<Set<Marker>> _fetchmarkers(
      VisibilityRadius? visibleRadius, String queryString) async {
    currentPosition = await _determinePosition();
    var markers = await RestApi()
        .fetchshops("/shops/queryShops?queryString=$queryString", widget.token);
    print("${markers.body}");
    if (markers.statusCode == 200) {
      shopsQuery = HomeShopsQuery.fromJson(json.decode(markers.body));
      if (shopsQuery.items!.length != 0) {
        shopsQuery.items?.forEach((element) {
          print("$element");
          if (element.shops!.length != 0) {
            if (element.planName == "Basic") {
              element.shops!.forEach((e) {
                double distance = calculateDistanceInKm(
                    currentPosition.latitude,
                    currentPosition.longitude,
                    e.latitude,
                    e.longitude);
                if (distance <= visibleRadius!.basic) {
                  shopmarkers!.add(
                    Marker(
                        markerId: MarkerId("${e.id}"),
                        position: LatLng(e.latitude, e.longitude),
                        icon: basicPinLocationIcon,
                        onTap: () => _fetchShop(e.id, element.planName)),
                  );
                }
              });
            } else if (element.planName == "Advanced") {
              element.shops!.forEach((e) {
                double distance = calculateDistanceInKm(
                    currentPosition.latitude,
                    currentPosition.longitude,
                    e.latitude,
                    e.longitude);
                //if (distance <= visibleRadius!.advanced) {
                  shopmarkers!.add(
                    Marker(
                        markerId: MarkerId("${e.id}"),
                        position: LatLng(e.latitude, e.longitude),
                        icon: advancedPinLocationIcon,
                        onTap: () => _fetchShop(e.id, element.planName)),
                  );
                //}
              });
            } else if (element.planName == "Enterprise") {
              element.shops!.forEach((e) {
                shopmarkers!.add(
                  Marker(
                      markerId: MarkerId("${e.id}"),
                      position: LatLng(e.latitude, e.longitude),
                      icon: enterprisePinLocationIcon,
                      onTap: () => _fetchShop(e.id, element.planName)),
                );
              });
            }
          }
        });
      }
      return shopmarkers!;
    } else if (markers.statusCode == 401 || markers.statusCode == 403) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Markers Error ${markers.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error ${markers.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Info',
        desc: 'Unable to fetch Markers',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();

      return Future.error("Error ${markers.statusCode}");
    }
  }

  void _fetchShop(String shopId, String planTwo) async {
    setState(() {
      plan = planTwo;
    });
    var oneShop = await RestApi().fetchoneshop(
        "/shops/queryShopsCategoriesAndItems?shopId=$shopId&customerId=${widget.id}",
        widget.token);
    if (oneShop.statusCode == 200) {
      _oneShop = OneShopQuery.fromJson(json.decode(oneShop.body));
      context.read<FavoriteProvider>().initial(_oneShop!.favorited);
      Fluttertoast.showToast(
          msg: "${_oneShop!.shop.shopName}",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 1,
          fontSize: 16.0);
    } else if (oneShop.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${oneShop.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Query Error',
        desc: 'Unable to fetch Shop',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    }
  }

  void togglesearchbar() {
    setState(() {
      showsearchbar = !showsearchbar;
    });
  }

  @override
  void dispose() {
    _connectionSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenwidth = MediaQuery.of(context).size.width;
    double screenheight = MediaQuery.of(context).size.height;

    void navigatetoprofile() {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CustomerProfilePage(
                  userId: widget.id,
                  token: widget.token,
                )),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          leading: (showsearchbar == true)
              ? GestureDetector(
                  onTap: togglesearchbar,
                  child: Icon(LineAwesomeIcons.arrow_left),
                )
              : null,
          title: (showsearchbar == true)
              ? TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Search shops , Categories and Items",
                  ),
                  keyboardType: TextInputType.emailAddress,
                  controller: searchbar,
                )
              : Text(
                  "InfiMart",
                  style: TextStyle(fontFamily: 'NanumGothic'),
                ),
          actions: [
            GestureDetector(
              onTap: () {
                if (showsearchbar == false) {
                  togglesearchbar();
                } else {
                  shopmarkers!.clear();
                  (searchbar.text == "")
                      ? setState(() {
                          fetchAppDefaults = _fetchAppDefaults("ALL");
                        })
                      : setState(() {
                          fetchAppDefaults = _fetchAppDefaults(searchbar.text);
                        });
                }
              },
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Icon(LineAwesomeIcons.search_location),
              ),
            ),
            (showsearchbar == false)
                ? Badge(
                    badgeColor: Color.fromARGB(255, 207, 118, 0),
                    badgeContent: null,
                    showBadge: true,
                    child: GestureDetector(
                      onTap: navigatetoprofile,
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: CircleAvatar(
                          foregroundImage: NetworkImage(
                            Adress.myip +
                                "/accountspool/fetchAccountThumbnail?id=${widget.id}",
                          ),
                        ),
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
      drawer: DrawerWidget(
        userId: widget.id,
        token: widget.token,
      ),
      drawerScrimColor: Colors.black38,
      body: Stack(
        children: [
          FutureBuilder(
              future: Future.wait([
                initialWork,
                fetchAppDefaults,
              ]),
              builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
                if (snapshot.hasData) {
                  return GoogleMap(
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(
                        currentPosition.latitude,
                        currentPosition.longitude,
                      ),
                      zoom: 14.4746,
                    ),
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                    markers: snapshot.data![1],
                  );
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Icon(
                          LineIcons.server,
                          color: Colors.grey,
                          size: 70.0,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text("$snapshot"),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: SpinKitFadingCube(
                      size: 60,
                      color: Color.fromARGB(255, 27, 81, 211),
                    ),
                  );
                }
              }),
          Container(
            child: DraggableScrollableSheet(
              maxChildSize: 1.0,
              minChildSize: 0.1,
              initialChildSize: 0.1,
              expand: true,
              builder:
                  (BuildContext context, ScrollController scrollController) {
                return (_oneShop == null)
                    ? Container()
                    : ShopProfileWidget(
                        scrollController: scrollController,
                        isdraggable: true,
                        background: Colors.white.withOpacity(0.8),
                        oneshop: _oneShop!,
                        plan: plan!,
                        token: widget.token,
                        userId: widget.id,
                      );
              },
            ),
          ),
          Container(
            height: screenheight * 0.1,
          ),
        ],
      ),
    );
  }
}
