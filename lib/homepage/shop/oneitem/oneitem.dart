import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/models/customercartrequest.dart';
import 'package:vendor/models/defaults.dart';
import 'package:flutter_number_picker/flutter_number_picker.dart';

import '../../../models/oneItemQuery/oneitemquery.dart';
import '../../../ip.dart';
import '../../../main.dart';
import '../../../restapi.dart';
import './oneItemTabs/itemDetails.dart';
import 'dart:math';

import './oneItemTabs/itemReturnPolicy.dart';
import './oneItemTabs/itemReviews.dart';

class OneItem extends StatefulWidget {
  OneItem({
    required this.icon,
    required this.itemId,
    required this.token,
    required this.userId,
  });
  final Icon icon;
  final String itemId;
  final String token;
  final String userId;

  @override
  createState() => _OneItemState();
}

class _OneItemState extends State<OneItem> with TickerProviderStateMixin {
  TabController? _tabcontroller;
  ScrollController? _scrollController;
  PageController pageViewController = PageController(initialPage: 0);
  late Future<OneItemQuery> _fetchOneItemDetails;
  OneItemQuery? oneItemQuery;
  int noOfItems = 1;

  @override
  void initState() {
    super.initState();
    _tabcontroller = TabController(length: 3, vsync: this);
    _scrollController = ScrollController();
    _fetchOneItemDetails = fetchOneItemDetails(widget.itemId);
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<OneItemQuery> fetchOneItemDetails(String? id) async {
    var oneItem = await RestApi().queryOneItem(
        "/items/queryOneItem?userId=${widget.userId}&itemId=${widget.itemId}",
        widget.token);
    if (oneItem.statusCode == 200) {
      print("${oneItem.body}");
      oneItemQuery = OneItemQuery.fromJson(json.decode(oneItem.body));
      return Future.value(oneItemQuery);
    } else if (oneItem.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${oneItem.statusCode}",
        desc: 'Verification error , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error ${oneItem.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Unexpected Error ${oneItem.statusCode}",
        desc: '${oneItem.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      return Future.error('Error ${oneItem.statusCode}');
    }
  }

  void addItemToCart(CustomerCartRequest customerCartRequest) async {
    var cartItem = await RestApi()
        .addItemToCart("/items/cartItems", widget.token, customerCartRequest);
    if (cartItem.statusCode == 201) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Success",
        desc: 'Item was Added to Cart',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      return Future.value(oneItemQuery);
    } else if (cartItem.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${cartItem.statusCode}",
        desc: 'Verification error , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error ${cartItem.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Unexpected Error ${cartItem.statusCode}",
        desc: '${cartItem.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      return Future.error('Error ${cartItem.statusCode}');
    }
  }

  @override
  void dispose() {
    _tabcontroller!.dispose();
    _scrollController!.dispose();
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void showQuantityModal(int maxItems, String itemId, String shopId) async {
      CustomerCartRequest customerCartRequest = CustomerCartRequest(
        customerId: widget.userId,
        itemId: itemId,
        noOfItems: noOfItems,
        shopId: shopId,
      );
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return StatefulBuilder(
                builder: (BuildContext context, StateSetter setModalState) {
              return Container(
                height: screenheight * 0.3,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CustomNumberPicker(
                      initialValue: 1,
                      maxValue: maxItems,
                      minValue: 1,
                      step: 1,
                      onValue: (value) {
                        setState(() => noOfItems = int.parse(value.toString()));
                      },
                    ),
                    TextButton(
                      onPressed: () => addItemToCart(customerCartRequest),
                      child: Text("Add to cart"),
                    ),
                  ],
                ),
              );
            });
          });
    }

    return Scaffold(
      endDrawer: Drawer(
        child: SafeArea(
          top: true,
          child: Container(
            color: Colors.white,
            height: screenheight,
            width: double.infinity,
            child: Container(
              height: screenheight,
              child: (oneItemQuery == null)
                  ? Center(
                      child: Text("Failed to load page"),
                    )
                  : ListView(
                      children: [
                        ListTile(
                          onTap: null,
                          leading: Icon(
                            Icons.star_border,
                          ),
                          title: Text("Item Reviews"),
                        ),
                        ListTile(
                          onTap: null,
                          leading: Icon(
                            Icons.edit,
                          ),
                          title: Text("Edit Item"),
                        ),
                        ListTile(
                          onTap: null,
                          leading: Icon(
                            LineAwesomeIcons.info_circle,
                          ),
                          title: Text("Feature a discount"),
                        ),
                        ListTile(
                          onTap: null,
                          leading: Icon(
                            LineIcons.trash,
                          ),
                          title: Text("Delete Item"),
                        ),
                      ],
                    ),
            ),
          ),
        ),
      ),
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: FutureBuilder(
            future: _fetchOneItemDetails,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return Scaffold(
                  floatingActionButton:
                      /*(snapshot.data.owner.id == widget.userId)
                          ? Container()
                          : */
                      FloatingActionButton(
                    onPressed: () => showQuantityModal(
                        snapshot.data.item.noOfItems,
                        snapshot.data.item.id,
                        snapshot.data.item.shopId),
                    child: Icon(
                      LineAwesomeIcons.shopping_cart_arrow_down,
                      color: Color.fromARGB(255, 207, 118, 0),
                    ),
                    backgroundColor: Color.fromARGB(255, 27, 81, 211),
                  ),
                  body: DefaultTabController(
                    length: 3,
                    initialIndex: 0,
                    child: NestedScrollView(
                      controller: _scrollController,
                      headerSliverBuilder: (context, value) {
                        return [
                          SliverAppBar(
                            floating: true,
                            snap: false,
                            pinned: true,
                            forceElevated: value,
                            expandedHeight: screenheight * 0.7,
                            flexibleSpace: FlexibleSpaceBar(
                              collapseMode: CollapseMode.parallax,
                              centerTitle: true,
                              background: Column(
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    height: screenheight * 0.4,
                                    width: screenwidth * 0.8,
                                    child: (snapshot.data.itemImages.length ==
                                            0)
                                        ? ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                            child: FittedBox(
                                              fit: BoxFit.cover,
                                              child: Container(
                                                height: screenheight * 0.9,
                                                width: screenwidth * 0.9,
                                                color: Colors.transparent,
                                                child: GestureDetector(
                                                  onTap: null,
                                                  child: Center(
                                                    child: Image.asset(
                                                      ErrorImage.noimage,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        : PageView(
                                            controller: pageViewController,
                                            allowImplicitScrolling: true,
                                            children: List.generate(
                                              snapshot.data.itemImages.length,
                                              (index) => Padding(
                                                padding: EdgeInsets.all(5),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                  child: FittedBox(
                                                    fit: BoxFit.cover,
                                                    child: Container(
                                                      height:
                                                          screenheight * 0.9,
                                                      width: screenwidth * 0.9,
                                                      color: Colors.transparent,
                                                      child: GestureDetector(
                                                        onTap: null,
                                                        child: Image(
                                                          fit: BoxFit.cover,
                                                          image: NetworkImage(
                                                              Adress.myip +
                                                                  "/items/fetchitemuploadthumbnail?id=${snapshot.data.itemImages[index].id}"),
                                                          errorBuilder:
                                                              (BuildContext
                                                                      context,
                                                                  Object
                                                                      exception,
                                                                  StackTrace?
                                                                      stackTrace) {
                                                            return Image.asset(
                                                              ErrorImage
                                                                  .noimage,
                                                            );
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                  ),
                                  (snapshot.data.itemImages.length == 0 ||
                                          snapshot.data.itemImages.length == 1)
                                      ? Container()
                                      : Container(
                                          height: screenheight * 0.1,
                                          width: double.infinity,
                                          child: Center(
                                            child: SmoothPageIndicator(
                                              controller: pageViewController,
                                              count: snapshot
                                                  .data.itemImages.length,
                                              effect: WormEffect(
                                                dotWidth: 10.0,
                                                dotHeight: 10.0,
                                                radius: 10.0,
                                                activeDotColor: Color.fromARGB(
                                                    255, 27, 81, 211),
                                              ),
                                            ),
                                          ),
                                        ),
                                ],
                              ),
                            ),
                            title: ListTile(
                              title: Text(
                                "${snapshot.data.item.itemName}",
                                softWrap: true,
                                textAlign: TextAlign.start,
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                              subtitle: Text(
                                "${snapshot.data.item.ammount}",
                              ),
                            ),
                            bottom: TabBar(
                              controller: _tabcontroller,
                              tabs: <Widget>[
                                Tab(
                                  icon: Icon(
                                    LineAwesomeIcons.info_circle,
                                  ),
                                ),
                                Tab(
                                  icon: Icon(
                                    LineAwesomeIcons.file_contract,
                                  ),
                                ),
                                Tab(
                                  icon: Icon(
                                    Icons.rate_review_outlined,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ];
                      },
                      body: TabBarView(
                        controller: _tabcontroller,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                              top: 40,
                            ),
                            child: ItemInformation(
                              oneItemQuery: snapshot.data,
                              token: widget.token,
                              userId: widget.userId,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 40,
                            ),
                            child: ItemReturnPolicy(
                              returnPolicy: snapshot.data.returnPolicy,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 40,
                            ),
                            child: ItemReviewsTab(
                              oneItemQuery: snapshot.data!,
                              userId: widget.userId,
                              token: widget.token,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("$snapshot"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
    );
  }
}
