import 'package:flutter/material.dart';

class DetailInfo extends StatefulWidget {
  DetailInfo({
    required this.icon,
    required this.detail,
  });
  final IconData icon;
  final String detail;

  @override
  createState() => _DetailInfoState();
}

class _DetailInfoState extends State<DetailInfo> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 10.0,
        right: 10.0,
        bottom: 5.0,
        top: 5.0,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white60,
          border: Border.all(
            color: Colors.black12,
            width: 1.0,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: ListTile(
          leading: Icon(widget.icon),
          title: Text(
            '${widget.detail}',
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}
