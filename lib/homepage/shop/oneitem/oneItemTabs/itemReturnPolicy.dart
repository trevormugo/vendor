import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

import '../../../../models/oneItemQuery/returnpolicy.dart';

import '../oneItemDetail/detailinfo.dart';

class ItemReturnPolicy extends StatefulWidget {
  ItemReturnPolicy({
    required this.returnPolicy,
  });

  final List<ReturnPolicy> returnPolicy;
  @override
  State createState() => _ItemReturnPolicyInstance();
}

class _ItemReturnPolicyInstance extends State<ItemReturnPolicy> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.returnPolicy.length,
      itemBuilder: (contex, index) => DetailInfo(
        icon: LineAwesomeIcons.file_contract,
        detail: '${widget.returnPolicy[index].policyText}',
      ),
    );
  }
}
