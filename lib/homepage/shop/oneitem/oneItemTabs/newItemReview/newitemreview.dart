import 'dart:math';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/shop/oneitem/oneitem.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/itemReviewRequest.dart';
import 'package:vendor/models/oneItemQuery/itemreviewers.dart';
import 'package:vendor/models/oneShopQuery/items.dart';
import 'package:vendor/models/oneShopQuery/oneshopquery.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/reviewshoprequest.dart';
import 'package:vendor/restapi.dart';

class NewItemReview extends StatefulWidget {
  NewItemReview({
    this.previousReview,
    required this.token,
    required this.userId,
    required this.item,
    required this.itemReviews,
  });
  final ItemReviewers? previousReview;
  final List<ItemReviewers> itemReviews;
  final Items item;
  final String token;
  final String userId;
  @override
  State createState() => _NewItemReviewState();
}

class _NewItemReviewState extends State<NewItemReview> {
  double ratingnum = 3.0;
  PageController pageViewController = PageController(initialPage: 0);
  TextEditingController _reviewcontroller = TextEditingController();
  bool _operationinprogress = false;

  @override
  void initState() {
    super.initState();
    if (widget.previousReview != null) {
      _reviewcontroller.text = widget.previousReview!.reviews.review;
    }
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void reviewItem(Items item) async {
    setState(() {
      _operationinprogress = true;
    });
    ReviewItemRequest reviewItemRequest = ReviewItemRequest(
      itemId: item.id,
      userId: widget.userId,
      rating: ratingnum,
      review: _reviewcontroller.text,
    );
    var response = await RestApi()
        .reviewItem("/items/reviewItem", widget.token, reviewItemRequest);
    if (response.statusCode == 201) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'reviewed ${item.itemName}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  void deleteShopReview(String id) async {
    setState(() {
      _operationinprogress = true;
    });
    var response = await RestApi()
        .deleteItemReview("/items/deleteReview/$id", widget.token);
    if (response.statusCode == 204) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Review was deleted',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  @override
  void dispose() {
    _reviewcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Stack(children: [
      Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            title: ListTile(
              title: Text(
                'Rate and review ${widget.item.itemName}',
                softWrap: true,
                textAlign: TextAlign.start,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ),
        ),
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: constraints.maxWidth,
                      minHeight: constraints.maxHeight),
                  child: (widget.previousReview == null)
                      ? Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: screenheight * 0.7,
                              width: double.infinity,
                              child: Center(
                                child: PageView(
                                  controller: pageViewController,
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Rate Item',
                                          softWrap: true,
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                            fontSize: 60,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        Text(
                                          '$ratingnum',
                                          softWrap: true,
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                            fontSize: 60,
                                            fontWeight: FontWeight.w800,
                                          ),
                                        ),
                                        RatingBar.builder(
                                          initialRating: 3,
                                          minRating: 1,
                                          direction: Axis.horizontal,
                                          allowHalfRating: true,
                                          itemCount: 5,
                                          itemPadding: EdgeInsets.symmetric(
                                              horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Color.fromARGB(
                                                255, 27, 81, 211),
                                          ),
                                          onRatingUpdate: (rating) {
                                            setState(() {
                                              ratingnum = rating;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                    Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            height: (screenheight * 0.7) * 0.9,
                                            width: double.infinity,
                                            child: Center(
                                              child: Padding(
                                                padding: EdgeInsets.only(
                                                  left: 10.0,
                                                  right: 10.0,
                                                ),
                                                child: TextField(
                                                  obscureText: false,
                                                  maxLines: 8,
                                                  controller: _reviewcontroller,
                                                  decoration: InputDecoration(
                                                    labelStyle: TextStyle(
                                                      color: Color.fromARGB(
                                                          255, 27, 81, 211),
                                                      fontSize: 13,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                        color: Color.fromARGB(
                                                            255, 27, 81, 211),
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(12),
                                                      ),
                                                    ),
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(12),
                                                      ),
                                                    ),
                                                    labelText: "Review Store",
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            height: (screenheight * 0.7) * 0.1,
                                            width: double.infinity,
                                            child: Center(
                                              child: TextButton(
                                                onPressed: () => (_reviewcontroller
                                                            .text ==
                                                        "")
                                                    ? Fluttertoast.showToast(
                                                        msg:
                                                            "Input required for review",
                                                        toastLength:
                                                            Toast.LENGTH_SHORT,
                                                        gravity:
                                                            ToastGravity.TOP,
                                                        timeInSecForIosWeb: 1,
                                                        fontSize: 16.0)
                                                    : reviewItem(widget.item),
                                                child: Text(
                                                  'Submit',
                                                  softWrap: true,
                                                  textAlign: TextAlign.start,
                                                  overflow: TextOverflow.fade,
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w800,
                                                    color: Color.fromARGB(
                                                        255, 27, 81, 211),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ]),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: screenheight * 0.1,
                              width: double.infinity,
                              child: Center(
                                child: SmoothPageIndicator(
                                  controller: pageViewController,
                                  count: 2,
                                  effect: WormEffect(
                                    dotWidth: 10.0,
                                    dotHeight: 10.0,
                                    radius: 10.0,
                                    activeDotColor:
                                        Color.fromARGB(255, 27, 81, 211),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: screenheight * 0.1,
                              width: double.infinity,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        WidgetSpan(
                                          child: Icon(
                                            Icons.star_border,
                                            size: 17,
                                          ),
                                        ),
                                        TextSpan(
                                          text: '${widget.item.itemRating}',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Color.fromARGB(
                                                255, 27, 81, 211),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: screenwidth * 0.6,
                                    child: Center(
                                      child: Text(
                                        '${widget.itemReviews.length} reviews and ratings',
                                        softWrap: true,
                                        textAlign: TextAlign.center,
                                        overflow: TextOverflow.fade,
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w800,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container()
                                ],
                              ),
                            ),
                          ],
                        )
                      : Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: screenheight * 0.7,
                              width: double.infinity,
                              child: Center(
                                child: PageView(
                                  controller: pageViewController,
                                  children: [
                                    Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'You Rated',
                                          softWrap: true,
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                            fontSize: 60,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        Text(
                                          '${widget.previousReview!.reviews.rating}',
                                          softWrap: true,
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(
                                            fontSize: 60,
                                            fontWeight: FontWeight.w800,
                                          ),
                                        ),
                                        RatingBar.builder(
                                          initialRating: widget
                                              .previousReview!.reviews.rating,
                                          minRating: 1,
                                          ignoreGestures: true,
                                          direction: Axis.horizontal,
                                          allowHalfRating: true,
                                          itemCount: 5,
                                          itemPadding: EdgeInsets.symmetric(
                                              horizontal: 4.0),
                                          itemBuilder: (context, _) => Icon(
                                            Icons.star,
                                            color: Color.fromARGB(
                                                255, 27, 81, 211),
                                          ),
                                          onRatingUpdate: (rating) {
                                            setState(() {
                                              ratingnum = rating;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                    Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            height: (screenheight * 0.7) * 0.9,
                                            width: double.infinity,
                                            child: Center(
                                              child: Padding(
                                                padding: EdgeInsets.only(
                                                  left: 10.0,
                                                  right: 10.0,
                                                ),
                                                child: TextField(
                                                  obscureText: false,
                                                  maxLines: 8,
                                                  controller: _reviewcontroller,
                                                  decoration: InputDecoration(
                                                    labelStyle: TextStyle(
                                                      color: Color.fromARGB(
                                                          255, 27, 81, 211),
                                                      fontSize: 13,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                        color: Color.fromARGB(
                                                            255, 27, 81, 211),
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(12),
                                                      ),
                                                    ),
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                        Radius.circular(12),
                                                      ),
                                                    ),
                                                    labelText: "Review Store",
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            height: (screenheight * 0.7) * 0.1,
                                            width: double.infinity,
                                            child: Center(
                                              child: TextButton(
                                                onPressed: () => (_reviewcontroller
                                                            .text ==
                                                        "")
                                                    ? Fluttertoast.showToast(
                                                        msg:
                                                            "Input required for review",
                                                        toastLength:
                                                            Toast.LENGTH_SHORT,
                                                        gravity:
                                                            ToastGravity.TOP,
                                                        timeInSecForIosWeb: 1,
                                                        fontSize: 16.0)
                                                    : reviewItem(widget.item),
                                                child: Text(
                                                  'Submit',
                                                  softWrap: true,
                                                  textAlign: TextAlign.start,
                                                  overflow: TextOverflow.fade,
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w800,
                                                    color: Color.fromARGB(
                                                        255, 27, 81, 211),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ]),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: screenheight * 0.1,
                              width: double.infinity,
                              child: Center(
                                child: SmoothPageIndicator(
                                  controller: pageViewController,
                                  count: 2,
                                  effect: WormEffect(
                                    dotWidth: 10.0,
                                    dotHeight: 10.0,
                                    radius: 10.0,
                                    activeDotColor:
                                        Color.fromARGB(255, 27, 81, 211),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: screenheight * 0.1,
                              width: double.infinity,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        WidgetSpan(
                                          child: Icon(
                                            Icons.star_border,
                                            size: 17,
                                          ),
                                        ),
                                        TextSpan(
                                          text: '${widget.item.itemRating}',
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Color.fromARGB(
                                                255, 27, 81, 211),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: screenwidth * 0.6,
                                    child: Center(
                                        child: (widget.itemReviews.length == 1)
                                            ? Text(
                                                '${widget.itemReviews.length} rating',
                                                softWrap: true,
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.fade,
                                                style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )
                                            : Text(
                                                '${widget.itemReviews.length} ratings',
                                                softWrap: true,
                                                textAlign: TextAlign.center,
                                                overflow: TextOverflow.fade,
                                                style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )),
                                  ),
                                  GestureDetector(
                                    onTap: () => deleteShopReview(
                                        widget.previousReview!.reviews.id),
                                    child: Icon(
                                      LineIcons.trash,
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                ),
              );
            },
          ),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
