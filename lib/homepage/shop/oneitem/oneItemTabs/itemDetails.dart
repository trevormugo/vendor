import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/profilepage.dart';
import 'package:vendor/homepage/profiles/remoteprofilepage.dart';
import 'package:vendor/models/defaults.dart';

import '../../../../models/oneItemQuery/oneitemquery.dart';

import '../../../../models/shops.dart';
import '../../../shop/details/onedetail.dart';
import '../../../shop/oneitem/oneItemDetail/detailinfo.dart';

import '../../../shop/details/onedetail.dart';
import '../../shopprofile.dart';

class ItemInformation extends StatefulWidget {
  ItemInformation({
    required this.oneItemQuery,
    required this.token,
    required this.userId,
  });
  final OneItemQuery oneItemQuery;
  final String token;
  final String userId;
  @override
  State createState() => _ItemInformationInstance();
}

class _ItemInformationInstance extends State<ItemInformation> {
  void _shopsProfile(String shopId) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ShopProfile(
          shopId: shopId,
          token: widget.token,
          userId: widget.userId,
        ),
      ),
    );
  }

  void _ownersProfile(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (id == prefs.get('userid').toString()) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CustomerProfilePage(
            userId: id,
            token: widget.token,
          ),
        ),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => RemoteProfilePage(
            userId: id,
            token: widget.token,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      children: <Widget>[
        (widget.oneItemQuery.item.sold == true)
            ? OneDetail(
                icon: LineAwesomeIcons.times,
                detail: 'Witing to restock',
              )
            : OneDetail(
                icon: LineAwesomeIcons.check,
                detail: 'Item Available',
              ),
        DetailInfo(
          icon: LineAwesomeIcons.shopping_bag,
          detail: "${widget.oneItemQuery.item.noOfItemSales} sales",
        ),
        DetailInfo(
          icon: LineAwesomeIcons.warehouse,
          detail: "${widget.oneItemQuery.item.noOfItems} in inventory",
        ),
        (widget.oneItemQuery.item.canBeDelivered == true)
            ? DetailInfo(
                icon: LineAwesomeIcons.truck,
                detail: 'Delivery Available',
              )
            : DetailInfo(
                icon: LineAwesomeIcons.shopping_cart,
                detail: 'Delivery Unavailable',
              ),
        DetailInfo(
          icon: Icons.rate_review_outlined,
          detail: "Rating ${widget.oneItemQuery.item.itemRating}",
        ),
        (widget.oneItemQuery.plan == "Basic")
            ? GestureDetector(
                onTap: () => _shopsProfile(widget.oneItemQuery.item.shopId),
                child: DetailInfo(
                  icon: PlanIcons.basic,
                  detail: 'Shop Profile',
                ),
              )
            : (widget.oneItemQuery.plan == "Advanced")
                ? GestureDetector(
                    onTap: () => _shopsProfile(widget.oneItemQuery.item.shopId),
                    child: DetailInfo(
                      icon: PlanIcons.advanced,
                      detail: 'Shop Profile',
                    ),
                  )
                : GestureDetector(
                    onTap: () => _shopsProfile(widget.oneItemQuery.item.shopId),
                    child: DetailInfo(
                      icon: PlanIcons.enterprise,
                      detail: 'Shop Profile',
                    ),
                  ),
        GestureDetector(
          onTap: () => _ownersProfile(widget.oneItemQuery.owner.id),
          child: DetailInfo(
            icon: LineAwesomeIcons.user,
            detail: 'Owner Profile',
          ),
        ),
      ],
    );
  }
}
