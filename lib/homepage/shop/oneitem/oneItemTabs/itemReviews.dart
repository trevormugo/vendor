import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/shop/oneitem/oneItemTabs/newItemReview/newitemreview.dart';
import 'package:vendor/models/oneItemQuery/oneitemquery.dart';
import 'package:vendor/models/oneShopQuery/items.dart';
import '../../../../models/oneItemQuery/itemreviewers.dart';
import '../../../../models/profileQueries/sales.dart';

class ItemReviewsTab extends StatefulWidget {
  ItemReviewsTab({
    required this.oneItemQuery,
    required this.userId,
    required this.token,
  });

  final OneItemQuery oneItemQuery;
  final String userId;
  final String token;

  @override
  State createState() => _ItemReviewsState();
}

class _ItemReviewsState extends State<ItemReviewsTab> {
  double? ratingnum = 0.0;
  PageController pageViewController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    widget.oneItemQuery.itemReviews!.forEach((element) {
      ratingnum = (ratingnum! + element.reviews.rating) /
          widget.oneItemQuery.itemReviews!.length;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void navigatetoNewItemReview(Items item) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => NewItemReview(
            userId: widget.userId,
            token: widget.token,
            itemReviews: widget.oneItemQuery.itemReviews!,
            item: item,
            previousReview: widget.oneItemQuery.previousReviews,
          ),
        ),
      );
    }

    return (widget.oneItemQuery.itemReviews!.length == 0)
        ? Center(
            child: Column(children: [
              Text("No item reviews yet"),
              TextButton(
                onPressed: () =>
                    navigatetoNewItemReview(widget.oneItemQuery.item),
                child: Text("Review Item"),
              )
            ]),
          )
        : ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: widget.oneItemQuery.itemReviews!.length + 1,
            itemBuilder: (contex, index) => (index == 0)
                ? TextButton(
                    onPressed: () =>
                        navigatetoNewItemReview(widget.oneItemQuery.item),
                    child: Text("Review Item"),
                  )
                : ListTile(
                    leading: Icon(
                      LineAwesomeIcons.store,
                    ),
                    title: Text(
                        '${widget.oneItemQuery.itemReviews![index - 1].reviews.review}'),
                    subtitle: Text(
                        '${widget.oneItemQuery.itemReviews![index - 1].user.fullName}'),
                    trailing: Text(
                        '${widget.oneItemQuery.itemReviews![index - 1].reviews.rating}'),
                  ),
          );
  }
}
