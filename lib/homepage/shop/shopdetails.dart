import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/favoriteshoprequest.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/providers/detailupdate_provider.dart';
import 'package:vendor/restapi.dart';

import '../../homepage/profiles/profilepage.dart';
import '../../homepage/profiles/remoteprofilepage.dart';

import '../../ip.dart';
import '../../models/oneShopQuery/oneshopquery.dart';

import 'dart:math';

import './details/onedetail.dart';

class ShopDetails extends StatefulWidget {
  ShopDetails({
    required this.oneShopQuery,
    required this.token,
    required this.userId,
    required this.callBack,
  });
  final OneShopQuery oneShopQuery;
  final String token;
  final String userId;
  final VoidCallback callBack;

  @override
  State createState() => _ShopDetailsState();
}

class _ShopDetailsState extends State<ShopDetails> {
  PageController pageViewController = PageController(initialPage: 0);
  List<Widget>? imagePanel;
  bool _operationinprogress = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    Future<void> _ownersProfile(String id) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (id == prefs.get('userid').toString()) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CustomerProfilePage(
              token: widget.token,
              userId: id,
            ),
          ),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => RemoteProfilePage(
              token: widget.token,
              userId: id,
            ),
          ),
        );
      }
    }

    void logout() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove('userid');
      prefs.remove('useremail');
      prefs.remove('token');
      prefs.remove('userphonenumber');
      prefs.remove('username');
      prefs.remove('deviceToken');
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
      dispose();
    }

    void favoriteShop(Shop shop) async {
      setState(() {
        _operationinprogress = true;
      });
      LikeShopRequest likeShopRequest = LikeShopRequest(
        shopId: shop.id,
        userId: widget.userId,
      );
      var response = await RestApi()
          .favoriteShop("/shops/likeShop", widget.token, likeShopRequest);
      if (response.statusCode == 201) {
        setState(() {
          _operationinprogress = false;
        });
        context.read<FavoriteProvider>().favorite();
        widget.callBack();
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code ${response.statusCode}",
          desc: '${shop.shopName} added to favorites',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else if (response.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${response.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${response.statusCode}",
          desc: '${response.body}',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      }
    }

    void unfavoriteShop(Shop shop) async {
      setState(() {
        _operationinprogress = true;
      });
      var response = await RestApi()
          .unFavoriteShop("/shops/unlikeShop/${shop.id}", widget.token);
      if (response.statusCode == 204) {
        setState(() {
          _operationinprogress = false;
        });
        widget.callBack();
        context.read<FavoriteProvider>().unfavorite();
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code ${response.statusCode}",
          desc: '${shop.shopName} was removed from favorites',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else if (response.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${response.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${response.statusCode}",
          desc: '${response.body}',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      }
    }

    return Stack(children: [
      Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            title: ListTile(
              title: Text(
                '${widget.oneShopQuery.shop.shopName}',
                softWrap: true,
                textAlign: TextAlign.start,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                ),
              ),
              subtitle: Text(
                'details',
              ),
            ),
          ),
        ),
        extendBodyBehindAppBar: false,
        floatingActionButton:
            /*(widget.oneShopQuery.shop.ownerId == widget.userId)
                ? Container()
                :*/
            (context.watch<FavoriteProvider>().isfavorited)
                ? FloatingActionButton(
                    onPressed: () => unfavoriteShop(widget.oneShopQuery.shop),
                    child: Icon(
                      Icons.favorite,
                      color: Color.fromARGB(255, 207, 118, 0),
                    ),
                    backgroundColor: Color.fromARGB(255, 27, 81, 211),
                  )
                : FloatingActionButton(
                    onPressed: () => favoriteShop(widget.oneShopQuery.shop),
                    child: Icon(
                      Icons.favorite_border,
                      color: Color.fromARGB(255, 207, 118, 0),
                    ),
                    backgroundColor: Color.fromARGB(255, 27, 81, 211),
                  ),
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: constraints.maxWidth,
                      minHeight: constraints.maxHeight),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      (widget.oneShopQuery.shopImages!.length == 0)
                          ? Container(
                              height: screenheight * 0.4,
                              width: double.infinity,
                              child: Center(
                                child: Image.asset(
                                  PlanImages.basic,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          : Container(
                              height: screenheight * 0.4,
                              width: double.infinity,
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.all(5),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: FittedBox(
                                      child: Container(
                                        height: screenheight * 0.9,
                                        width: screenwidth * 0.9,
                                        color: Colors.transparent,
                                        child: (widget.oneShopQuery.planName ==
                                                PlanNames.basic)
                                            ? Image.asset(
                                                PlanImages.basic,
                                              )
                                            : (widget.oneShopQuery.planName ==
                                                    PlanNames.advanced)
                                                ? Image.asset(
                                                    PlanImages.advanced,
                                                  )
                                                : (widget.oneShopQuery
                                                            .planName ==
                                                        PlanNames.enterprise)
                                                    ? Image.asset(
                                                        PlanImages.enterprise,
                                                      )
                                                    : Image.asset(
                                                        ErrorImage.noimage,
                                                      ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                      (widget.oneShopQuery.shopImages!.length == 0)
                          ? Container()
                          : Container(
                              height: screenheight * 0.1,
                              width: double.infinity,
                              child: Center(
                                child: SmoothPageIndicator(
                                  controller: pageViewController,
                                  count: widget.oneShopQuery.shopImages!.length,
                                  effect: WormEffect(
                                    dotWidth: 10.0,
                                    dotHeight: 10.0,
                                    radius: 10.0,
                                    activeDotColor:
                                        Color.fromARGB(255, 27, 81, 211),
                                  ),
                                ),
                              ),
                            ),
                      OneDetail(
                        icon: Icons.store_mall_directory_outlined,
                        detail: '${widget.oneShopQuery.shop.shopName}',
                      ),
                      OneDetail(
                        icon: Icons.schedule_outlined,
                        detail: '${widget.oneShopQuery.shop.openHours}',
                      ),
                      OneDetail(
                          icon: Icons.star_border_purple500_outlined,
                          detail: '${widget.oneShopQuery.shop.rating} / 5'),
                      OneDetail(
                          icon: Icons.favorite_border_sharp,
                          detail:
                              '${widget.oneShopQuery.shopFavorers!.length} favorites'),
                      OneDetail(
                        icon: Icons.shopping_cart_sharp,
                        detail:
                            '${widget.oneShopQuery.shopSales!.length} sales',
                      ),
                      OneDetail(
                        icon: Icons.rate_review_outlined,
                        detail:
                            '${widget.oneShopQuery.shopReviews!.length} ratings and reviews',
                      ),
                      (widget.oneShopQuery.shop.deliveries == true)
                          ? OneDetail(
                              icon: Icons.local_shipping_outlined,
                              detail: 'Available Deliveries',
                            )
                          : Container(),
                      OneDetail(
                        icon: Icons.person_outline_sharp,
                        detail: 'Owners profile',
                        navigate: () =>
                            _ownersProfile(widget.oneShopQuery.shop.ownerId),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
