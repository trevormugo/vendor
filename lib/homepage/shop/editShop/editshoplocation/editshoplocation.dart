import 'dart:async';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/deliverylocationrequest.dart';
import 'package:vendor/models/oneShopQuery/oneshopquery.dart';
import 'package:vendor/models/shops.dart';

import '../../../../restapi.dart';

class EditShopLocation extends StatefulWidget {
  EditShopLocation({
    required this.userId,
    required this.token,
    required this.oneShopQuery,
    required this.callBack,
  });
  final String userId;
  final String token;
  final OneShopQuery oneShopQuery;
  final Function callBack;

  @override
  State createState() => _EditShopLocationInstance();
}

class _EditShopLocationInstance extends State<EditShopLocation> {
  Completer<GoogleMapController> _controller = Completer();
  late Future<Position> determinePosition;

  Marker? oneMarker;

  bool operationinprogress = false;

  late BitmapDescriptor shopLocationIcon;

  @override
  void initState() {
    super.initState();

    determinePosition = _determinePosition();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    await setCustomMapPin();
    if (!serviceEnabled) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc: 'Location services are disabled to continue',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Please Allow Location Permission',
          desc: 'Location permissions are required to continue',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc:
            'Location permissions are permanently denied, we cannot request permissions',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return Future.value(Geolocator.getCurrentPosition());
  }

  Future<void> setCustomMapPin() async {
    shopLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.basic);
    oneMarker = Marker(
      markerId: MarkerId("oneMarker"),
      position: LatLng(widget.oneShopQuery.shop.latitude,
          widget.oneShopQuery.shop.longitude),
      icon: shopLocationIcon,
    );
  }

  void setMarkerPosition(LatLng position) async {
    setState(() {
      oneMarker = Marker(
        markerId: MarkerId("oneMarker"),
        position: LatLng(position.latitude, position.longitude),
        icon: shopLocationIcon,
      );
    });
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future updateShopLocation(String id) async {
    if (oneMarker == null) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: "First specify a location",
        desc: null,
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      UpdateShopRequest updateShopRequest = UpdateShopRequest(
        ownerId: widget.oneShopQuery.shop.ownerId,
        latitude: oneMarker!.position.latitude,
        longitude: oneMarker!.position.longitude,
        shopName: widget.oneShopQuery.shop.shopName,
        openHours: widget.oneShopQuery.shop.openHours,
      );
      var response = await RestApi().editShopDetail(
          "/shops/editShopDetail?shopId=$id", widget.token, updateShopRequest);
      if (response.statusCode == 204) {
        setState(() {
          widget.callBack();
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code ${response.statusCode}",
          desc: 'Shop Location Updated',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        return Future.value(response);
      } else if (response.statusCode == 401) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${response.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
        return Future.error('Error  ${response.statusCode}');
      } else {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${response.statusCode}",
          desc: '${response.body}',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        return Future.error('Error  ${response.statusCode}');
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void showLocationTagModal() async {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text(
              "Update shop location to lat ${oneMarker!.position.latitude} and long ${oneMarker!.position.longitude}"),
          actions: <Widget>[
            TextButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Add',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                updateShopLocation(widget.oneShopQuery.shop.id);
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Edit Shop Locations',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      floatingActionButton: FloatingActionButton(
        onPressed: () => (oneMarker == null)
            ? Fluttertoast.showToast(
                msg: "Location not added",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.TOP,
                timeInSecForIosWeb: 1,
                fontSize: 16.0,
              )
            : showLocationTagModal(),
        child: Icon(
          LineAwesomeIcons.plus,
          color: Color.fromARGB(255, 207, 118, 0),
        ),
        backgroundColor: Color.fromARGB(255, 27, 81, 211),
      ),
      body: Container(
        width: screenwidth,
        height: screenheight,
        child: FutureBuilder(
            future: determinePosition,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return (operationinprogress == true)
                    ? Center(
                        child: SpinKitFadingCube(
                          size: 60,
                          color: Color.fromARGB(255, 207, 118, 0),
                        ),
                      )
                    : Center(
                        child: GoogleMap(
                          onTap: (position) => setMarkerPosition(position),
                          mapType: MapType.normal,
                          initialCameraPosition: CameraPosition(
                            target: LatLng(
                              widget.oneShopQuery.shop.latitude,
                              widget.oneShopQuery.shop.longitude,
                            ),
                            zoom: 14.4746,
                          ),
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                          markers: (oneMarker == null) ? {} : {oneMarker!},
                        ),
                      );
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("${snapshot.error}"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
    );
  }
}
