import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/listtiles/itemtile.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/listtiles/shoptile.dart';
import 'package:vendor/homepage/shop/editShop/editshoplocation/editshoplocation.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/notifications/notificationsresponse.dart';
import 'package:vendor/models/oneShopQuery/oneshopquery.dart';
import 'package:vendor/models/shops.dart';
import 'package:vendor/restapi.dart';

class EditShop extends StatefulWidget {
  EditShop({
    required this.token,
    required this.userId,
    required this.oneShopQuery,
    required this.callBack,
  });
  final String token;
  final String userId;
  final OneShopQuery oneShopQuery;
  final Function callBack;

  @override
  State createState() => _EditShopInstance();
}

class _EditShopInstance extends State<EditShop> {
  PageController pageViewController = PageController(initialPage: 0);
  final picker = ImagePicker();
  String dropdownValue = "Camera";
  File? image;
  ImageSource imagesrc = ImageSource.camera;
  static TextEditingController shopnamecontroller = TextEditingController();
  static TextEditingController openhourscontroller = TextEditingController();
  static TextEditingController closinghourscontroller = TextEditingController();
  bool _operationinprogress = false;
  late double shoplatitude;
  late double shoplongitude;

  @override
  void initState() {
    super.initState();
    shoplatitude = widget.oneShopQuery.shop.latitude;
    shoplongitude = widget.oneShopQuery.shop.longitude;
    shopnamecontroller.text = widget.oneShopQuery.shop.shopName;
    openhourscontroller.text = widget.oneShopQuery.shop.openHours.split("-")[0];
    closinghourscontroller.text =
        widget.oneShopQuery.shop.openHours.split("-")[1];
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<void> _showFirstTimePicker() async {
    final TimeOfDay? result =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (result != null) {
      setState(() {
        openhourscontroller.text = result.format(context);
      });
    }
  }

  Future<void> _showSecondTimePicker() async {
    final TimeOfDay? result =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (result != null) {
      setState(() {
        closinghourscontroller.text = result.format(context);
      });
    }
  }

  @override
  void dispose() {
    //shopnamecontroller.dispose();
    //openhourscontroller.dispose();
    //closinghourscontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void navigatetoeditshoplocation(OneShopQuery oneShopQuery) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EditShopLocation(
            token: widget.token,
            userId: widget.userId,
            oneShopQuery: oneShopQuery,
            callBack: widget.callBack,
          ),
        ),
      );
    }

    void updateShopInfo(String id) async {
      setState(() {
        _operationinprogress = true;
      });
      UpdateShopRequest updateShopRequest = UpdateShopRequest(
        ownerId: widget.oneShopQuery.shop.ownerId,
        latitude: widget.oneShopQuery.shop.latitude,
        longitude: widget.oneShopQuery.shop.longitude,
        shopName: shopnamecontroller.text,
        openHours:
            "${openhourscontroller.text} - ${closinghourscontroller.text}",
      );
      var response = await RestApi().editShopDetail(
          "/shops/editShopDetail?shopId=$id", widget.token, updateShopRequest);
      if (response.statusCode == 204) {
        setState(() {
          _operationinprogress = false;
        });
        widget.callBack();
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code ${response.statusCode}",
          desc: 'Shop details were updated',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else if (response.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${response.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${response.statusCode}",
          desc: '${response.body}',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      }
    }

    return Stack(children: [
      Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            title: ListTile(
              title: Text(
                'Edit Shop',
                softWrap: true,
                textAlign: TextAlign.start,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ),
        ),
        extendBodyBehindAppBar: true,
        floatingActionButton:
            /*(snapshot.data.owner.id == widget.userId)
                          ? Container()
                          : */
            FloatingActionButton(
          onPressed: () => navigatetoeditshoplocation(widget.oneShopQuery),
          child: Icon(
            LineAwesomeIcons.map_marked,
            color: Color.fromARGB(255, 207, 118, 0),
          ),
          backgroundColor: Color.fromARGB(255, 27, 81, 211),
        ),
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: LayoutBuilder(builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        controller: shopnamecontroller,
                        obscureText: false,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromARGB(255, 27, 81, 211),
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          labelText: "Shop name",
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        onTap: _showFirstTimePicker,
                        controller: openhourscontroller,
                        obscureText: false,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 27, 81, 211)),
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          labelText: "Open hours",
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        onTap: _showSecondTimePicker,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: closinghourscontroller,
                        obscureText: false,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 27, 81, 211)),
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          labelText: "Closing hours",
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.all(20),
                      child: TextButton(
                        onPressed: () =>
                            updateShopInfo(widget.oneShopQuery.shop.id),
                        child: Text("Update Shop"),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
