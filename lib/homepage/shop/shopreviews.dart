import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/shop/categories/onecategory.dart';
import 'package:vendor/homepage/shop/editShop/editShop.dart';
import 'package:vendor/homepage/shop/shopdetails.dart';
import 'package:vendor/homepage/shop/newshopreviews.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/reviewshoprequest.dart';

import '../../ip.dart';
import '../../main.dart';
import '../../models/oneShopQuery/oneshopquery.dart';
import '../../horizontalitems.dart';
import '../../../restapi.dart';

class ShopReviews extends StatefulWidget {
  ShopReviews({
    required this.oneShopQuery,
    required this.token,
    required this.userId,
  });
  final OneShopQuery oneShopQuery;
  final String token;
  final String userId;
  @override
  State createState() => _ShopReviewsTwoState();
}

class _ShopReviewsTwoState extends State<ShopReviews> {
  double ratingnum = 3.0;
  PageController pageViewController = PageController(initialPage: 0);
  TextEditingController _reviewcontroller = TextEditingController();
  bool _operationinprogress = false;
  ScrollController? _scrollController;

  @override
  void initState() {
    super.initState();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void reviewShop(Shop shop) async {
    setState(() {
      _operationinprogress = true;
    });
    ReviewShopRequest reviewShopRequest = ReviewShopRequest(
      shopId: shop.id,
      userId: widget.userId,
      rating: ratingnum,
      review: _reviewcontroller.text,
    );
    var response = await RestApi()
        .reviewShop("/shops/reviewShop", widget.token, reviewShopRequest);
    if (response.statusCode == 201) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'reviewed ${shop.shopName}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  void deleteShopReview(String id) async {
    setState(() {
      _operationinprogress = true;
    });
    var response =
        await RestApi().deleteReview("/shops/deleteReview/$id", widget.token);
    if (response.statusCode == 201) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Review was deleted',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigateToNewShopReview() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => NewShopReview(
            oneShopQuery: widget.oneShopQuery,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Shop Reviews',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      floatingActionButton:
          /*(widget.oneShopQuery.shopReviewersForProfile == null)
              ? null
              :*/
          FloatingActionButton(
        onPressed: navigateToNewShopReview,
        child: Icon(
          LineIcons.plus,
          color: Color.fromARGB(255, 207, 118, 0),
        ),
        backgroundColor: Color.fromARGB(255, 27, 81, 211),
      ),
      extendBodyBehindAppBar: true,
      body: (widget.oneShopQuery.shopReviews!.length == 0)
          ? Center(
              child: Text("No shop reviews yet"),
            )
          : ListView.builder(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              itemCount: widget.oneShopQuery.shopReviews!.length,
              itemBuilder: (BuildContext context, int index) => ListTile(
                leading: CircleAvatar(),
                title: Text(
                  '${widget.oneShopQuery.shopReviews![index].user.userName}',
                  softWrap: true,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                subtitle: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text:
                            '${widget.oneShopQuery.shopReviews![index].reviews.review} ',
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.black87,
                        ),
                      ),
                      TextSpan(
                        text:
                            ' - ${TimeConversion.readTimestamp(widget.oneShopQuery.shopReviews![index].reviews.timestamp)}',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromARGB(255, 27, 81, 211),
                        ),
                      ),
                    ],
                  ),
                ),
                trailing: RichText(
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Icon(
                          Icons.star_border,
                          size: 17,
                        ),
                      ),
                      TextSpan(
                        text:
                            '${widget.oneShopQuery.shopReviews![index].reviews.rating}',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black87,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
