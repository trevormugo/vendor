import 'dart:convert';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/shop/categories/onecategory.dart';
import 'package:vendor/homepage/shop/editShop/editShop.dart';
import 'package:vendor/homepage/shop/shopdetails.dart';
import 'package:vendor/homepage/shop/newshopreviews.dart';
import 'package:vendor/homepage/shop/shopreviews.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/providers/detailupdate_provider.dart';

import '../../ip.dart';
import '../../main.dart';
import '../../models/oneShopQuery/oneshopquery.dart';

import '../../models/shops.dart';
import '../../horizontalitems.dart';
import '../maps/bottomwidget/bottomwidget.dart';
import '../../../restapi.dart';

class ShopProfile extends StatefulWidget {
  ShopProfile({
    required this.shopId,
    required this.token,
    required this.userId,
  });
  final String shopId;
  final String token;
  final String userId;
  @override
  State createState() => _ShopProfileState();
}

class _ShopProfileState extends State<ShopProfile> {
  OneShopQuery? _oneShop;
  ScrollController? _scrollController;

  late Future<OneShopQuery> _fetchOneShopDetails;

  PageController pageViewController = PageController(initialPage: 0);

  bool _operationinprogress = false;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _fetchOneShopDetails = fetchOneShopDetails(widget.shopId);
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<OneShopQuery> fetchOneShopDetails(String? id) async {
    var oneShop = await RestApi().fetchoneshop(
        "/shops/queryShopsCategoriesAndItems?shopId=$id&customerId=${widget.userId}",
        widget.token);
    if (oneShop.statusCode == 200) {
      print("${json.decode(oneShop.body)}");
      _oneShop = OneShopQuery.fromJson(json.decode(oneShop.body));
      setState(() {});
      context.read<FavoriteProvider>().initial(_oneShop!.favorited);
      return Future.value(_oneShop);
    } else if (oneShop.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${oneShop.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error ${oneShop.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Query Error',
        desc: 'Unable to fetch Shop',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error ${oneShop.statusCode}');
    }
  }

  void reloadPage() {
    setState(() {
      _fetchOneShopDetails = fetchOneShopDetails(widget.shopId);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void naigatetoreviews(OneShopQuery oneShopQuery) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopReviews(
            oneShopQuery: oneShopQuery,
            userId: widget.userId,
            token: widget.token,
          ),
        ),
      );
    }

    void navigatetodetails(OneShopQuery oneShopQuery) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopDetails(
            oneShopQuery: oneShopQuery,
            token: widget.token,
            userId: widget.userId,
            callBack: reloadPage,
          ),
        ),
      );
    }

    void navigatetoeditshop(OneShopQuery oneShopQuery) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EditShop(
            oneShopQuery: oneShopQuery,
            token: widget.token,
            userId: widget.userId,
            callBack: reloadPage,
          ),
        ),
      );
    }

    void navigatetocategory() {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneCategory(
                  categoryname: 'category',
                  categoryicon: Icon(
                    LineAwesomeIcons.utensils,
                    color: Colors.lightGreenAccent,
                  ),
                  token: widget.token,
                  userId: widget.userId,
                )),
      );
    }

    void deleteShop(String shopId) async {
      setState(() {
        _operationinprogress = true;
      });
      var response =
          await RestApi().deleteShop("/shops/deleteShop/$shopId", widget.token);
      if (response.statusCode == 204) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code ${response.statusCode}",
          desc: 'Shop successfuly deleted',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else if (response.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${response.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${response.statusCode}",
          desc: '${response.body}',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      }
    }

    void confirmDeleteShop(String id) async {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text('Delete this shop?'),
          content: Text(
              'Shops can only be deleted while no orders or assignments are still active.Note that deleting this shop will also delete any data linked to this shop.And it is not recoverable.'),
          actions: <Widget>[
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                deleteShop(id);
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }

    return Scaffold(
      endDrawer: Drawer(
        child: SafeArea(
          top: true,
          child: Container(
            color: Colors.white,
            height: screenheight,
            width: double.infinity,
            child: Container(
              height: screenheight,
              child: (_oneShop == null)
                  ? Center(
                      child: Text("Failed to load page"),
                    )
                  : ListView(
                      children: [
                        ListTile(
                          onTap: () => naigatetoreviews(_oneShop!),
                          leading: Icon(
                            Icons.star_border,
                          ),
                          title: Text("Shop Reviews"),
                        ),
                        ListTile(
                          onTap: () => navigatetoeditshop(_oneShop!),
                          leading: Icon(
                            Icons.edit,
                          ),
                          title: Text("Edit Shop"),
                        ),
                        ListTile(
                          onTap: () => navigatetodetails(_oneShop!),
                          leading: Icon(
                            LineAwesomeIcons.info_circle,
                          ),
                          title: Text("Shop Details"),
                        ),
                        ListTile(
                          onTap: () => confirmDeleteShop(_oneShop!.shop.id),
                          leading: Icon(
                            LineIcons.trash,
                          ),
                          title: Text("Delete Shop"),
                        ),
                      ],
                    ),
            ),
          ),
        ),
      ),
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: FutureBuilder(
            future: _fetchOneShopDetails,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return NestedScrollView(
                  controller: _scrollController,
                  headerSliverBuilder: (context, value) {
                    return [
                      SliverAppBar(
                        floating: true,
                        snap: false,
                        pinned: true,
                        forceElevated: value,
                        expandedHeight: screenheight * 0.7,
                        leading: IconButton(
                          icon: Icon(LineIcons.arrowLeft),
                          iconSize: 20.0,
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        flexibleSpace: FlexibleSpaceBar(
                          collapseMode: CollapseMode.parallax,
                          centerTitle: true,
                          background: Container(
                            height: screenheight * 0.4,
                            width: screenwidth * 0.8,
                            child: Padding(
                              padding: EdgeInsets.all(5),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),
                                child: FittedBox(
                                  fit: BoxFit.cover,
                                  child: Container(
                                    height: screenheight * 0.9,
                                    width: screenwidth * 0.9,
                                    color: Colors.transparent,
                                    child: GestureDetector(
                                      onTap: null,
                                      child: (snapshot.data.planName ==
                                              PlanNames.basic)
                                          ? Image.asset(
                                              PlanImages.basic,
                                            )
                                          : (snapshot.data.planName ==
                                                  PlanNames.advanced)
                                              ? Image.asset(
                                                  PlanImages.advanced,
                                                )
                                              : (snapshot.data.planName ==
                                                      PlanNames.enterprise)
                                                  ? Image.asset(
                                                      PlanImages.enterprise,
                                                    )
                                                  : Image.asset(
                                                      ErrorImage.noimage,
                                                    ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        title: ListTile(
                          title: Text(
                            "${snapshot.data.shop.shopName}",
                            softWrap: true,
                            textAlign: TextAlign.start,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                      ),
                    ];
                  },
                  body: (snapshot.data.categories.length == 0)
                      ? Center(
                          child: Text("No categories yet"),
                        )
                      : ListView.builder(
                          itemCount: snapshot.data.categories.length,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemBuilder: (BuildContext context, int index) {
                            return Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: GestureDetector(
                                    onTap: navigatetocategory,
                                    child: Text(
                                      '${snapshot.data.categories[index].categoryName}',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w800,
                                        fontFamily: 'NanumGothic',
                                      ),
                                    ),
                                  ),
                                ),
                                HorizontalItems(
                                  items: snapshot.data.categories[index].items,
                                  token: widget.token,
                                  userId: widget.userId,
                                ),
                              ],
                            );
                          },
                        ),
                );
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("${snapshot}"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
    );
  }
}
