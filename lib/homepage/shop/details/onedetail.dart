import 'package:flutter/material.dart';

class OneDetail extends StatefulWidget {
  OneDetail({
    required this.icon,
    required this.detail,
    this.navigate,
  });
  final IconData icon;
  final String detail;
  final Function? navigate;

  @override
  createState() => _OneDetailState();
}

class _OneDetailState extends State<OneDetail> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 10.0,
        right: 10.0,
        bottom: 5.0,
        top: 5.0,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white60,
          border: Border.all(
            color: Colors.black12,
            width: 1.0,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: ListTile(
          leading: Icon(widget.icon),
          onTap: () => widget.navigate,
          title: Text(
            '${widget.detail}',
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }
}
