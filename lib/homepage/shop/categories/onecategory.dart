import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import 'dart:math';

import '../../../models/insight.dart';
import '../oneitem/oneitem.dart';

class OneCategory extends StatefulWidget {
  OneCategory({
    required this.categoryname,
    required this.categoryicon,
    required this.token,
    required this.userId,
  });

  final String categoryname;
  final Icon categoryicon;
  final String token;
  final String userId;
  @override
  State createState() => _OneCategoryInstance();
}

class _OneCategoryInstance extends State<OneCategory> {
  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigate() {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneItem(
                  icon: widget.categoryicon,
                  itemId:  'itemid',
                  userId: widget.userId,
                  token: widget.token,
                )),
      );
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: RichText(
              text: TextSpan(
                children: [
                  WidgetSpan(
                    child: widget.categoryicon,
                  ),
                  TextSpan(
                    text: '${widget.categoryname}',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.black,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ],
              ),
            ),
            subtitle: Text(
              '12 items',
            ),
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: 19,
        itemBuilder: (contex, index) => Container(
          height: screenheight * 0.6,
          width: screenwidth * 0.9,
          margin: EdgeInsets.only(
            top: 5,
            bottom: 5,
          ),
          child: Card(
            elevation: 2,
            child: GridTile(
              header: GridTileBar(
                leading: widget.categoryicon,
                title: Text(
                  'MODARANI Cut Off Denim Shorts for Women Frayed Distressed Jean Short Cute Mid Rise Ripped Hot Shorts Comfy Stretchy',
                  softWrap: true,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                trailing: GestureDetector(
                  onTap: null,
                  child: Icon(
                    LineIcons.shoppingCartArrowDown,
                    color: Colors.black,
                  ),
                ),
              ),
              child: GestureDetector(
                onTap: null,
                child: Container(
                  height: double.infinity,
                  width: double.infinity,
                ),
              ),
              footer: GridTileBar(
                title: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'Ksh 5000',
                        style: TextStyle(
                          fontSize: 17,
                          color: Colors.black87,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      TextSpan(
                        text: ' +600',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                          decoration: TextDecoration.lineThrough,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      TextSpan(
                        text: ' +300 delivery',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ),
                subtitle: Text(
                  '500 left in stock',
                  softWrap: true,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                trailing: GestureDetector(
                  onTap: navigate,
                  child: Icon(
                    Icons.info_outline,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
