import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:math';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/profiletabs/affiliatedshops.dart';

import '../../models/profileQueries/profilequeries.dart';

import '../../main.dart';
import '../../restapi.dart';
import './profileheader/profileimage.dart';
import './profiletabs/information.dart';
import './profiletabs/favorites.dart';
import './profiletabs/shops.dart';
import '../../models/insight.dart';

class RemoteProfilePage extends StatefulWidget {
  RemoteProfilePage({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _RemoteProfilePageInstance();
}

class _RemoteProfilePageInstance extends State<RemoteProfilePage>
    with TickerProviderStateMixin {
  TabController? _tabcontroller;
  ScrollController? _scrollController;
  late Future<ProfileQueries> _fetchProfileDetails;
  ProfileQueries? profileQueries;
  List<InlineSpan>? roles = [];

  List<Insight> insights = [
    Insight(
      paintcolor: Colors.amber,
      insightpercentage: 0.67,
      category: 'fashion',
      icon: Icon(
        Icons.checkroom_sharp,
        color: Colors.amber,
      ),
    ),
    Insight(
      paintcolor: Colors.redAccent[200],
      insightpercentage: 0.22,
      category: 'games',
      icon: Icon(
        Icons.games_outlined,
        color: Colors.redAccent[200],
      ),
    ),
    Insight(
      paintcolor: Colors.lightGreenAccent,
      insightpercentage: 0.53,
      category: 'food',
      icon: Icon(
        Icons.restaurant,
        color: Colors.lightGreenAccent,
      ),
    ),
    Insight(
      paintcolor: Colors.deepPurple,
      insightpercentage: 0.83,
      category: 'electronics',
      icon: Icon(
        Icons.computer_sharp,
        color: Colors.deepPurple,
      ),
    ),
    Insight(
      paintcolor: Colors.yellow,
      insightpercentage: 0.70,
      category: 'services',
      icon: Icon(
        Icons.miscellaneous_services,
        color: Colors.yellow,
      ),
    ),
  ];

  @override
  void initState() {
    super.initState();
    _tabcontroller = TabController(length: 3, vsync: this);
    _scrollController = ScrollController();
    _fetchProfileDetails = fetchProfileDetails();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<ProfileQueries> fetchProfileDetails() async {
    var oneProfile = await RestApi().profileQuery(
        "/accountspool/profileQuery/${widget.userId}", widget.userId);
    if (oneProfile.statusCode == 200) {
      print("${oneProfile.body.toString()}");
      profileQueries = ProfileQueries.fromJson(json.decode(oneProfile.body));
      profileQueries!.roles.forEach((element) {
        roles!.add(
          TextSpan(
            text: '${element}',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
        );
      });
      /*
      if (profileQueries!.roles.contains("Vendor")) {
        profileQueries!.vendorSales!.purchases!.map((e) => null);
        var t = groupBy(profileQueries!.vendorSales!.purchases!,
            (obj) => (obj as Map)['categories']['id']);
        print("Vendor $t");
        return Future.value(profileQueries);
      } else if (profileQueries!.roles.contains("Customer")) {
        profileQueries!.vendorSales!.purchases!.map((e) => null);
        var t = groupBy(profileQueries!.vendorSales!.purchases!,
            (obj) => (obj as Map)['categories']['id']);
        print("Customer $t");
        return Future.value(profileQueries);
      } else if (profileQueries!.roles.contains("Rider")) {
        profileQueries!.vendorSales!.purchases!.map((e) => null);
        var t = groupBy(profileQueries!.vendorSales!.purchases!,
            (obj) => (obj as Map)['categories']['id']);
        print("Rider $t");
        return Future.value(profileQueries);
      } else {
        return Future.error('Error');
      }*/
      return Future.value(profileQueries);
    } else if (oneProfile.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${oneProfile.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Unexpected Error ${oneProfile.statusCode}",
        desc: 'User not found',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      return Future.error('Error');
    }
  }

  @override
  void dispose() {
    _tabcontroller!.dispose();
    _scrollController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: FutureBuilder(
            future: _fetchProfileDetails,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return DefaultTabController(
                  length: 3,
                  initialIndex: 0,
                  child: NestedScrollView(
                    controller: _scrollController,
                    headerSliverBuilder: (context, value) {
                      return [
                        SliverAppBar(
                          floating: true,
                          snap: false,
                          pinned: true,
                          forceElevated: value,
                          expandedHeight: screenheight * 0.7,
                          flexibleSpace: FlexibleSpaceBar(
                            collapseMode: CollapseMode.parallax,
                            centerTitle: true,
                            background: Wrap(
                              alignment: WrapAlignment.center,
                              runAlignment: WrapAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: screenheight * 0.5,
                                  width: screenwidth,
                                  child: CenteredWidgetInsightsBorder(
                                    insights: insights,
                                    shouldanimate: true,
                                    backgroundstrokewidth: 6,
                                    foregroundstrokewidth: 6,
                                    middle: Container(
                                      child: Center(
                                        child: CircleAvatar(
                                          maxRadius: (screenwidth * 0.2) / 2,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Wrap(
                                  alignment: WrapAlignment.center,
                                  runAlignment: WrapAlignment.center,
                                  direction: Axis.horizontal,
                                  children: [
                                    Container(
                                      height: screenheight * 0.1,
                                      width: screenwidth * 0.2,
                                      child: CenteredWidgetInsightsBorder(
                                        shouldanimate: true,
                                        insights: [insights[0]],
                                        middle: insights[0].icon,
                                        backgroundstrokewidth: 6,
                                        foregroundstrokewidth: 6,
                                      ),
                                    ),
                                    Container(
                                      height: screenheight * 0.1,
                                      width: screenwidth * 0.2,
                                      child: CenteredWidgetInsightsBorder(
                                        shouldanimate: true,
                                        insights: [insights[1]],
                                        middle: insights[1].icon,
                                        backgroundstrokewidth: 6,
                                        foregroundstrokewidth: 6,
                                      ),
                                    ),
                                    Container(
                                      height: screenheight * 0.1,
                                      width: screenwidth * 0.2,
                                      child: CenteredWidgetInsightsBorder(
                                        shouldanimate: true,
                                        insights: [insights[2]],
                                        middle: insights[2].icon,
                                        backgroundstrokewidth: 6,
                                        foregroundstrokewidth: 6,
                                      ),
                                    ),
                                    Container(
                                      height: screenheight * 0.1,
                                      width: screenwidth * 0.2,
                                      child: CenteredWidgetInsightsBorder(
                                        shouldanimate: true,
                                        insights: [insights[3]],
                                        middle: insights[3].icon,
                                        backgroundstrokewidth: 6,
                                        foregroundstrokewidth: 6,
                                      ),
                                    ),
                                    Container(
                                      height: screenheight * 0.1,
                                      width: screenwidth * 0.2,
                                      child: CenteredWidgetInsightsBorder(
                                        shouldanimate: true,
                                        insights: [insights[4]],
                                        middle: insights[4].icon,
                                        backgroundstrokewidth: 6,
                                        foregroundstrokewidth: 6,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          title: ListTile(
                            title: Text(
                              '${snapshot.data.user.userName}',
                              softWrap: true,
                              textAlign: TextAlign.start,
                              overflow: TextOverflow.fade,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                            subtitle: RichText(
                              text: TextSpan(
                                children: roles,
                              ),
                            ),
                          ),
                          bottom: TabBar(
                            controller: _tabcontroller,
                            tabs: <Widget>[
                              Tab(
                                icon: Icon(
                                  LineAwesomeIcons.info_circle,
                                ),
                              ),
                              Tab(
                                icon: Icon(
                                  LineAwesomeIcons.heart,
                                ),
                              ),
                              Tab(
                                icon: (snapshot.data.roles.contains("Rider"))
                                    ? Icon(
                                        LineAwesomeIcons.truck,
                                      )
                                    : (snapshot.data.roles.contains("Vendor"))
                                        ? Icon(
                                            LineAwesomeIcons.store,
                                          )
                                        : Container(),
                              ),
                            ],
                          ),
                        ),
                      ];
                    },
                    body: TabBarView(
                      controller: _tabcontroller,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(
                            top: 40,
                          ),
                          child: ProfileInformation(
                            queries: snapshot.data,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            top: 40,
                          ),
                          child: FavoritesTab(
                            shopsFavored: snapshot.data.shopFavorites,
                            userId: widget.userId,
                            token: widget.token,
                          ),
                        ),
                        /*(snapshot.data.roles.contains("Vendor"))
                            ? Container(
                                margin: EdgeInsets.only(
                                  top: 40,
                                ),
                                child: ShopsTab(
                                  insights: insights,
                                ),
                              )
                            :*/
                        (snapshot.data.roles.contains("Rider"))
                            ? RiderAffiliationsTab(
                                riderShopAffiliationsQuery:
                                    snapshot.data.riderShopAffiliationsQuery,
                                token: widget.token,
                              )
                            : Container(
                                child: Center(
                                  child: Text("Not Affiliated"),
                                ),
                              ),
                      ],
                    ),
                  ),
                );
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("${snapshot.error}"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
    );
  }
}
