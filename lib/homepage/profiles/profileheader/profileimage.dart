import 'package:flutter/material.dart';

import 'dart:math';

import './profileinsightssummary.dart';
import '../../../models/insight.dart';

class CenteredWidgetInsightsBorder extends StatefulWidget {
  CenteredWidgetInsightsBorder({
    required this.middle,
    required this.insights,
    required this.shouldanimate,
    required this.backgroundstrokewidth,
    required this.foregroundstrokewidth,
  });
  final Widget middle;
  final List<Insight> insights;
  final bool shouldanimate;
  final double backgroundstrokewidth;
  final double foregroundstrokewidth;

  @override
  State createState() => _CenteredWidgetInsightsBorderInstance();
}

class _CenteredWidgetInsightsBorderInstance
    extends State<CenteredWidgetInsightsBorder> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        ProfileInsightSummary(
          insights: widget.insights,
          shouldanimate: widget.shouldanimate,
          backgroundstrokewidth: widget.backgroundstrokewidth,
          foregroundstrokewidth: widget.foregroundstrokewidth,
        ),
        widget.middle,
      ],
    );
  }
}
