import 'package:flutter/material.dart';
import 'dart:math';

import '../../../models/insight.dart';

class ProfileInsightSummary extends StatefulWidget {
  ProfileInsightSummary({
    required this.insights,
    required this.shouldanimate,
    required this.backgroundstrokewidth,
    required this.foregroundstrokewidth,
  });
  final List<Insight> insights;
  final bool shouldanimate;
  final double backgroundstrokewidth;
  final double foregroundstrokewidth;

  @override
  State createState() => _ProfileInsightSummary();
}

class _ProfileInsightSummary extends State<ProfileInsightSummary>
    with TickerProviderStateMixin {
  late AnimationController _insightanimationcontroller;

  late Animation insight1;
  late Animation insight2;
  late Animation insight3;
  late Animation insight4;
  late Animation insight5;

  @override
  void initState() {
    super.initState();
    _insightanimationcontroller = AnimationController(
      duration: Duration(seconds: 2),
      vsync: this,
    );
    if (widget.shouldanimate == true) {
      insight1 = Tween(
        begin: 0.0,
        end: widget.insights[0].insightpercentage,
      ).animate(
        CurvedAnimation(
          parent: _insightanimationcontroller,
          curve: Curves.easeIn,
        ),
      );
      if (widget.insights.length == 5) {
        insight2 = Tween<double>(
          begin: 0.0,
          end: widget.insights[1].insightpercentage,
        ).animate(
          CurvedAnimation(
            parent: _insightanimationcontroller,
            curve: Curves.easeIn,
          ),
        );
        insight3 = Tween<double>(
          begin: 0.0,
          end: widget.insights[2].insightpercentage,
        ).animate(
          CurvedAnimation(
            parent: _insightanimationcontroller,
            curve: Curves.easeIn,
          ),
        );
        insight4 = Tween<double>(
          begin: 0.0,
          end: widget.insights[3].insightpercentage,
        ).animate(
          CurvedAnimation(
            parent: _insightanimationcontroller,
            curve: Curves.easeIn,
          ),
        );
        insight5 = Tween<double>(
          begin: 0.0,
          end: widget.insights[4].insightpercentage,
        ).animate(
          CurvedAnimation(
            parent: _insightanimationcontroller,
            curve: Curves.easeIn,
          ),
        );
      }
      _insightanimationcontroller.forward();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _insightanimationcontroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _insightanimationcontroller,
      builder: (context, child) {
        return CustomPaint(
          child: Container(),
          foregroundPainter: (widget.insights.length == 5)
              ? InsightsPainter(
                  insight1: insight1.value,
                  insight2: insight2.value,
                  insight3: insight3.value,
                  insight4: insight4.value,
                  insight5: insight5.value,
                  insights: widget.insights,
                  backgroundstrokewidth: widget.backgroundstrokewidth,
                  foregroundstrokewidth: widget.foregroundstrokewidth,
                )
              : (widget.shouldanimate == true)
                  ? InsightsPainter(
                      insight1: insight1.value,
                      insights: widget.insights,
                      backgroundstrokewidth: widget.backgroundstrokewidth,
                      foregroundstrokewidth: widget.foregroundstrokewidth,
                    )
                  : InsightsPainter(
                      insight1: widget.insights[0].insightpercentage,
                      insights: widget.insights,
                      backgroundstrokewidth: widget.backgroundstrokewidth,
                      foregroundstrokewidth: widget.foregroundstrokewidth,
                    ),
        );
      },
    );
  }
}

class InsightsPainter extends CustomPainter {
  InsightsPainter({
    required this.insights,
    required this.insight1,
    required this.backgroundstrokewidth,
    required this.foregroundstrokewidth,
    this.insight2,
    this.insight3,
    this.insight4,
    this.insight5,
  });
  final List<Insight> insights;
  double insight1;
  double? insight2;
  double? insight3;
  double? insight4;
  double? insight5;
  double backgroundstrokewidth;
  double foregroundstrokewidth;

  @override
  void paint(Canvas canvas, Size size) {
    final backgroundPaint = Paint()
      ..color = Colors.grey.withOpacity(0.4)
      ..strokeWidth = backgroundstrokewidth
      ..style = PaintingStyle.stroke;
    final Offset center = size.center(Offset.zero);
    final Size constrainedSize = size - Offset(5, 5) as Size;
    final shortestSide = min(constrainedSize.width, constrainedSize.height);

    final double startangle = -(pi * 0.5);
    double radius = shortestSide / 4;
    int index = 0;
    double? sweep;

    insights.forEach((insight) {
      (index == 0)
          ? sweep = insight1
          : (index == 1)
              ? sweep = insight2
              : (index == 2)
                  ? sweep = insight3
                  : (index == 3)
                      ? sweep = insight4
                      : (index == 4)
                          ? sweep = insight5
                          : sweep = 0.0;
      final foregroundpaint = Paint()
        ..color = insight.paintcolor!
        ..strokeWidth = foregroundstrokewidth
        ..strokeCap = StrokeCap.round
        ..style = PaintingStyle.stroke;
      final double endangle = (2 * pi * sweep!);
      canvas.drawCircle(center, radius, backgroundPaint);
      canvas.drawArc(Rect.fromCircle(center: center, radius: radius),
          startangle, endangle, false, foregroundpaint);
      radius = radius - (0.03 * shortestSide);
      index = index + 1;
    });
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
