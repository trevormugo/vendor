import 'dart:convert';
import 'dart:ui';

import "package:collection/collection.dart";
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:math';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/qrpage/qrscanner.dart';
import 'package:vendor/homepage/profiles/profiletabs/affiliatedshops.dart';
import 'package:vendor/homepage/profiles/settings/deliverylocationssetting.dart';
import 'package:vendor/homepage/profiles/settings/editprofile.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/models/account.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/defaults.dart';

import '../../../models/profileQueries/profilequeries.dart';

import '../../main.dart';
import '../../restapi.dart';
import './profileheader/profileimage.dart';
import './profiletabs/information.dart';
import './profiletabs/favorites.dart';
import './profiletabs/shops.dart';
import '../../models/insight.dart';

class CustomerProfilePage extends StatefulWidget {
  CustomerProfilePage({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _CustomerProfilePageInstance();
}

class _CustomerProfilePageInstance extends State<CustomerProfilePage>
    with TickerProviderStateMixin {
  TabController? _tabcontroller;
  ScrollController? _scrollController;
  late Future<ProfileQueries> _fetchProfileDetails;
  ProfileQueries? profileQueries;
  List<InlineSpan>? roles = [];
  bool _operationinprogress = false;

  List<Insight> insights = [
    Insight(
      paintcolor: Colors.amber,
      insightpercentage: 0.67,
      category: 'fashion',
      icon: Icon(
        Icons.checkroom_sharp,
        color: Colors.amber,
      ),
    ),
    Insight(
      paintcolor: Colors.redAccent[200],
      insightpercentage: 0.22,
      category: 'games',
      icon: Icon(
        Icons.games_outlined,
        color: Colors.redAccent[200],
      ),
    ),
    Insight(
      paintcolor: Colors.lightGreenAccent,
      insightpercentage: 0.53,
      category: 'food',
      icon: Icon(
        Icons.restaurant,
        color: Colors.lightGreenAccent,
      ),
    ),
    Insight(
      paintcolor: Colors.deepPurple,
      insightpercentage: 0.83,
      category: 'electronics',
      icon: Icon(
        Icons.computer_sharp,
        color: Colors.deepPurple,
      ),
    ),
    Insight(
      paintcolor: Colors.yellow,
      insightpercentage: 0.70,
      category: 'services',
      icon: Icon(
        Icons.miscellaneous_services,
        color: Colors.yellow,
      ),
    ),
  ];

  @override
  void initState() {
    super.initState();
    _tabcontroller = TabController(length: 3, vsync: this);
    _scrollController = ScrollController();
    _fetchProfileDetails = fetchProfileDetails();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<ProfileQueries> fetchProfileDetails() async {
    var oneProfile = await RestApi().profileQuery(
        "/accountspool/profileQuery/${widget.userId}", widget.token);
    if (oneProfile.statusCode == 200) {
      print("${oneProfile.body.toString()}");
      profileQueries = ProfileQueries.fromJson(json.decode(oneProfile.body));
      profileQueries!.roles.forEach((element) {
        roles!.add(
          TextSpan(
            text: '$element | ',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black,
              fontWeight: FontWeight.w800,
            ),
          ),
        );
      });
      return Future.value(profileQueries);
    } else if (oneProfile.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${oneProfile.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Unexpected Error ${oneProfile.statusCode}",
        desc: 'User not found',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      return Future.error('Error');
    }
  }

  void registerRider(String userId) async {
    setState(() {
      _operationinprogress = true;
    });
    RegisterRoleRequest registerRoleRequest = RegisterRoleRequest(
      id: userId,
    );
    var response = await RestApi().registerrider(
        "/accountspool/registerrider", registerRoleRequest, widget.token);
    if (response.statusCode == 200) {
      setState(() {
        _operationinprogress = false;
        _fetchProfileDetails = fetchProfileDetails();
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'You are now a rider',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  @override
  void dispose() {
    _tabcontroller!.dispose();
    _scrollController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void deliverylocationssettings() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DeliveryLocationSetting(
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void editprofilesettings(Account account) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EditProfile(
            token: widget.token,
            userId: widget.userId,
            userAccount: account,
          ),
        ),
      );
    }

    void navigateToQrScanner(Account account) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => QrScannerPage(
            token: widget.token,
            userId: widget.userId,
            connString: '',
            orderType: 'delivery fee payment',
            userName: "Scan rider's code",
          ),
        ),
      );
    }

    void showSettingsModal() {
      showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        builder: (context) {
          return Container(
            child: (profileQueries == null)
                ? Center(
                    child: Text("Couldn't query profiles"),
                  )
                : ListView(
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      ListTile(
                        onTap: () => editprofilesettings(profileQueries!.user),
                        leading: Icon(
                          LineIcons.userEdit,
                        ),
                        title: Text('Edit Profile'),
                        trailing: Icon(
                          LineIcons.angleRight,
                        ),
                      ),
                      ListTile(
                        onTap: deliverylocationssettings,
                        leading: Icon(
                          LineIcons.mapMarker,
                        ),
                        title: Text('Delivery Locations'),
                        trailing: Icon(
                          LineIcons.angleRight,
                        ),
                      ),
                      /*ListTile(
                        leading: Icon(
                          LineIcons.creditCard,
                        ),
                        title: Text('Payment Settings'),
                        trailing: Icon(
                          LineIcons.angleRight,
                        ),
                      ),*/
                      ListTile(
                        onTap: () => navigateToQrScanner(profileQueries!.user),
                        leading: Icon(
                          Icons.qr_code_sharp,
                        ),
                        title: Text('Pay Delivery'),
                        trailing: Icon(
                          LineIcons.angleRight,
                        ),
                      ),
                      ListTile(
                        leading: Icon(
                          LineIcons.moneyBill,
                        ),
                        title: Text('Subscriptions'),
                        trailing: Icon(
                          LineIcons.angleRight,
                        ),
                      ),
                      /*ListTile(
                  leading: Icon(
                    LineIcons.coins,
                  ),
                  title: Text('Tokens'),
                  trailing: Icon(
                    LineIcons.angleRight,
                  ),
                ),*/
                    ],
                  ),
          );
        },
      );
    }

    return Stack(children: [
      Scaffold(
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: FutureBuilder(
              future: _fetchProfileDetails,
              builder: (context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return DefaultTabController(
                    length: 3,
                    initialIndex: 0,
                    child: NestedScrollView(
                      controller: _scrollController,
                      headerSliverBuilder: (context, value) {
                        return [
                          SliverAppBar(
                            floating: true,
                            snap: false,
                            pinned: true,
                            forceElevated: value,
                            expandedHeight: screenheight * 0.7,
                            actions: [
                              GestureDetector(
                                onTap: showSettingsModal,
                                child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Icon(
                                    LineAwesomeIcons.cog,
                                  ),
                                ),
                              ),
                            ],
                            flexibleSpace: FlexibleSpaceBar(
                              collapseMode: CollapseMode.parallax,
                              centerTitle: true,
                              background: Wrap(
                                alignment: WrapAlignment.center,
                                runAlignment: WrapAlignment.center,
                                children: <Widget>[
                                  Container(
                                    height: screenheight * 0.5,
                                    width: screenwidth,
                                    child: CenteredWidgetInsightsBorder(
                                      insights: insights,
                                      /*[
                                        Insight(
                                          paintcolor: Colors.amber,
                                          insightpercentage: 0.67,
                                          category: profileQueries!
                                              .vendorSales!
                                              .purchases![0]
                                              .categories!
                                              .categoryName!,
                                          icon: Icon(
                                            Icons.checkroom_sharp,
                                            color: Colors.amber,
                                          ),
                                        ),
                                      ],*/
                                      shouldanimate: true,
                                      backgroundstrokewidth: 6,
                                      foregroundstrokewidth: 6,
                                      middle: Container(
                                        child: Center(
                                          child: CircleAvatar(
                                            maxRadius: (screenwidth * 0.2) / 2,
                                            foregroundImage: NetworkImage(
                                              Adress.myip +
                                                  "/accountspool/fetchAccountThumbnail?id=${widget.userId}",
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Wrap(
                                    alignment: WrapAlignment.center,
                                    runAlignment: WrapAlignment.center,
                                    direction: Axis.horizontal,
                                    children: [
                                      Container(
                                        height: screenheight * 0.1,
                                        width: screenwidth * 0.2,
                                        child: CenteredWidgetInsightsBorder(
                                          shouldanimate: true,
                                          insights: [insights[0]],
                                          middle: insights[0].icon,
                                          backgroundstrokewidth: 6,
                                          foregroundstrokewidth: 6,
                                        ),
                                      ),
                                      Container(
                                        height: screenheight * 0.1,
                                        width: screenwidth * 0.2,
                                        child: CenteredWidgetInsightsBorder(
                                          shouldanimate: true,
                                          insights: [insights[1]],
                                          middle: insights[1].icon,
                                          backgroundstrokewidth: 6,
                                          foregroundstrokewidth: 6,
                                        ),
                                      ),
                                      Container(
                                        height: screenheight * 0.1,
                                        width: screenwidth * 0.2,
                                        child: CenteredWidgetInsightsBorder(
                                          shouldanimate: true,
                                          insights: [insights[2]],
                                          middle: insights[2].icon,
                                          backgroundstrokewidth: 6,
                                          foregroundstrokewidth: 6,
                                        ),
                                      ),
                                      Container(
                                        height: screenheight * 0.1,
                                        width: screenwidth * 0.2,
                                        child: CenteredWidgetInsightsBorder(
                                          shouldanimate: true,
                                          insights: [insights[3]],
                                          middle: insights[3].icon,
                                          backgroundstrokewidth: 6,
                                          foregroundstrokewidth: 6,
                                        ),
                                      ),
                                      Container(
                                        height: screenheight * 0.1,
                                        width: screenwidth * 0.2,
                                        child: CenteredWidgetInsightsBorder(
                                          shouldanimate: true,
                                          insights: [insights[4]],
                                          middle: insights[4].icon,
                                          backgroundstrokewidth: 6,
                                          foregroundstrokewidth: 6,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            title: ListTile(
                              title: Text(
                                '${snapshot.data.user.userName}',
                                softWrap: true,
                                textAlign: TextAlign.start,
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                              subtitle: RichText(
                                text: TextSpan(
                                  children: roles,
                                ),
                              ),
                            ),
                            bottom: TabBar(
                              controller: _tabcontroller,
                              tabs: <Widget>[
                                Tab(
                                  icon: Icon(
                                    LineAwesomeIcons.info_circle,
                                  ),
                                ),
                                Tab(
                                  icon: Icon(
                                    LineAwesomeIcons.heart,
                                  ),
                                ),
                                Tab(
                                  icon: (snapshot.data.roles.contains("Rider"))
                                      ? Icon(
                                          LineAwesomeIcons.truck,
                                        )
                                      : (snapshot.data.roles.contains("Vendor"))
                                          ? Icon(
                                              LineAwesomeIcons.store,
                                            )
                                          : Icon(
                                              LineAwesomeIcons.briefcase,
                                            ),
                                ),
                              ],
                            ),
                          ),
                        ];
                      },
                      body: TabBarView(
                        controller: _tabcontroller,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                              top: 40,
                            ),
                            child: ProfileInformation(
                              queries: snapshot.data,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              top: 40,
                            ),
                            child: FavoritesTab(
                              shopsFavored: snapshot.data.shopFavorites,
                              token: widget.token,
                              userId: widget.userId,
                            ),
                          ),
                          /*(snapshot.data.roles.contains("Vendor"))
                              ? Container(
                                  margin: EdgeInsets.only(
                                    top: 40,
                                  ),
                                  child: ShopsTab(
                                    insights: insights,
                                  ),
                                )
                              : */
                          (snapshot.data.roles.contains("Rider"))
                              ? RiderAffiliationsTab(
                                  riderShopAffiliationsQuery:
                                      snapshot.data.riderShopAffiliationsQuery,
                                  token: widget.token,
                                )
                              : (snapshot.data.roles.contains("Customer") ||
                                      snapshot.data.roles.contains("Vendor"))
                                  ? Wrap(
                                      direction: Axis.vertical,
                                      alignment: WrapAlignment.spaceAround,
                                      runAlignment: WrapAlignment.center,
                                      children: <Widget>[
                                          ElevatedButton.icon(
                                            label: Text('Become a rider'),
                                            icon: Icon(
                                              LineAwesomeIcons.truck,
                                            ),
                                            onPressed: () =>
                                                registerRider(widget.userId),
                                            style: ElevatedButton.styleFrom(
                                              minimumSize:
                                                  Size(screenwidth * 0.8, 70),
                                              primary: Colors.green,
                                            ),
                                          ),
                                        ])
                                  : Container(),
                        ],
                      ),
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Icon(
                          LineIcons.server,
                          color: Colors.grey,
                          size: 70.0,
                        ),
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text("${snapshot.error}"),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Center(
                    child: SpinKitFadingCube(
                      size: 60,
                      color: Color.fromARGB(255, 27, 81, 211),
                    ),
                  );
                }
              }),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
