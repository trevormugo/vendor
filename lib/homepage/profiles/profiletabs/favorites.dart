import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:vendor/homepage/profiles/profiletabs/shoplocation/shoplocation.dart';
import 'package:vendor/homepage/shop/shopprofile.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';

import 'package:vendor/models/profileQueries/shopsfavored.dart';

import 'dart:math';

class FavoritesTab extends StatefulWidget {
  FavoritesTab({
    required this.shopsFavored,
    required this.token,
    required this.userId,
  });
  final List<ShopsFavored> shopsFavored;
  final String token;
  final String userId;
  @override
  State createState() => _FavoritesTabInstance();
}

class _FavoritesTabInstance extends State<FavoritesTab> {
  @override
  Widget build(BuildContext context) {
    void navigatetoshopprofile(String shopId) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopProfile(
            shopId: shopId,
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    void navigateToShopLocations(Shop shop) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopLocation(
            shop: shop,
          ),
        ),
      );
    }

    return (widget.shopsFavored.length == 0)
        ? Center(
            child: Text("No liked shops yet"),
          )
        : ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: widget.shopsFavored.length,
            itemBuilder: (contex, index) => Card(
              child: ListTile(
                leading: GestureDetector(
                  onTap: () => navigatetoshopprofile(
                      widget.shopsFavored[index].shop!.id),
                  child: Icon(
                    LineAwesomeIcons.store,
                  ),
                ),
                title: GestureDetector(
                  onTap: () => navigatetoshopprofile(
                      widget.shopsFavored[index].shop!.id),
                  child: Text('${widget.shopsFavored[index].shop!.shopName}'),
                ),
                subtitle: Text(
                    '${TimeConversion.readTimestamp(widget.shopsFavored[index].shopFavorObject!.timestamp!)}'),
                trailing: GestureDetector(
                  onTap: () =>
                      navigateToShopLocations(widget.shopsFavored[index].shop!),
                  child: Icon(LineAwesomeIcons.map_marker),
                ),
              ),
            ),
          );
  }
}
