import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

import 'dart:math';

import '../../../models/insight.dart';
import '../profileheader/profileimage.dart';

class ShopsTab extends StatefulWidget {
  ShopsTab({
    required this.insights,
  });
  final List<Insight> insights;
  @override
  State createState() => _ShopsTabInstance();
}

class _ShopsTabInstance extends State<ShopsTab> {


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: 5,
      itemBuilder: (contex, index) => Container(
        margin: EdgeInsets.only(
          bottom: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.white60,
          border: Border.all(
            color: Colors.black12,
            width: 1.0,
          ),
        ),
        width: double.infinity,
        height: screenheight * 0.5,
        child: GridTile(
          header: GridTileBar(
            leading: Icon(
              LineAwesomeIcons.store,
              color: Colors.black,
            ),
            title: Text(
              'name of store',
              softWrap: false,
              textAlign: TextAlign.center,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontWeight: FontWeight.w800,
              ),
            ),
            trailing: Icon(
              Icons.edit_outlined,
              color: Colors.black,
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Center(
              child: Icon(
                LineIcons.mapMarker,
                size: 140,
                color: Colors.black,
              ),
            ),
          ),
          footer: GridTileBar(
            title: Text(
              '12days ago',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
