import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/profileQueries/rideraffiliationsquery.dart';
import 'package:vendor/restapi.dart';

import 'shoplocation/shoplocation.dart';

class RiderAffiliationsTab extends StatefulWidget {
  RiderAffiliationsTab({
    required this.riderShopAffiliationsQuery,
    required this.token,
  });
  final List<RiderShopAffiliationsQuery> riderShopAffiliationsQuery;
  final String token;
  @override
  State createState() => _RiderAffiliationsTabInstance();
}

class _RiderAffiliationsTabInstance extends State<RiderAffiliationsTab> {
  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  void removeShopFromAffiliationRequest(String shopId, String riderId) async {
    var response = await RestApi().removeShopFromRiderAffiliation(
        "/ridermanagement/removeShopFromAffiliation?shopId=$shopId&riderId=$riderId",
        widget.token);
    if (response.statusCode == 204) {
      setState(() {
        widget.riderShopAffiliationsQuery
            .removeWhere((element) => element.affiliation!.riderId == riderId);
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'Rider unaffiliated from shop',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else if (response.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${response.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    void navigateToShopLocations(Shop shop) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ShopLocation(
            shop: shop,
          ),
        ),
      );
    }

    void confirmItemRemovalFromCart(String riderId, String vendorId) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
          title: Text('Unaffiliate youreself from this shop?'),
          actions: <Widget>[
            TextButton(
              child: Text(
                'No',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(
                'Yes',
                style: TextStyle(
                  color: Color.fromARGB(255, 27, 81, 211),
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              onPressed: () {
                removeShopFromAffiliationRequest(riderId, vendorId);
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    }

    return (widget.riderShopAffiliationsQuery.length == 0)
        ? Center(
            child: Text("No affiliated shops yet"),
          )
        : ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: widget.riderShopAffiliationsQuery.length,
            itemBuilder: (contex, index) => Card(
              child: ListTile(
                leading: Image.asset(
                  PlanImages.basic,
                  fit: BoxFit.cover,
                ),
                onTap: () => navigateToShopLocations(
                    widget.riderShopAffiliationsQuery[index].shops!),
                title: Text(
                    '${widget.riderShopAffiliationsQuery[index].shops!.shopName}'),
                subtitle: Text(
                    '${TimeConversion.readTimestamp(widget.riderShopAffiliationsQuery[index].affiliation!.timestamp!)}'),
                trailing: GestureDetector(
                  onTap: () => confirmItemRemovalFromCart(
                      widget.riderShopAffiliationsQuery[index].affiliation!
                          .riderId!,
                      widget.riderShopAffiliationsQuery[index].shops!.ownerId),
                  child: Icon(
                    LineAwesomeIcons.times_circle,
                    color: Colors.red,
                  ),
                ),
              ),
            ),
          );
  }
}
