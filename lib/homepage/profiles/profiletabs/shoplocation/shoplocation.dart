import 'dart:async';
import 'dart:math';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';

class ShopLocation extends StatefulWidget {
  ShopLocation({
    required this.shop,
  });
  final Shop shop;
  @override
  State createState() => _ShopLocationInstance();
}

class _ShopLocationInstance extends State<ShopLocation> {
  Completer<GoogleMapController> _controller = Completer();
  late Future<Position> determinePosition;
  static Marker? oneMarker;
  bool operationinprogress = false;
  late BitmapDescriptor shopLocationIcon;

  @override
  void initState() {
    determinePosition = _determinePosition();
    super.initState();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    await setCustomMapPin();
    if (!serviceEnabled) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc: 'Location services are disabled to continue',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Please Allow Location Permission',
          desc: 'Location permissions are required to continue',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Please Allow Location Permission',
        desc:
            'Location permissions are permanently denied, we cannot request permissions',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return Future.value(Geolocator.getCurrentPosition());
  }

  Future<void> setCustomMapPin() async {
    shopLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), PlanImages.basic);
    setState(() {
      oneMarker = Marker(
        markerId: MarkerId("oneMarker"),
        position: LatLng(widget.shop.latitude, widget.shop.longitude),
        icon: shopLocationIcon,
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              "Shop ${widget.shop.shopName}'s location",
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: Container(
        width: screenwidth,
        height: screenheight,
        child: FutureBuilder(
            future: determinePosition,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return (operationinprogress == true)
                    ? Center(
                        child: SpinKitFadingCube(
                          size: 60,
                          color: Color.fromARGB(255, 207, 118, 0),
                        ),
                      )
                    : Center(
                        child: GoogleMap(
                          mapType: MapType.normal,
                          initialCameraPosition: CameraPosition(
                            target: LatLng(
                              widget.shop.latitude,
                              widget.shop.longitude,
                            ),
                            zoom: 14.4746,
                          ),
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                          markers: (oneMarker == null) ? {} : {oneMarker!},
                        ),
                      );
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("${snapshot.error}"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
    );
  }
}
