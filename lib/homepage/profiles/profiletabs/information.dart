import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

import '../../../models/profileQueries/profilequeries.dart';

import '../../../models/applicationUser/applicationuser.dart';

import '../../shop/details/onedetail.dart';

class ProfileInformation extends StatefulWidget {
  ProfileInformation({
    required this.queries,
  });
  final ProfileQueries queries;
  @override
  State createState() => _ProfileImageInstance();
}

class _ProfileImageInstance extends State<ProfileInformation> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      children: <Widget>[
        OneDetail(
          icon: LineAwesomeIcons.identification_badge,
          detail: "${widget.queries.user.fullName}",
        ),
        OneDetail(
          icon: LineAwesomeIcons.at,
          detail: "${widget.queries.user.email}",
        ),
        OneDetail(
          icon: LineAwesomeIcons.phone,
          detail: "${widget.queries.user.phoneNumber}",
        ),
        (widget.queries.roles.contains("Vendor"))
            ? OneDetail(
                icon: LineAwesomeIcons.user_tag,
                detail: 'Vendor',
              )
            : (widget.queries.roles.contains("Rider"))
                ? OneDetail(
                    icon: LineAwesomeIcons.user_tag,
                    detail: 'Rider',
                  )
                : (widget.queries.roles.contains("Customer"))
                    ? OneDetail(
                        icon: LineAwesomeIcons.user_tag,
                        detail: 'Customer',
                      )
                    : OneDetail(
                        icon: LineAwesomeIcons.user_tag,
                        detail: 'Role not found',
                      ),
        OneDetail(
          icon: LineAwesomeIcons.heart,
          detail: 'Liked ${widget.queries.shopFavorites!.length} stores',
        ),
        OneDetail(
          icon: Icons.rate_review_outlined,
          detail: '${widget.queries.shopReviews!.length} ratings and reviews',
        ),
        OneDetail(
          icon: LineAwesomeIcons.shopping_bag,
          detail: '${widget.queries.vendorSales!.purchases!.length} purchases',
        ),
        (widget.queries.riderShopAffiliationsQuery!.length == 0)
            ? OneDetail(
                icon: Icons.work_off_outlined,
                detail: 'Not affiliated',
              )
            : OneDetail(
                icon: LineAwesomeIcons.briefcase,
                detail:
                    '${widget.queries.riderShopAffiliationsQuery!.length} affiliations',
              ),
        (widget.queries.user.emailConfirmed == true)
            ? OneDetail(
                icon: Icons.verified_user_outlined,
                detail: 'Authentication complete',
              )
            : OneDetail(
                icon: Icons.verified_user_outlined,
                detail: 'Not authenticated',
              ),
        (widget.queries.user.phoneNumberConfirmed == true)
            ? OneDetail(
                icon: LineAwesomeIcons.certificate,
                detail: 'Verified user',
              )
            : OneDetail(
                icon: LineAwesomeIcons.certificate,
                detail: 'Not verified',
              ),
      ],
    );
  }
}
