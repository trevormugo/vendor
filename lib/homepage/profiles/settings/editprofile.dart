import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/listtiles/itemtile.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/listtiles/shoptile.dart';
import 'package:vendor/homepage/shop/editShop/editshoplocation/editshoplocation.dart';
import 'package:vendor/ip.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/notifications/notificationsresponse.dart';
import 'package:vendor/models/oneShopQuery/oneshopquery.dart';
import 'package:vendor/models/shops.dart';
import 'package:vendor/models/updateaccountimage.dart';
import 'package:vendor/models/updateaccountrequest.dart';
import 'package:vendor/restapi.dart';

class EditProfile extends StatefulWidget {
  EditProfile({
    required this.token,
    required this.userId,
    required this.userAccount,
  });
  final String token;
  final String userId;
  final Account userAccount;

  @override
  State createState() => _EditProfileInstance();
}

class _EditProfileInstance extends State<EditProfile> {
  PageController pageViewController = PageController(initialPage: 0);
  final picker = ImagePicker();
  String dropdownValue = "Camera";
  File? image;
  ImageSource imagesrc = ImageSource.camera;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _fullnamecontroller = TextEditingController();
  TextEditingController _usernamecontroller = TextEditingController();
  TextEditingController _phonenumbercontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  bool _operationinprogress = false;
  late double shoplatitude;
  late double shoplongitude;

  @override
  void initState() {
    super.initState();
    _fullnamecontroller.text = widget.userAccount.fullName;
    _usernamecontroller.text = widget.userAccount.userName;
    _phonenumbercontroller.text = widget.userAccount.phoneNumber;
    _emailcontroller.text = widget.userAccount.email;
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  @override
  void dispose() {
    _fullnamecontroller.dispose();
    _usernamecontroller.dispose();
    _phonenumbercontroller.dispose();
    _emailcontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void updateAccountInfo() async {
      setState(() {
        _operationinprogress = true;
      });
      UpdateAccountRequest updateAccountRequest = UpdateAccountRequest(
        id: widget.userId,
        email: _emailcontroller.text,
        fullName: _fullnamecontroller.text,
        phoneNumber: _phonenumbercontroller.text,
        userName: _usernamecontroller.text,
      );
      var response = await RestApi().updateAccount(
          "/accountspool/updateAccount", updateAccountRequest, widget.token);
      if (response.statusCode == 200) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code ${response.statusCode}",
          desc: 'Account details were updated',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else if (response.statusCode == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error ${response.statusCode}",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else if (response.statusCode == 403) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Verification Error ${response.statusCode}",
          desc: 'Unauthorized',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error ${response.statusCode}",
          desc: '${response.body}',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      }
    }

    void updateAccountImage(File image) async {
      setState(() {
        _operationinprogress = true;
      });
      AccountThumbnailUploadRequest accountThumbnailUploadRequest =
          AccountThumbnailUploadRequest(
        userId: widget.userId,
        imageFile: image,
      );
      var response = await RestApi().replaceAccountImage(
          "/accountspool/replaceAccountImage?userId=${widget.userId}",
          accountThumbnailUploadRequest,
          widget.token);
      if (response == 200) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code $response",
          desc: 'Account image updated',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        Navigator.pop(context);
      } else if (response == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error response",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else if (response == 403) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Verification Error $response",
          desc: 'Unauthorized',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error $response",
          desc: 'Unexpected error occured',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      }
    }

    void addImage(File image) async {
      setState(() {
        _operationinprogress = true;
      });
      AccountThumbnailUploadRequest accountThumbnailUploadRequest =
          AccountThumbnailUploadRequest(
        userId: widget.userId,
        imageFile: image,
      );
      var response = await RestApi().uploadAccountImage(
          "/accountspool/uploadAccountImage?userId=${widget.userId}",
          accountThumbnailUploadRequest,
          widget.token);
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code $response",
        desc: 'Account image added',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      if (response == 200) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.SUCCES,
          animType: AnimType.BOTTOMSLIDE,
          title: "Code $response",
          desc: 'Account image added',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
        Navigator.pop(context);
      } else if (response == 401) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "App Defaults Error response",
          desc: 'Verification token expired , Log In required',
          btnCancelOnPress: () => logout(),
          btnOkOnPress: () => logout(),
        )..show();
      } else if (response == 403) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Verification Error $response",
          desc: 'Unauthorized',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: "Error $response",
          desc: 'Unecpected error occured',
          btnCancelOnPress: null,
          btnOkOnPress: null,
        )..show();
      }
    }

    Future editImage(bool isNew) async {
      final pickedFile = await picker.pickImage(source: imagesrc);
      if (pickedFile == null) {
        Fluttertoast.showToast(
            msg: "No image was selected",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 1,
            fontSize: 16.0);
      } else {
        setState(() {
          image = File(pickedFile.path);
        });

        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) => AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
            title:
                (isNew) ? Text('New shop image?') : Text('Update shop image?'),
            content: (image == null)
                ? Center(
                    child: Text("could not load image"),
                  )
                : ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: FittedBox(
                      child: Container(
                        height: screenheight * 0.5,
                        width: screenwidth * 0.8,
                        color: Color.fromARGB(255, 27, 81, 211),
                        child: Image.file(
                          image!,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
            actions: <Widget>[
              TextButton(
                child: Text(
                  'No',
                  style: TextStyle(
                    color: Color.fromARGB(255, 27, 81, 211),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text(
                  'Yes',
                  style: TextStyle(
                    color: Color.fromARGB(255, 27, 81, 211),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                onPressed: () {
                  (image == null)
                      ? Fluttertoast.showToast(
                          msg: "No image was selected",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.TOP,
                          timeInSecForIosWeb: 1,
                          fontSize: 16.0)
                      : (isNew == true)
                          ? addImage(image!)
                          : updateAccountImage(image!);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      }
    }

    return Stack(children: [
      Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            title: ListTile(
              title: Text(
                'Edit Account',
                softWrap: true,
                textAlign: TextAlign.start,
                overflow: TextOverflow.fade,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ),
        ),
        extendBodyBehindAppBar: true,
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: LayoutBuilder(builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: screenheight * 0.4,
                      width: double.infinity,
                      child: Center(
                        child: GestureDetector(
                          onTap: () {
                            (widget.userAccount.hasImage!)
                                ? editImage(true)
                                : editImage(false);
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: FittedBox(
                              child: Container(
                                height: screenheight * 0.5,
                                width: screenwidth * 0.8,
                                color: Color.fromARGB(255, 27, 81, 211),
                                child: (widget.userAccount.hasImage!)
                                    ? Icon(
                                        LineIcons.camera,
                                        color: Color.fromARGB(255, 207, 118, 0),
                                      )
                                    : Image.network(
                                        Adress.myip +
                                            "/accountspool/fetchAccountThumbnail?id=${widget.userAccount.id}",
                                        errorBuilder: (BuildContext context,
                                            Object exception,
                                            StackTrace? stackTrace) {
                                          return Image.asset(
                                            ErrorImage.noimage,
                                            fit: BoxFit.cover,
                                          );
                                        },
                                        fit: BoxFit.cover,
                                      ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: screenheight * 0.1,
                      width: double.infinity,
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 5,
                        style: TextStyle(
                          color: Color.fromARGB(255, 207, 118, 0),
                        ),
                        underline: Container(
                          height: 2,
                          color: Color.fromARGB(255, 27, 81, 211),
                        ),
                        onChanged: (String? newValue) {
                          if (newValue == "Gallery") {
                            setState(() {
                              dropdownValue = newValue!;
                              imagesrc = ImageSource.gallery;
                            });
                          } else {
                            setState(() {
                              dropdownValue = newValue!;
                              imagesrc = ImageSource.camera;
                            });
                          }
                        },
                        items: <String>['Camera', 'Gallery']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                              enabled: !_operationinprogress,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                              controller: _fullnamecontroller,
                              obscureText: false,
                              decoration: InputDecoration(
                                  labelStyle: TextStyle(
                                    color: Color.fromARGB(255, 27, 81, 211),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromARGB(255, 27, 81, 211),
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  labelText: "Enter your Full name"),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                              enabled: !_operationinprogress,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                              controller: _usernamecontroller,
                              obscureText: false,
                              decoration: InputDecoration(
                                  labelStyle: TextStyle(
                                    color: Color.fromARGB(255, 27, 81, 211),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromARGB(255, 27, 81, 211),
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  labelText: "Enter your user name"),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                              enabled: !_operationinprogress,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                              controller: _phonenumbercontroller,
                              obscureText: false,
                              decoration: InputDecoration(
                                  labelStyle: TextStyle(
                                    color: Color.fromARGB(255, 27, 81, 211),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromARGB(255, 27, 81, 211),
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  labelText:
                                      "Enter your phone number(+254 format)"),
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.all(20),
                            child: TextFormField(
                              enabled: !_operationinprogress,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                              controller: _emailcontroller,
                              obscureText: false,
                              decoration: InputDecoration(
                                  labelStyle: TextStyle(
                                    color: Color.fromARGB(255, 27, 81, 211),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromARGB(255, 27, 81, 211),
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  labelText: "Enter your email"),
                            ),
                          ),
                          TextButton(
                            onPressed: (_operationinprogress == true)
                                ? null
                                : updateAccountInfo,
                            child: Text(
                              "Update Account",
                              style: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
