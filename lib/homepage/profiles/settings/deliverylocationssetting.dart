import 'dart:convert';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/settings/deliverylocationlist/deliverylocationlist.dart';
import 'package:vendor/models/deliveryLocationsResponse/deliverylocationsresponse.dart';
import 'package:vendor/restapi.dart';
import 'package:vendor/models/updateDefaultLocationRequest.dart';

import '../../../main.dart';
import 'adddeliverylocation/adddeliverylocation.dart';

class DeliveryLocationSetting extends StatefulWidget {
  DeliveryLocationSetting({
    required this.userId,
    required this.token,
  });
  final String userId;
  final String token;
  @override
  State createState() => _DeliveryLocationSettingInstance();
}

class _DeliveryLocationSettingInstance extends State<DeliveryLocationSetting> {
  late Future<DeliveryLocationsResponse> _fetchDeliveryLocations;
  late DeliveryLocationsResponse _deliverylocations;

  @override
  void initState() {
    super.initState();
    _fetchDeliveryLocations = fetchDeliveryLocations();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future<DeliveryLocationsResponse> fetchDeliveryLocations() async {
    var deliveryLocations = await RestApi().getDeliveryLocations(
        "/items/getDeliveryLocations/${widget.userId}", widget.token);
    if (deliveryLocations.statusCode == 200) {
      print("${deliveryLocations.body}");
      _deliverylocations = DeliveryLocationsResponse.fromJson(
          json.decode(deliveryLocations.body));
      return Future.value(_deliverylocations);
    } else if (deliveryLocations.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${deliveryLocations.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
      return Future.error('Error  ${deliveryLocations.statusCode}');
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${deliveryLocations.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      return Future.error('Error  ${deliveryLocations.statusCode}');
    }
  }

  void addDeliveryLocation() {
    print("YDfdf");
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddDeiveryLocations(
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: ListTile(
            title: Text(
              'Delivery Locations',
              softWrap: true,
              textAlign: TextAlign.start,
              overflow: TextOverflow.fade,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: FutureBuilder(
          future: _fetchDeliveryLocations,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return (snapshot.data.deliveryLocations == null)
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Center(
                          child: Text("Delivery locations not specified"),
                        ),
                        TextButton(
                            onPressed: () => addDeliveryLocation,
                            child: Text("Add delivery location"))
                      ],
                    )
                  : DeliveryLocationList(
                      deliveryLocations: snapshot.data.deliveryLocations,
                      token: widget.token,
                      userId: widget.userId,
                    );
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Icon(
                      LineIcons.server,
                      color: Colors.grey,
                      size: 70.0,
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text("${snapshot.error}"),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: SpinKitFadingCube(
                  size: 60,
                  color: Color.fromARGB(255, 27, 81, 211),
                ),
              );
            }
          }),
    );
  }
}
