import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/profiles/settings/adddeliverylocation/adddeliverylocation.dart';
import 'package:vendor/main.dart';
import 'package:vendor/models/customerCartResponse/deliverylocations.dart';
import 'package:vendor/models/updateDefaultLocationRequest.dart';
import 'package:vendor/restapi.dart';

class DeliveryLocationList extends StatefulWidget {
  DeliveryLocationList({
    required this.deliveryLocations,
    required this.userId,
    required this.token,
    this.callback,
  });
  final List<DeliveryLocations> deliveryLocations;
  final String userId;
  final String token;
  final Function? callback;
  @override
  State createState() => _DeliveryLocationListInstance();
}

class _DeliveryLocationListInstance extends State<DeliveryLocationList> {
  int selectedIndex = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  Future updateDefaultLocation(String id) async {
    UpdateDefaultLocationRequest updateDefaultLocationRequest =
        UpdateDefaultLocationRequest(
      chosenId: id,
      customerId: widget.userId,
    );
    var deliveryLocations = await RestApi().changeDefaultDeliveryLocation(
        "/items/changeDefaultDeliveryLocation",
        widget.token,
        updateDefaultLocationRequest);
    if (deliveryLocations.statusCode == 204) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Status ${deliveryLocations.statusCode}",
        desc: 'Default delivery location updated',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
      if (widget.callback != null) {
        widget.callback!();
      }
    } else if (deliveryLocations.statusCode == 401) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "App Defaults Error ${deliveryLocations.statusCode}",
        desc: 'Verification token expired , Log In required',
        btnCancelOnPress: () => logout(),
        btnOkOnPress: () => logout(),
      )..show();
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${deliveryLocations.statusCode}",
        desc: 'Unexpected Error',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    void addDeliveryLocation() {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddDeiveryLocations(
            token: widget.token,
            userId: widget.userId,
          ),
        ),
      );
    }

    return SafeArea(
      top: true,
      bottom: false,
      left: false,
      right: false,
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          itemCount: widget.deliveryLocations.length + 1,
          itemBuilder: (BuildContext context, int index) {
            if (index != (widget.deliveryLocations.length + 1) - 1) {
              if (widget.deliveryLocations[index].defaultLocation == true) {
                selectedIndex = index;
              }
            }

            return (index == 0)
                ? TextButton(
                    onPressed: () => addDeliveryLocation(),
                    child: Text("Add Delivery Location"),
                  )
                : ListTile(
                    selected: (selectedIndex == index - 1) ? true : false,
                    onTap: () => (selectedIndex == index - 1)
                        ? null
                        : updateDefaultLocation(
                            widget.deliveryLocations[index - 1].id),
                    leading: Icon(
                      LineIcons.mapMarker,
                    ),
                    title: Text(
                        '${widget.deliveryLocations[index - 1].locationTag}'),
                    trailing: (selectedIndex == index - 1)
                        ? Icon(
                            LineIcons.check,
                            color: Colors.green,
                          )
                        : null,
                  );
          }),
    );
  }
}
