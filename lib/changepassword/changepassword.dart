import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

import '../restapi.dart';
import '../main.dart';

class PasswordChange extends StatefulWidget {
  PasswordChange({required this.email});
  final String email;
  @override
  _PasswordChangeInstance createState() => _PasswordChangeInstance();
}

class _PasswordChangeInstance extends State<PasswordChange> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _newpasswordcontroller = TextEditingController();
  TextEditingController _repeatpasswordcontroller = TextEditingController();
  late Map<String, String> resetpassreq;
  void _changepassword() async {
    if (_formKey.currentState!.validate()) {
      if (_repeatpasswordcontroller.text == _newpasswordcontroller.text) {
        resetpassreq = {
          "Email": widget.email,
          "Password": _newpasswordcontroller.text
        };
        var response = await RestApi()
            .resetpassword("/useraccounts/resetpassword", resetpassreq);
        if (response.statusCode == 200) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Success',
            desc: 'Successfuly changed password',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => LoginPage(),
            ),
          );
        } else if (response.statusCode == 400) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: 'Account doesnt exist',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: 'Server error',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      } else {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Error',
          desc: 'Passwprds dont match',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      }
    } else {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Input required',
        desc: 'please provide input',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Text("Change Password"),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(
          builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 50,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _repeatpasswordcontroller,
                          obscureText: false,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter your new password"),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 50,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _newpasswordcontroller,
                          obscureText: false,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Repeat new password"),
                        ),
                      ),
                      TextButton(
                        onPressed: _changepassword,
                        child: Text(
                          "Change Password",
                          style: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
