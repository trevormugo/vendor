import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:vendor/models/defaults.dart';
import 'dart:math';

import './homepage/shop/oneitem/oneitem.dart';
import './models/shops.dart';
import 'ip.dart';
import './models/oneShopQuery/items.dart';

class HorizontalItems extends StatefulWidget {
  HorizontalItems({
    this.items,
    required this.token,
    required this.userId,
  });
  final List<Items>? items;
  final String token;
  final String userId;
  @override
  _HorizontalItemsInstance createState() => _HorizontalItemsInstance();
}

class _HorizontalItemsInstance extends State<HorizontalItems> {
  void _ontap(index) {}

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    void navigate(String itemId) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneItem(
                  icon: Icon(
                    Icons.restaurant,
                    color: Colors.lightGreenAccent,
                  ),
                  itemId: itemId,
                  userId: widget.userId,
                  token: widget.token,
                )),
      );
    }

    return Container(
      height: screenheight * 0.33,
      width: double.infinity,
      margin: EdgeInsets.only(
        bottom: 5,
      ),
      child: ListView.builder(
        itemCount: widget.items!.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () => navigate(widget.items![index].id),
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: FittedBox(
                        fit: BoxFit.cover,
                        child: Container(
                          height: screenheight * 0.22,
                          width: screenwidth * 0.5,
                          color: Colors.transparent,
                          child: GestureDetector(
                            onTap: null,
                            child: Image(
                              fit: BoxFit.cover,
                              image: NetworkImage(Adress.myip +
                                  "/items/fetchuploadthumbnailbyitemid?id=${widget.items![index].id}"),
                              errorBuilder: (BuildContext context,
                                  Object exception, StackTrace? stackTrace) {
                                return Image.asset(
                                  ErrorImage.noimage,
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: screenheight * 0.06,
                      width: screenwidth * 0.5,
                      child: ListTile(
                        title: Text(
                          "${widget.items![index].itemName}",
                          overflow: TextOverflow.ellipsis,
                        ),
                        subtitle: Text("${widget.items![index].ammount} ksh"),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
