import 'dart:convert';
import 'dart:ui';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/maps.dart';

import '../main.dart';
import '../models/logInRequest/loginrequest.dart';
import '../models/signUpRequest/signuprequest.dart';
import '../models/applicationUser/applicationuser.dart';
import '../changepassword/changepassword.dart';
import '../restapi.dart';

class VerifyCode extends StatefulWidget {
  VerifyCode(
      {required this.accountcreation,
      required this.emailrecipient,
      this.signupbody});
  final bool accountcreation;
  final String emailrecipient;
  final SignUpRequest? signupbody;
  @override
  _VerifyCodeInstance createState() => _VerifyCodeInstance();
}

class _VerifyCodeInstance extends State<VerifyCode> {
  TextEditingController _codecontroller = TextEditingController();
  bool _operationinprogress = false;
  int counter = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _sendcode() async {
    setState(() {
      _operationinprogress = true;
    });
    if (counter == 4) {
      if (widget.accountcreation == true) {
        var deletedresponse = await RestApi().deleteverificationresource(
            '/verification/deleteresource?email=${widget.emailrecipient}');
        if (deletedresponse.statusCode == 204) {
          setState(() {
            _operationinprogress = false;
          });
          AwesomeDialog(
            context: context,
            dialogType: DialogType.WARNING,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Bad Request',
            desc: 'No more attempts',
            btnCancelOnPress: () {
              Navigator.pop(context);
            },
            btnOkOnPress: () {
              Navigator.pop(context);
            },
          )..show();
        } else {
          setState(() {
            _operationinprogress = false;
          });
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Server error',
            desc: 'Bad Request , No more attempts',
            btnCancelOnPress: () {
              Navigator.pop(context);
            },
            btnOkOnPress: () {
              Navigator.pop(context);
            },
          )..show();
        }
      } else {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.WARNING,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Bad Request',
          desc: 'No more attempts',
          btnCancelOnPress: () {
            Navigator.pop(context);
          },
          btnOkOnPress: () {
            Navigator.pop(context);
          },
        )..show();
      }
    } else {
      if (_codecontroller.text == "") {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Bad Request',
          desc: 'Please Input Something',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      } else {
        var response = await RestApi().verifycode(
            "/verification/verifyCode?email=${widget.emailrecipient}&code=${_codecontroller.text}");
        if (response.statusCode == 200) {
          if (widget.accountcreation == false) {
            setState(() {
              _operationinprogress = false;
            });
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => PasswordChange(
                  email: widget.emailrecipient,
                ),
              ),
            );
          } else {
            var signupresponse = await RestApi()
                .signup("/accountspool/signup", widget.signupbody!);
            if (signupresponse.statusCode == 201) {
              //successful signup now log in user
              SharedPreferences prefs = await SharedPreferences.getInstance();
              await prefs.setBool('firstRun', true);
              setState(() {
                _operationinprogress = false;
              });
              AwesomeDialog(
                context: context,
                dialogType: DialogType.SUCCES,
                animType: AnimType.BOTTOMSLIDE,
                title: 'Success',
                desc: 'Log In with youre credentials',
                btnCancelOnPress: () {},
                btnOkOnPress: () {},
              )..show();
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => LoginPage()),
              );
            } else {
              setState(() {
                _operationinprogress = false;
              });
              AwesomeDialog(
                context: context,
                dialogType: DialogType.ERROR,
                animType: AnimType.BOTTOMSLIDE,
                title: 'Bad Request Code ${signupresponse.statusCode}',
                desc: '${signupresponse.body}',
                btnCancelOnPress: () {
                  Navigator.pop(context);
                },
                btnOkOnPress: () {
                  Navigator.pop(context);
                },
              )..show();
            }
          }
        } else {
          counter++;
          setState(() {
            _operationinprogress = false;
          });
          AwesomeDialog(
            context: context,
            dialogType: DialogType.WARNING,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Incorrect Code',
            desc: 'Attempts (${4 - counter})',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            title: Text("Verify code"),
          ),
        ),
        extendBodyBehindAppBar: true,
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minWidth: constraints.maxWidth,
                      minHeight: constraints.maxHeight),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 50,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          enabled: !_operationinprogress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _codecontroller,
                          keyboardType: TextInputType.number,
                          obscureText: false,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter you\'re code"),
                        ),
                      ),
                      TextButton(
                        onPressed:
                            (_operationinprogress == true) ? null : _sendcode,
                        child: Text(
                          "Verify Code",
                          style: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
