// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inventoriesresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InventoriesResponse _$InventoriesResponseFromJson(Map<String, dynamic> json) {
  return InventoriesResponse(
    items: (json['items'] as List<dynamic>)
        .map((e) => Categories.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$InventoriesResponseToJson(
        InventoriesResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
