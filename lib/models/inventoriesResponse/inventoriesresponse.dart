import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/oneShopQuery/categories.dart';



part 'inventoriesresponse.g.dart';

@JsonSerializable()
class InventoriesResponse {
  final List<Categories> items;

  InventoriesResponse({
    required this.items,
  });

  factory InventoriesResponse.fromJson(Map<String, dynamic> json) =>
      _$InventoriesResponseFromJson(json);
}
