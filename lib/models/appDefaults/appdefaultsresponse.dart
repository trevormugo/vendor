import 'package:json_annotation/json_annotation.dart';

import './vendorplans.dart';
import './visibilityradius.dart';

part 'appdefaultsresponse.g.dart';

@JsonSerializable()
class AppDefaultsResponse {
  final VendorPlans plans;
  final VisibilityRadius visibility;

  AppDefaultsResponse({
    required this.plans,
    required this.visibility,
  });

  factory AppDefaultsResponse.fromJson(Map<String, dynamic> json) => _$AppDefaultsResponseFromJson(json);
}