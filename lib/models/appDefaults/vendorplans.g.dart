// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vendorplans.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VendorPlans _$VendorPlansFromJson(Map<String, dynamic> json) {
  return VendorPlans(
    basic: json['basic'] as String?,
    advanced: json['advanced'] as String?,
    enterprise: json['enterprise'] as String?,
  );
}

Map<String, dynamic> _$VendorPlansToJson(VendorPlans instance) =>
    <String, dynamic>{
      'basic': instance.basic,
      'advanced': instance.advanced,
      'enterprise': instance.enterprise,
    };
