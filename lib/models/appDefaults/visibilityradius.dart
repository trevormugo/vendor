import 'package:json_annotation/json_annotation.dart';

part 'visibilityradius.g.dart';

@JsonSerializable()
class VisibilityRadius {
  final int basic;
  final int advanced;

  VisibilityRadius({
    required this.basic,
    required this.advanced,
  });

  factory VisibilityRadius.fromJson(Map<String, dynamic> json) =>  _$VisibilityRadiusFromJson(json);
}