// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appdefaultsresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppDefaultsResponse _$AppDefaultsResponseFromJson(Map<String, dynamic> json) {
  return AppDefaultsResponse(
    plans: VendorPlans.fromJson(json['plans'] as Map<String, dynamic>),
    visibility:
        VisibilityRadius.fromJson(json['visibility'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AppDefaultsResponseToJson(
        AppDefaultsResponse instance) =>
    <String, dynamic>{
      'plans': instance.plans,
      'visibility': instance.visibility,
    };
