// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'visibilityradius.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VisibilityRadius _$VisibilityRadiusFromJson(Map<String, dynamic> json) {
  return VisibilityRadius(
    basic: json['basic'] as int,
    advanced: json['advanced'] as int,
  );
}

Map<String, dynamic> _$VisibilityRadiusToJson(VisibilityRadius instance) =>
    <String, dynamic>{
      'basic': instance.basic,
      'advanced': instance.advanced,
    };
