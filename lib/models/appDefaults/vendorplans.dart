import 'package:json_annotation/json_annotation.dart';

part 'vendorplans.g.dart';

@JsonSerializable()
class VendorPlans {
  final String? basic;
  final String? advanced;
  final String? enterprise;

  VendorPlans({this.basic, this.advanced, this.enterprise});

  factory VendorPlans.fromJson(Map<String, dynamic> json) => _$VendorPlansFromJson(json);
}
