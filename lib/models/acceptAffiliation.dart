class AcceptAffiliation {
  final String userId;
  final String vendorId;

  AcceptAffiliation({
    required this.userId,
    required this.vendorId,
  });

  Map<String, dynamic> toJson() => {
        'UserId': userId,
        'VendorId': vendorId,
      };
}
