import 'package:json_annotation/json_annotation.dart';

part 'supervisoraffiliations.g.dart';

@JsonSerializable()
class SuperVisorAffiliations {
  final String? id;
  final String? superVisorId;
  final String? shopId;
  final bool? active;
  final int? timestamp;
  SuperVisorAffiliations({
    this.id,
    this.superVisorId,
    this.shopId,
    this.active,
    this.timestamp,
  });

  factory SuperVisorAffiliations.fromJson(Map<String, dynamic> json) =>
      _$SuperVisorAffiliationsFromJson(json);
}
