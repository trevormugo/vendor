import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';
import 'package:vendor/models/supervisorsTabQuery/onesupervisoraffiliation.dart';
import 'package:vendor/models/supervisorsTabQuery/onesupervisoraffiliationrequest.dart';

part 'supervisorstabquaery.g.dart';

@JsonSerializable()
class SuperVisorsTabQuery {
  final List<OneSuperVisorAffiliation>? affiliatedSuperVisors;
  final List<OneSuperVisorAffiliationRequest>? affiliationRequestSent;
  final List<ShopWithPlans>? shopWithPlan;
  SuperVisorsTabQuery({
    this.affiliatedSuperVisors,
    this.affiliationRequestSent,
    this.shopWithPlan,
  });

  factory SuperVisorsTabQuery.fromJson(Map<String, dynamic> json) => _$SuperVisorsTabQueryFromJson(json);
  
}
 