// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'supervisorstabquaery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuperVisorsTabQuery _$SuperVisorsTabQueryFromJson(Map<String, dynamic> json) {
  return SuperVisorsTabQuery(
    affiliatedSuperVisors: (json['affiliatedSuperVisors'] as List<dynamic>?)
        ?.map(
            (e) => OneSuperVisorAffiliation.fromJson(e as Map<String, dynamic>))
        .toList(),
    affiliationRequestSent: (json['affiliationRequestSent'] as List<dynamic>?)
        ?.map((e) =>
            OneSuperVisorAffiliationRequest.fromJson(e as Map<String, dynamic>))
        .toList(),
    shopWithPlan: (json['shopWithPlan'] as List<dynamic>?)
        ?.map((e) => ShopWithPlans.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$SuperVisorsTabQueryToJson(
        SuperVisorsTabQuery instance) =>
    <String, dynamic>{
      'affiliatedSuperVisors': instance.affiliatedSuperVisors,
      'affiliationRequestSent': instance.affiliationRequestSent,
      'shopWithPlan': instance.shopWithPlan,
    };
