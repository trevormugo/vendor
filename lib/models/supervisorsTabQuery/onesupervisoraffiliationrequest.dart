import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/profileQueries/rideraffiliations.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';
import 'package:vendor/models/supervisorsTabQuery/supervisoraffiliations.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';

part 'onesupervisoraffiliationrequest.g.dart';

@JsonSerializable()
class OneSuperVisorAffiliationRequest {
  final Account? supervisor;
  final SuperVisorAffiliations? affiliation;
  final List<ShopWithPlans>? shopWithPlan;
  OneSuperVisorAffiliationRequest({
    this.supervisor,
    this.affiliation,
    this.shopWithPlan,
  });

  factory OneSuperVisorAffiliationRequest.fromJson(Map<String, dynamic> json) => _$OneSuperVisorAffiliationRequestFromJson(json);
  
}
