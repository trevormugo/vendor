// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'onesupervisoraffiliation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneSuperVisorAffiliation _$OneSuperVisorAffiliationFromJson(
    Map<String, dynamic> json) {
  return OneSuperVisorAffiliation(
    supervisor: json['supervisor'] == null
        ? null
        : Account.fromJson(json['supervisor'] as Map<String, dynamic>),
    affiliation: json['affiliation'] == null
        ? null
        : SuperVisorAffiliations.fromJson(
            json['affiliation'] as Map<String, dynamic>),
    shopWithPlan: (json['shopWithPlan'] as List<dynamic>?)
        ?.map((e) => ShopWithPlans.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OneSuperVisorAffiliationToJson(
        OneSuperVisorAffiliation instance) =>
    <String, dynamic>{
      'supervisor': instance.supervisor,
      'affiliation': instance.affiliation,
      'shopWithPlan': instance.shopWithPlan,
    };
