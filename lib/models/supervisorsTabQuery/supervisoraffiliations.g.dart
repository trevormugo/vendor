// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'supervisoraffiliations.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuperVisorAffiliations _$SuperVisorAffiliationsFromJson(
    Map<String, dynamic> json) {
  return SuperVisorAffiliations(
    id: json['id'] as String?,
    superVisorId: json['superVisorId'] as String?,
    shopId: json['shopId'] as String?,
    active: json['active'] as bool?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$SuperVisorAffiliationsToJson(
        SuperVisorAffiliations instance) =>
    <String, dynamic>{
      'id': instance.id,
      'superVisorId': instance.superVisorId,
      'shopId': instance.shopId,
      'active': instance.active,
      'timestamp': instance.timestamp,
    };
