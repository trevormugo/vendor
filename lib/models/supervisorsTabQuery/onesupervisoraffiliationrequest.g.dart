// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'onesupervisoraffiliationrequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneSuperVisorAffiliationRequest _$OneSuperVisorAffiliationRequestFromJson(
    Map<String, dynamic> json) {
  return OneSuperVisorAffiliationRequest(
    supervisor: json['supervisor'] == null
        ? null
        : Account.fromJson(json['supervisor'] as Map<String, dynamic>),
    affiliation: json['affiliation'] == null
        ? null
        : SuperVisorAffiliations.fromJson(
            json['affiliation'] as Map<String, dynamic>),
    shopWithPlan: (json['shopWithPlan'] as List<dynamic>?)
        ?.map((e) => ShopWithPlans.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OneSuperVisorAffiliationRequestToJson(
        OneSuperVisorAffiliationRequest instance) =>
    <String, dynamic>{
      'supervisor': instance.supervisor,
      'affiliation': instance.affiliation,
      'shopWithPlan': instance.shopWithPlan,
    };
