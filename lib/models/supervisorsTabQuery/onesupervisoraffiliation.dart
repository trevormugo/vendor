import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/profileQueries/rideraffiliations.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';
import 'package:vendor/models/supervisorsTabQuery/supervisoraffiliations.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';

part 'onesupervisoraffiliation.g.dart';

@JsonSerializable()
class OneSuperVisorAffiliation {
  final Account? supervisor;
  final SuperVisorAffiliations? affiliation;
  final List<ShopWithPlans>? shopWithPlan;
  OneSuperVisorAffiliation({
    this.supervisor,
    this.affiliation,
    this.shopWithPlan,
  });

  factory OneSuperVisorAffiliation.fromJson(Map<String, dynamic> json) => _$OneSuperVisorAffiliationFromJson(json);
  
}
