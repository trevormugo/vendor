import 'package:json_annotation/json_annotation.dart';

import './responseitems.dart';

part 'homeshopquery.g.dart';


@JsonSerializable()
class HomeShopsQuery {
  final List<ResponseItems>? items;

  HomeShopsQuery({
    this.items,
  });

  factory HomeShopsQuery.fromJson(Map<String, dynamic> json) => _$HomeShopsQueryFromJson(json);
}
