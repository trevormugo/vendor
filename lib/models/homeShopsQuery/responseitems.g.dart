// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'responseitems.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseItems _$ResponseItemsFromJson(Map<String, dynamic> json) {
  return ResponseItems(
    planName: json['planName'] as String,
    noOfShopsQueried: json['noOfShopsQueried'] as int,
    shops: (json['shops'] as List<dynamic>?)
        ?.map((e) => Shop.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ResponseItemsToJson(ResponseItems instance) =>
    <String, dynamic>{
      'planName': instance.planName,
      'noOfShopsQueried': instance.noOfShopsQueried,
      'shops': instance.shops,
    };
