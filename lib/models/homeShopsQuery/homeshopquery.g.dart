// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'homeshopquery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeShopsQuery _$HomeShopsQueryFromJson(Map<String, dynamic> json) {
  return HomeShopsQuery(
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => ResponseItems.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$HomeShopsQueryToJson(HomeShopsQuery instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
