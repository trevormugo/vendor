import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';


part 'responseitems.g.dart';

@JsonSerializable()
class ResponseItems {
  final String planName;
  final int noOfShopsQueried;
  final List<Shop>? shops;

  ResponseItems({
    required this.planName,
    required this.noOfShopsQueried,
    this.shops,
  });

  factory ResponseItems.fromJson(Map<String, dynamic> json) =>
      _$ResponseItemsFromJson(json);
}
