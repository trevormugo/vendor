// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopwithplans.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopWithPlans _$ShopWithPlansFromJson(Map<String, dynamic> json) {
  return ShopWithPlans(
    shop: Shop.fromJson(json['shop'] as Map<String, dynamic>),
    planName: json['planName'] as String?,
  );
}

Map<String, dynamic> _$ShopWithPlansToJson(ShopWithPlans instance) =>
    <String, dynamic>{
      'shop': instance.shop,
      'planName': instance.planName,
    };
