import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/ridersTabQuery/oneaffiliation.dart';
import 'package:vendor/models/ridersTabQuery/oneaffiliationrequest.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';

part 'riderstabquery.g.dart';

@JsonSerializable()
class RidersTabQuery {
  final List<OneAffiliation>? affiliatedRiders;
  final List<OneAffiliationRequest>? affiliationRequestSent;
  final List<ShopWithPlans>? shopWithPlan;
  RidersTabQuery({
    this.affiliatedRiders,
    this.affiliationRequestSent,
    this.shopWithPlan,
  });

  factory RidersTabQuery.fromJson(Map<String, dynamic> json) => _$RidersTabQueryFromJson(json);
  
}
