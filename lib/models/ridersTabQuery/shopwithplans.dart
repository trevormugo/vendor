import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';

part 'shopwithplans.g.dart';

@JsonSerializable()
class ShopWithPlans {
  final Shop shop;
  final String? planName;
  ShopWithPlans({
    required this.shop,
    this.planName,
  });

  factory ShopWithPlans.fromJson(Map<String, dynamic> json) => _$ShopWithPlansFromJson(json);
  
}
