import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/profileQueries/rideraffiliations.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';

part 'oneaffiliationrequest.g.dart';

@JsonSerializable()
class OneAffiliationRequest {
  final Account? rider;
  final RiderAffiliations? affiliation;
  final List<ShopWithPlans>? shopWithPlan;
  OneAffiliationRequest({
    this.rider,
    this.affiliation,
    this.shopWithPlan,
  });

  factory OneAffiliationRequest.fromJson(Map<String, dynamic> json) => _$OneAffiliationRequestFromJson(json);
  
}
