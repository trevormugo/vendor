// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oneaffiliation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneAffiliation _$OneAffiliationFromJson(Map<String, dynamic> json) {
  return OneAffiliation(
    rider: json['rider'] == null
        ? null
        : Account.fromJson(json['rider'] as Map<String, dynamic>),
    affiliation: json['affiliation'] == null
        ? null
        : RiderAffiliations.fromJson(
            json['affiliation'] as Map<String, dynamic>),
    shopWithPlan: (json['shopWithPlan'] as List<dynamic>?)
        ?.map((e) => ShopWithPlans.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OneAffiliationToJson(OneAffiliation instance) =>
    <String, dynamic>{
      'rider': instance.rider,
      'affiliation': instance.affiliation,
      'shopWithPlan': instance.shopWithPlan,
    };
