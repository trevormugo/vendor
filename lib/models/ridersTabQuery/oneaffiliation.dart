import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/profileQueries/rideraffiliations.dart';
import 'package:vendor/models/ridersTabQuery/shopwithplans.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';

part 'oneaffiliation.g.dart';

@JsonSerializable()
class OneAffiliation {
  final Account? rider;
  final RiderAffiliations? affiliation;
  final List<ShopWithPlans>? shopWithPlan;
  OneAffiliation({
    this.rider,
    this.affiliation,
    this.shopWithPlan,
  });

  factory OneAffiliation.fromJson(Map<String, dynamic> json) => _$OneAffiliationFromJson(json);
  
}
