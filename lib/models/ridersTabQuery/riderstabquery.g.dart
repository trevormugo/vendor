// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'riderstabquery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RidersTabQuery _$RidersTabQueryFromJson(Map<String, dynamic> json) {
  return RidersTabQuery(
    affiliatedRiders: (json['affiliatedRiders'] as List<dynamic>?)
        ?.map((e) => OneAffiliation.fromJson(e as Map<String, dynamic>))
        .toList(),
    affiliationRequestSent: (json['affiliationRequestSent'] as List<dynamic>?)
        ?.map((e) => OneAffiliationRequest.fromJson(e as Map<String, dynamic>))
        .toList(),
    shopWithPlan: (json['shopWithPlan'] as List<dynamic>?)
        ?.map((e) => ShopWithPlans.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$RidersTabQueryToJson(RidersTabQuery instance) =>
    <String, dynamic>{
      'affiliatedRiders': instance.affiliatedRiders,
      'affiliationRequestSent': instance.affiliationRequestSent,
      'shopWithPlan': instance.shopWithPlan,
    };
