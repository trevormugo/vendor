// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopreviews.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopReviews _$ShopReviewsFromJson(Map<String, dynamic> json) {
  return ShopReviews(
    id: json['id'] as String,
    review: json['review'] as String,
    rating: (json['rating'] as num).toDouble(),
    shopId: json['shopId'] as String,
    userId: json['userId'] as String,
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$ShopReviewsToJson(ShopReviews instance) =>
    <String, dynamic>{
      'id': instance.id,
      'review': instance.review,
      'rating': instance.rating,
      'shopId': instance.shopId,
      'userId': instance.userId,
      'timestamp': instance.timestamp,
    };
