import 'package:json_annotation/json_annotation.dart';

part 'shopreviews.g.dart';

@JsonSerializable()
class ShopReviews {
  final String id;
  final String review;
  final double rating;
  final String shopId;
  final String userId;
  final int timestamp;

  ShopReviews({
    required this.id,
    required this.review,
    required this.rating,
    required this.shopId,
    required this.userId,
    required this.timestamp,
  });

  factory ShopReviews.fromJson(Map<String, dynamic> json) =>
      _$ShopReviewsFromJson(json);
}
