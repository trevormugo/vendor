import 'package:json_annotation/json_annotation.dart';

part 'items.g.dart';

@JsonSerializable()
class Items {
  final String id;
  final String itemName;
  final String itemCategoryId;
  final double? itemRating;
  final int? noOfItems;
  final int? noOfItemSales;
  final String shopId;
  final bool canBeDelivered;
  final String vendorId;
  final int timestamp;
  final int ammount;
  final bool sold;

  Items({
    required this.id,
    required this.itemName,
    required this.itemCategoryId,
    this.itemRating,
    this.noOfItems,
    this.noOfItemSales,
    required this.shopId,
    required this.canBeDelivered,
    required this.vendorId,
    required this.timestamp,
    required this.ammount,
    required this.sold,
  });

  factory Items.fromJson(Map<String, dynamic> json) => _$ItemsFromJson(json);
}
