// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oneshopquery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneShopQuery _$OneShopQueryFromJson(Map<String, dynamic> json) {
  return OneShopQuery(
    planName: json['planName'] as String?,
    shop: Shop.fromJson(json['shop'] as Map<String, dynamic>),
    shopImages: (json['shopImages'] as List<dynamic>?)
        ?.map((e) => ShopImages.fromJson(e as Map<String, dynamic>))
        .toList(),
    categories: (json['categories'] as List<dynamic>?)
        ?.map((e) => Categories.fromJson(e as Map<String, dynamic>))
        .toList(),
    shopFavorers: (json['shopFavorers'] as List<dynamic>?)
        ?.map((e) => Account.fromJson(e as Map<String, dynamic>))
        .toList(),
    shopReviews: (json['shopReviews'] as List<dynamic>?)
        ?.map((e) => ShopReviewers.fromJson(e as Map<String, dynamic>))
        .toList(),
    shopSales: (json['shopSales'] as List<dynamic>?)
        ?.map((e) => Sales.fromJson(e as Map<String, dynamic>))
        .toList(),
    customerPurchases: (json['customerPurchases'] as List<dynamic>?)
        ?.map((e) => Sales.fromJson(e as Map<String, dynamic>))
        .toList(),
    termsAndConditions: (json['termsAndConditions'] as List<dynamic>?)
        ?.map((e) => TermsAndConditions.fromJson(e as Map<String, dynamic>))
        .toList(),
    favorited: json['favorited'] as bool,
    reviewed: json['reviewed'] as bool,
    hasShoped: json['hasShoped'] as bool,
    shopReviewersForProfile: json['shopReviewersForProfile'] == null
        ? null
        : ShopReviewers.fromJson(
            json['shopReviewersForProfile'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OneShopQueryToJson(OneShopQuery instance) =>
    <String, dynamic>{
      'planName': instance.planName,
      'shop': instance.shop,
      'shopImages': instance.shopImages,
      'categories': instance.categories,
      'shopFavorers': instance.shopFavorers,
      'shopReviews': instance.shopReviews,
      'shopSales': instance.shopSales,
      'customerPurchases': instance.customerPurchases,
      'termsAndConditions': instance.termsAndConditions,
      'favorited': instance.favorited,
      'reviewed': instance.reviewed,
      'hasShoped': instance.hasShoped,
      'shopReviewersForProfile': instance.shopReviewersForProfile,
    };
