import 'package:json_annotation/json_annotation.dart';

part 'shop.g.dart';

@JsonSerializable()
class Shop {
  final String id;
  final String shopName;
  final String openHours;
  final double rating;
  final int noOFFavorites;
  final int noOfSales;
  final int noOFRatingsAndReviews;
  final bool deliveries;
  final String ownerId;
  final double longitude;
  final double latitude;
  final int timestamp;

  Shop({
    required this.id,
    required this.shopName,
    required this.openHours,
    required this.rating,
    required this.noOFFavorites,
    required this.noOfSales,
    required this.noOFRatingsAndReviews,
    required this.deliveries,
    required this.ownerId,
    required this.longitude,
    required this.latitude,
    required this.timestamp,
  });

  factory Shop.fromJson(Map<String, dynamic> json) => _$ShopFromJson(json);
}
