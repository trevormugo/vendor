// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'termsandconditions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TermsAndConditions _$TermsAndConditionsFromJson(Map<String, dynamic> json) {
  return TermsAndConditions(
    id: json['id'] as String?,
    shopId: json['shopId'] as String?,
    deletable: (json['deletable'] as num?)?.toDouble(),
    termsAndConditionsText: json['termsAndConditionsText'] as String?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$TermsAndConditionsToJson(TermsAndConditions instance) =>
    <String, dynamic>{
      'id': instance.id,
      'shopId': instance.shopId,
      'deletable': instance.deletable,
      'termsAndConditionsText': instance.termsAndConditionsText,
      'timestamp': instance.timestamp,
    };
