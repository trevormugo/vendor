// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopimages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopImages _$ShopImagesFromJson(Map<String, dynamic> json) {
  return ShopImages(
    id: json['id'] as String,
    shopId: json['shopId'] as String,
    imageNo: json['imageNo'] as int?,
    photomimetype: json['photomimetype'] as String,
    photoext: json['photoext'] as String,
    photofilesize: json['photofilesize'] as int,
    photoPath: json['photoPath'] as String,
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$ShopImagesToJson(ShopImages instance) =>
    <String, dynamic>{
      'id': instance.id,
      'shopId': instance.shopId,
      'imageNo': instance.imageNo,
      'photomimetype': instance.photomimetype,
      'photoext': instance.photoext,
      'photofilesize': instance.photofilesize,
      'photoPath': instance.photoPath,
      'timestamp': instance.timestamp,
    };
