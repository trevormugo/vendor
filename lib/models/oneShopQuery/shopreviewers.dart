import 'package:json_annotation/json_annotation.dart';


import '../applicationUser/applicationuser.dart';
import './shopimages.dart';
import './shop.dart';
import './categories.dart';
import '../profileQueries/sales.dart';
import './shopreviews.dart';
import './termsandconditions.dart';

part 'shopreviewers.g.dart';

@JsonSerializable()
class ShopReviewers {
  final Account user;
  final ShopReviews reviews;

  ShopReviewers({
    required this.user,
    required this.reviews,
  });

  factory ShopReviewers.fromJson(Map<String, dynamic> json) =>
      _$ShopReviewersFromJson(json);
}
