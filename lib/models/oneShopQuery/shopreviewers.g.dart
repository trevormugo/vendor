// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopreviewers.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopReviewers _$ShopReviewersFromJson(Map<String, dynamic> json) {
  return ShopReviewers(
    user: Account.fromJson(json['user'] as Map<String, dynamic>),
    reviews: ShopReviews.fromJson(json['reviews'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ShopReviewersToJson(ShopReviewers instance) =>
    <String, dynamic>{
      'user': instance.user,
      'reviews': instance.reviews,
    };
