// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Shop _$ShopFromJson(Map<String, dynamic> json) {
  return Shop(
    id: json['id'] as String,
    shopName: json['shopName'] as String,
    openHours: json['openHours'] as String,
    rating: (json['rating'] as num).toDouble(),
    noOFFavorites: json['noOFFavorites'] as int,
    noOfSales: json['noOfSales'] as int,
    noOFRatingsAndReviews: json['noOFRatingsAndReviews'] as int,
    deliveries: json['deliveries'] as bool,
    ownerId: json['ownerId'] as String,
    longitude: (json['longitude'] as num).toDouble(),
    latitude: (json['latitude'] as num).toDouble(),
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$ShopToJson(Shop instance) => <String, dynamic>{
      'id': instance.id,
      'shopName': instance.shopName,
      'openHours': instance.openHours,
      'rating': instance.rating,
      'noOFFavorites': instance.noOFFavorites,
      'noOfSales': instance.noOfSales,
      'noOFRatingsAndReviews': instance.noOFRatingsAndReviews,
      'deliveries': instance.deliveries,
      'ownerId': instance.ownerId,
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'timestamp': instance.timestamp,
    };
