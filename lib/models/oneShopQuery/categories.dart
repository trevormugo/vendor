import 'package:json_annotation/json_annotation.dart';

import './items.dart';

part 'categories.g.dart';

@JsonSerializable()
class Categories {
  final String categoryName;
  final String shopCategoryId;
  final List<Items>? items;

  Categories({
    required this.categoryName,
    required this.shopCategoryId,
    this.items,
  });

  factory Categories.fromJson(Map<String, dynamic> json) =>
      _$CategoriesFromJson(json);
}
