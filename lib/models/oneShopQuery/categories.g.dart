// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categories.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Categories _$CategoriesFromJson(Map<String, dynamic> json) {
  return Categories(
    categoryName: json['categoryName'] as String,
    shopCategoryId: json['shopCategoryId'] as String,
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => Items.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$CategoriesToJson(Categories instance) =>
    <String, dynamic>{
      'categoryName': instance.categoryName,
      'shopCategoryId': instance.shopCategoryId,
      'items': instance.items,
    };
