import 'package:json_annotation/json_annotation.dart';

import '../applicationUser/applicationuser.dart';
import './shopimages.dart';
import './shop.dart';
import './categories.dart';
import '../profileQueries/sales.dart';
import './shopreviewers.dart';
import './termsandconditions.dart';

part 'oneshopquery.g.dart';

@JsonSerializable()
class OneShopQuery {
  final String? planName;
  final Shop shop;
  final List<ShopImages>? shopImages;
  final List<Categories>? categories;
  final List<Account>? shopFavorers;
  final List<ShopReviewers>? shopReviews;
  final List<Sales>? shopSales;
  final List<Sales>? customerPurchases;
  final List<TermsAndConditions>? termsAndConditions;
  final bool favorited;
  final bool reviewed;
  final bool hasShoped;
  final ShopReviewers? shopReviewersForProfile;
  OneShopQuery({
    this.planName,
    required this.shop,
    this.shopImages,
    this.categories,
    this.shopFavorers,
    this.shopReviews,
    this.shopSales,
    this.customerPurchases,
    this.termsAndConditions,
    required this.favorited,
    required this.reviewed,
    required this.hasShoped,
    this.shopReviewersForProfile,
  });

  factory OneShopQuery.fromJson(Map<String, dynamic> json) =>
      _$OneShopQueryFromJson(json);
}
