import 'package:json_annotation/json_annotation.dart';

part 'termsandconditions.g.dart';

@JsonSerializable()
class TermsAndConditions {
  final String? id;
  final String? shopId;
  final double? deletable;
  final String? termsAndConditionsText;
  final int? timestamp;

  TermsAndConditions({
    this.id,
    this.shopId,
    this.deletable,
    this.termsAndConditionsText,
    this.timestamp,
  });

  factory TermsAndConditions.fromJson(Map<String, dynamic> json) =>
      _$TermsAndConditionsFromJson(json);
}
