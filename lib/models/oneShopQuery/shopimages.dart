import 'package:json_annotation/json_annotation.dart';

part 'shopimages.g.dart';

@JsonSerializable()
class ShopImages {
  final String id;
  final String shopId;
  final int? imageNo;
  final String photomimetype;
  final String photoext;
  final int photofilesize;
  final String photoPath;
  final int timestamp;

  ShopImages({
    required this.id,
    required this.shopId,
    this.imageNo,
    required this.photomimetype,
    required this.photoext,
    required this.photofilesize,
    required this.photoPath,
    required this.timestamp,
  });

  factory ShopImages.fromJson(Map<String, dynamic> json) =>
      _$ShopImagesFromJson(json);
}
