// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'items.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Items _$ItemsFromJson(Map<String, dynamic> json) {
  return Items(
    id: json['id'] as String,
    itemName: json['itemName'] as String,
    itemCategoryId: json['itemCategoryId'] as String,
    itemRating: (json['itemRating'] as num?)?.toDouble(),
    noOfItems: json['noOfItems'] as int?,
    noOfItemSales: json['noOfItemSales'] as int?,
    shopId: json['shopId'] as String,
    canBeDelivered: json['canBeDelivered'] as bool,
    vendorId: json['vendorId'] as String,
    timestamp: json['timestamp'] as int,
    ammount: json['ammount'] as int,
    sold: json['sold'] as bool,
  );
}

Map<String, dynamic> _$ItemsToJson(Items instance) => <String, dynamic>{
      'id': instance.id,
      'itemName': instance.itemName,
      'itemCategoryId': instance.itemCategoryId,
      'itemRating': instance.itemRating,
      'noOfItems': instance.noOfItems,
      'noOfItemSales': instance.noOfItemSales,
      'shopId': instance.shopId,
      'canBeDelivered': instance.canBeDelivered,
      'vendorId': instance.vendorId,
      'timestamp': instance.timestamp,
      'ammount': instance.ammount,
      'sold': instance.sold,
    };
