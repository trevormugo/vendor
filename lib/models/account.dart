import 'package:vendor/models/shops.dart';

import './shops.dart';
import './applicationUser/applicationuser.dart';
import './oneShopQuery/shop.dart';

class VerificationRequest {
  final String? email;
  final String userName;
  final String phoneNumber;
  VerificationRequest({
    this.email,
    required this.userName,
    required this.phoneNumber,
  });

  Map<String, dynamic> toJson() => {
        'Email': email,
        'UserName': userName,
        'PhoneNumber': phoneNumber,
      };
}

class RegisterRoleRequest {
  final String? id;
  RegisterRoleRequest({
    this.id,
  });

  Map<String, dynamic> toJson() => {
        'Id': id,
      };
}

class RegisterVendorRequest {
  final String id;
  final String planName;
  RegisterVendorRequest({
    required this.id,
    required this.planName,
  });

  Map<String, dynamic> toJson() => {'Id': id, 'PlanName': planName};
}
