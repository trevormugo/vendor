class AffiliateRider {
  final String? riderId;
  final String? shopId;
  final String? vendorId;
  final String? inititatorUser;

  AffiliateRider({
    this.riderId,
    this.shopId,
    this.vendorId,
    this.inititatorUser,
  });

  Map<String, dynamic> toJson() => {
        'riderId': riderId,
        'shopId': shopId,
        'vendorId': vendorId,
        'InititatorUser': inititatorUser,
      };
}
