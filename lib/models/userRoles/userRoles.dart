import 'package:json_annotation/json_annotation.dart';

part 'userRoles.g.dart';

@JsonSerializable()
class UserRoles {
  final List<String> roles;
  final int unreadNotifications;
  final int shoppingCart;
  final int incomingGoods;
  final int unreadAssignments;
  final int unreadOrders;
  final int unreadInventory;
  final int unreadRiders;
  final int unreadSupervisors;

  UserRoles({
    required this.roles,
    required this.unreadNotifications,
    required this.shoppingCart,
    required this.incomingGoods,
    required this.unreadAssignments,
    required this.unreadOrders,
    required this.unreadInventory,
    required this.unreadRiders,
    required this.unreadSupervisors,
  });

  factory UserRoles.fromJson(Map<String, dynamic> json) =>
      _$UserRolesFromJson(json);
}
