class ReturnPolicyRequest {
  final String itemId;
  final String policyText;
  ReturnPolicyRequest({
    required this.itemId,
    required this.policyText,
  });

  Map<String, dynamic> toJson() => {
        'ItemId': itemId,
        'PolicyText': policyText,
      };
}
