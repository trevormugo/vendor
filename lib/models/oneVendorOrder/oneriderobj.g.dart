// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oneriderobj.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneRiderObj _$OneRiderObjFromJson(Map<String, dynamic> json) {
  return OneRiderObj(
    isAssigned: json['isAssigned'] as bool?,
    rider: json['rider'] == null
        ? null
        : Account.fromJson(json['rider'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OneRiderObjToJson(OneRiderObj instance) =>
    <String, dynamic>{
      'isAssigned': instance.isAssigned,
      'rider': instance.rider,
    };
