// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'actuallorders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActuallOrders _$ActuallOrdersFromJson(Map<String, dynamic> json) {
  return ActuallOrders(
    orders: (json['orders'] as List<dynamic>?)
        ?.map((e) => OneOrder.fromJson(e as Map<String, dynamic>))
        .toList(),
    toBeDelivered: json['toBeDelivered'] as bool?,
    qrMappingId: json['qrMappingId'] as String?,
    oneRiderObj: (json['oneRiderObj'] as List<dynamic>?)
        ?.map((e) => OneRiderObj.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ActuallOrdersToJson(ActuallOrders instance) =>
    <String, dynamic>{
      'toBeDelivered': instance.toBeDelivered,
      'qrMappingId': instance.qrMappingId,
      'orders': instance.orders,
      'oneRiderObj': instance.oneRiderObj,
    };
