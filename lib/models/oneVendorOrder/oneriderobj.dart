import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';

part 'oneriderobj.g.dart';

@JsonSerializable()
class OneRiderObj {
  final bool? isAssigned;
  final Account? rider;
  OneRiderObj({
    this.isAssigned,
    this.rider,
  });

  factory OneRiderObj.fromJson(Map<String, dynamic> json) =>
      _$OneRiderObjFromJson(json);
}
