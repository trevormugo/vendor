// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'onevendororderresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneVendorOrderResponse _$OneVendorOrderResponseFromJson(
    Map<String, dynamic> json) {
  return OneVendorOrderResponse(
    oneVendorOrder: (json['oneVendorOrder'] as List<dynamic>)
        .map((e) => ActuallOrders.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OneVendorOrderResponseToJson(
        OneVendorOrderResponse instance) =>
    <String, dynamic>{
      'oneVendorOrder': instance.oneVendorOrder,
    };
