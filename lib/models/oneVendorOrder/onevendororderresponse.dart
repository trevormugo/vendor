import 'package:json_annotation/json_annotation.dart';

import 'actuallorders.dart';


part 'onevendororderresponse.g.dart';


@JsonSerializable()
class OneVendorOrderResponse {
  final List<ActuallOrders> oneVendorOrder;

  OneVendorOrderResponse({
    required this.oneVendorOrder,
  });

  factory OneVendorOrderResponse.fromJson(Map<String, dynamic> json) => _$OneVendorOrderResponseFromJson(json);

  
}