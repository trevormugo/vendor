import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/oneVendorOrder/oneriderobj.dart';

part 'actuallorders.g.dart';

@JsonSerializable()
class ActuallOrders {
  final bool? toBeDelivered;
  final String? qrMappingId;
  final List<OneOrder>? orders;
  final List<OneRiderObj>? oneRiderObj;
  ActuallOrders({
    this.orders,
    this.toBeDelivered,
    this.qrMappingId,
    this.oneRiderObj,
  });

  factory ActuallOrders.fromJson(Map<String, dynamic> json) =>
      _$ActuallOrdersFromJson(json);
}
