// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ridersearch.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiderSearch _$RiderSearchFromJson(Map<String, dynamic> json) {
  return RiderSearch(
    rider: json['rider'] == null
        ? null
        : Account.fromJson(json['rider'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RiderSearchToJson(RiderSearch instance) =>
    <String, dynamic>{
      'rider': instance.rider,
    };
