import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/profileQueries/rideraffiliations.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';

part 'ridersearch.g.dart';

@JsonSerializable()
class RiderSearch {
  final Account? rider;
  RiderSearch({
    this.rider,
  });

  factory RiderSearch.fromJson(Map<String, dynamic> json) => _$RiderSearchFromJson(json);
  
}
