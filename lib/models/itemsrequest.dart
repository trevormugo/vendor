import 'dart:io';

import './applicationUser/applicationuser.dart';
import './oneShopQuery/items.dart';
/*
class AvailableCategoriesResponse {
  final List<AvailableCategories>? items;

  AvailableCategoriesResponse({this.items});

  factory AvailableCategoriesResponse.fromJson(Map<String, dynamic> json) {
    return AvailableCategoriesResponse(items: json['Items']);
  }
}*/

class CreateItemRequest {
  final String itemName;
  final String itemCategoryId;
  final int noOfItems;
  final String shopId;
  final bool canBeDelivered;
  final String vendorId;
  final int ammount;

  CreateItemRequest({
    required this.itemName,
    required this.itemCategoryId,
    required this.noOfItems,
    required this.shopId,
    required this.canBeDelivered,
    required this.vendorId,
    required this.ammount,
  });

  Map<String, dynamic> toJson() => {
        'ItemName': itemName,
        'ItemCategoryId': itemCategoryId,
        'NoOfItems': noOfItems,
        'ShopId': shopId,
        'CanBeDelivered': canBeDelivered,
        'VendorId': vendorId,
        'Ammount': ammount,
      };
}

class ItemThumbnailUploadRequest {
  final String itemId;
  final File thumbnail;

  ItemThumbnailUploadRequest({
    required this.itemId,
    required this.thumbnail,
  });
}
