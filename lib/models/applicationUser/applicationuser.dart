import 'package:json_annotation/json_annotation.dart';

part 'applicationuser.g.dart';

@JsonSerializable()
class Account {
  final String id;
  final String token;
  final String userName;
  final String email;
  final String phoneNumber;
  final String? country;
  final String fullName;
  final bool phoneNumberConfirmed;
  final bool? hasImage;
  final bool emailConfirmed;
  final int timestamp;

  Account({
    required this.id,
    required this.token,
    required this.userName,
    required this.email,
    required this.phoneNumber,
    this.country,
    required this.fullName,
    required this.phoneNumberConfirmed,
    this.hasImage,
    required this.emailConfirmed,
    required this.timestamp,
  });

  factory Account.fromJson(Map<String, dynamic> json) =>
      _$AccountFromJson(json);
}
