// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'applicationuser.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Account _$AccountFromJson(Map<String, dynamic> json) {
  return Account(
    id: json['id'] as String,
    token: json['token'] as String,
    userName: json['userName'] as String,
    email: json['email'] as String,
    phoneNumber: json['phoneNumber'] as String,
    country: json['country'] as String?,
    fullName: json['fullName'] as String,
    phoneNumberConfirmed: json['phoneNumberConfirmed'] as bool,
    hasImage: json['hasImage'] as bool?,
    emailConfirmed: json['emailConfirmed'] as bool,
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$AccountToJson(Account instance) => <String, dynamic>{
      'id': instance.id,
      'token': instance.token,
      'userName': instance.userName,
      'email': instance.email,
      'phoneNumber': instance.phoneNumber,
      'country': instance.country,
      'fullName': instance.fullName,
      'phoneNumberConfirmed': instance.phoneNumberConfirmed,
      'hasImage': instance.hasImage,
      'emailConfirmed': instance.emailConfirmed,
      'timestamp': instance.timestamp,
    };
