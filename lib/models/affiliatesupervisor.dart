class AffiliateSupervisor {
  final String? supervisorId;
  final String? shopId;
  final String? vendorId;
  final String? inititatorUser;

  AffiliateSupervisor({
    this.supervisorId,
    this.shopId,
    this.vendorId,
    this.inititatorUser,
  });

  Map<String, dynamic> toJson() => {
        'SupervisorId': supervisorId,
        'shopId': shopId,
        'vendorId': vendorId,
        'InititatorUser': inititatorUser,
      };
}
