import 'dart:io';

import './applicationUser/applicationuser.dart';
import './oneShopQuery/items.dart';
/*
class AvailableCategoriesResponse {
  final List<AvailableCategories>? items;

  AvailableCategoriesResponse({this.items});

  factory AvailableCategoriesResponse.fromJson(Map<String, dynamic> json) {
    return AvailableCategoriesResponse(items: json['Items']);
  }
}*/

class CreateShopRequest {
  final String shopName;
  final String country;
  final String openHours;
  final String ownerId;
  final double longitude;
  final double latitude;

  CreateShopRequest({
    required this.shopName,
    required this.country,
    required this.openHours,
    required this.ownerId,
    required this.longitude,
    required this.latitude,
  });

  Map<String, dynamic> toJson() => {
        'ShopName': shopName,
        'Country': country,
        'OpenHours': openHours,
        'OwnerId': ownerId,
        'Longitude': longitude,
        'Latitude': latitude,
      };
}


class UpdateShopRequest {
  final String shopName;
  final String openHours;
  final String ownerId;
  final double longitude;
  final double latitude;

  UpdateShopRequest({
    required this.shopName,
    required this.openHours,
    required this.ownerId,
    required this.longitude,
    required this.latitude,
  });

  Map<String, dynamic> toJson() => {
        'ShopName': shopName,
        'OpenHours': openHours,
        'OwnerId': ownerId,
        'Longitude': longitude,
        'Latitude': latitude,
      };
}

class ShopThumbnailUploadRequest {
  final String shopId;
  final File imageFile;

  ShopThumbnailUploadRequest({
    required this.shopId,
    required this.imageFile,
  });

}