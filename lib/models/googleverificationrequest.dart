class GoogleVerificationRequest {
  final String purchaseToken;
  final String productId;

  GoogleVerificationRequest({
    required this.purchaseToken,
    required this.productId,
  });

  Map<String, dynamic> toJson() => {
        'PurchaseToken': purchaseToken,
        'productId': productId,
      };
}
