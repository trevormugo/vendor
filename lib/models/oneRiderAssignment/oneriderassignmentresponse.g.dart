// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oneriderassignmentresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneRiderAssignmentResponse _$OneRiderAssignmentResponseFromJson(
    Map<String, dynamic> json) {
  return OneRiderAssignmentResponse(
    oneRiderAssignment: ActuallAssignment.fromJson(
        json['oneRiderAssignment'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OneRiderAssignmentResponseToJson(
        OneRiderAssignmentResponse instance) =>
    <String, dynamic>{
      'oneRiderAssignment': instance.oneRiderAssignment,
    };
