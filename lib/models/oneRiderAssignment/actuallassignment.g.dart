// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'actuallassignment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActuallAssignment _$ActuallAssignmentFromJson(Map<String, dynamic> json) {
  return ActuallAssignment(
    orders: (json['orders'] as List<dynamic>?)
        ?.map((e) => OneOrder.fromJson(e as Map<String, dynamic>))
        .toList(),
    assignment:
        Assignments.fromJson(json['assignment'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ActuallAssignmentToJson(ActuallAssignment instance) =>
    <String, dynamic>{
      'orders': instance.orders,
      'assignment': instance.assignment,
    };
