import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/profileQueries/assignments.dart';

part 'actuallassignment.g.dart';

@JsonSerializable()
class ActuallAssignment {
  final List<OneOrder>? orders;
  final Assignments assignment;

  ActuallAssignment({
    this.orders,
    required this.assignment,
  });

  factory ActuallAssignment.fromJson(Map<String, dynamic> json) =>
      _$ActuallAssignmentFromJson(json);
}
  