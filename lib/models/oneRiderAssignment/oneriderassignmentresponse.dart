import 'package:json_annotation/json_annotation.dart';

import 'actuallassignment.dart';


part 'oneriderassignmentresponse.g.dart';


@JsonSerializable()
class OneRiderAssignmentResponse {
  final ActuallAssignment oneRiderAssignment;

  OneRiderAssignmentResponse({
    required this.oneRiderAssignment,
  });

  factory OneRiderAssignmentResponse.fromJson(Map<String, dynamic> json) => _$OneRiderAssignmentResponseFromJson(json);

  
}