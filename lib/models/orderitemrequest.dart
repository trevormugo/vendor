class OrderItemRequest {
  final String customerCartId;
  final String deliveryLocationId;
  final bool toBeDelivered;
  OrderItemRequest({
    required this.customerCartId,
    required this.deliveryLocationId,
    required this.toBeDelivered,
  });

  Map<String, dynamic> toJson() => {
        'CustomerCartId': customerCartId,
        'DeliveryLocationId': deliveryLocationId,
        'ToBeDelivered': toBeDelivered,
      };
}
