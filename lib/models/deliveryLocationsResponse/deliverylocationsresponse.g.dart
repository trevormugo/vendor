// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'deliverylocationsresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeliveryLocationsResponse _$DeliveryLocationsResponseFromJson(
    Map<String, dynamic> json) {
  return DeliveryLocationsResponse(
    deliveryLocations: (json['deliveryLocations'] as List<dynamic>?)
        ?.map((e) => DeliveryLocations.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$DeliveryLocationsResponseToJson(
        DeliveryLocationsResponse instance) =>
    <String, dynamic>{
      'deliveryLocations': instance.deliveryLocations,
    };
