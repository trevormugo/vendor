import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/customerCartResponse/deliverylocations.dart';


part 'deliverylocationsresponse.g.dart';


@JsonSerializable()
class DeliveryLocationsResponse {
  final List<DeliveryLocations>? deliveryLocations;
  DeliveryLocationsResponse({
    this.deliveryLocations, 
  });

  factory DeliveryLocationsResponse.fromJson(Map<String, dynamic> json) => _$DeliveryLocationsResponseFromJson(json);
}
