// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inventorytabsresponseitem.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InventoryTab _$InventoryTabFromJson(Map<String, dynamic> json) {
  return InventoryTab(
    shop: json['shop'] == null
        ? null
        : Shop.fromJson(json['shop'] as Map<String, dynamic>),
    shopCategoriesNo: json['shopCategoriesNo'] as int?,
    planName: json['planName'] as String?,
  );
}

Map<String, dynamic> _$InventoryTabToJson(InventoryTab instance) =>
    <String, dynamic>{
      'shop': instance.shop,
      'shopCategoriesNo': instance.shopCategoriesNo,
      'planName': instance.planName,
    };
