import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';



part 'inventorytabsresponseitem.g.dart';

@JsonSerializable()
class InventoryTab {
  final Shop? shop;
  final int? shopCategoriesNo;
  final String? planName;
  InventoryTab({
    this.shop,
    this.shopCategoriesNo,
    this.planName,
  });

  factory InventoryTab.fromJson(Map<String, dynamic> json) =>
      _$InventoryTabFromJson(json);
}
