import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/inventoryTabsResponse/inventorytabsresponseitem.dart';



part 'inventroiesouterlist.g.dart';

@JsonSerializable()
class InventoriesOuterList {
  final List<InventoryTab>? items;

  InventoriesOuterList({
    this.items,
  });

  factory InventoriesOuterList.fromJson(Map<String, dynamic> json) =>
      _$InventoriesOuterListFromJson(json);
}
