// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inventroiesouterlist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InventoriesOuterList _$InventoriesOuterListFromJson(Map<String, dynamic> json) {
  return InventoriesOuterList(
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => InventoryTab.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$InventoriesOuterListToJson(
        InventoriesOuterList instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
