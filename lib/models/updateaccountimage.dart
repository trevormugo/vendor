import 'dart:io';

class AccountThumbnailUploadRequest {
  final String userId;
  final File imageFile;

  AccountThumbnailUploadRequest({
    required this.userId,
    required this.imageFile,
  });
}
