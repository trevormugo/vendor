// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'availablecategoriesres.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AvailableCategoriesRes _$AvailableCategoriesResFromJson(
    Map<String, dynamic> json) {
  return AvailableCategoriesRes(
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => AvailableCategories.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$AvailableCategoriesResToJson(
        AvailableCategoriesRes instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
