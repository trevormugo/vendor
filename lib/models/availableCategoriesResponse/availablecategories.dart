import 'package:json_annotation/json_annotation.dart';

part 'availablecategories.g.dart';

@JsonSerializable()
class AvailableCategories {
  final String? id;
  final String? categoryName;
  final int? shopsWithCategoryNo;
  final int? timestamp;

  AvailableCategories({
    this.id,
    this.categoryName,
    this.shopsWithCategoryNo,
    this.timestamp,
  });

  factory AvailableCategories.fromJson(Map<String, dynamic> json) =>
      _$AvailableCategoriesFromJson(json);
}
