import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/availableCategoriesResponse/availablecategories.dart';

part 'availablecategoriesres.g.dart';

@JsonSerializable()
class AvailableCategoriesRes {
  final List<AvailableCategories>? items;

  AvailableCategoriesRes({
    this.items,
  });

  factory AvailableCategoriesRes.fromJson(Map<String, dynamic> json) =>
      _$AvailableCategoriesResFromJson(json);
}
