// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'availablecategories.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AvailableCategories _$AvailableCategoriesFromJson(Map<String, dynamic> json) {
  return AvailableCategories(
    id: json['id'] as String?,
    categoryName: json['categoryName'] as String?,
    shopsWithCategoryNo: json['shopsWithCategoryNo'] as int?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$AvailableCategoriesToJson(
        AvailableCategories instance) =>
    <String, dynamic>{
      'id': instance.id,
      'categoryName': instance.categoryName,
      'shopsWithCategoryNo': instance.shopsWithCategoryNo,
      'timestamp': instance.timestamp,
    };
