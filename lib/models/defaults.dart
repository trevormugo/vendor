import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import './insight.dart';

class PlanIcons {
  static IconData basic = LineAwesomeIcons.store;
  static IconData advanced = LineAwesomeIcons.warehouse;
  static IconData enterprise = LineAwesomeIcons.building;
}

class PlanImages {
  static String basic = 'assets/images/basic.png';
  static String advanced = 'assets/images/advanced.png';
  static String enterprise = 'assets/images/enterprise.png';
}

class ErrorImage {
  static String noimage = 'assets/images/noimage.png';
}

class PlanIconColors {
  static Color basic = Colors.green;
  static Color advanced = Color.fromARGB(255, 207, 118, 0);
  static Color enterprise = Color.fromARGB(255, 27, 81, 211);
}

class CategoryInsights {
  static List<Map<String, Insight>> insights = [
    {
      "fashion": Insight(
        paintcolor: Colors.amber,
        insightpercentage: 0.67,
        category: 'fashion',
        icon: Icon(
          Icons.checkroom_sharp,
          color: Colors.amber,
        ),
      ),
    },
    {
      "games": Insight(
        paintcolor: Colors.redAccent[200],
        insightpercentage: 0.22,
        category: 'games',
        icon: Icon(
          Icons.games_outlined,
          color: Colors.redAccent[200],
        ),
      ),
    },
    {
      "food": Insight(
        paintcolor: Colors.lightGreenAccent,
        insightpercentage: 0.53,
        category: 'food',
        icon: Icon(
          Icons.restaurant,
          color: Colors.lightGreenAccent,
        ),
      ),
    },
    {
      "electronics": Insight(
        paintcolor: Colors.deepPurple,
        insightpercentage: 0.83,
        category: 'electronics',
        icon: Icon(
          Icons.computer_sharp,
          color: Colors.deepPurple,
        ),
      ),
    },
    {
      "services": Insight(
        paintcolor: Colors.yellow,
        insightpercentage: 0.70,
        category: 'services',
        icon: Icon(
          Icons.miscellaneous_services,
          color: Colors.yellow,
        ),
      ),
    },
  ];
}

class LogOutUser {
  void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userid');
    prefs.remove('useremail');
    prefs.remove('token');
    prefs.remove('userphonenumber');
    prefs.remove('username');
    prefs.remove('deviceToken');
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }
}

class TimeConversion {
  static String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm a');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var diff = now.difference(date);
    var time = '';
    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' day ago';
      } else {
        time = diff.inDays.toString() + ' days ago';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' week ago';
      } else {
        time = (diff.inDays / 7).floor().toString() + ' weeks ago';
      }
    }
    return time;
  }
}

class PostAffiliatedShopResponse {
  final int index;
  final int statusCode;
  final String body;
  PostAffiliatedShopResponse({
    required this.index,
    required this.statusCode,
    required this.body,
  });
}

class HomeBaseLocation {
  static String home = 'assets/images/homebase.png';
}

class PlanNames {
  static String basic = "Basic";

  static String advanced = "Advanced";

  static String enterprise = "Enterprise";
}

class Perks {
  static List<Map<String, List<String>>> perks = [
    {
      'product1': [
        'Open up to 7 shops',
        'Add up to 7 categories',
        'Add up to 5 supervisors and 7 riders',
        'Maximum visibility of 2km',
      ],
    },
    {
      'product2': [
        'Open up to 10 shops',
        'Add up to 14 categories',
        'Add up to 7 supervisors and 10 riders',
        'Maximum visibility of 8km',
      ],
    },
    {
      'product3': [
        'Open up to 21 shops',
        'Add up to 20 categories',
        'Add up to unlimited supervisors and unlimited riders',
        'Global visibility',
      ],
    },
  ];
}

class PostAddOrderResponse {
  final int index;
  final int statusCode;
  final String body;
  PostAddOrderResponse({
    required this.index,
    required this.statusCode,
    required this.body,
  });
}

class WelcomePageItems {
  static List items = [
    {
      "header": "Welcome",
      "description": "Convinience whenever , wherever",
    },
    {
      "header": "Vendors",
      "description":
          "An online platform for vendors to sell their goods while minimizing the cost of delivery and increasing their store presence",
      "image": "assets/images/basic.png"
    },
    {
      "header": "Shoppers",
      "description": "Log in to our platform and shop among a variety of shops",
      "image": "assets/images/shopper-removebg-preview.png"
    },
    {
      "header": "Riders",
      "description":
          "Become a rider by affiliating yourself with a vendor , and earn money by delivering orders to shoppers",
      "image": "assets/images/ridericon-removebg-preview.png"
    },
    {
      "header": "Supervisors",
      "description":
          "Affiliate yourself with a vendor as a supervisor and manage the online shop in the absence of the vendor",
      "image": "assets/images/supervisorsimage-removebg-preview.png"
    },
  ];
}
