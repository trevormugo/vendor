class SignUpRequest {
  final String? userName;
  final String email;
  final String password;
  final String? fullName;
  final String? phoneNumber;
  final String? deviceToken;
  SignUpRequest({
    this.userName,
    required this.email,
    required this.password,
    this.fullName,
    this.phoneNumber,
    this.deviceToken,
  });

  Map<String, dynamic> toJson() => {
        'UserName': userName,
        'Email': email,
        'Password': password,
        'FullName': fullName,
        'PhoneNumber': phoneNumber,
        'DeviceToken': deviceToken,
      };
}


