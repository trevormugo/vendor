class UpdateDefaultLocationRequest {
  final String customerId;
  final String chosenId;

  UpdateDefaultLocationRequest({
    required this.customerId,
    required this.chosenId,
  });

  Map<String, dynamic> toJson() => {
        'customerId': customerId,
        'chosenId': chosenId,
      };
}
