// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'supervisorsearch.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SupervisorSearch _$SupervisorSearchFromJson(Map<String, dynamic> json) {
  return SupervisorSearch(
    supervisor: json['supervisor'] == null
        ? null
        : Account.fromJson(json['supervisor'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SupervisorSearchToJson(SupervisorSearch instance) =>
    <String, dynamic>{
      'supervisor': instance.supervisor,
    };
