import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/profileQueries/rideraffiliations.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';

part 'supervisorsearch.g.dart';

@JsonSerializable()
class SupervisorSearch {
  final Account? supervisor;
  SupervisorSearch({
    this.supervisor,
  });

  factory SupervisorSearch.fromJson(Map<String, dynamic> json) => _$SupervisorSearchFromJson(json);
  
}
