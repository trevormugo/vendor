class ReviewShopRequest {
  final String review;
  final double rating;
  final String shopId;
  final String userId;

  ReviewShopRequest({
    required this.shopId,
    required this.userId,
    required this.review,
    required this.rating,
  });

  Map<String, dynamic> toJson() => {
        'Review': review,
        'Rating': rating,
        'ShopId': shopId,
        'UserId': userId,
      };
}
