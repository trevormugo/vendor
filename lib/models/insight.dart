import 'package:flutter/cupertino.dart';

class Insight {
  Insight({
    required this.paintcolor,
    required this.insightpercentage,
    required this.category,
    required this.icon,
  });

  final Color? paintcolor;
  double insightpercentage;
  final String category;
  final Icon icon;
}
