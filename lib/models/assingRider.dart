class AssignRiderRequest {
  final String? riderId;
    final String? qrMappingId;

  AssignRiderRequest({
    this.riderId,
    this.qrMappingId
  });

  Map<String, dynamic> toJson() => {
        'riderId': riderId,
        'qrMappingId' : qrMappingId
      };
}