class UpdateAccountRequest {
  final String id;
  final String userName;
  final String email;
  final String fullName;
  final String phoneNumber;
  UpdateAccountRequest({
    required this.id,
    required this.userName,
    required this.email,
    required this.fullName,
    required this.phoneNumber,
  });

  Map<String, dynamic> toJson() => {
        'Id': id,
        'UserName': userName,
        'Email': email,
        'FullName': fullName,
        'PhoneNumber': phoneNumber,
      };
}
