import 'package:json_annotation/json_annotation.dart';

import 'notifications.dart';

part 'notificationsobj.g.dart';

@JsonSerializable()
class NotificationsObj {
  final Notifications notification;
  final String planName;
  NotificationsObj({
    required this.notification,
    required this.planName,
  });

  factory NotificationsObj.fromJson(Map<String, dynamic> json) =>
      _$NotificationsObjFromJson(json);
}
