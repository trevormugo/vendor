import 'package:json_annotation/json_annotation.dart';

part 'notifications.g.dart';

@JsonSerializable()
class Notifications {
  final String id;
  final String recieverId;
  final String doerId;
  final String title;
  final String subTitle;
  final String leading;
  final String type;
  final bool read;
  final int timestamp;

  Notifications({
    required this.id,
    required this.recieverId,
    required this.doerId,
    required this.title,
    required this.subTitle,
    required this.leading,
    required this.type,
    required this.read,
    required this.timestamp,
  });

  factory Notifications.fromJson(Map<String, dynamic> json) =>
      _$NotificationsFromJson(json);
}
