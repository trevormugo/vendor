import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/notifications/notificationsobj.dart';

import 'notifications.dart';

part 'notificationsresponse.g.dart';

@JsonSerializable()
class NotificationsResponse {
  final List<NotificationsObj> notifications;

  NotificationsResponse({
    required this.notifications,
  });

  factory NotificationsResponse.fromJson(Map<String, dynamic> json) =>
      _$NotificationsResponseFromJson(json);
}
