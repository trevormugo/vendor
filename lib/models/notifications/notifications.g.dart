// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notifications.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Notifications _$NotificationsFromJson(Map<String, dynamic> json) {
  return Notifications(
    id: json['id'] as String,
    recieverId: json['recieverId'] as String,
    doerId: json['doerId'] as String,
    title: json['title'] as String,
    subTitle: json['subTitle'] as String,
    leading: json['leading'] as String,
    type: json['type'] as String,
    read: json['read'] as bool,
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$NotificationsToJson(Notifications instance) =>
    <String, dynamic>{
      'id': instance.id,
      'recieverId': instance.recieverId,
      'doerId': instance.doerId,
      'title': instance.title,
      'subTitle': instance.subTitle,
      'leading': instance.leading,
      'type': instance.type,
      'read': instance.read,
      'timestamp': instance.timestamp,
    };
