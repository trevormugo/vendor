// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notificationsresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationsResponse _$NotificationsResponseFromJson(
    Map<String, dynamic> json) {
  return NotificationsResponse(
    notifications: (json['notifications'] as List<dynamic>)
        .map((e) => NotificationsObj.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NotificationsResponseToJson(
        NotificationsResponse instance) =>
    <String, dynamic>{
      'notifications': instance.notifications,
    };
