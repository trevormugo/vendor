// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notificationsobj.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationsObj _$NotificationsObjFromJson(Map<String, dynamic> json) {
  return NotificationsObj(
    notification:
        Notifications.fromJson(json['notification'] as Map<String, dynamic>),
    planName: json['planName'] as String,
  );
}

Map<String, dynamic> _$NotificationsObjToJson(NotificationsObj instance) =>
    <String, dynamic>{
      'notification': instance.notification,
      'planName': instance.planName,
    };
