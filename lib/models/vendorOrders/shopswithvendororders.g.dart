// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopswithvendororders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopsWithVendorOrders _$ShopsWithVendorOrdersFromJson(
    Map<String, dynamic> json) {
  return ShopsWithVendorOrders(
    shop: json['shop'] == null
        ? null
        : Shop.fromJson(json['shop'] as Map<String, dynamic>),
    planName: json['planName'] as String?,
    customers: (json['customers'] as List<dynamic>?)
        ?.map((e) => Account.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ShopsWithVendorOrdersToJson(
        ShopsWithVendorOrders instance) =>
    <String, dynamic>{
      'shop': instance.shop,
      'planName': instance.planName,
      'customers': instance.customers,
    };
