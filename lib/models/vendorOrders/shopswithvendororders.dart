import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';

part 'shopswithvendororders.g.dart';

@JsonSerializable()
class ShopsWithVendorOrders {
  final Shop? shop;
  final String? planName;
  final List<Account>? customers;
  ShopsWithVendorOrders({
    this.shop,
     this.planName,
    this.customers,
  });

  factory ShopsWithVendorOrders.fromJson(Map<String, dynamic> json) =>
      _$ShopsWithVendorOrdersFromJson(json);
}
