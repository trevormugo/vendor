import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/vendorOrders/shopswithvendororders.dart';


part 'vendorordersresponse.g.dart';


@JsonSerializable()
class VendorOrdersResponse {
  final List<ShopsWithVendorOrders>? shopsWithVendorOrders;

  VendorOrdersResponse({
    this.shopsWithVendorOrders,
  });

  factory VendorOrdersResponse.fromJson(Map<String, dynamic> json) => _$VendorOrdersResponseFromJson(json);

  
}