// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vendorordersresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VendorOrdersResponse _$VendorOrdersResponseFromJson(Map<String, dynamic> json) {
  return VendorOrdersResponse(
    shopsWithVendorOrders: (json['shopsWithVendorOrders'] as List<dynamic>?)
        ?.map((e) => ShopsWithVendorOrders.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$VendorOrdersResponseToJson(
        VendorOrdersResponse instance) =>
    <String, dynamic>{
      'shopsWithVendorOrders': instance.shopsWithVendorOrders,
    };
