// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'returnpolicyresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReturnPolicyResponse _$ReturnPolicyResponseFromJson(Map<String, dynamic> json) {
  return ReturnPolicyResponse(
    items: (json['items'] as List<dynamic>)
        .map((e) => ReturnPolicy.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ReturnPolicyResponseToJson(
        ReturnPolicyResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
