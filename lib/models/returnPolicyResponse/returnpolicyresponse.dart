import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/oneItemQuery/returnpolicy.dart';

part 'returnpolicyresponse.g.dart';

@JsonSerializable()
class ReturnPolicyResponse {
  final List<ReturnPolicy> items;
  
  ReturnPolicyResponse({
    required this.items,
  });

  factory ReturnPolicyResponse.fromJson(Map<String, dynamic> json) =>
      _$ReturnPolicyResponseFromJson(json);
}
