class ReviewItemRequest {
  final String review;
  final double rating;
  final String itemId;
  final String userId;

  ReviewItemRequest({
    required this.itemId,
    required this.userId,
    required this.review,
    required this.rating,
  });

  Map<String, dynamic> toJson() => {
        'Review': review,
        'Rating': rating,
        'ItemId': itemId,
        'UserId': userId,
      };
}
