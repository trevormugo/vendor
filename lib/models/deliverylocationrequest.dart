class DeliveryLocationRequest {
  final String customerId;
  final String locationTag;
  final double longitude;
  final double latitude;

  DeliveryLocationRequest({
    required this.customerId,
    required this.locationTag,
    required this.longitude,
    required this.latitude,
  });

  Map<String, dynamic> toJson() => {
        'customerId': customerId,
        'locationTag': locationTag,
        'Longitude': longitude,
        'Latitude': latitude,
      };
}
