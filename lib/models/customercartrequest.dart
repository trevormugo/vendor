class CustomerCartRequest {
  final String itemId;
  final String customerId;
  final String shopId;
  final int noOfItems;

  CustomerCartRequest({
    required this.itemId,
    required this.customerId,
    required this.shopId,
    required this.noOfItems,
  });

  Map<String, dynamic> toJson() => {
        'ItemId': itemId,
        'CustomerId': customerId,
        'ShopId': shopId,
        'NoOfItems': noOfItems,
      };
}
