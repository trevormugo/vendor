import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';

part 'shopswithriderassignments.g.dart';

@JsonSerializable()
class ShopsWithRiderAssignments {
  final Shop? shop;
  final String? planName;
  final List<Account>? customers;

  ShopsWithRiderAssignments({
    this.shop,
    this.customers,
    this.planName
  });

  factory ShopsWithRiderAssignments.fromJson(Map<String, dynamic> json) =>
      _$ShopsWithRiderAssignmentsFromJson(json);
}
