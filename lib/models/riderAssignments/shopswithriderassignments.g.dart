// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopswithriderassignments.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopsWithRiderAssignments _$ShopsWithRiderAssignmentsFromJson(
    Map<String, dynamic> json) {
  return ShopsWithRiderAssignments(
    shop: json['shop'] == null
        ? null
        : Shop.fromJson(json['shop'] as Map<String, dynamic>),
    customers: (json['customers'] as List<dynamic>?)
        ?.map((e) => Account.fromJson(e as Map<String, dynamic>))
        .toList(),
    planName: json['planName'] as String?,
  );
}

Map<String, dynamic> _$ShopsWithRiderAssignmentsToJson(
        ShopsWithRiderAssignments instance) =>
    <String, dynamic>{
      'shop': instance.shop,
      'planName': instance.planName,
      'customers': instance.customers,
    };
