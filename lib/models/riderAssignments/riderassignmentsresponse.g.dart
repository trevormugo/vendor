// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'riderassignmentsresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiderAssignmentsResponse _$RiderAssignmentsResponseFromJson(
    Map<String, dynamic> json) {
  return RiderAssignmentsResponse(
    shopOrdersWithRiderAssignments: (json['shopOrdersWithRiderAssignments']
            as List<dynamic>?)
        ?.map((e) => ShopsWithVendorOrders.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$RiderAssignmentsResponseToJson(
        RiderAssignmentsResponse instance) =>
    <String, dynamic>{
      'shopOrdersWithRiderAssignments': instance.shopOrdersWithRiderAssignments,
    };
