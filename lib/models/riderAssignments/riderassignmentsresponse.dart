import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/vendorOrders/shopswithvendororders.dart';

part 'riderassignmentsresponse.g.dart';

@JsonSerializable()
class RiderAssignmentsResponse {
  final List<ShopsWithVendorOrders>? shopOrdersWithRiderAssignments;

  RiderAssignmentsResponse({
    this.shopOrdersWithRiderAssignments,
  });

  factory RiderAssignmentsResponse.fromJson(Map<String, dynamic> json) =>
      _$RiderAssignmentsResponseFromJson(json);
}
