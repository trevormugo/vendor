import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/homepage/maps/drawer/drawerpages/orders.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/incomingGoods/incominggoodsordersforcustomer.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/incomingGoods/qrcodeidmappings.dart';
import 'package:vendor/models/oneShopQuery/items.dart';
import 'package:vendor/models/oneShopQuery/shop.dart';

part 'incominggoodsforcustomer.g.dart';

@JsonSerializable()
class IncomingGoodsForCustomer {
  final Shop? shop;
  final String? planName;
  final List<IncomingGoodsOrdersForCustomer>? incomingGoodsOrdersForCustomer;
  IncomingGoodsForCustomer({
    this.shop,
     this.planName,
    this.incomingGoodsOrdersForCustomer,
  });

  factory IncomingGoodsForCustomer.fromJson(Map<String, dynamic> json) =>
      _$IncomingGoodsForCustomerFromJson(json);
}
