// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incominggoodsordersforcustomer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IncomingGoodsOrdersForCustomer _$IncomingGoodsOrdersForCustomerFromJson(
    Map<String, dynamic> json) {
  return IncomingGoodsOrdersForCustomer(
    orders: (json['orders'] as List<dynamic>?)
        ?.map((e) => OneOrder.fromJson(e as Map<String, dynamic>))
        .toList(),
    qrCode: json['qrCode'] == null
        ? null
        : QrCodeIdMappings.fromJson(json['qrCode'] as Map<String, dynamic>),
    handOverUser: json['handOverUser'] == null
        ? null
        : Account.fromJson(json['handOverUser'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$IncomingGoodsOrdersForCustomerToJson(
        IncomingGoodsOrdersForCustomer instance) =>
    <String, dynamic>{
      'orders': instance.orders,
      'qrCode': instance.qrCode,
      'handOverUser': instance.handOverUser,
    };
