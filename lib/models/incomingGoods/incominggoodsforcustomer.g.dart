// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incominggoodsforcustomer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IncomingGoodsForCustomer _$IncomingGoodsForCustomerFromJson(
    Map<String, dynamic> json) {
  return IncomingGoodsForCustomer(
    shop: json['shop'] == null
        ? null
        : Shop.fromJson(json['shop'] as Map<String, dynamic>),
    planName: json['planName'] as String?,
    incomingGoodsOrdersForCustomer: (json['incomingGoodsOrdersForCustomer']
            as List<dynamic>?)
        ?.map((e) =>
            IncomingGoodsOrdersForCustomer.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$IncomingGoodsForCustomerToJson(
        IncomingGoodsForCustomer instance) =>
    <String, dynamic>{
      'shop': instance.shop,
      'planName': instance.planName,
      'incomingGoodsOrdersForCustomer': instance.incomingGoodsOrdersForCustomer,
    };
