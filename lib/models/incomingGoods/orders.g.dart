// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orders.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Orders _$OrdersFromJson(Map<String, dynamic> json) {
  return Orders(
    id: json['id'] as String?,
    qrCodeId: json['qrCodeId'] as String?,
    itemId: json['itemId'] as String?,
    noOfOrderedItems: json['noOfOrderedItems'] as int?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$OrdersToJson(Orders instance) => <String, dynamic>{
      'id': instance.id,
      'qrCodeId': instance.qrCodeId,
      'itemId': instance.itemId,
      'noOfOrderedItems': instance.noOfOrderedItems,
      'timestamp': instance.timestamp,
    };
