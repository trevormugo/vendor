import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/incomingGoods/incominggoodsforcustomer.dart';

part 'incominggoodsresponse.g.dart';

@JsonSerializable()
class IncomingGoodsResponse {
  final List<IncomingGoodsForCustomer>? incomingGoods;
  IncomingGoodsResponse({
    this.incomingGoods,
  });

  factory IncomingGoodsResponse.fromJson(Map<String, dynamic> json) =>
      _$IncomingGoodsResponseFromJson(json);
}
