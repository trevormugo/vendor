// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'qrcodeidmappings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QrCodeIdMappings _$QrCodeIdMappingsFromJson(Map<String, dynamic> json) {
  return QrCodeIdMappings(
    id: json['id'] as String?,
    qrCodeUid: json['qrCodeUid'] as String?,
    customerId: json['customerId'] as String?,
    customerLongitude: (json['customerLongitude'] as num?)?.toDouble(),
    customerLatitude: (json['customerLatitude'] as num?)?.toDouble(),
    vendorId: json['vendorId'] as String?,
    shopId: json['shopId'] as String?,
    assignmentId: json['assignmentId'] as String?,
    complete: json['complete'] as bool?,
    toBeDelivered: json['toBeDelivered'] as bool?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$QrCodeIdMappingsToJson(QrCodeIdMappings instance) =>
    <String, dynamic>{
      'id': instance.id,
      'qrCodeUid': instance.qrCodeUid,
      'customerId': instance.customerId,
      'customerLongitude': instance.customerLongitude,
      'customerLatitude': instance.customerLatitude,
      'vendorId': instance.vendorId,
      'shopId': instance.shopId,
      'assignmentId': instance.assignmentId,
      'complete': instance.complete,
      'toBeDelivered': instance.toBeDelivered,
      'timestamp': instance.timestamp,
    };
