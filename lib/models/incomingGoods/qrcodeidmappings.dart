import 'package:json_annotation/json_annotation.dart';


part 'qrcodeidmappings.g.dart';

@JsonSerializable()
class QrCodeIdMappings {
  final String? id;
  final String? qrCodeUid;
  final String? customerId;
  final double? customerLongitude;
  final double? customerLatitude;
  final String? vendorId;
  final String? shopId;
  final String? assignmentId;
  final bool? complete;
  final bool? toBeDelivered;
  final int? timestamp;
  QrCodeIdMappings({
    this.id,
    this.qrCodeUid,
    this.customerId,
    this.customerLongitude,
    this.customerLatitude,
    this.vendorId,
    this.shopId,
    this.assignmentId,
    this.complete,
    this.toBeDelivered,
    this.timestamp,
  });

  factory QrCodeIdMappings.fromJson(Map<String, dynamic> json) => _$QrCodeIdMappingsFromJson(json); 
}
