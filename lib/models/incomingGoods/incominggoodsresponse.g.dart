// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incominggoodsresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IncomingGoodsResponse _$IncomingGoodsResponseFromJson(
    Map<String, dynamic> json) {
  return IncomingGoodsResponse(
    incomingGoods: (json['incomingGoods'] as List<dynamic>?)
        ?.map(
            (e) => IncomingGoodsForCustomer.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$IncomingGoodsResponseToJson(
        IncomingGoodsResponse instance) =>
    <String, dynamic>{
      'incomingGoods': instance.incomingGoods,
    };
