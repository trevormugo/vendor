import 'package:json_annotation/json_annotation.dart';


part 'orders.g.dart';

@JsonSerializable()
class Orders {
  final String? id;
  final String? qrCodeId;
  final String? itemId;
  final int? noOfOrderedItems;
  final int? timestamp;
  Orders({
    this.id,
    this.qrCodeId,
    this.itemId,
    this.noOfOrderedItems,
    this.timestamp,
  });

  factory Orders.fromJson(Map<String, dynamic> json) => _$OrdersFromJson(json); 
}
