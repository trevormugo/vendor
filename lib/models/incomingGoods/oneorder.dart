import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/oneShopQuery/items.dart';

import 'orders.dart';


part 'oneorder.g.dart';

@JsonSerializable()
class OneOrder {
  final Items? item;
  final Orders? order;
  OneOrder({
    this.item,
    this.order,
  });

  factory OneOrder.fromJson(Map<String, dynamic> json) => _$OneOrderFromJson(json); 
}
