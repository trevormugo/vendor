// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oneorder.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneOrder _$OneOrderFromJson(Map<String, dynamic> json) {
  return OneOrder(
    item: json['item'] == null
        ? null
        : Items.fromJson(json['item'] as Map<String, dynamic>),
    order: json['order'] == null
        ? null
        : Orders.fromJson(json['order'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OneOrderToJson(OneOrder instance) => <String, dynamic>{
      'item': instance.item,
      'order': instance.order,
    };
