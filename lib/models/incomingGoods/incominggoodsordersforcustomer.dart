import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/applicationUser/applicationuser.dart';
import 'package:vendor/models/incomingGoods/oneorder.dart';
import 'package:vendor/models/incomingGoods/qrcodeidmappings.dart';

part 'incominggoodsordersforcustomer.g.dart';

@JsonSerializable()
class IncomingGoodsOrdersForCustomer {
  final List<OneOrder>? orders;
  final QrCodeIdMappings? qrCode;
  final Account? handOverUser;
  IncomingGoodsOrdersForCustomer({
    this.orders,
    this.qrCode,
    this.handOverUser
  });

  factory IncomingGoodsOrdersForCustomer.fromJson(Map<String, dynamic> json) =>
      _$IncomingGoodsOrdersForCustomerFromJson(json);
}
