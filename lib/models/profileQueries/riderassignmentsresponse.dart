import 'package:json_annotation/json_annotation.dart';

import '../profileQueries/riderprofileinsights.dart';

import './assignments.dart';

part 'riderassignmentsresponse.g.dart';

@JsonSerializable()
class RiderAssignmentsResponse {
  final int? eachCategoryCap;
  final List<RiderProfileInsights>? riderAssignments;
  RiderAssignmentsResponse({
    this.eachCategoryCap,
    this.riderAssignments,
  });

  factory RiderAssignmentsResponse.fromJson(Map<String, dynamic> json) => _$RiderAssignmentsResponseFromJson(json);
}
