// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sales.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sales _$SalesFromJson(Map<String, dynamic> json) {
  return Sales(
    id: json['id'] as String?,
    qrCodeId: json['qrCodeId'] as String?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$SalesToJson(Sales instance) => <String, dynamic>{
      'id': instance.id,
      'qrCodeId': instance.qrCodeId,
      'timestamp': instance.timestamp,
    };
