import 'package:json_annotation/json_annotation.dart';


import '../profileQueries/shopsfavored.dart';

import '../oneShopQuery/shopreviews.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';
import './shopfavorites.dart';
import './rideraffiliationsquery.dart';
import './customerpurchases.dart';
import './riderassignmentsresponse.dart';

part 'profilequeries.g.dart';

@JsonSerializable()
class ProfileQueries {
  final Account user;
  final List<String> roles;
  final List<RiderShopAffiliationsQuery>? riderShopAffiliationsQuery;
  final List<ShopsFavored>? shopFavorites;
  final List<Shop>? shops;
  final List<ShopReviews>? shopReviews;
  final CustomerPurchases? vendorSales;
  final RiderAssignmentsResponse? riderAssignments;
  ProfileQueries({
    required this.user,
    required this.roles,
    this.riderShopAffiliationsQuery,
    this.shopFavorites,
    this.shops,
    this.shopReviews,
    this.vendorSales,
    this.riderAssignments,
  });

  factory ProfileQueries.fromJson(Map<String, dynamic> json) =>
      _$ProfileQueriesFromJson(json);
}
