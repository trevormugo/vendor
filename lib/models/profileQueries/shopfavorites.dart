import 'package:json_annotation/json_annotation.dart';


part 'shopfavorites.g.dart';


@JsonSerializable()
class ShopFavorites {
  final String? id;
  final String? shopId;
  final String? userId;
  final int? timestamp;

  ShopFavorites({
    this.id,
    this.shopId,
    this.userId,
    this.timestamp,
  });

  factory ShopFavorites.fromJson(Map<String, dynamic> json) => _$ShopFavoritesFromJson(json);
}
