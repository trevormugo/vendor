import 'package:json_annotation/json_annotation.dart';

import '../availableCategoriesResponse/availablecategories.dart';


import './sales.dart';

part 'riderprofileinsights.g.dart';

@JsonSerializable()
class RiderProfileInsights {
  final AvailableCategories? categories;
  final int? assignments;
  RiderProfileInsights({
    this.categories,
    this.assignments,
  });

  factory RiderProfileInsights.fromJson(Map<String, dynamic> json) => _$RiderProfileInsightsFromJson(json);
}
