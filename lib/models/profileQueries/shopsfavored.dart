import 'package:json_annotation/json_annotation.dart';

import '../profileQueries/shopfavorites.dart';
import '../profileQueries/shopfavorites.dart';

import '../oneShopQuery/shop.dart';

part 'shopsfavored.g.dart';


@JsonSerializable()
class ShopsFavored {
  final Shop? shop;
  final ShopFavorites? shopFavorObject;

  ShopsFavored({
    this.shop,
    this.shopFavorObject,
  });

  factory ShopsFavored.fromJson(Map<String, dynamic> json) => _$ShopsFavoredFromJson(json);
}
