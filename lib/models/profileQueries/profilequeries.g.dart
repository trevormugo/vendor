// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profilequeries.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileQueries _$ProfileQueriesFromJson(Map<String, dynamic> json) {
  return ProfileQueries(
    user: Account.fromJson(json['user'] as Map<String, dynamic>),
    roles: (json['roles'] as List<dynamic>).map((e) => e as String).toList(),
    riderShopAffiliationsQuery:
        (json['riderShopAffiliationsQuery'] as List<dynamic>?)
            ?.map((e) =>
                RiderShopAffiliationsQuery.fromJson(e as Map<String, dynamic>))
            .toList(),
    shopFavorites: (json['shopFavorites'] as List<dynamic>?)
        ?.map((e) => ShopsFavored.fromJson(e as Map<String, dynamic>))
        .toList(),
    shops: (json['shops'] as List<dynamic>?)
        ?.map((e) => Shop.fromJson(e as Map<String, dynamic>))
        .toList(),
    shopReviews: (json['shopReviews'] as List<dynamic>?)
        ?.map((e) => ShopReviews.fromJson(e as Map<String, dynamic>))
        .toList(),
    vendorSales: json['vendorSales'] == null
        ? null
        : CustomerPurchases.fromJson(
            json['vendorSales'] as Map<String, dynamic>),
    riderAssignments: json['riderAssignments'] == null
        ? null
        : RiderAssignmentsResponse.fromJson(
            json['riderAssignments'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ProfileQueriesToJson(ProfileQueries instance) =>
    <String, dynamic>{
      'user': instance.user,
      'roles': instance.roles,
      'riderShopAffiliationsQuery': instance.riderShopAffiliationsQuery,
      'shopFavorites': instance.shopFavorites,
      'shops': instance.shops,
      'shopReviews': instance.shopReviews,
      'vendorSales': instance.vendorSales,
      'riderAssignments': instance.riderAssignments,
    };
