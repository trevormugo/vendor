// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customerpurchases.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerPurchases _$CustomerPurchasesFromJson(Map<String, dynamic> json) {
  return CustomerPurchases(
    eachCategoryCap: json['eachCategoryCap'] as int?,
    purchases: (json['purchases'] as List<dynamic>?)
        ?.map((e) => ProfileInsights.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$CustomerPurchasesToJson(CustomerPurchases instance) =>
    <String, dynamic>{
      'eachCategoryCap': instance.eachCategoryCap,
      'purchases': instance.purchases,
    };
