// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rideraffiliations.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiderAffiliations _$RiderAffiliationsFromJson(Map<String, dynamic> json) {
  return RiderAffiliations(
    id: json['id'] as String?,
    riderId: json['riderId'] as String?,
    shopId: json['shopId'] as String?,
    active: json['active'] as bool?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$RiderAffiliationsToJson(RiderAffiliations instance) =>
    <String, dynamic>{
      'id': instance.id,
      'riderId': instance.riderId,
      'shopId': instance.shopId,
      'active': instance.active,
      'timestamp': instance.timestamp,
    };
