// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profileinsights.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileInsights _$ProfileInsightsFromJson(Map<String, dynamic> json) {
  return ProfileInsights(
    categories: json['categories'] == null
        ? null
        : AvailableCategories.fromJson(
            json['categories'] as Map<String, dynamic>),
    sales: json['sales'] as int?,
  );
}

Map<String, dynamic> _$ProfileInsightsToJson(ProfileInsights instance) =>
    <String, dynamic>{
      'categories': instance.categories,
      'sales': instance.sales,
    };
