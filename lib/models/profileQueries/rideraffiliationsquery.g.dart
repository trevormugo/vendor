// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rideraffiliationsquery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiderShopAffiliationsQuery _$RiderShopAffiliationsQueryFromJson(
    Map<String, dynamic> json) {
  return RiderShopAffiliationsQuery(
    shops: json['shops'] == null
        ? null
        : Shop.fromJson(json['shops'] as Map<String, dynamic>),
    affiliation: json['affiliation'] == null
        ? null
        : RiderAffiliations.fromJson(
            json['affiliation'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RiderShopAffiliationsQueryToJson(
        RiderShopAffiliationsQuery instance) =>
    <String, dynamic>{
      'shops': instance.shops,
      'affiliation': instance.affiliation,
    };
