// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'riderassignmentsresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiderAssignmentsResponse _$RiderAssignmentsResponseFromJson(
    Map<String, dynamic> json) {
  return RiderAssignmentsResponse(
    eachCategoryCap: json['eachCategoryCap'] as int?,
    riderAssignments: (json['riderAssignments'] as List<dynamic>?)
        ?.map((e) => RiderProfileInsights.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$RiderAssignmentsResponseToJson(
        RiderAssignmentsResponse instance) =>
    <String, dynamic>{
      'eachCategoryCap': instance.eachCategoryCap,
      'riderAssignments': instance.riderAssignments,
    };
