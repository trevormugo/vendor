import 'package:json_annotation/json_annotation.dart';

import '../applicationUser/applicationuser.dart';
import '../oneShopQuery/shop.dart';
import './shopfavorites.dart';
import './rideraffiliationsquery.dart';
import './customerpurchases.dart';

part 'sales.g.dart';

@JsonSerializable()
class Sales {
  final String? id;
  final String? qrCodeId;
  final int? timestamp;
  Sales({
    this.id,
    this.qrCodeId,
    this.timestamp,
  });

  factory Sales.fromJson(Map<String, dynamic> json) => _$SalesFromJson(json);
  
}
