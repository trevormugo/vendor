// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'assignments.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Assignments _$AssignmentsFromJson(Map<String, dynamic> json) {
  return Assignments(
    id: json['id'] as String?,
    qrMappingId: json['qrMappingId'] as String?,
    riderAssignedId: json['riderAssignedId'] as String?,
    shopId: json['shopId'] as String?,
    assignmentCodeUid: json['assignmentCodeUid'] as String?,
    customerId: json['customerId'] as String?,
    customerLongitude: (json['customerLongitude'] as num?)?.toDouble(),
    customerLatitude: (json['customerLatitude'] as num?)?.toDouble(),
    status: json['status'] as bool?,
    timestamp: json['timestamp'] as int?,
    read: json['read'] as bool?,
  );
}

Map<String, dynamic> _$AssignmentsToJson(Assignments instance) =>
    <String, dynamic>{
      'id': instance.id,
      'qrMappingId': instance.qrMappingId,
      'riderAssignedId': instance.riderAssignedId,
      'shopId': instance.shopId,
      'assignmentCodeUid': instance.assignmentCodeUid,
      'customerId': instance.customerId,
      'customerLongitude': instance.customerLongitude,
      'customerLatitude': instance.customerLatitude,
      'status': instance.status,
      'read': instance.read,
      'timestamp': instance.timestamp,
    };
