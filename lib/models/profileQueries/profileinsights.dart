import 'package:json_annotation/json_annotation.dart';

import '../availableCategoriesResponse/availablecategories.dart';


import '../oneShopQuery/categories.dart';

import './sales.dart';

part 'profileinsights.g.dart';

@JsonSerializable()
class ProfileInsights {
  final AvailableCategories? categories;
  final int? sales;
  ProfileInsights({
    this.categories,
    this.sales,
  });

  factory ProfileInsights.fromJson(Map<String, dynamic> json) => _$ProfileInsightsFromJson(json);
}
