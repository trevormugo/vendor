import 'package:json_annotation/json_annotation.dart';

import '../profileQueries/profileinsights.dart';

import './sales.dart';

part 'customerpurchases.g.dart';

@JsonSerializable()
class CustomerPurchases {
  final int? eachCategoryCap;
  final List<ProfileInsights>? purchases;
  CustomerPurchases({
    this.eachCategoryCap,
    this.purchases,
  });

  factory CustomerPurchases.fromJson(Map<String, dynamic> json) => _$CustomerPurchasesFromJson(json);
}
