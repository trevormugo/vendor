import 'package:json_annotation/json_annotation.dart';

part 'rideraffiliations.g.dart';

@JsonSerializable()
class RiderAffiliations {
  final String? id;
  final String? riderId;
  final String? shopId;
  final bool? active;
  final int? timestamp;
  RiderAffiliations({
    this.id,
    this.riderId,
    this.shopId,
    this.active,
    this.timestamp,
  });

  factory RiderAffiliations.fromJson(Map<String, dynamic> json) =>
      _$RiderAffiliationsFromJson(json);
}
