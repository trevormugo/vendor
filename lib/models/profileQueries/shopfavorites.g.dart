// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopfavorites.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopFavorites _$ShopFavoritesFromJson(Map<String, dynamic> json) {
  return ShopFavorites(
    id: json['id'] as String?,
    shopId: json['shopId'] as String?,
    userId: json['userId'] as String?,
    timestamp: json['timestamp'] as int?,
  );
}

Map<String, dynamic> _$ShopFavoritesToJson(ShopFavorites instance) =>
    <String, dynamic>{
      'id': instance.id,
      'shopId': instance.shopId,
      'userId': instance.userId,
      'timestamp': instance.timestamp,
    };
