import 'package:json_annotation/json_annotation.dart';

import '../oneShopQuery/shop.dart';
import './rideraffiliations.dart';

part 'rideraffiliationsquery.g.dart';

@JsonSerializable()
class RiderShopAffiliationsQuery {
  final Shop? shops;
  final RiderAffiliations? affiliation;
  RiderShopAffiliationsQuery({
    this.shops,
    this.affiliation,
  });

  factory RiderShopAffiliationsQuery.fromJson(Map<String, dynamic> json) => _$RiderShopAffiliationsQueryFromJson(json);
}