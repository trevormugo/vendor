import 'package:json_annotation/json_annotation.dart';

part 'assignments.g.dart';

@JsonSerializable()
class Assignments {
  final String? id;
  final String? qrMappingId;
  final String? riderAssignedId;
  final String? shopId;
  final String? assignmentCodeUid;
  final String? customerId;
  final double? customerLongitude;
  final double? customerLatitude;
  final bool? status;
  final bool? read;
  final int? timestamp;
  Assignments({
    this.id,
    this.qrMappingId,
    this.riderAssignedId,
    this.shopId,
    this.assignmentCodeUid,
    this.customerId,
    this.customerLongitude,
    this.customerLatitude,
    this.status,
    this.timestamp,
    this.read,
  });

  factory Assignments.fromJson(Map<String, dynamic> json) =>
      _$AssignmentsFromJson(json);
}
