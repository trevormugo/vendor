// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'riderprofileinsights.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiderProfileInsights _$RiderProfileInsightsFromJson(Map<String, dynamic> json) {
  return RiderProfileInsights(
    categories: json['categories'] == null
        ? null
        : AvailableCategories.fromJson(
            json['categories'] as Map<String, dynamic>),
    assignments: json['assignments'] as int?,
  );
}

Map<String, dynamic> _$RiderProfileInsightsToJson(
        RiderProfileInsights instance) =>
    <String, dynamic>{
      'categories': instance.categories,
      'assignments': instance.assignments,
    };
