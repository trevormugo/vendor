// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shopsfavored.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopsFavored _$ShopsFavoredFromJson(Map<String, dynamic> json) {
  return ShopsFavored(
    shop: json['shop'] == null
        ? null
        : Shop.fromJson(json['shop'] as Map<String, dynamic>),
    shopFavorObject: json['shopFavorObject'] == null
        ? null
        : ShopFavorites.fromJson(
            json['shopFavorObject'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ShopsFavoredToJson(ShopsFavored instance) =>
    <String, dynamic>{
      'shop': instance.shop,
      'shopFavorObject': instance.shopFavorObject,
    };
