class LikeShopRequest {
  final String shopId;
  final String userId;

  LikeShopRequest({
    required this.shopId,
    required this.userId,
  });

  Map<String, dynamic> toJson() => {
        'ShopId': shopId,
        'UserId': userId,
      };
}
