// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'itemreviewers.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemReviewers _$ItemReviewersFromJson(Map<String, dynamic> json) {
  return ItemReviewers(
    user: Account.fromJson(json['user'] as Map<String, dynamic>),
    reviews: ItemReviews.fromJson(json['reviews'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ItemReviewersToJson(ItemReviewers instance) =>
    <String, dynamic>{
      'user': instance.user,
      'reviews': instance.reviews,
    };
