import 'package:json_annotation/json_annotation.dart';

part 'itemimages.g.dart';

@JsonSerializable()
class ItemImages {
  final String id;
  final String itemId;
  final int? imageNo;
  final String photomimetype;
  final String photoext;
  final int photofilesize;
  final String photoPath;
  final int timestamp;

  ItemImages({
    required this.id,
    required this.itemId,
    this.imageNo,
    required this.photomimetype,
    required this.photoext,
    required this.photofilesize,
    required this.photoPath,
    required this.timestamp,
  });

  factory ItemImages.fromJson(Map<String, dynamic> json) =>
      _$ItemImagesFromJson(json);
}
