import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/homepage/shop/oneitem/oneItemTabs/itemReviews.dart';


import '../applicationUser/applicationuser.dart';
import './itemreviews.dart';

part 'itemreviewers.g.dart';

@JsonSerializable()
class ItemReviewers {
  final Account user;
  final ItemReviews reviews;

  ItemReviewers({
    required this.user,
    required this.reviews,
  });

  factory ItemReviewers.fromJson(Map<String, dynamic> json) =>
      _$ItemReviewersFromJson(json);
}
