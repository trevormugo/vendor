import 'package:json_annotation/json_annotation.dart';

part 'returnpolicy.g.dart';

@JsonSerializable()
class ReturnPolicy {
  final String id;
  final String? itemId;
  final bool deletable;
  final String policyText;
  final int timestamp;

  ReturnPolicy({
    required this.id,
    this.itemId,
    required this.deletable,
    required this.policyText,
    required this.timestamp,
  });

  factory ReturnPolicy.fromJson(Map<String, dynamic> json) =>
      _$ReturnPolicyFromJson(json);
}
