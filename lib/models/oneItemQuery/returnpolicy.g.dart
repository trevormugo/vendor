// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'returnpolicy.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReturnPolicy _$ReturnPolicyFromJson(Map<String, dynamic> json) {
  return ReturnPolicy(
    id: json['id'] as String,
    itemId: json['itemId'] as String?,
    deletable: json['deletable'] as bool,
    policyText: json['policyText'] as String,
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$ReturnPolicyToJson(ReturnPolicy instance) =>
    <String, dynamic>{
      'id': instance.id,
      'itemId': instance.itemId,
      'deletable': instance.deletable,
      'policyText': instance.policyText,
      'timestamp': instance.timestamp,
    };
