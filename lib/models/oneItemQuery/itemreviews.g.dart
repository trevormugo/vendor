// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'itemreviews.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemReviews _$ItemReviewsFromJson(Map<String, dynamic> json) {
  return ItemReviews(
    id: json['id'] as String,
    review: json['review'] as String,
    rating: (json['rating'] as num).toDouble(),
    itemId: json['itemId'] as String,
    userId: json['userId'] as String,
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$ItemReviewsToJson(ItemReviews instance) =>
    <String, dynamic>{
      'id': instance.id,
      'review': instance.review,
      'rating': instance.rating,
      'itemId': instance.itemId,
      'userId': instance.userId,
      'timestamp': instance.timestamp,
    };
