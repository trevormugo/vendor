import 'package:json_annotation/json_annotation.dart';

import '../profileQueries/sales.dart';
import '../oneShopQuery/items.dart';
import '../applicationUser/applicationuser.dart';
import './itemimages.dart';
import './itemreviewers.dart';
import './returnpolicy.dart';

part 'oneitemquery.g.dart';

@JsonSerializable()
class OneItemQuery {
  final String plan;
  final Items item;
  final bool hasBoughtItemBefore;
  final bool hasReviewedBefore;
  final ItemReviewers? previousReviews;
  final List<Sales>? userPreviousPurchases;
  final List<ItemImages>? itemImages;
  final Account owner;
  final List<ItemReviewers>? itemReviews;
  final List<ReturnPolicy> returnPolicy;

  OneItemQuery({
    required this.plan,
    required this.item,
    this.previousReviews,
    required this.hasBoughtItemBefore,
    required this.hasReviewedBefore,
    this.userPreviousPurchases,
    this.itemImages,
    required this.owner,
    this.itemReviews,
    required this.returnPolicy,
  });

  factory OneItemQuery.fromJson(Map<String, dynamic> json) =>
      _$OneItemQueryFromJson(json);
}
