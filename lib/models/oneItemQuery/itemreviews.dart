import 'package:json_annotation/json_annotation.dart';

part 'itemreviews.g.dart';

@JsonSerializable()
class ItemReviews {
  final String id;
  final String review;
  final double rating;
  final String itemId;
  final String userId;
  final int timestamp;

  ItemReviews({
    required this.id,
    required this.review,
    required this.rating,
    required this.itemId,
    required this.userId,
    required this.timestamp,
  });

  factory ItemReviews.fromJson(Map<String, dynamic> json) =>
      _$ItemReviewsFromJson(json);
}
