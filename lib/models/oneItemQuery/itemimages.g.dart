// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'itemimages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ItemImages _$ItemImagesFromJson(Map<String, dynamic> json) {
  return ItemImages(
    id: json['id'] as String,
    itemId: json['itemId'] as String,
    imageNo: json['imageNo'] as int?,
    photomimetype: json['photomimetype'] as String,
    photoext: json['photoext'] as String,
    photofilesize: json['photofilesize'] as int,
    photoPath: json['photoPath'] as String,
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$ItemImagesToJson(ItemImages instance) =>
    <String, dynamic>{
      'id': instance.id,
      'itemId': instance.itemId,
      'imageNo': instance.imageNo,
      'photomimetype': instance.photomimetype,
      'photoext': instance.photoext,
      'photofilesize': instance.photofilesize,
      'photoPath': instance.photoPath,
      'timestamp': instance.timestamp,
    };
