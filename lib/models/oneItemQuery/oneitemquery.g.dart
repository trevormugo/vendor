// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oneitemquery.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OneItemQuery _$OneItemQueryFromJson(Map<String, dynamic> json) {
  return OneItemQuery(
    plan: json['plan'] as String,
    item: Items.fromJson(json['item'] as Map<String, dynamic>),
    previousReviews: json['previousReviews'] == null
        ? null
        : ItemReviewers.fromJson(
            json['previousReviews'] as Map<String, dynamic>),
    hasBoughtItemBefore: json['hasBoughtItemBefore'] as bool,
    hasReviewedBefore: json['hasReviewedBefore'] as bool,
    userPreviousPurchases: (json['userPreviousPurchases'] as List<dynamic>?)
        ?.map((e) => Sales.fromJson(e as Map<String, dynamic>))
        .toList(),
    itemImages: (json['itemImages'] as List<dynamic>?)
        ?.map((e) => ItemImages.fromJson(e as Map<String, dynamic>))
        .toList(),
    owner: Account.fromJson(json['owner'] as Map<String, dynamic>),
    itemReviews: (json['itemReviews'] as List<dynamic>?)
        ?.map((e) => ItemReviewers.fromJson(e as Map<String, dynamic>))
        .toList(),
    returnPolicy: (json['returnPolicy'] as List<dynamic>)
        .map((e) => ReturnPolicy.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OneItemQueryToJson(OneItemQuery instance) =>
    <String, dynamic>{
      'plan': instance.plan,
      'item': instance.item,
      'hasBoughtItemBefore': instance.hasBoughtItemBefore,
      'hasReviewedBefore': instance.hasReviewedBefore,
      'previousReviews': instance.previousReviews,
      'userPreviousPurchases': instance.userPreviousPurchases,
      'itemImages': instance.itemImages,
      'owner': instance.owner,
      'itemReviews': instance.itemReviews,
      'returnPolicy': instance.returnPolicy,
    };
