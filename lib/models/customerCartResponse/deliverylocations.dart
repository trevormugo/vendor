import 'package:json_annotation/json_annotation.dart';

part 'deliverylocations.g.dart';

@JsonSerializable()
class DeliveryLocations {
  final String id;
  final String customerId;
  final String locationTag;
  final bool defaultLocation;
  final double customerLongitude;
  final double customerLatitude;
  final int timestamp;

  DeliveryLocations({
    required this.id,
    required this.customerId,
    required this.locationTag,
    required this.defaultLocation,
    required this.customerLongitude,
    required this.customerLatitude,
    required this.timestamp,
  });

  factory DeliveryLocations.fromJson(Map<String, dynamic> json) =>
      _$DeliveryLocationsFromJson(json);
}
 