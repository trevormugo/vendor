import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/customerCartResponse/deliverylocations.dart';


import './customercartobject.dart';


part 'deliverydetails.g.dart';


@JsonSerializable()
class DeliveryDetails {
  final String locationId;
  final int deliveryAmmount;
  DeliveryDetails({
    required this.locationId,
    required this.deliveryAmmount,
  });

  factory DeliveryDetails.fromJson(Map<String, dynamic> json) => _$DeliveryDetailsFromJson(json);
}
