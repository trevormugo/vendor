// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customercartresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerCartResponse _$CustomerCartResponseFromJson(Map<String, dynamic> json) {
  return CustomerCartResponse(
    items: (json['items'] as List<dynamic>?)
        ?.map((e) => CustomerCartObject.fromJson(e as Map<String, dynamic>))
        .toList(),
    deliveryLocations: (json['deliveryLocations'] as List<dynamic>?)
        ?.map((e) => DeliveryLocations.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$CustomerCartResponseToJson(
        CustomerCartResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
      'deliveryLocations': instance.deliveryLocations,
    };
