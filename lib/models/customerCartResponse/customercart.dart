import 'package:json_annotation/json_annotation.dart';

part 'customercart.g.dart';

@JsonSerializable()
class CustomerCart {
  final String id;
  final String itemId;
  final String shopId;
  final String customerId;
  final int noOfItems;
  final int timestamp;

  CustomerCart({
    required this.id,
    required this.itemId,
    required this.shopId,
    required this.customerId,
    required this.noOfItems,
    required this.timestamp,
  });

  factory CustomerCart.fromJson(Map<String, dynamic> json) =>
      _$CustomerCartFromJson(json);
}
