// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customercartobject.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerCartObject _$CustomerCartObjectFromJson(Map<String, dynamic> json) {
  return CustomerCartObject(
    customerCartObject: CustomerCart.fromJson(
        json['customerCartObject'] as Map<String, dynamic>),
    item: Items.fromJson(json['item'] as Map<String, dynamic>),
    itemImages: json['itemImages'] == null
        ? null
        : ItemImages.fromJson(json['itemImages'] as Map<String, dynamic>),
    deliveryDetails: json['deliveryDetails'] == null
        ? null
        : DeliveryDetails.fromJson(
            json['deliveryDetails'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CustomerCartObjectToJson(CustomerCartObject instance) =>
    <String, dynamic>{
      'customerCartObject': instance.customerCartObject,
      'item': instance.item,
      'itemImages': instance.itemImages,
      'deliveryDetails': instance.deliveryDetails,
    };
