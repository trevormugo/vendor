// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'deliverydetails.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeliveryDetails _$DeliveryDetailsFromJson(Map<String, dynamic> json) {
  return DeliveryDetails(
    locationId: json['locationId'] as String,
    deliveryAmmount: json['deliveryAmmount'] as int,
  );
}

Map<String, dynamic> _$DeliveryDetailsToJson(DeliveryDetails instance) =>
    <String, dynamic>{
      'locationId': instance.locationId,
      'deliveryAmmount': instance.deliveryAmmount,
    };
