import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/customerCartResponse/deliverydetails.dart';
import 'package:vendor/models/oneItemQuery/itemimages.dart';
import 'package:vendor/models/oneShopQuery/items.dart';

import './customercart.dart';

part 'customercartobject.g.dart';

@JsonSerializable()
class CustomerCartObject {
  final CustomerCart customerCartObject;
  final Items item;
  final ItemImages? itemImages;
  final DeliveryDetails? deliveryDetails;
  CustomerCartObject({
    required this.customerCartObject,
    required this.item,
    this.itemImages,
    this.deliveryDetails,
  });

  factory CustomerCartObject.fromJson(Map<String, dynamic> json) =>
      _$CustomerCartObjectFromJson(json);
}
