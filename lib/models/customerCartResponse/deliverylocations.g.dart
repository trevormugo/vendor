// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'deliverylocations.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeliveryLocations _$DeliveryLocationsFromJson(Map<String, dynamic> json) {
  return DeliveryLocations(
    id: json['id'] as String,
    customerId: json['customerId'] as String,
    locationTag: json['locationTag'] as String,
    defaultLocation: json['defaultLocation'] as bool,
    customerLongitude: (json['customerLongitude'] as num).toDouble(),
    customerLatitude: (json['customerLatitude'] as num).toDouble(),
    timestamp: json['timestamp'] as int,
  );
}

Map<String, dynamic> _$DeliveryLocationsToJson(DeliveryLocations instance) =>
    <String, dynamic>{
      'id': instance.id,
      'customerId': instance.customerId,
      'locationTag': instance.locationTag,
      'defaultLocation': instance.defaultLocation,
      'customerLongitude': instance.customerLongitude,
      'customerLatitude': instance.customerLatitude,
      'timestamp': instance.timestamp,
    };
