import 'package:json_annotation/json_annotation.dart';
import 'package:vendor/models/customerCartResponse/deliverylocations.dart';


import './customercartobject.dart';


part 'customercartresponse.g.dart';


@JsonSerializable()
class CustomerCartResponse {
  final List<CustomerCartObject>? items;
  final List<DeliveryLocations>? deliveryLocations;
  CustomerCartResponse({
    this.items,
    this.deliveryLocations,
  });

  factory CustomerCartResponse.fromJson(Map<String, dynamic> json) => _$CustomerCartResponseFromJson(json);
}
