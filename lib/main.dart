import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:vendor/providers/detailupdate_provider.dart';
import 'package:vendor/providers/shopimage_provider.dart';
import 'package:vendor/welcomepage/welcomepage.dart';

import './restapi.dart';
import 'homepage/maps/maps.dart';

import 'dart:convert';

import './models/logInRequest/loginrequest.dart';
import './forgotpassword/forgotpassword.dart';
import './signup/signup.dart';
import 'models/applicationUser/applicationuser.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("Handling a background message: ${message.messageId}");
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  SharedPreferences.getInstance().then((prefs) {
    runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => FavoriteProvider()),
          ChangeNotifierProvider(create: (_) => ShopImageProvider()),
        ],
        child: MyApp(prefs: prefs),
      ),
    );
  });
}

class MyApp extends StatelessWidget {
  MyApp({this.prefs});
  final prefs;
  late String token;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Root',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.white,
        accentColor: Color.fromARGB(255, 27, 81, 211),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        backgroundColor: Colors.white,
      ),
      home: _decideMainPage(prefs),
    );
  }
}

Widget _decideMainPage(dynamic prefs) {
  if (prefs.getString('token') == null) {
    return LoginPage();
  } else {
    return CustomerMap(
      token: prefs.getString('token'),
      id: prefs.getString('userid'),
    );
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _passcontroller = TextEditingController();

  bool _operationinprogress = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _emailcontroller.dispose();
    _passcontroller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void _createaccount() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SignUpPage()),
      );
    }

    void _forgotpassword() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ForgotPassword()),
      );
    }

    void _login() async {
      if (_formKey.currentState!.validate()) {
        if (_operationinprogress == true) {
          AwesomeDialog(
            context: context,
            dialogType: DialogType.INFO,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Info',
            desc: 'Operation in progress.',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          setState(() {
            _operationinprogress = true;
          });
          var loginresponse = await RestApi().login(
              "/accountspool/login",
              LogInRequest(
                email: _emailcontroller.text,
                password: _passcontroller.text,
              ));
          if (loginresponse.statusCode == 200) {
            setState(() {
              _operationinprogress = false;
            });
            print("${loginresponse.body}");
            var account = Account.fromJson(json.decode(loginresponse.body));
            SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setString('userid', account.id);
            await prefs.setString('useremail', account.email);
            await prefs.setString('token', account.token);
            await prefs.setString('userphonenumber', account.phoneNumber);
            await prefs.setString('username', account.userName);

            /* TO DO */
            //VERIFY PURCHASES LOGIC BEFORE ROUTING
            await prefs.setString('subid', "id");
            bool? isFirstRun = prefs.getBool("firstRun");
            if (isFirstRun != null) {
              if (isFirstRun == true) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        WelcomePage(token: account.token, userId: account.id),
                  ),
                );
              } else {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        CustomerMap(id: account.id, token: account.token),
                  ),
                );
              }
            } else {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      CustomerMap(id: account.id, token: account.token),
                ),
              );
            }
          } else {
            setState(() {
              _operationinprogress = false;
            });
            AwesomeDialog(
              context: context,
              dialogType: DialogType.ERROR,
              animType: AnimType.BOTTOMSLIDE,
              title: '${loginresponse.statusCode}',
              desc: '${loginresponse.body}',
              btnCancelOnPress: () {},
              btnOkOnPress: () {},
            )..show();
          }
        }
      }
    }

    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextButton(
                    onPressed: _forgotpassword,
                    child: Text(
                      "Forgot password?",
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Center(
                    child: Image.asset(
                      "assets/images/RealLogo.png",
                      //scale: 4,
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            enabled: !_operationinprogress,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                            controller: _emailcontroller,
                            obscureText: false,
                            decoration: InputDecoration(
                              // contentPadding: EdgeInsets.symmetric(
                              //     vertical: 10.0, horizontal: 10.0),
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter your email",
                            ),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.all(20),
                          child: TextFormField(
                            enabled: !_operationinprogress,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            controller: _passcontroller,
                            obscureText: true,
                            keyboardType: TextInputType.visiblePassword,
                            decoration: InputDecoration(
                              // contentPadding: EdgeInsets.symmetric(
                              //     vertical: 10.0, horizontal: 10.0),
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromARGB(255, 27, 81, 211)),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter your password",
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: _login,
                          child: Text(
                            "Log In",
                            style: TextStyle(
                              color: Color.fromARGB(255, 27, 81, 211),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 50,
                      width: 50,
                      child: (_operationinprogress == true)
                          ? CircularProgressIndicator(
                              backgroundColor: Colors.transparent)
                          : Container(),
                    ),
                  ),
                  TextButton(
                    onPressed:
                        (_operationinprogress == true) ? null : _createaccount,
                    child: Text(
                      "Dont have an account?",
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
