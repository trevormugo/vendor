import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:vendor/models/account.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

import '../models/signUpRequest/signuprequest.dart';

import '../verifycode/verifycode.dart';
import '../restapi.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageInstance createState() => _SignUpPageInstance();
}

class _SignUpPageInstance extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _fullnamecontroller = TextEditingController();
  TextEditingController _usernamecontroller = TextEditingController();
  TextEditingController _phonenumbercontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _passcontroller = TextEditingController();
  TextEditingController _repeatpasswordcontroller = TextEditingController();
  String? phoneNumber;
  bool _operationinprogress = false;
  bool isValid = false;
  static SignUpRequest? values;
  void _signup() async {
    setState(() {
      _operationinprogress = true;
    });
    if (_formKey.currentState!.validate()) {
      if (_passcontroller.text != _repeatpasswordcontroller.text) {
        setState(() {
          _operationinprogress = false;
        });

        AwesomeDialog(
          context: context,
          dialogType: DialogType.ERROR,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Error',
          desc: 'Passwords dont match',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      } else if (_passcontroller.text.length < 9) {
        setState(() {
          _operationinprogress = false;
        });
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: 'Error',
          desc: 'A 10 character password is required',
          btnCancelOnPress: () {},
          btnOkOnPress: () {},
        )..show();
      } else {
        if (phoneNumber == null) {
          setState(() {
            _operationinprogress = false;
          });
          AwesomeDialog(
            context: context,
            dialogType: DialogType.ERROR,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Error',
            desc: 'Phone Number inuput is required',
            btnCancelOnPress: () {},
            btnOkOnPress: () {},
          )..show();
        } else {
          if (!isValid) {
            setState(() {
              _operationinprogress = false;
            });
            AwesomeDialog(
              context: context,
              dialogType: DialogType.ERROR,
              animType: AnimType.BOTTOMSLIDE,
              title: 'Error',
              desc: 'The phone number you entered is not valid',
              btnCancelOnPress: null,
              btnOkOnPress: null,
            )..show();
          } else {
            values = SignUpRequest(
              fullName: _fullnamecontroller.text,
              userName: _usernamecontroller.text,
              phoneNumber: phoneNumber!.trim().split("+")[1],
              email: _emailcontroller.text.trim(),
              password: _passcontroller.text.trim(),
            );
            var response = await RestApi().sendemailtocreateaccount(
                "/verification/sendcode",
                VerificationRequest(
                  email: _emailcontroller.text.trim(),
                  userName: _usernamecontroller.text,
                  phoneNumber: phoneNumber!.trim().split("+")[1],
                ));
            if (response.statusCode == 201 || response.statusCode == 200) {
              setState(() {
                _operationinprogress = false;
              });
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => VerifyCode(
                          accountcreation: true,
                          emailrecipient: _emailcontroller.text.trim(),
                          signupbody: values,
                        )),
              );
            } else if (response.statusCode == 400) {
              setState(() {
                _operationinprogress = false;
              });
              AwesomeDialog(
                context: context,
                dialogType: DialogType.ERROR,
                animType: AnimType.BOTTOMSLIDE,
                title: 'Code ${response.statusCode}',
                desc: '${response.body}',
                btnCancelOnPress: () {},
                btnOkOnPress: () {},
              )..show();
            } else {
              setState(() {
                _operationinprogress = false;
              });
              AwesomeDialog(
                context: context,
                dialogType: DialogType.ERROR,
                animType: AnimType.BOTTOMSLIDE,
                title: 'Code ${response.statusCode}',
                desc: '${response.body}',
                btnCancelOnPress: () {},
                btnOkOnPress: () {},
              )..show();
            }
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: AppBar(
            title: Text("Sign up"),
          ),
        ),
        extendBodyBehindAppBar: true,
        body: SafeArea(
          top: true,
          bottom: false,
          left: false,
          right: false,
          child: LayoutBuilder(builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                    minHeight: constraints.maxHeight),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          enabled: !_operationinprogress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _fullnamecontroller,
                          obscureText: false,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter your Full name"),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          enabled: !_operationinprogress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _usernamecontroller,
                          obscureText: false,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter your user name"),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.all(20),
                        child: InternationalPhoneNumberInput(
                          onInputChanged: (PhoneNumber number) {
                            setState(() {
                              print(number.phoneNumber);
                              phoneNumber = number.phoneNumber;
                            });
                          },
                          onInputValidated: (bool value) {
                            setState(() {
                              print(value);
                              isValid = value;
                            });
                          },
                          isEnabled: !_operationinprogress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          selectorConfig: SelectorConfig(
                            selectorType: PhoneInputSelectorType.DROPDOWN,
                          ),
                          hintText: "Enter your phone number",
                          ignoreBlank: false,
                          autoValidateMode: AutovalidateMode.onUserInteraction,
                          selectorTextStyle: TextStyle(color: Colors.black),
                          textFieldController: _phonenumbercontroller,
                          formatInput: false,
                          keyboardType: TextInputType.numberWithOptions(
                              signed: true, decimal: true),
                          inputBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          inputDecoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: (isValid)
                                    ? Color.fromARGB(255, 27, 81, 211)
                                    : Colors.red,
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: (isValid)
                                      ? Color.fromARGB(255, 27, 81, 211)
                                      : Colors.red,
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText:
                                  "Enter your phone number(+254 format)"),
                          onSaved: (PhoneNumber number) {
                            print('On Saved: $number');
                          },
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          enabled: !_operationinprogress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _emailcontroller,
                          obscureText: false,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter your email"),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          enabled: !_operationinprogress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _passcontroller,
                          obscureText: true,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Enter your password"),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.all(20),
                        child: TextFormField(
                          enabled: !_operationinprogress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _repeatpasswordcontroller,
                          obscureText: true,
                          decoration: InputDecoration(
                              labelStyle: TextStyle(
                                color: Color.fromARGB(255, 27, 81, 211),
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color.fromARGB(255, 27, 81, 211),
                                ),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(12),
                                ),
                              ),
                              labelText: "Repeat password"),
                        ),
                      ),
                      TextButton(
                        onPressed:
                            (_operationinprogress == true) ? null : _signup,
                        child: Text(
                          "Create Account",
                          style: TextStyle(
                            color: Color.fromARGB(255, 27, 81, 211),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
        ),
      ),
      (_operationinprogress == true)
          ? BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                child: Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                ),
              ),
            )
          : Container(),
    ]);
  }
}
