import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'dart:math';

import './customerdetails.dart';
import '../homepage/maps/maps.dart';
import './pagelayout/pagelayout.dart';
import './cards/cards.dart';

class AccountPool extends StatefulWidget {
  AccountPool({
    required this.id,
    required this.email,
    required this.phonenumber,
    required this.username,
  });
  final String? id;
  final String? email;
  final String? phonenumber;
  final String? username;
  @override
  _AccountPoolInstance createState() => _AccountPoolInstance();
}

class _AccountPoolInstance extends State<AccountPool> {
  PageController pageViewController = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Text(
            "Subscriptions",
            style: TextStyle(fontFamily: 'NanumGothic'),
          ),
          actions: [
            GestureDetector(
              onTap: null,
              child: Icon(
                LineIcons.arrowRight,
              ),
            ),
          ],
        ),
      ),
      extendBodyBehindAppBar: true,
      body: PageLayout(
        id: widget.id,
        email: widget.email,
        phonenumber: widget.phonenumber,
        username: widget.username,
      ),
    );
  }
}
