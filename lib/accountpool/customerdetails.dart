import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:square_in_app_payments/in_app_payments.dart';
import 'package:square_in_app_payments/models.dart';
import 'package:geolocator/geolocator.dart';

import '../customdialog.dart';
import '../homepage/maps/maps.dart';
import '../restapi.dart';

class CustomerDetails extends StatefulWidget {
  CustomerDetails({
    required this.email,
    required this.phonenumber,
    required this.id,
    required this.username,
    required this.currency,
    required this.ammount,
  });
  final String email;
  final String phonenumber;
  final String id;
  final String username;
  final String currency;
  final double ammount;
  @override
  _CustomerDetailsInstance createState() => _CustomerDetailsInstance();
}

class _CustomerDetailsInstance extends State<CustomerDetails> {
  @override
  void initState() {
    super.initState();
    InAppPayments.setSquareApplicationId(
        'sandbox-sq0idb-2sJMKHOOa92UD4-_0uVzxA');
    initializepermissions();
    _locationsdemostate();
  }

  final _formKey = GlobalKey<FormState>();
  TextEditingController _firstnamescontroller = TextEditingController();
  TextEditingController _surnamecontroller = TextEditingController();
  TextEditingController _phonecontroller = TextEditingController();
  late String locationid;
  late Contact contact;

  void _locationsdemostate() {
    locationid = "LD0XHDP99B4X0";
  }

  void initializepermissions() async {
    LocationPermission checkpermission = await Geolocator.checkPermission();
    if (checkpermission == LocationPermission.whileInUse ||
        checkpermission == LocationPermission.always) {
      print("permissions granted");
    } else {
      LocationPermission requestpermission =
          await Geolocator.requestPermission();
    }
  }


  void _create() async {
    
  }

  void errorhandler(String errortitle) {
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          title: Text("Customer Details"),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: LayoutBuilder(builder: (context, constraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: constraints.maxWidth,
                  minHeight: constraints.maxHeight),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 50,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: _firstnamescontroller,
                        obscureText: false,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.elliptical(30, 30),
                                topRight: Radius.elliptical(30, 30),
                              ),
                            ),
                            labelText: "Enter you\'re Given names"),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 50,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: _surnamecontroller,
                        obscureText: false,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.elliptical(30, 30),
                                topRight: Radius.elliptical(30, 30),
                              ),
                            ),
                            labelText: "Enter you\'re Surname"),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 50,
                      margin: EdgeInsets.all(20),
                      child: TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                        controller: _phonecontroller,
                        obscureText: false,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.elliptical(30, 30),
                                topRight: Radius.elliptical(30, 30),
                              ),
                            ),
                            labelText:
                                "Enter you\'re Phonenumber(country code format)"),
                      ),
                    ),
                    TextButton(
                      onPressed: _create,
                      child: Text(
                        "Create",
                        style: TextStyle(
                          color: Colors.deepPurple,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
