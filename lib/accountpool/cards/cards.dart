import 'package:flutter/material.dart';
import 'dart:math';

class CardDetail extends StatefulWidget {
  CardDetail({
    required this.headercolor,
    required this.planname,
    required this.planprice,
    required this.currency,
    required this.planperks,
    required this.email,
    required this.phonenumber,
    required this.id,
    required this.username,
  });
  final Color headercolor;
  final String planname;
  final String planprice;
  final String currency;
  final List<String> planperks;
  final String? email;
  final String? phonenumber;
  final String? id;
  final String? username;
  @override
  State createState() => _CardDetailInstance();
}

class _CardDetailInstance extends State<CardDetail> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);

    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: ListView.builder(
        padding: EdgeInsets.zero,
        itemCount: widget.planperks.length + 1,
        itemBuilder: (context, index) {
          return (index == 0)
              ? Container(
                  height: screenheight * 0.4,
                  width: double.infinity,
                  color: widget.headercolor,
                  child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text(
                          '${widget.planname}',
                          softWrap: true,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            fontSize: 38,
                            color: Colors.white,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        Text(
                          '${widget.planprice} ${widget.currency} / mth',
                          softWrap: false,
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ]),
                )
              : ListTile(
                  onTap: null,
                  title: Text(
                    '${widget.planperks[index - 1]}',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                );
        },
      ),
    );
  }
}
