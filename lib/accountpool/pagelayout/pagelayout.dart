import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icons.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:vendor/models/account.dart';
import 'package:vendor/models/defaults.dart';
import 'package:vendor/models/googleverificationrequest.dart';
import 'package:vendor/restapi.dart';
import 'dart:math';

import '../customerdetails.dart';
import '../../homepage/maps/maps.dart';
import '../cards/cards.dart';

class PageLayout extends StatefulWidget {
  PageLayout({
    required this.id,
    required this.email,
    required this.phonenumber,
    required this.username,
  });
  final String? id;
  final String? email;
  final String? phonenumber;
  final String? username;
  @override
  State createState() => _PageLayoutInstance();
}

class _PageLayoutInstance extends State<PageLayout> {
  PageController pageViewController = PageController(initialPage: 0);
  late StreamSubscription<ConnectionResult> _connectionSubscription;
  late StreamSubscription<PurchasedItem?> _purchaseUpdatedSubscription;
  late StreamSubscription<PurchaseResult?> _purchaseErrorSubscription;
  List<IAPItem>? _products = [];
  List<PurchasedItem> _pastPurchases = [];
  bool _operationinprogress = false;
  late Future<List<IAPItem>> _initIAP;

  @override
  void initState() {
    super.initState();
    _initIAP = initConnection();
  }

  Future<List<IAPItem>> initConnection() async {
    await FlutterInappPurchase.instance.initConnection;
    _connectionSubscription =
        FlutterInappPurchase.connectionUpdated.listen((connected) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Store Availability',
        desc: '$connected',
        btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
    });
    _purchaseUpdatedSubscription = FlutterInappPurchase.purchaseUpdated
        .listen((PurchasedItem? productItem) async {
      if (Platform.isAndroid) {
        await _handlePurchaseUpdateAndroid(productItem);
      } else {
        await _handlePurchaseUpdateIOS(productItem);
      }
    });
    _purchaseErrorSubscription = FlutterInappPurchase.purchaseError
        .listen((PurchaseResult? purchaseError) {
      _callErrorListeners(purchaseError!.message);
    });
    _getPastPurchases();
    return await _getItems();
  }

  Future<List<IAPItem>> _getItems() async {
    _products = await FlutterInappPurchase.instance
        .getSubscriptions(["product1", "product2", "product3"]);
    AwesomeDialog(
      context: context,
      dialogType: DialogType.INFO,
      animType: AnimType.BOTTOMSLIDE,
      title: 'Store Availability',
      desc: '${_products!.length}',
      btnCancelOnPress: () {},
      btnOkOnPress: () {},
    )..show();
    return Future.value(_products);
  }

  void _getPastPurchases() async {
    if (Platform.isIOS) {
      return;
    }
    List<PurchasedItem>? purchasedItems =
        await FlutterInappPurchase.instance.getAvailablePurchases();
    if (purchasedItems == null) {
      return;
    }
    for (var purchasedItem in purchasedItems) {
      bool isValid = false;

      if (Platform.isAndroid) {
        Map map = json.decode(purchasedItem.transactionReceipt!);
        if (!map['acknowledged']) {
          isValid = await _verifyPurchase(purchasedItem);
          if (isValid) {
            FlutterInappPurchase.instance.finishTransaction(purchasedItem);
            /*PREMIUM USER*/
            _becomeAVendor(_products!
                .firstWhere(
                    (element) => element.productId == purchasedItem.productId)
                .title!);
          }
        } else {
          /*PREMIUM USER*/
          _becomeAVendor(_products!
              .firstWhere(
                  (element) => element.productId == purchasedItem.productId)
              .title!);
        }
      }
    }
    _pastPurchases.addAll(purchasedItems);
  }

  void _callErrorListeners(String? error) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.ERROR,
      animType: AnimType.BOTTOMSLIDE,
      title: "Error",
      desc: "$error",
      btnCancelOnPress: null,
      btnOkOnPress: null,
    )..show();
  }

  _verifyAndFinishTransaction(PurchasedItem purchasedItem) async {
    bool isValid = false;
    isValid = await _verifyPurchase(purchasedItem);
    if (isValid) {
      FlutterInappPurchase.instance.finishTransaction(purchasedItem);
      /*PREMIUM USER*/
      _becomeAVendor(_products!
          .firstWhere((element) => element.productId == purchasedItem.productId)
          .title!);
    } else {
      _callErrorListeners("Varification failed");
    }
  }

  Future<void> _handlePurchaseUpdateIOS(PurchasedItem? purchasedItem) async {
    switch (purchasedItem!.transactionStateIOS) {
      case TransactionState.deferred:
        break;
      case TransactionState.failed:
        _callErrorListeners("Transaction Failed");
        FlutterInappPurchase.instance.finishTransaction(purchasedItem);
        break;
      case TransactionState.purchased:
        await _verifyAndFinishTransaction(purchasedItem);
        break;
      case TransactionState.purchasing:
        break;
      case TransactionState.restored:
        FlutterInappPurchase.instance.finishTransaction(purchasedItem);
        break;
      default:
    }
  }

  /// 0 : UNSPECIFIED_STATE
  /// 1 : PURCHASED
  /// 2 : PENDING
  Future<void> _handlePurchaseUpdateAndroid(
      PurchasedItem? purchasedItem) async {
    if (purchasedItem!.purchaseStateAndroid!.index == 1) {
      if (purchasedItem.isAcknowledgedAndroid!) {
        await _verifyAndFinishTransaction(purchasedItem);
      }
    } else {
      _callErrorListeners("Something went wrong");
    }
  }

  Future buyProduct(String itemId) async {
    try {
      await FlutterInappPurchase.instance.requestSubscription(itemId);
    } catch (error) {
      _callErrorListeners(error.toString());
    }
  }

  Future<bool> _verifyPurchase(PurchasedItem purchasedItem) async {
    GoogleVerificationRequest googleVerificationRequest =
        GoogleVerificationRequest(
      productId: purchasedItem.productId!,
      purchaseToken: purchasedItem.purchaseToken!,
    );
    var response = await RestApi().verifyPurchaseToken(
        "/accountspool/verifyPurchaseToken", googleVerificationRequest);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  void _becomeAVendor(String planName) async {
    RegisterVendorRequest registerVendorRequest = RegisterVendorRequest(
      id: widget.id!,
      planName: planName,
    );
    var response = await RestApi()
        .registervendor("/accountspool/registervendor", registerVendorRequest);
    if (response.statusCode == 200) {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: "Code ${response.statusCode}",
        desc: 'You are now a vendor',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    } else {
      setState(() {
        _operationinprogress = false;
      });
      AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: "Error ${response.statusCode}",
        desc: '${response.body}',
        btnCancelOnPress: null,
        btnOkOnPress: null,
      )..show();
    }
  }

  @override
  void dispose() async {
    super.dispose();
    _connectionSubscription.cancel();
    _purchaseErrorSubscription.cancel();
    _purchaseUpdatedSubscription.cancel();
    await FlutterInappPurchase.instance.endConnection;
  }

  @override
  Widget build(BuildContext context) {
    double screenheight = max(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    double screenwidth = min(
        MediaQuery.of(context).size.width, MediaQuery.of(context).size.height);
    return SafeArea(
      top: true,
      bottom: false,
      left: false,
      right: false,
      child: Scaffold(
        body: FutureBuilder(
            future: _initIAP,
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return (snapshot.data.length == 0)
                    ? Center(
                        child: Text("No plans are available at the moment"),
                      )
                    : LayoutBuilder(builder: (context, constraints) {
                        return SingleChildScrollView(
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                                minWidth: constraints.maxWidth,
                                minHeight: constraints.maxHeight),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  height: screenheight * 0.8,
                                  width: screenwidth * 0.9,
                                  child: PageView(
                                    controller: pageViewController,
                                    children: (_pastPurchases.length != 0)
                                        ? List.generate(
                                            _pastPurchases.length,
                                            (index) => (snapshot.data.any(
                                                    (element) =>
                                                        element.productId ==
                                                        _pastPurchases[index]
                                                            .productId))
                                                ? Center(
                                                    child: Text(
                                                        "Plan no longer availale"),
                                                  )
                                                : GestureDetector(
                                                    onTap: null,
                                                    child: CardDetail(
                                                      headercolor:
                                                          Color.fromARGB(
                                                              255, 27, 81, 211),
                                                      planname:
                                                          '${snapshot.data[index].title}',
                                                      planprice:
                                                          '${snapshot.data[index].price}',
                                                      currency:
                                                          '${snapshot.data[index].currency}',
                                                      email: widget.email,
                                                      phonenumber:
                                                          widget.phonenumber,
                                                      id: widget.id,
                                                      username: widget.username,
                                                      planperks: (Perks
                                                              .perks[index]
                                                              .containsKey(
                                                                  snapshot
                                                                      .data[
                                                                          index]
                                                                      .title))
                                                          ? Perks.perks[index][
                                                              snapshot
                                                                  .data[index]
                                                                  .title]!
                                                          : [],
                                                    ),
                                                  ),
                                          )
                                        : List.generate(
                                            snapshot.data.length,
                                            (index) => GestureDetector(
                                              onTap: () => buyProduct(snapshot
                                                  .data[index].productId!),
                                              child: CardDetail(
                                                headercolor: Color.fromARGB(
                                                    255, 27, 81, 211),
                                                planname:
                                                    '${snapshot.data[index].productId}',
                                                planprice:
                                                    '${snapshot.data![index].price}',
                                                currency:
                                                    '${snapshot.data[index].currency}',
                                                email: widget.email,
                                                phonenumber: widget.phonenumber,
                                                id: widget.id,
                                                username: widget.username,
                                                planperks: (Perks.perks[index]
                                                        .containsKey(snapshot
                                                            .data[index]
                                                            .productId))
                                                    ? Perks.perks[index][
                                                        snapshot.data[index]
                                                            .productId]!
                                                    : [],
                                              ),
                                            ),
                                          ),
                                  ),
                                ),
                                (snapshot.data.length == 0)
                                    ? Container(
                                        child: null,
                                      )
                                    : Container(
                                        height: screenheight * 0.1,
                                        width: double.infinity,
                                        child: Center(
                                          child: SmoothPageIndicator(
                                            controller: pageViewController,
                                            count: snapshot.data.length,
                                            effect: WormEffect(
                                              dotWidth: 10.0,
                                              dotHeight: 10.0,
                                              radius: 10.0,
                                              activeDotColor: Color.fromARGB(
                                                  255, 27, 81, 211),
                                            ),
                                          ),
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        );
                      });
              } else if (snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Icon(
                        LineIcons.server,
                        color: Colors.grey,
                        size: 70.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text("${snapshot.error}"),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(
                  child: SpinKitFadingCube(
                    size: 60,
                    color: Color.fromARGB(255, 27, 81, 211),
                  ),
                );
              }
            }),
      ),
    );
  }
}
