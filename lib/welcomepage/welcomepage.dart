import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendor/homepage/maps/maps.dart';
import 'package:vendor/models/defaults.dart';
import 'dart:math';

class WelcomePage extends StatefulWidget {
  WelcomePage({
    required this.token,
    required this.userId,
  });
  final String token;
  final String userId;
  @override
  _WelcomePageInstance createState() => _WelcomePageInstance();
}

class _WelcomePageInstance extends State<WelcomePage> {
  List<Widget> slides = WelcomePageItems.items
      .map((item) => Container(
          padding: EdgeInsets.symmetric(horizontal: 18.0),
          child: Column(
            children: <Widget>[
              (item['header'] == "Welcome")
                  ? Flexible(
                      flex: 1,
                      child: Container(),
                    )
                  : Flexible(
                      flex: 1,
                      fit: FlexFit.tight,
                      child: Image.asset(
                        item['image'],
                        fit: BoxFit.fitWidth,
                        width: 220.0,
                        alignment: Alignment.bottomCenter,
                      ),
                    ),
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      Text(item['header'],
                          style: TextStyle(
                              fontSize: 50.0,
                              fontWeight: FontWeight.w300,
                              color: Color(0XFF3F3D56),
                              height: 2.0)),
                      Text(
                        item['description'],
                        style: TextStyle(
                            color: Colors.grey,
                            letterSpacing: 1.2,
                            fontSize: 16.0,
                            height: 1.3),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              )
            ],
          )))
      .toList();

  List<Widget> indicator() => List<Widget>.generate(
      slides.length,
      (index) => Container(
            margin: EdgeInsets.symmetric(horizontal: 3.0),
            height: 10.0,
            width: 10.0,
            decoration: BoxDecoration(
                color: currentPage.round() == index
                    ? Color.fromARGB(255, 27, 81, 211)
                    : Color.fromARGB(255, 27, 81, 211).withOpacity(0.2),
                borderRadius: BorderRadius.circular(10.0)),
          ));

  double currentPage = 0.0;
  final _pageViewController = new PageController();

  @override
  void initState() {
    super.initState();

    _pageViewController.addListener(() {
      setState(() {
        currentPage = _pageViewController.page!;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageViewController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void navigatetomaps() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setBool('firstRun', false);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) =>
              CustomerMap(id: widget.userId, token: widget.token),
        ),
      );
    }

    return Scaffold(
      floatingActionButton: (currentPage == WelcomePageItems.items.length - 1)
          ? FloatingActionButton(
              onPressed: navigatetomaps,
              child: Icon(
                LineIcons.arrowRight,
                color: Color.fromARGB(255, 207, 118, 0),
              ),
              backgroundColor: Color.fromARGB(255, 27, 81, 211),
            )
          : null,
      body: Container(
        child: Stack(
          children: <Widget>[
            PageView.builder(
              controller: _pageViewController,
              itemCount: slides.length,
              itemBuilder: (BuildContext context, int index) {
                return slides[index];
              },
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.only(top: 70.0),
                padding: EdgeInsets.symmetric(vertical: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: indicator(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
