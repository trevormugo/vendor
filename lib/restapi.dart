import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:http/http.dart' as http;
//import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path/path.dart';
import 'package:signalr_core/signalr_core.dart';
import 'package:http/io_client.dart';
import 'package:vendor/models/acceptAffiliation.dart';
import 'package:vendor/models/affiliaterider.dart';
import 'package:vendor/models/affiliatesupervisor.dart';
import 'package:vendor/models/assingRider.dart';
import 'package:vendor/models/customercartrequest.dart';
import 'package:vendor/models/deliverylocationrequest.dart';
import 'package:vendor/models/favoriteshoprequest.dart';
import 'package:vendor/models/googleverificationrequest.dart';
import 'package:vendor/models/itemReviewRequest.dart';
import 'package:vendor/models/itemsrequest.dart';
import 'package:vendor/models/orderitemrequest.dart';
import 'package:vendor/models/returnpolicyrequest.dart';
import 'package:vendor/models/reviewshoprequest.dart';
import 'package:vendor/models/shops.dart';
import 'package:vendor/models/updateDefaultLocationRequest.dart';
import 'package:vendor/models/updateaccountimage.dart';
import 'package:vendor/models/updateaccountrequest.dart';

import './ip.dart';
import './models/account.dart';
import './models/logInRequest/loginrequest.dart';
import './models/signUpRequest/signuprequest.dart';

class RestApi {
  /*USER ACCOUNTS*/

  //POST: /accountspool/signup
  Future<http.Response> signup(
      String endpoint, SignUpRequest signupbody) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode(signupbody),
    );
    return response;
  }

  //PUT: /accountspool/updateAccount
  Future<http.Response> updateAccount(String endpoint,
      UpdateAccountRequest updateAccountRequest, String token) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(updateAccountRequest),
    );
    return response;
  }

  //PUT: /accountspool/replaceAccountImage?id=id
  Future<int> replaceAccountImage(String endpoint,
      AccountThumbnailUploadRequest accountThumbnailUploadRequest, String token) async {
    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    };
    var req = http.MultipartRequest(
      'PUT',
      Uri.parse(Adress.myip + endpoint),
    );
    req.headers.addAll(headers);
    req.fields["UserId"] = accountThumbnailUploadRequest.userId;
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      accountThumbnailUploadRequest.imageFile.path,
      filename: basename(accountThumbnailUploadRequest.imageFile.path),
    ));

    var res = await req.send();
    return res.statusCode;
  }

 //POST: /accountspool/uploadAccountImage?id=id
  Future<int> uploadAccountImage(String endpoint,
      AccountThumbnailUploadRequest accountThumbnailUploadRequest, String token) async {
    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    };
    var req = http.MultipartRequest(
      'POST',
      Uri.parse(Adress.myip + endpoint),
    );
    req.headers.addAll(headers);
    req.fields["UserId"] = accountThumbnailUploadRequest.userId;
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      accountThumbnailUploadRequest.imageFile.path,
      filename: basename(accountThumbnailUploadRequest.imageFile.path),
    ));

    var res = await req.send();
    return res.statusCode;
  }

  //POST: /accountspool/login
  Future<http.Response> login(
      String endpoint, LogInRequest logInRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode(logInRequest),
    );
    return response;
  }

  //POST: /accountspool/registercustomer
  Future<http.Response> registercustomer(
      String endpoint, RegisterRoleRequest registerRoleRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode(registerRoleRequest),
    );
    return response;
  }

  //POST: /accountspool/registervendor
  Future<http.Response> registervendor(
      String endpoint, RegisterVendorRequest registerVendorRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode(registerVendorRequest),
    );
    return response;
  }

  //POST: /accountspool/registerrider
  Future<http.Response> registerrider(String endpoint,
      RegisterRoleRequest registerRoleRequest, String token) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(registerRoleRequest),
    );
    return response;
  }

  //PUT: /accountspool/resetpassword
  Future<http.Response> resetpassword(String endpoint, Map resetpassreq) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode(resetpassreq),
    );
    return response;
  }

  //GET: /accountspool/profileQuery/{id}
  Future<http.Response> profileQuery(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //POST: /accountspool/verifyPurchaseToken
  Future<http.Response> verifyPurchaseToken(String endpoint,
      GoogleVerificationRequest googleVerificationRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode(googleVerificationRequest),
    );
    return response;
  }

  /*VERIFICATION*/

  //PUT: /verification/resetpassword
  Future<http.Response> sendemailtochangepasswd(
      String endpoint, String email) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode({'Email': email}),
    );
    return response;
  }

  //POST: /verification/sendcode
  Future<http.Response> sendemailtocreateaccount(
      String endpoint, VerificationRequest verificationRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
      body: jsonEncode(verificationRequest),
    );
    return response;
  }

  //GET: /verification/verifybymodel?email=params&code=params
  Future<http.Response> verifycode(String endpoint) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
      },
    );
    return response;
  }

  //Delete: /verification/deleteresource?email=email
  Future<http.Response> deleteverificationresource(String endpoint) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
      },
    );
    return response;
  }

  /*HOMEPAGE*/

  //GET: /shops/queryShops?userId=userId&longitude=long&latitude=lat
  Future<http.Response> fetchshops(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /shops/queryShopsCategoriesAndItems?shopId=shopId&userId=userId
  Future<http.Response> fetchoneshop(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /shops/queryAppDefaults
  Future<http.Response> fetchappdefaults(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /shops/checkDeviceToken?userId=userId&deviceToken=deviceToken
  Future<http.Response> checkDeviceToken(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /shops/queryCategories
  Future<http.Response> queryCategories(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /shops/queryForInventoryTab/vendorid
  Future<http.Response> queryForInventoryTab(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /shops/queryInventories/shopId
  Future<http.Response> queryInventories(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //POST: /shops/createShop
  Future<http.Response> createShop(
      String endpoint, String token, CreateShopRequest createShop) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        //'Authorization': 'Bearer $token',
      },
      body: jsonEncode(createShop),
    );
    return response;
  }

  //POST: /shops/thumbnailfileupload
  Future<int> shopThumbnailFileUpload(String endpoint,
      ShopThumbnailUploadRequest singleupload, String token) async {
    Map<String, String> headers = {
      // 'Accept': 'Application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    };
    var req = http.MultipartRequest(
      'POST',
      Uri.parse(Adress.myip + endpoint),
    );
    req.headers.addAll(headers);
    req.fields["ShopId"] = singleupload.shopId;
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      singleupload.imageFile.path,
      filename: basename(singleupload.imageFile.path),
    ));

    var res = await req.send();
    return res.statusCode;
  }

  //GET: /shops/fetchuploadthumbnail?id=id
  Future<http.Response> fetchuploadthumbnail(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /*DRAWER PAGE*/

  //GET: /accountspool/getUserRoles/{id}
  Future<http.Response> getUserRoles(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: items/getCustomerCartItemsByCustomerId/id
  Future<http.Response> getCustomerCartItemsByCustomerId(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //POST: /items/cartItems
  Future<http.Response> addItemToCart(String endpoint, String token,
      CustomerCartRequest customerCartRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(customerCartRequest),
    );
    return response;
  }

  //POST: /items/orderItem
  Future<http.Response> orderItems(
      String endpoint, String token, OrderItemRequest orderItemRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(orderItemRequest),
    );
    return response;
  }

  //DELETE: /items/removeItemFromCart/{id}
  Future<http.Response> removeItemFromCart(
      String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /*ONE ITEM*/

  //GET: /items/queryOneItem?userId=userId&itemId=itemId
  Future<http.Response> queryOneItem(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /items/getReturnPolicies
  Future<http.Response> queryReturnPolicies(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //POST: /items/addReturnPolicy
  Future<http.Response> addReturnPolicy(String endpoint, String token,
      ReturnPolicyRequest returnPolicyRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        //'Authorization': 'Bearer $token',
      },
      body: jsonEncode(returnPolicyRequest),
    );
    return response;
  }

  //POST: /items/thumbnailfileupload
  Future<int> itemThumbnailFileUpload(String endpoint,
      ItemThumbnailUploadRequest singleupload, String token) async {
    Map<String, String> headers = {
      // 'Accept': 'Application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    };
    var req = http.MultipartRequest(
      'POST',
      Uri.parse(Adress.myip + endpoint),
    );
    req.headers.addAll(headers);
    req.fields["ItemId"] = singleupload.itemId;
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      singleupload.thumbnail.path,
      filename: basename(singleupload.thumbnail.path),
    ));

    var res = await req.send();
    return res.statusCode;
  }

  //POST: /items/postItem
  Future<http.Response> addItem(String endpoint, String token,
      CreateItemRequest createItemRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        //'Authorization': 'Bearer $token',
      },
      body: jsonEncode(createItemRequest),
    );
    return response;
  }

  /* INCOMING GOODS */

  //GET: /items/getShopsWithIncomingGoodsForCustomer/id
  Future<http.Response> queryIncomingGoods(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //DELETE: /items/cancelOrder?customerId=customerId&shopId=shopId
  Future<http.Response> cancelOrder(String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /* VENDOR ORDERS */

  //GET: /items/getShopsWithOrdersForVendor/id
  Future<http.Response> queryVendorOrders(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /items/getCustomerOrdersByCustomerIdForVendor/id
  Future<http.Response> queryOneVendorOrder(String endpoint, String token,
      AssignRiderRequest assignRiderRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(assignRiderRequest),
    );
    return response;
  }

  //PUT: /items/assignRider
  Future<http.Response> assignRider(String endpoint, String token,
      AssignRiderRequest assignRiderRequest) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(assignRiderRequest),
    );
    return response;
  }

  /* RIDER ASSIGNMENTS */

  //GET: /items/getAllAssignmentsForRider/id
  Future<http.Response> queryRiderAssignments(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /items/GetCustomerOrdersByCustomerIdForRider?customerid=id&riderid=id
  Future<http.Response> queryOneRiderAssignment(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /* RIDER AFFILIATIONS */

  //GET: /ridermanagement/ridersTabQuery?vendorId=id
  Future<http.Response> quaryRidersTab(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /ridermanagement/searchrider?riderEmail=email
  Future<http.Response> searchRider(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //POST: /ridermanagement/affiliateRider
  Future<http.Response> affiliateRider(
      String endpoint, String token, AffiliateRider affiliateRider) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(affiliateRider),
    );
    return response;
  }

  //PUT: /ridermanagement/acceptAffiliatinRequest
  Future<http.Response> acceptRiderAffiliatinRequest(String endpoint,
      String token, AcceptAffiliation acceptAffiliation) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(acceptAffiliation),
    );
    return response;
  }

  //DELETE: /ridermanagement/removeShopFromAffiliation?shopId=id&supervisorId=supervisorId
  Future<http.Response> removeShopFromRiderAffiliation(
      String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //DELETE: /ridermanagement/unaffiliateRider?vendorId=vendorId&supervisorId=supervisorId
  Future<http.Response> unaffiliateRider(String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /* SUPERVISOR AFFILIATIONS */

  //GET:  /supervisormanagement/supervisorsTabQuery?vendorId=id
  Future<http.Response> quarySuppervisorsTab(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //GET: /supervisormanagement/searchsupervisor?userEmail=id
  Future<http.Response> searchSupervisor(String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //POST: /supervisormanagement/affiliateSupervisor
  Future<http.Response> affiliateSupervisor(String endpoint, String token,
      AffiliateSupervisor affiliateSupervisor) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(affiliateSupervisor),
    );
    return response;
  }

  //PUT: /supervisormanagement/acceptAffiliatinRequest
  Future<http.Response> acceptAffiliatinRequest(String endpoint, String token,
      AcceptAffiliation acceptAffiliation) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(acceptAffiliation),
    );
    return response;
  }

  //DELETE: /supervisormanagement/removeShopFromAffiliation?shopId=id&supervisorId=supervisorId
  Future<http.Response> removeShopFromAffiliation(
      String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //DELETE: /supervisormanagement/unaffiliateSupervisor?vendorId=vendorId&supervisorId=supervisorId
  Future<http.Response> unaffiliateSupervisor(
      String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /* DELIVERY LOCATION */

  //POST: /items/addDeliveryLocation
  Future<http.Response> addDeliveryLocation(String endpoint, String token,
      DeliveryLocationRequest deliveryLocationRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(deliveryLocationRequest),
    );
    return response;
  }

  //GET: /items/getDeliveryLocations/id
  Future<http.Response> getDeliveryLocations(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  //PUT: /items/changeDefaultDeliveryLocation
  Future<http.Response> changeDefaultDeliveryLocation(
      String endpoint,
      String token,
      UpdateDefaultLocationRequest updateDefaultLocationRequest) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(updateDefaultLocationRequest),
    );
    return response;
  }

  /*QR CODE SCANNER */
  //GET: /items/sendPurchaseItemRequest?uid=uid
  Future<http.Response> sendPurchaseItemRequest(
      String endpoint, String token) async {
    final response = await http.get(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /*FAVORITE*/
  //POST. /shops/likeShop
  Future<http.Response> favoriteShop(
      String endpoint, String token, LikeShopRequest likeShopRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(likeShopRequest),
    );
    return response;
  }

  //DELETE /shops/unlikeShop/id
  Future<http.Response> unFavoriteShop(String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /*SHOP REVIEW*/
  //POST: /shops/reviewShop
  Future<http.Response> reviewShop(String endpoint, String token,
      ReviewShopRequest reviewShopRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(reviewShopRequest),
    );
    return response;
  }

  //DELETE: /shops/deleteReview/{id}
  Future<http.Response> deleteReview(String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /*SHOP UPDATE*/
  //PUT: /shops/replaceShopImage
  Future<int> shopThumbnailFileUploadUpdate(String endpoint,
      ShopThumbnailUploadRequest singleupload, String token) async {
    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer $token',
    };
    var req = http.MultipartRequest(
      'PUT',
      Uri.parse(Adress.myip + endpoint),
    );
    req.headers.addAll(headers);
    req.fields["ShopId"] = singleupload.shopId;
    req.files.add(await http.MultipartFile.fromPath(
      'Thumbnail',
      singleupload.imageFile.path,
      filename: basename(singleupload.imageFile.path),
    ));

    var res = await req.send();
    return res.statusCode;
  }

  //PUT: /shops/editShopDetail?id=shopId
  Future<http.Response> editShopDetail(String endpoint, String token,
      UpdateShopRequest updateShopRequest) async {
    final response = await http.put(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(updateShopRequest),
    );
    return response;
  }

  /*DELETE THIS SHOP*/
  //DELETE: /shops/deleteShop/{id}
  Future<http.Response> deleteShop(String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }

  /*ITEMS REVIEW*/
  //POST: /items/reviewItem
  Future<http.Response> reviewItem(String endpoint, String token,
      ReviewItemRequest reviewItemRequest) async {
    final response = await http.post(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Content-Type': 'Application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(reviewItemRequest),
    );
    return response;
  }

  //DELETE: /items/deleteReview/{id}
  Future<http.Response> deleteItemReview(String endpoint, String token) async {
    final response = await http.delete(
      Uri.parse(Adress.myip + endpoint),
      headers: {
        'Accept': 'Application/json',
        'Authorization': 'Bearer $token',
      },
    );
    return response;
  }
}

class SignalRSocket {
  static late HubConnection connection;

  static Future initconnection(String id, String devicetoken) async {
    connection = HubConnectionBuilder()
        .withUrl(
            Adress.myip + "/alertshub?userid=$id&deviceToken=$devicetoken",
            HttpConnectionOptions(
              logging: (level, message) => print(message),
            ))
        .build();
    await connection.start();
    /*
      LISTEN FOR EVENTS   
    */
    connection.on("Follow", (message) {});

    connection.on("UnFollow", (message) {});

    connection.on("Like", (message) {});

    connection.on("UnLike", (message) {});

    connection.on("AddMarker", (message) {
      print(message);
      //NotificationsTabInstance.addnotificationobject(notification);
    });

    connection.on("RemoveMarker", (message) {});

    connection.on("AddMarkerrsvp", (message) {});

    connection.on("RemoveMarkerrsvp", (message) {});

    connection.on("chats", (message) {});

    connection.on("Unsendchat", (message) {});

    connection.on("ReplyChat", (message) {});

    connection.on("Forward", (message) {});
  }

  Future follow() async {
    await connection.invoke("FollowAccount", args: ["followrequest"]);
  }
}
