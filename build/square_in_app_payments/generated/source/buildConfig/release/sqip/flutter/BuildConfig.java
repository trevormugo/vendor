/**
 * Automatically generated file. DO NOT MODIFY
 */
package sqip.flutter;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "sqip.flutter";
  public static final String BUILD_TYPE = "release";
}
