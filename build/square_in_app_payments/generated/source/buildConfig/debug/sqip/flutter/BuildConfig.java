/**
 * Automatically generated file. DO NOT MODIFY
 */
package sqip.flutter;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "sqip.flutter";
  public static final String BUILD_TYPE = "debug";
}
